package com.itx360;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * Created by Thilanga-Ehl on 8/23/2016.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private final CargodriveAuthenticationSuccessHandler cargodriveAuthenticationSuccessHandler;

    private final CargodriveAuthenticationLogoutHandler cargodriveAuthenticationLogoutHandler;

    @Autowired
    public SpringSecurityConfig(CargodriveAuthenticationSuccessHandler cargodriveAuthenticationSuccessHandler, CargodriveAuthenticationLogoutHandler cargodriveAuthenticationLogoutHandler) {
        this.cargodriveAuthenticationSuccessHandler = cargodriveAuthenticationSuccessHandler;
        this.cargodriveAuthenticationLogoutHandler = cargodriveAuthenticationLogoutHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().permitAll()
                .and()
                .formLogin().loginPage("/login")
                .usernameParameter("username").passwordParameter("password").successHandler(cargodriveAuthenticationSuccessHandler)
                .and().rememberMe().rememberMeParameter("remember-me").rememberMeCookieName("my-remember-me").tokenValiditySeconds(86400)
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login").logoutSuccessHandler(cargodriveAuthenticationLogoutHandler)
                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied")
                .and()
                .csrf()
                .and()
                .sessionManagement()
//                .maximumSessions(1)
//                .maxSessionsPreventsLogin(true)
//                .expiredUrl("/login?Session Expired")
//                .and()
                .invalidSessionUrl("/login?Invalid Session")
                .sessionFixation().changeSessionId();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        Context ctx = new InitialContext();
        DataSource dataSource = (DataSource) ctx.lookup("java:/CargodriveDS");
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("SELECT username,password, enabled FROM users WHERE upper(username)=upper(?)")
//                .passwordEncoder(new ShaPasswordEncoder(256))
                .passwordEncoder(new ShaPasswordEncoder(1))
                .authoritiesByUsernameQuery("SELECT\n" +
                        "                        u.USERNAME AS username,\n" +
                        "                        a.AUTHORITY AS authority\n" +
                        "                    FROM\n" +
                        "                        USERS u,\n" +
                        "                        USER_GROUPS ug,\n" +
                        "                        GROUP_AUTHORITIES ga,\n" +
                        "                        AUTHORITIES a\n" +
                        "                    WHERE\n" +
                        "                        upper(u.USERNAME) = upper(?) AND\n" +
                        "                        u.USER_SEQ = ug.USER_SEQ AND\n" +
                        "                        ug.GROUP_SEQ = ga.GROUP_SEQ AND\n" +
                        "                        ga.AUTHORITY_SEQ = a.AUTHORITY_SEQ AND\n" +
                        "                        ug.STATUS=1");

    }

}
