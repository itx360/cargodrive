package com.itx360;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


@Service
public class CargodriveSessionTimeoutHandler implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
//        System.out.println("==== Session is created ====");
//        httpSessionEvent.getSession().setMaxInactiveInterval(5 * 60);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        try {
           /* List<SecurityContext> lstSecurityContext = sessionDestroyedEvent.getSecurityContexts();
            UserDetails ud;
            for (SecurityContext securityContext : lstSecurityContext) {
                ud = (UserDetails) securityContext.getAuthentication().getPrincipal();
                UserLogInAudit userLogInAudit = this.userLogInAuditService.lastLogInAudit(ud.getUsername());
                userLogInAudit.setLogoutDate(new Date());
                this.userLogInAuditService.save(userLogInAudit);
            }*/
//            System.out.println("==== Session is destroyed ====");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
