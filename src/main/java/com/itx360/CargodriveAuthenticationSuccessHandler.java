package com.itx360;

import com.itx360.cargodrive.config.datalayer.model.CompanyProfile;
import com.itx360.cargodrive.config.datalayer.model.UserLogInAudit;
import com.itx360.cargodrive.config.datalayer.service.CompanyProfileService;
import com.itx360.cargodrive.config.datalayer.service.UserLogInAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class CargodriveAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final CompanyProfileService companyProfileService;

    private final UserLogInAuditService userLogInAuditService;

    @Autowired
    public CargodriveAuthenticationSuccessHandler(CompanyProfileService companyProfileService, UserLogInAuditService userLogInAuditService) {
        this.companyProfileService = companyProfileService;
        this.userLogInAuditService = userLogInAuditService;
    }

    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        try {
            UserLogInAudit userLogInAudit = new UserLogInAudit();
            userLogInAudit.setUsername(authentication.getName());
            userLogInAudit.setLoginDate(new Date());
            userLogInAudit.setRemoteAddress(request.getRemoteAddr());
            this.userLogInAuditService.save(userLogInAudit);
            List<CompanyProfile> companyProfileList = this.companyProfileService.getActiveCompanyListByUsername(authentication.getName());
            if (companyProfileList.size() == 1) {
                request.getSession().setAttribute("companyProfileSeq", companyProfileList.get(0).getCompanyProfileSeq());
                response.sendRedirect("/");
            } else if (companyProfileList.size() > 1) {
                response.sendRedirect("/companySelection");
            } else {
                response.sendRedirect("/login?No Comapany");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
