package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.AgentNetworkManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AgentNetwork;
import com.itx360.cargodrive.config.datalayer.service.AgentNetworkService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 10/7/2016.
 */
@Service
public class AgentNetworkManagementControllerManagerImpl implements AgentNetworkManagementControllerManager {

    private final AgentNetworkService agentNetworkService;

    @Autowired
    public AgentNetworkManagementControllerManagerImpl(AgentNetworkService agentNetworkService) {
        this.agentNetworkService = agentNetworkService;
    }

    @Override
    public ResponseObject saveAgentNetwork(AgentNetwork agentNetwork, Principal principal, HttpServletRequest request) {
        ResponseObject responseObject = new ResponseObject("Agent Network Saved Successfully", true);
        try {

            Integer companyProfileSeq = Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString());

            agentNetwork.setCreatedBy(principal.getName());
            agentNetwork.setCreatedDate(new Date());
            agentNetwork.setLastModifiedBy(principal.getName());
            agentNetwork.setLastModifiedDate(new Date());
           agentNetwork.setCompanyProfileSeq(companyProfileSeq);

            agentNetwork = this.agentNetworkService.save(agentNetwork);
            responseObject.setObject(agentNetwork);
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Agent Network Already Exist!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }

        return responseObject;
    }

    @Override
    public ResponseObject updateAgentNetwork(AgentNetwork agentNetwork, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Agent Network Successfully Updated", true);
        try{
            AgentNetwork agentNetworkDB = this.agentNetworkService.findOne(agentNetwork.getAgentNetworkSeq());

            if(agentNetworkDB != null){
                agentNetworkDB.setAgentNetworkCode(agentNetwork.getAgentNetworkCode());
                agentNetworkDB.setAgentNetworkName(agentNetwork.getAgentNetworkName());
                agentNetworkDB.setLastModifiedBy(principal.getName());
                agentNetworkDB.setLastModifiedDate(new Date());
                agentNetworkDB.setStatus(agentNetwork.getStatus());
                agentNetworkDB= this.agentNetworkService.save(agentNetworkDB);
                responseObject.setObject(agentNetworkDB);
            }else {
                responseObject.setMessage("Agent Network Not Found!!");
                responseObject.setStatus(false);
            }
        }catch (Exception e){
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Agent Network Already Exist!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }

        return responseObject;
    }

    @Override
    public List<AgentNetwork> searchAgentNetwork(String agentNetworkCode, String agentNetworkName) {
        List<AgentNetwork> agentNetworkList = null;

        try{
            if(agentNetworkCode.equals("") && agentNetworkName.equals("")){
                agentNetworkList = this.agentNetworkService.findAll();
            }else if(!agentNetworkCode.equals("") && agentNetworkName.equals("")){
                agentNetworkList = this.agentNetworkService.findByAgentNetworkCodeContainingIgnoreCase(agentNetworkCode);
            }else if(agentNetworkCode.equals("") && !agentNetworkName.equals("")){
                agentNetworkList = this.agentNetworkService.findByAgentNetworkNameContainingIgnoreCase(agentNetworkName);
            }else if(!agentNetworkCode.equals("") && !agentNetworkName.equals("")){
                agentNetworkList = this.agentNetworkService.findByAgentNetworkCodeContainingIgnoreCaseAndAgentNetworkNameContainingIgnoreCase(agentNetworkCode,agentNetworkName);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return agentNetworkList;
    }
}
