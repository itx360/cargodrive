package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.ConsortiumManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Consortium;
import com.itx360.cargodrive.config.datalayer.service.ConsortiumService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Nuwanr on 9/28/2016.
 */
@Service
public class ConsortiumManagementControllerManagerImpl implements ConsortiumManagementControllerManager {

    private final ConsortiumService consortiumService;

    @Autowired
    public ConsortiumManagementControllerManagerImpl(ConsortiumService consortiumService) {
        this.consortiumService = consortiumService;
    }

    @Override
    public ResponseObject saveConsortium(Consortium consortium, Principal principal) {
        ResponseObject responseObject;
        try {
            consortium.setCreatedBy(principal.getName());
            consortium.setCreatedDate(new Date());
            consortium.setLastModifiedBy(principal.getName());
            consortium.setLastModifiedDate(new Date());
            this.consortiumService.save(consortium);
            responseObject = new ResponseObject("Consortium saved successfully", true);
            responseObject.setObject(consortium);
        } catch (Exception e) {
            e.printStackTrace();
            responseObject = new ResponseObject("Error saving new consortium", false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateConsortium(Consortium consortium, Principal principal) {
        ResponseObject responseObject;
        try {
            Consortium dbConsortium = this.consortiumService.findOne(consortium.getConsortiumSeq());
            if (dbConsortium != null) {
                dbConsortium.setLastModifiedBy(principal.getName());
                dbConsortium.setLastModifiedDate(new Date());
                dbConsortium.setConsortiumName(consortium.getConsortiumName().toUpperCase());
                dbConsortium.setStatus(consortium.getStatus());
                this.consortiumService.save(dbConsortium);
                responseObject = new ResponseObject("Consortium updated successfully", true);
                responseObject.setObject(dbConsortium);
            } else {
                responseObject = new ResponseObject("Consortium not found!!", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject = new ResponseObject("Error updating consortium", false);
        }
        return responseObject;
    }

    @Override
    public List<Consortium> searchConsortiumByConsortiumName(String consortiumName) {
        List<Consortium> consortiumList= null;
        try {
            if (consortiumName.equals("")) {
                consortiumList= this.consortiumService.findAll();
            } else {
               consortiumList = this.consortiumService.findByConsortiumNameContainingIgnoreCase(consortiumName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  consortiumList;
    }
}

