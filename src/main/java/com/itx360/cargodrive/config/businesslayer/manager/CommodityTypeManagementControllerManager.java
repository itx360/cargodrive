package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.CommodityType;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 8:13 PM).
 */
public interface CommodityTypeManagementControllerManager {
    ResponseObject createCommodityType(CommodityType commodityType, String username);

    ResponseObject updateCommodityType(CommodityType commodityType, String username);

    List<CommodityType> searchCommodityType(String commodityTypeCode, String description);
}
