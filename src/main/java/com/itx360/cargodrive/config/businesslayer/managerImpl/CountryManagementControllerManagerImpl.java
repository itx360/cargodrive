package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.CountryManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Country;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shanakajay on 9/20/2016.
 */
@Service
public class CountryManagementControllerManagerImpl implements CountryManagementControllerManager {

    private final CountryService countryService;

    @Autowired
    public CountryManagementControllerManagerImpl(CountryService countryService) {
        this.countryService = countryService;
    }

    @Override
    public ResponseObject createCountry(Country country, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Country Successfully Saved", true);
        try {
            if (this.countryService.findByCountryName(country.getCountryName()) == null) {
                country.setCreatedBy(principal.getName());
                country.setCreatedDate(new Date());
                country.setLastModifiedBy(principal.getName());
                country.setLastModifiedDate(new Date());
                country = this.countryService.save(country);
                responseObject.setObject(country);
            } else {
                responseObject.setMessage("Country already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateCountry(Country country, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Country Successfully Updated", true);
        try {
            Country dbCountry = this.countryService.findOne(country.getCountrySeq());
            if (dbCountry != null) {
                dbCountry.setCountryCode(country.getCountryCode());
                dbCountry.setCountryName(country.getCountryName());
                dbCountry.setRegionSeq(country.getRegionSeq());
                dbCountry.setCurrencySeq(country.getCurrencySeq());
                dbCountry.setIataCode(country.getIataCode());
                dbCountry.setStatus(country.getStatus());
                dbCountry.setLastModifiedBy(principal.getName());
                dbCountry.setLastModifiedDate(new Date());
                this.countryService.save(dbCountry);
            } else {
                responseObject.setMessage("Country not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Updating Country");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<Country> searchCountry(String countryName, String countryCode) {
        List<Country> countryList = new ArrayList<>();
        try {
            if (countryName.equals("") && countryCode.equals("")) {
                countryList = countryService.findAll();
            } else if (!countryName.equals("") && countryCode.equals("")) {
                countryList = countryService.findByCountryNameContainingIgnoreCase(countryName);
            } else if (countryName.equals("") && !countryCode.equals("")) {
                countryList = countryService.findByCountryCodeContainingIgnoreCase(countryCode);
            } else if (!countryName.equals("") && !countryCode.equals("")) {
                countryList = countryService.findByCountryNameContainingIgnoreCaseAndCountryCodeContainingIgnoreCase(countryName, countryCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countryList;
    }
}
