package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.HsCategory;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 9:58 PM).
 */
public interface HsCategoryManagementControllerManager {
    ResponseObject createHsCategory(HsCategory hsCategory, String username);

    ResponseObject updateHsCategory(HsCategory hsCategory, String username);

    List<HsCategory> searchHsCategoryName(String searchHsCategoryName, String description);
}
