package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.HsCategoryManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.HsCategory;
import com.itx360.cargodrive.config.datalayer.service.HsCategoryService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 9:59 PM).
 */
@Service
public class HsCategoryManagementControllerManagerImpl implements HsCategoryManagementControllerManager {

    private final HsCategoryService hsCategoryService;

    @Autowired
    public HsCategoryManagementControllerManagerImpl(HsCategoryService hsCategoryService) {
        this.hsCategoryService = hsCategoryService;
    }

    @Override
    public ResponseObject createHsCategory(HsCategory hsCategory, String username) {
        ResponseObject responseObject = new ResponseObject("HS Category Saved Successfully", true);
        try {
            HsCategory dbHsCategory = this.hsCategoryService.findByHsCategoryName(hsCategory.getHsCategoryName());
            if (dbHsCategory == null) {
                hsCategory.setCreatedBy(username);
                hsCategory.setCreatedDate(new Date());
                hsCategory.setLastModifiedBy(username);
                hsCategory.setLastModifiedDate(new Date());
                hsCategory = this.hsCategoryService.save(hsCategory);
                responseObject.setObject(hsCategory);
            } else {
                responseObject.setMessage("HS Category already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateHsCategory(HsCategory hsCategory, String username) {
        ResponseObject responseObject = new ResponseObject("HS Category Updated Successfully", true);
        try {
            HsCategory dbHsCategory = this.hsCategoryService.findOne(hsCategory.getHsCategorySeq());
            if (dbHsCategory != null) {
                dbHsCategory.setLastModifiedBy(username);
                dbHsCategory.setLastModifiedDate(new Date());
                dbHsCategory.setDescription(hsCategory.getDescription());
                dbHsCategory.setHsCategoryName(hsCategory.getHsCategoryName());
                dbHsCategory.setStatus(hsCategory.getStatus());
                dbHsCategory = this.hsCategoryService.save(dbHsCategory);
                responseObject.setObject(dbHsCategory);
            } else {
                responseObject.setMessage("HS Category Not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("HS Category Already Exists");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<HsCategory> searchHsCategoryName(String hsCategoryName, String description) {
        List<HsCategory> hsCategoryList = null;
        try {
            if (hsCategoryName == null) {
                if (description == null) {
                    hsCategoryList = this.hsCategoryService.findAll();
                } else {
                    hsCategoryList = this.hsCategoryService.findByDescriptionContainingIgnoreCase(description);
                }
            } else {
                if (description == null) {
                    hsCategoryList = this.hsCategoryService.findByHsCategoryNameContainingIgnoreCase(hsCategoryName);
                } else {
                    hsCategoryList = this.hsCategoryService.findByHsCategoryNameContainingIgnoreCaseAndDescriptionContainingIgnoreCase(hsCategoryName, description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hsCategoryList;
    }
}
