package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.BankManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Bank;
import com.itx360.cargodrive.config.datalayer.service.BankService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shanakajay on 9/26/2016.
 */
@Service
public class BankManagementControllerManagerImpl implements BankManagementControllerManager {

    private final BankService bankService;

    @Autowired
    public BankManagementControllerManagerImpl(BankService bankService) {
        this.bankService = bankService;
    }

    @Override
    public ResponseObject createBank(Bank bank, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Bank Successfully Saved", true);
        try {
            if (this.bankService.findByBankName(bank.getBankName()) == null) {
                if(this.bankService.findByBankCodeIgnoreCase(bank.getBankCode())==null) {
                    bank.setCreatedBy(principal.getName());
                    bank.setCreatedDate(new Date());
                    bank.setLastModifiedBy(principal.getName());
                    bank.setLastModifiedDate(new Date());
                    bank = this.bankService.save(bank);
                    responseObject.setObject(bank);
                }else{
                    responseObject.setMessage("Bank Code already exist!!");
                    responseObject.setStatus(false);
                }
            } else {
                responseObject.setMessage("Bank already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateBank(Bank bank, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Bank Successfully Updated", true);
        try {
            Bank dbBank = this.bankService.findOne(bank.getBankSeq());
            if (dbBank != null) {
                dbBank.setBankCode(bank.getBankCode());
                dbBank.setBankName(bank.getBankName());
                dbBank.setSlipsCode(bank.getSlipsCode());
                dbBank.setStatus(bank.getStatus());
                dbBank.setLastModifiedBy(principal.getName());
                dbBank.setLastModifiedDate(new Date());
                this.bankService.save(dbBank);
            } else {
                responseObject.setMessage("Bank not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Bank Already Exists");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<Bank> searchBank(String bankName, String bankCode) {
        List<Bank> bankList = new ArrayList<>();
        try {
            if (bankName.equals("") && bankCode.equals("")) {
                bankList = bankService.findAll();
            } else if (!bankName.equals("") && bankCode.equals("")) {
                bankList = bankService.findByBankNameContainingIgnoreCase(bankName);
            } else if (bankName.equals("") && !bankCode.equals("")) {
                bankList = bankService.findByBankCodeContainingIgnoreCase(bankCode);
            } else if (!bankName.equals("") && !bankCode.equals("")) {
                bankList = bankService.findByBankNameContainingIgnoreCaseAndBankCodeContainingIgnoreCase(bankName, bankCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bankList;
    }

}
