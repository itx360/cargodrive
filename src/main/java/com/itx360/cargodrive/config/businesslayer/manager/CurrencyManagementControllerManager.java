package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Currency;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/19/2016
 * Time: 4:56 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CurrencyManagementControllerManager {
    ResponseObject saveCurrency(Currency currency, Principal principal);

    ResponseObject updateCurrency(Currency currency, Principal principal);

    List<Currency> searchCurrencyByCurrencyNameAndCurrencyDisplay(String currencyName, String currencyDisplay);
}
