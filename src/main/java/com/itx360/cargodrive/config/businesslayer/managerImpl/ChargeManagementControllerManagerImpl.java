package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.ChargeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Charge;
import com.itx360.cargodrive.config.datalayer.model.ChargeMode;
import com.itx360.cargodrive.config.datalayer.model.CompanyModule;
import com.itx360.cargodrive.config.datalayer.model.Module;
import com.itx360.cargodrive.config.datalayer.service.ChargeModeService;
import com.itx360.cargodrive.config.datalayer.service.ChargeService;
import com.itx360.cargodrive.config.datalayer.service.CompanyModuleService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;

/**
 * Created by shanakajay on 9/27/2016.
 */
@Service
public class ChargeManagementControllerManagerImpl implements ChargeManagementControllerManager {

    private final ChargeService chargeService;
    private final ChargeModeService chargeModeService;
    private final CompanyModuleService companyModuleService;

    @Autowired
    public ChargeManagementControllerManagerImpl(ChargeService chargeService, ChargeModeService chargeModeService, CompanyModuleService companyModuleService) {
        this.chargeService = chargeService;
        this.chargeModeService = chargeModeService;
        this.companyModuleService = companyModuleService;
    }

    @Override
    public ResponseObject createCharge(Charge charge, List<Integer> modeSeqList, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Charge Successfully Saved", true);
        try {
            if (this.chargeService.findByChargeName(charge.getChargeName()) == null) {
                Set<ChargeMode> chargeModes = new HashSet<ChargeMode>();
                if (modeSeqList != null) {
                    for (Integer modeSeq : modeSeqList) {
                        ChargeMode chargeMode = new ChargeMode();
                        chargeMode.setStatus(1);
                        chargeMode.setCompanyModuleSeq(modeSeq);
                        chargeMode.setCreatedBy(principal.getName());
                        chargeMode.setCreatedDate(new Date());
                        chargeMode.setLastModifiedBy(principal.getName());
                        chargeMode.setLastModifiedDate(new Date());
                        chargeModes.add(chargeMode);
                    }
                    charge.setChargeModes(chargeModes);
                }
                charge.setCreatedBy(principal.getName());
                charge.setCreatedDate(new Date());
                charge.setLastModifiedBy(principal.getName());
                charge.setLastModifiedDate(new Date());
                //charge.setChargeModes(chargeModes);
                charge = this.chargeService.save(charge);
                responseObject.setObject(charge);
            } else {
                responseObject.setMessage("Charge already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateCharge(Charge charge, List<Integer> modeSeqList, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Charge Successfully Updated", true);
        Set<ChargeMode> chargeModes = new HashSet<ChargeMode>();
        try {
            Charge dbCharge = this.chargeService.findOne(charge.getChargeSeq());
            if (dbCharge != null) {
                if (modeSeqList != null) {
                    List<Integer> newModeSeqList = new ArrayList<>();
                    List<Integer> existingModeSeqList = new ArrayList<>();
                    for (ChargeMode dbChargeMode : dbCharge.getChargeModes()) {
                        dbChargeMode.setCreatedBy(principal.getName());
                        dbChargeMode.setCreatedDate(new Date());
                        dbChargeMode.setLastModifiedBy(principal.getName());
                        dbChargeMode.setLastModifiedDate(new Date());
                        dbChargeMode.setStatus(0);
                        if (modeSeqList.contains(dbChargeMode.getCompanyModuleSeq())) {
                            dbChargeMode.setStatus(1);
                            existingModeSeqList.add(dbChargeMode.getCompanyModuleSeq());
                        }
                        chargeModes.add(dbChargeMode);
                    }
                    /*if(modeSeqList.size() > existingModeSeqList.size()){
                        for (Integer modeSeq : modeSeqList) {}
                        if (modeSeqList.contains(dbChargeMode.getModuleSeq())) {

                        }

                    }*/
                    /*dbCharge.getChargeModes().clear();
                    dbCharge.getChargeModes().addAll(chargeModes);*/
                }
                dbCharge.setChargeName(charge.getChargeName());
                dbCharge.setDescription(charge.getDescription());
                dbCharge.setStatus(charge.getStatus());
                dbCharge.setLastModifiedBy(principal.getName());
                dbCharge.setLastModifiedDate(new Date());
                dbCharge.getChargeModes().clear();
                dbCharge.getChargeModes().addAll(chargeModes);
                this.chargeService.save(dbCharge);
            } else {
                responseObject.setMessage("Charge not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<Charge> searchCharge(String chargeName, String description, Integer customerProfileSeq) {
        List<Charge> chargeList = new ArrayList<>();
        try {
            if (chargeName.equals("") && description.equals("")) {
                chargeList = chargeService.findAll();
            } else if (!chargeName.equals("") && description.equals("")) {
                chargeList = chargeService.findByChargeNameContainingIgnoreCase(chargeName);
            } else if (chargeName.equals("") && !description.equals("")) {
                chargeList = chargeService.findByDescriptionContainingIgnoreCase(description);
            } else if (!chargeName.equals("") && !description.equals("")) {
                chargeList = chargeService.findByChargeNameContainingIgnoreCaseAndDescriptionContainingIgnoreCase(chargeName, description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chargeList;
    }

    public List<CompanyModule> getFinanceEnabledCompanyModuleList(HttpServletRequest request) {
        List<CompanyModule> companyModuleList = new ArrayList<>();
        try {
            Integer customerProfileSeq = Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString());
            for (CompanyModule companyModule : this.companyModuleService.findByCompanyProfileSeq(customerProfileSeq)) {
                Module module = companyModule.getModule();
                if (module.getFinanceEnabled() != null) {
                    if (module.getFinanceEnabled() == 1) {
                        companyModuleList.add(companyModule);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return companyModuleList;
    }


}
