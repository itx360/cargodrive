package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.HsCodeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.HsCode;
import com.itx360.cargodrive.config.datalayer.service.HsCodeService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 9:59 PM).
 */
@Service
public class HsCodeManagementControllerManagerImpl implements HsCodeManagementControllerManager {

    private final HsCodeService hsCodeService;

    @Autowired
    public HsCodeManagementControllerManagerImpl(HsCodeService hsCodeService) {
        this.hsCodeService = hsCodeService;
    }

    @Override
    public ResponseObject createHsCode(HsCode hsCode, String username) {
        ResponseObject responseObject = new ResponseObject("HS Code Saved Successfully", true);
        try {
            HsCode dbHsCode = this.hsCodeService.findByHsCodeName(hsCode.getHsCodeName());
            if (dbHsCode == null) {
                hsCode.setCreatedBy(username);
                hsCode.setCreatedDate(new Date());
                hsCode.setLastModifiedBy(username);
                hsCode.setLastModifiedDate(new Date());
                hsCode = this.hsCodeService.save(hsCode);
                responseObject.setObject(hsCode);
            } else {
                responseObject.setMessage("HS Code Already Exists!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateHsCode(HsCode hsCode, String username) {
        ResponseObject responseObject = new ResponseObject("HS Code Updated Successfully", true);
        try {
            HsCode dbHsCode = this.hsCodeService.findOne(hsCode.getHsCodeSeq());
            if (dbHsCode != null) {
                dbHsCode.setLastModifiedBy(username);
                dbHsCode.setLastModifiedDate(new Date());
                dbHsCode.setDescription(hsCode.getDescription());
                dbHsCode.setHsCodeName(hsCode.getHsCodeName());
                dbHsCode.setHsCategorySeq(hsCode.getHsCategorySeq());
                dbHsCode.setStatus(hsCode.getStatus());
                dbHsCode = this.hsCodeService.save(dbHsCode);
                responseObject.setObject(dbHsCode);
            } else {
                responseObject.setMessage("HS Code Not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("HS Code Already Exists!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<HsCode> searchHsCode(String hsCodeName, String description, Integer hsCategorySeq) {
        List<HsCode> hsCodeList = null;
        try {
            if (hsCategorySeq.equals(-1)) {
                if (hsCodeName.equals("")) {
                    if (description.equals("")) {
                        hsCodeList = this.hsCodeService.findAll();
                    } else {
                        hsCodeList = this.hsCodeService.findByDescriptionContainingIgnoreCase(description);
                    }
                } else {
                    if (description.equals("")) {
                        hsCodeList = this.hsCodeService.findByHsCodeNameContainingIgnoreCase(hsCodeName);
                    } else {
                        hsCodeList = this.hsCodeService.findByHsCodeNameContainingIgnoreCaseAndDescriptionContainingIgnoreCase(hsCodeName, description);
                    }
                }
            } else {
                if (hsCodeName.equals("")) {
                    if (description.equals("")) {
                        hsCodeList = this.hsCodeService.findByHsCategorySeq(hsCategorySeq);
                    } else {
                        hsCodeList = this.hsCodeService.findByHsCategorySeqAndDescriptionContainingIgnoreCase(hsCategorySeq, description);
                    }
                } else {
                    if (description.equals("")) {
                        hsCodeList = this.hsCodeService.findByHsCategorySeqAndHsCodeNameContainingIgnoreCase(hsCategorySeq, hsCodeName);
                    } else {
                        hsCodeList = this.hsCodeService.findByHsCategorySeqAndHsCodeNameContainingIgnoreCaseAndDescriptionContainingIgnoreCase(hsCategorySeq, hsCodeName, description);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hsCodeList;
    }
}
