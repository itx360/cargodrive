package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Bank;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 9/26/2016.
 */
public interface BankManagementControllerManager {

    ResponseObject createBank(Bank bank, Principal principal);

    ResponseObject updateBank(Bank bank, Principal principal);

    List<Bank> searchBank(String bankName, String bankCode);
}
