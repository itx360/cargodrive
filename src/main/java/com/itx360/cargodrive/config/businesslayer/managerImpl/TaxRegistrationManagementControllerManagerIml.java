package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.TaxRegistrationManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.TaxRegistration;
import com.itx360.cargodrive.config.datalayer.service.TaxRegistrationService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilangaj on 10/4/2016 4:53 PM).
 */
@Service
public class TaxRegistrationManagementControllerManagerIml implements TaxRegistrationManagementControllerManager {

    private final TaxRegistrationService taxRegistrationService;

    @Autowired
    public TaxRegistrationManagementControllerManagerIml(TaxRegistrationService taxRegistrationService) {
        this.taxRegistrationService = taxRegistrationService;
    }

    @Override
    public ResponseObject createTaxRegistration(TaxRegistration taxRegistration, String username) {
        ResponseObject responseObject = new ResponseObject("Tax Registration Saved Successfully", true);
        try {
            TaxRegistration dbTaxRegistration = this.taxRegistrationService.findByTaxNameAndCompanyProfileSeq(taxRegistration.getTaxName(), taxRegistration.getCompanyProfileSeq());
            if (dbTaxRegistration == null) {
                taxRegistration.setCreatedBy(username);
                taxRegistration.setCreatedDate(new Date());
                taxRegistration.setLastModifiedBy(username);
                taxRegistration.setLastModifiedDate(new Date());
                taxRegistration = this.taxRegistrationService.save(taxRegistration);
                responseObject.setObject(taxRegistration);
            } else {
                responseObject.setMessage("Tax Registration already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateTaxRegistration(TaxRegistration taxRegistration, String username) {
        ResponseObject responseObject = new ResponseObject("Tax Registration Updated Successfully", true);
        try {
            TaxRegistration dbTaxRegistration = this.taxRegistrationService.findOne(taxRegistration.getTaxRegistrationSeq());
            if (dbTaxRegistration != null) {
                dbTaxRegistration.setLastModifiedBy(username);
                dbTaxRegistration.setLastModifiedDate(new Date());
                dbTaxRegistration.setRemarks(taxRegistration.getRemarks());
                dbTaxRegistration.setTaxName(taxRegistration.getTaxName());
                dbTaxRegistration.setCountrySeq(taxRegistration.getCountrySeq());
                dbTaxRegistration.setTaxRate(taxRegistration.getTaxRate());
                dbTaxRegistration.setStatus(taxRegistration.getStatus());
                dbTaxRegistration = this.taxRegistrationService.save(dbTaxRegistration);
                responseObject.setObject(dbTaxRegistration);
            } else {
                responseObject.setMessage("Tax Registration Not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Tax Registration Already Exists");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<TaxRegistration> searchTaxRegistration(String taxName, String remarks, Integer countrySeq, Integer customerProfileSeq) {
        List<TaxRegistration> taxRegistrationList = null;
        try {
            if (countrySeq.equals(-1)) {

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taxRegistrationList;
    }
}
