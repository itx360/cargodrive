package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.AgentNetwork;
import com.itx360.cargodrive.master.utility.ResponseObject;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 10/7/2016.
 */
public interface AgentNetworkManagementControllerManager {

    ResponseObject saveAgentNetwork(AgentNetwork agentNetwork, Principal principal,HttpServletRequest request);

    ResponseObject updateAgentNetwork(AgentNetwork agentNetwork,Principal principal);

    List<AgentNetwork> searchAgentNetwork(String agentNetworkCode, String agentNetworkName);
}
