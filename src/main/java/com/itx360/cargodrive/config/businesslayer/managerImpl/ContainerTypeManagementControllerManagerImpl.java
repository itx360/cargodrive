package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.ContainerTypeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.ContainerType;
import com.itx360.cargodrive.config.datalayer.service.ContainerTypeService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/5/2016
 * Time: 2:36 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ContainerTypeManagementControllerManagerImpl implements ContainerTypeManagementControllerManager {

    private final ContainerTypeService containerTypeService;

    @Autowired
    public ContainerTypeManagementControllerManagerImpl(ContainerTypeService containerTypeService) {
        this.containerTypeService = containerTypeService;
    }

    @Override
    public ResponseObject saveContainerType(ContainerType containerType, Principal principal) {
        ResponseObject responseObject;
        try {
            ContainerType dbContainerType = this.containerTypeService.findByAsycudaCodeContainingIgnoreCase(containerType.getAsycudaCode());
            if (dbContainerType == null) {
                containerType.setCreatedBy(principal.getName());
                containerType.setCreatedDate(new Date());
                containerType.setLastModifiedBy(principal.getName());
                containerType.setLastModifiedDate(new Date());
                this.containerTypeService.save(containerType);
                responseObject = new ResponseObject("Container Type Saved Successfully", true);
                responseObject.setObject(containerType);
            } else {
                responseObject = new ResponseObject("Container Type Already Exists !!", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject = new ResponseObject(ExceptionUtils.getRootCauseMessage(e), false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateContainerType(ContainerType containerType, Principal principal) {
        ResponseObject responseObject;
        try {
            ContainerType dbContainerType = this.containerTypeService.findOne(containerType.getContainerTypeSeq());
            if (dbContainerType != null) {
                dbContainerType.setLastModifiedBy(principal.getName());
                dbContainerType.setLastModifiedDate(new Date());
                dbContainerType.setStatus(containerType.getStatus());
                dbContainerType.setAsycudaCode(containerType.getAsycudaCode());
                dbContainerType.setLength(containerType.getLength());
                dbContainerType.setHeight(containerType.getHeight());
                dbContainerType.setWidth(containerType.getWidth());
                dbContainerType.setDescription(containerType.getDescription());
                this.containerTypeService.save(dbContainerType);
                responseObject = new ResponseObject("Container Type Saved Successfully", true);
                responseObject.setObject(dbContainerType);
            } else {
                responseObject = new ResponseObject("Container Type Not Found !!", false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject = new ResponseObject("Container Type Already Exists !!", false);
            } else {
                e.printStackTrace();
                responseObject = new ResponseObject(ExceptionUtils.getRootCauseMessage(e), false);
            }
        }
        return responseObject;
    }

    @Override
    public List<ContainerType> searchContainerType(Integer containerSizeSeq, Integer containerCategorySeq, String description) {
        List<ContainerType> containerTypeList = null;
        try {
            if (containerSizeSeq == -1 && containerCategorySeq == -1 && description.equals("")) {
                containerTypeList = this.containerTypeService.findAll();
            } else if (containerSizeSeq != -1 && containerCategorySeq == -1 && description.equals("")) {
                containerTypeList = this.containerTypeService.findByContainerSizeContainerSizeSeq(containerSizeSeq);
            } else if (containerSizeSeq == -1 && containerCategorySeq != -1 && description.equals("")) {
                containerTypeList = this.containerTypeService.findByContainerCategoryContainerCategorySeq(containerCategorySeq);
            } else if (containerSizeSeq == -1 && containerCategorySeq == -1 && !description.equals("")) {
                containerTypeList = this.containerTypeService.findByDescriptionContainingIgnoreCase(description);
            } else if (containerSizeSeq != -1 && containerCategorySeq != -1 && description.equals("")) {
                containerTypeList = this.containerTypeService.findByContainerSizeContainerSizeSeqAndContainerCategoryContainerCategorySeq(containerSizeSeq, containerCategorySeq);
            } else if (containerSizeSeq != -1 && containerCategorySeq == -1 && !description.equals("")) {
                containerTypeList = this.containerTypeService.findByContainerSizeContainerSizeSeqAndDescriptionContainingIgnoreCase(containerSizeSeq, description);
            } else if (containerSizeSeq == -1 && containerCategorySeq != -1 && !description.equals("")) {
                containerTypeList = this.containerTypeService.findByContainerCategoryContainerCategorySeqAndDescriptionContainingIgnoreCase(containerCategorySeq, description);
            } else {
                containerTypeList = this.containerTypeService.findByContainerSizeContainerSizeSeqAndContainerCategoryContainerCategorySeqAndDescriptionContainingIgnoreCase(containerSizeSeq,
                        containerCategorySeq,
                        description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return containerTypeList;
    }
}
