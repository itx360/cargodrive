package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.CompanyProfileManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.CompanyModule;
import com.itx360.cargodrive.config.datalayer.model.CompanyProfile;
import com.itx360.cargodrive.config.datalayer.service.CompanyModuleService;
import com.itx360.cargodrive.config.datalayer.service.CompanyProfileService;
import com.itx360.cargodrive.master.datalayer.model.UploadedDocument;
import com.itx360.cargodrive.master.datalayer.service.UploadedDocumentService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Harshaa on 9/7/2016.
 */

@Service
public class CompanyProfileManagementControllerManagerImpl implements CompanyProfileManagementControllerManager {

    private final CompanyProfileService companyProfileService;
    private final CompanyModuleService companyModuleService;
    private final UploadedDocumentService uploadedDocumentService;

    @Autowired
    public CompanyProfileManagementControllerManagerImpl(CompanyProfileService companyProfileService,
                                                         CompanyModuleService companyModuleService,
                                                         UploadedDocumentService uploadedDocumentService) {
        this.companyProfileService = companyProfileService;
        this.uploadedDocumentService = uploadedDocumentService;
        this.companyModuleService = companyModuleService;
    }

    @Override
    @Transactional
    public ResponseObject createCompany(CompanyProfile companyProfile, MultipartFile multipartFile, Principal principal) {
        ResponseObject responseObject = new ResponseObject("CompanyProfile creation failed", false);
        try {
            //save logo file
            UploadedDocument file = this.uploadedDocumentService.save(multipartFile, principal.getName());
            companyProfile.setUploadDocumentSeq(file.getUploadedDocumentSeq());
            //save companyProfile data
            companyProfile.setCreatedBy(principal.getName());
            companyProfile.setCreatedDate(new Date());
            companyProfile.setLastModifiedBy(principal.getName());
            companyProfile.setLastModifiedDate(new Date());
            if (companyProfile.getStatus() == null) {
                companyProfile.setStatus(0);
            }

            // set addressbook data
            AddressBook addressBook = new AddressBook();
            addressBook.setEmail(companyProfile.getEmail());
            addressBook.setTelephone(companyProfile.getTelephoneNumber());
            addressBook.setCreatedBy(principal.getName());
            addressBook.setCreatedDate(new Date());
            addressBook.setLastModifiedBy(principal.getName());
            addressBook.setLastModifiedDate(new Date());

            companyProfile.setAddressBook(addressBook);

            companyProfile = this.companyProfileService.save(companyProfile);
            responseObject.setObject(companyProfile);
            responseObject.setMessage("Company creation successful");

        } catch (Exception ex) {
            ex.printStackTrace();
            responseObject.setStatus(false);
            responseObject.setMessage(ex.getMessage());
        }
        responseObject.setObject(companyProfile);
        return responseObject;
    }

    @Override
    @Transactional
    public ResponseObject updateCompany(CompanyProfile companyProfile, MultipartFile multipartFile, Principal principal) {
        ResponseObject responseObject = new ResponseObject("CompanyProfile update failed", true);
        CompanyProfile dbCompanyProfile = this.companyProfileService.findOne(companyProfile.getCompanyProfileSeq());
        if (dbCompanyProfile != null) {
            try {
                //save logo file
                UploadedDocument file = this.uploadedDocumentService.save(multipartFile, principal.getName());
                companyProfile.setUploadDocumentSeq(file.getUploadedDocumentSeq());
            } catch (Exception ex) {
                ex.printStackTrace();
                responseObject.setMessage(ex.getMessage());
                responseObject.setStatus(false);
            }
            //update companyProfile data
            dbCompanyProfile.setCompanyName(companyProfile.getCompanyName());
            dbCompanyProfile.setDescription(companyProfile.getDescription());
            dbCompanyProfile.setUploadDocumentSeq(companyProfile.getUploadDocumentSeq());
            dbCompanyProfile.setLastModifiedBy(principal.getName());
            dbCompanyProfile.setLastModifiedDate(new Date());
            if (companyProfile.getStatus() != null) {
                dbCompanyProfile.setStatus(companyProfile.getStatus());
            }
            // update address book data
            dbCompanyProfile.getAddressBook().setTelephone(companyProfile.getTelephoneNumber());
            dbCompanyProfile.getAddressBook().setEmail(companyProfile.getEmail());
            dbCompanyProfile.getAddressBook().setLastModifiedBy(principal.getName());
            dbCompanyProfile.getAddressBook().setLastModifiedDate(new Date());

            companyProfile = this.companyProfileService.save(dbCompanyProfile);
            responseObject.setObject(companyProfile);
            responseObject.setMessage("Company update successful !!");
        } else {
            responseObject.setMessage("CompanyProfile not found !!");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    @Transactional
    public ResponseObject createCompanyModules(Integer companyProfileSeq, List<Integer> moduleSeqList, Principal principal) {
        ResponseObject responseObject = new ResponseObject("CompanyProfile Module Assignment Failed", false);
        try {
            if (moduleSeqList.size() > 0) {
                for (Integer moduleSeq : moduleSeqList) {
                    CompanyModule companyModule = new CompanyModule();
                    companyModule.setCompanyProfileSeq(companyProfileSeq);
                    companyModule.setModuleSeq(moduleSeq);
                    companyModule.setCreatedBy(principal.getName());
                    companyModule.setCreatedDate(new Date());
                    companyModule.setStatus(1);
                    this.companyModuleService.save(companyModule);
                }
                responseObject.setMessage("CompanyProfile Module Assignment Successful");
                responseObject.setStatus(true);
            } else {
                responseObject.setMessage("No Modules Assigned");
                responseObject.setStatus(false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseObject;
    }

    @Override
    @Transactional
    public ResponseObject removeCompanyModules(Integer companyProfileSeq, List<Integer> moduleSeqList, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Module Removal Failed", false);
        try {
            for (Integer moduleSeq : moduleSeqList) {
                CompanyModule companyModule = this.companyModuleService.findByCompanyProfileSeqAndModuleSeq(companyProfileSeq, moduleSeq);
                companyModule.setStatus(0);
            }
            responseObject.setMessage("Module Removal Successful");
            responseObject.setStatus(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseObject;
    }
}
