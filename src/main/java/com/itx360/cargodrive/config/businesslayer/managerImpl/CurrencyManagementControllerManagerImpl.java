package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.CurrencyManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Currency;
import com.itx360.cargodrive.config.datalayer.service.CurrencyService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/19/2016
 * Time: 4:56 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CurrencyManagementControllerManagerImpl implements CurrencyManagementControllerManager {

    private final CurrencyService currencyService;

    @Autowired
    public CurrencyManagementControllerManagerImpl(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    /**
     * method used to save new currency
     *
     * @param currency  Currency
     * @param principal Principal
     * @return ResponseObject
     */
    @Override
    public ResponseObject saveCurrency(Currency currency, Principal principal) {
        ResponseObject responseObject;
        try {
            Currency dbCurrency = this.currencyService.findByCurrencyCodeIgnoreCase(currency.getCurrencyCode());
            if (dbCurrency == null) {
                currency.setCreatedBy(principal.getName());
                currency.setCreatedDate(new Date());
                currency.setLastModifiedBy(principal.getName());
                currency.setLastModifiedDate(new Date());
                currency.setCurrencyCode(currency.getCurrencyCode().toUpperCase());
                currency.setCurrencyName(currency.getCurrencyName().toUpperCase());
                this.currencyService.save(currency);
                responseObject = new ResponseObject("Currency saved successfully", true);
                responseObject.setObject(currency);
            } else {
                responseObject = new ResponseObject("Currency already exit!!", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject = new ResponseObject("Error saving new currency", false);
        }
        return responseObject;
    }

    /**
     * method used to update currency
     *
     * @param currency  Currency
     * @param principal Principal
     * @return ResponseObject
     */
    @Override
    public ResponseObject updateCurrency(Currency currency, Principal principal) {
        ResponseObject responseObject;
        try {
            Currency dbCurrency = this.currencyService.findOne(currency.getCurrencySeq());
            if (dbCurrency != null) {
                dbCurrency.setLastModifiedBy(principal.getName());
                dbCurrency.setLastModifiedDate(new Date());
                dbCurrency.setCurrencyCode(currency.getCurrencyCode().toUpperCase());
                dbCurrency.setCurrencyName(currency.getCurrencyName().toUpperCase());
                dbCurrency.setCurrencyDisplay(currency.getCurrencyDisplay());
                dbCurrency.setStatus(currency.getStatus());
                this.currencyService.save(dbCurrency);
                responseObject = new ResponseObject("Currency updated successfully", true);
                responseObject.setObject(dbCurrency);
            } else {
                responseObject = new ResponseObject("Currency not found!!", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject = new ResponseObject("Error updating currency", false);
        }
        return responseObject;
    }

    /**
     * method used to search currency
     *
     * @param currencyName    currency name
     * @param currencyDisplay currency display name
     * @return currency list
     */
    @Override
    public List<Currency> searchCurrencyByCurrencyNameAndCurrencyDisplay(String currencyName, String currencyDisplay) {
        List<Currency> currencyList = null;
        try {
            if (currencyName.equals("") && currencyDisplay.equals("")) {
                currencyList = this.currencyService.findAll();
            } else if (!currencyName.equals("") && currencyDisplay.equals("")) {
                currencyList = this.currencyService.findByCurrencyNameContainingIgnoreCase(currencyName);
            } else if (!currencyDisplay.equals("") && currencyName.equals("")) {
                currencyList = this.currencyService.findByCurrencyDisplayContainingIgnoreCase(currencyDisplay);
            } else {
                currencyList = this.currencyService.findByCurrencyNameContainingIgnoreCaseAndCurrencyDisplayContainingIgnoreCase(currencyName, currencyDisplay);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currencyList;
    }
}
