package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.CompanyProfile;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.List;

/**
 * Created by Harshaa on 9/7/2016.
 */
public interface CompanyProfileManagementControllerManager {

    ResponseObject createCompany(CompanyProfile companyProfile, MultipartFile multipartFile, Principal principal);

    ResponseObject updateCompany(CompanyProfile companyProfile, MultipartFile multipartFile, Principal principal);

    ResponseObject createCompanyModules(Integer companyProfileSeq, List<Integer> moduleSeqList, Principal principal);

    ResponseObject removeCompanyModules(Integer companyProfileSeq, List<Integer> moduleSeqList, Principal principal);
}
