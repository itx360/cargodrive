package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.PackageType;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/4/2016
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
public interface PackageTypeManagementControllerManager {
    ResponseObject savePackageType(PackageType packageType, Principal principal);

    ResponseObject updatePackageType(PackageType packageType, Principal principal);

    List<PackageType> searchPackageType(String packageTypeName, String packageTypeCode, String description);
}
