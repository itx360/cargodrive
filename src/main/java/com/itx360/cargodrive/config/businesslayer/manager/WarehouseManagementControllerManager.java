package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Warehouse;
import com.itx360.cargodrive.master.utility.ResponseObject;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 9/29/2016.
 */
public interface WarehouseManagementControllerManager {

    ResponseObject saveWarehouse(AddressBook addressBook, Warehouse warehouse, Principal principal, HttpServletRequest request);

    ResponseObject updateWarehouse(AddressBook addressBook,Warehouse warehouse,Principal principal);

    List<Warehouse> searchWarehouse(String warehouseCode, String warehouseName, Integer warehouseSeq);



}
