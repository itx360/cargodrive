package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.UserDetailsManager;
import com.itx360.cargodrive.config.datalayer.model.User;
import com.itx360.cargodrive.config.datalayer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by shanakajay on 8/22/2016.
 */
@Service
public class UserDetailsManagerImpl implements UserDetailsManager {

    @Autowired
    UserService userService;

    @Transactional
    public User getUserByUserSeq(int id) {
        return userService.getUserByUserSeq(id);
    }
}
