package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.CommodityCategoryManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.CommodityCategory;
import com.itx360.cargodrive.config.datalayer.service.CommodityCategoryService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/26/2016
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CommodityCategoryManagementControllerManagerImpl implements CommodityCategoryManagementControllerManager {

    private final CommodityCategoryService commodityCategoryService;

    @Autowired
    public CommodityCategoryManagementControllerManagerImpl(CommodityCategoryService commodityCategoryService) {
        this.commodityCategoryService = commodityCategoryService;
    }

    /**
     * method for creating new commodity category
     *
     * @param commodityCategory CommodityCategory
     * @param principal         Principal
     * @return ResponseObject
     */
    @Override
    public ResponseObject saveCommodityCategory(CommodityCategory commodityCategory, Principal principal) {
        ResponseObject responseObject;
        try {
            CommodityCategory dbCommodityCategoryList = this.commodityCategoryService.findByCommodityCodeIgnoreCase(commodityCategory.getCommodityCode());
            if (dbCommodityCategoryList == null) {
                commodityCategory.setCreatedBy(principal.getName());
                commodityCategory.setCreatedDate(new Date());
                commodityCategory.setLastModifiedBy(principal.getName());
                commodityCategory.setLastModifiedDate(new Date());
                commodityCategory.setCommodityCode(commodityCategory.getCommodityCode().toUpperCase());
                commodityCategory.setCommodityName(commodityCategory.getCommodityName().toUpperCase());
                this.commodityCategoryService.save(commodityCategory);
                responseObject = new ResponseObject("Commodity Category Saved Successfully", true);
                responseObject.setObject(commodityCategory);
            } else {
                responseObject = new ResponseObject("Commodity Category Already Exists!!", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject = new ResponseObject("Error Saving Commodity Category", false);
        }
        return responseObject;
    }

    /**
     * method for update existing commodity category
     *
     * @param commodityCategory CommodityCategory
     * @param principal         Principal
     * @return ResponseObject
     */
    @Override
    public ResponseObject updateCommodityCategory(CommodityCategory commodityCategory, Principal principal) {
        ResponseObject responseObject;
        try {
            CommodityCategory dbCommodityCategory = this.commodityCategoryService.findOne(commodityCategory.getCommodityCategorySeq());
            if (dbCommodityCategory != null) {
                dbCommodityCategory.setStatus(commodityCategory.getStatus());
                dbCommodityCategory.setLastModifiedBy(principal.getName());
                dbCommodityCategory.setLastModifiedDate(new Date());
                dbCommodityCategory.setCommodityName(commodityCategory.getCommodityName().toUpperCase());
                dbCommodityCategory.setCommodityCode(commodityCategory.getCommodityCode().toUpperCase());
                this.commodityCategoryService.save(dbCommodityCategory);
                responseObject = new ResponseObject("Commodity Category Updated Successfully", true);
                responseObject.setObject(dbCommodityCategory);
            } else {
                responseObject = new ResponseObject("Commodity Category  Not Found!!", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject = new ResponseObject("Error Updating Commodity Category ", false);
        }
        return responseObject;
    }

    /**
     * method for search commodity category details
     *
     * @param commodityCode commodityCode
     * @param commodityName commodityName
     * @return CommodityCategory List
     */
    @Override
    public List<CommodityCategory> searchCommodityCategoryByCommodityCodeAndCommodityName(String commodityCode, String commodityName) {
        List<CommodityCategory> commodityCategoryList = null;
        try {
            if (commodityCode.equals("") && commodityName.equals("")) {
                commodityCategoryList = this.commodityCategoryService.findAll();
            } else if (!commodityName.equals("") && commodityName.equals("")) {
                commodityCategoryList = this.commodityCategoryService.findByCommodityCodeContainingIgnoreCase(commodityCode);
            } else if (!commodityName.equals("") && commodityCode.equals("")) {
                commodityCategoryList = this.commodityCategoryService.findByCommodityNameContainingIgnoreCase(commodityName);
            } else {
                commodityCategoryList = this.commodityCategoryService.findByCommodityCodeContainingIgnoreCaseAndCommodityNameContainingIgnoreCase(commodityCode, commodityName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commodityCategoryList;
    }
}
