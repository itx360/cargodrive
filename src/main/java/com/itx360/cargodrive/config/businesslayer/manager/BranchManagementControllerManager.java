package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Branch;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 9/26/2016.
 */
public interface BranchManagementControllerManager {

    ResponseObject saveBranch(AddressBook addressBook,Branch branch,Principal principal);

    ResponseObject updateBranch(AddressBook addressBook,Branch branch,Principal principal);

    List<Branch> searchBranch(String branchCode,String branchName,String bankName);

}
