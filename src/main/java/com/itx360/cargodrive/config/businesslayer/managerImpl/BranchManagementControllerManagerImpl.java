package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.BranchManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Branch;
import com.itx360.cargodrive.config.datalayer.service.BranchService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 9/26/2016.
 */
@Service
public class BranchManagementControllerManagerImpl implements BranchManagementControllerManager {

    private final BranchService branchService;

    @Autowired
    public BranchManagementControllerManagerImpl(BranchService branchService) {
        this.branchService = branchService;
    }

    @Override
    public ResponseObject saveBranch(AddressBook addressBook,Branch branch, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Branch Saved Successfully", true);
        try {
            Branch dbBranch = this.branchService.findByBranchCodeIgnoreCaseAndBranchNameIgnoreCaseAndBankBankSeq(branch.getBranchCode(), branch.getBranchName(), branch.getBankSeq());
            if (dbBranch == null) {
                branch.setCreatedBy(principal.getName());
                branch.setCreatedDate(new Date());
                branch.setLastModifiedBy(principal.getName());
                branch.setLastModifiedDate(new Date());
                branch.setStatus(1);

                AddressBook dbAddressBook = addressBook;
                dbAddressBook.setCreatedBy(principal.getName());
                dbAddressBook.setCreatedDate(new Date());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                branch.setAddressBook(dbAddressBook);

                branch = this.branchService.save(branch);
                responseObject.setObject(branch);

            } else {
                responseObject.setMessage("Branch Already Exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Branch Already Exist!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateBranch(AddressBook addressBook,Branch branch,Principal principal) {
        ResponseObject responseObject = new ResponseObject("Branch Successfully Updated", true);
        try {
            Branch branchDB = this.branchService.findOne(branch.getBranchSeq());
            if (branchDB != null) {
                branchDB.setBankSeq(branch.getBankSeq());
                branchDB.setBranchCode(branch.getBranchCode());
                branchDB.setBranchName(branch.getBranchName());
                branchDB.setStatus(branch.getStatus());
                branchDB.setLastModifiedBy(principal.getName());
                branchDB.setLastModifiedDate(new Date());

                AddressBook dbAddressBook = branchDB.getAddressBook();
                dbAddressBook.setAddress1(addressBook.getAddress1());
                dbAddressBook.setAddress2(addressBook.getAddress2());
                dbAddressBook.setCity(addressBook.getCity());
                dbAddressBook.setZip(addressBook.getZip());
                dbAddressBook.setTelephone(addressBook.getTelephone());
                dbAddressBook.setTelephoneExtension(addressBook.getTelephoneExtension());
                dbAddressBook.setMobile(addressBook.getMobile());
                dbAddressBook.setFax(addressBook.getFax());
                dbAddressBook.setEmail(addressBook.getEmail());
                dbAddressBook.setWebsite(addressBook.getWebsite());
                dbAddressBook.setState(addressBook.getState());
                dbAddressBook.setCountrySeq(addressBook.getCountrySeq());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                branchDB.setAddressBook(dbAddressBook);

                branchDB = this.branchService.save(branchDB);
                responseObject.setObject(branchDB);
            } else {
                responseObject.setMessage("Branch Not Found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Branch Already Exist!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<Branch> searchBranch(String branchCode, String branchName, String bankName) {
        List<Branch> branchList = null;
        try {
            if (branchCode.equals("") && branchName.equals("") && bankName.equals("")) {
                branchList = branchService.findAll();
            } else if (!branchCode.equals("") && branchName.equals("") && bankName.equals("")) {
                branchList = branchService.findByBranchCodeContainingIgnoreCase(branchCode);
            } else if (branchCode.equals("") && !branchName.equals("") && bankName.equals("")) {
                branchList = branchService.findByBranchNameContainingIgnoreCase(branchName);
            } else if (branchCode.equals("") && branchName.equals("") && !bankName.equals("")) {
                branchList = branchService.findByBankBankNameContainingIgnoreCase(bankName);
            } else if (!branchCode.equals("") && !branchName.equals("") && bankName.equals("")) {
                branchList = branchService.findByBranchCodeContainingIgnoreCaseAndBranchNameContainingIgnoreCase(branchCode, branchName);
            } else if (!branchCode.equals("") && branchName.equals("") && !bankName.equals("")) {
                branchList = branchService.findByBranchCodeContainingIgnoreCaseAndBankBankNameContainingIgnoreCase(branchCode, bankName);
            } else if (branchCode.equals("") && !branchName.equals("") && !bankName.equals("")) {
                branchList = branchService.findByBranchNameContainingIgnoreCaseAndBankBankNameContainingIgnoreCase(branchName, bankName);
            } else if (!branchCode.equals("") && !branchName.equals("") && !bankName.equals("")) {
                branchList = branchService.findByBranchCodeContainingIgnoreCaseAndBranchNameContainingIgnoreCaseAndBankBankNameContainingIgnoreCase(branchCode, branchName, bankName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchList;
    }
}
