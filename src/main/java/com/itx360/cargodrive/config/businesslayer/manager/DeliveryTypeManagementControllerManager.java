package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Bank;
import com.itx360.cargodrive.config.datalayer.model.DeliveryType;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 10/5/2016.
 */
public interface DeliveryTypeManagementControllerManager {

    ResponseObject createDeliveryType(DeliveryType deliveryType, Principal principal);

    ResponseObject updateDeliveryType(DeliveryType deliveryType, Principal principal);

    List<DeliveryType> searchDeliveryType(String deliveryTypeCode, String description, String deliveryTypeMode);

}
