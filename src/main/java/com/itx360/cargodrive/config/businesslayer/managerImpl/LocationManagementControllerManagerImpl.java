package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.LocationManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Location;
import com.itx360.cargodrive.config.datalayer.service.LocationService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Thilangaj on 9/20/2016 2:50 PM).
 */
@Service
public class LocationManagementControllerManagerImpl implements LocationManagementControllerManager {

    private final LocationService locationService;

    @Autowired
    public LocationManagementControllerManagerImpl(LocationService locationService) {
        this.locationService = locationService;
    }

    @Override
    public ResponseObject createLocation(Location location, String username) {
        ResponseObject responseObject = new ResponseObject("Location Successfully Saved", true);
        try {
            Location dbLocation = this.locationService.findByLocationName(location.getLocationName());
            if (dbLocation == null) {
                location.setCreatedBy(username);
                location.setCreatedDate(new Date());
                location.setModifiedBy(username);
                location.setModifiedDate(new Date());
                location = this.locationService.save(location);
                responseObject.setObject(location);
            } else {
                responseObject.setMessage("Location already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateLocation(Location location, String username) {
        ResponseObject responseObject = new ResponseObject("Location Successfully Updated", true);
        try {
            Location dbLocation = this.locationService.findOne(location.getLocationSeq());
            if (dbLocation != null) {
                dbLocation.setModifiedBy(username);
                dbLocation.setModifiedDate(new Date());
                dbLocation.setCountrySeq(location.getCountrySeq());
                dbLocation.setLocationName(location.getLocationName());
                dbLocation.setStatus(location.getStatus());
                dbLocation = this.locationService.save(dbLocation);
                responseObject.setObject(dbLocation);
            } else {
                responseObject.setMessage("Location Not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<Location> searchLocation(String locationName, Integer countrySeq) {
        List<Location> locationList = null;
        try {
            if (locationName == null) {
                if (countrySeq.equals(-1)) {
                    locationList = this.locationService.findAll();
                } else {
                    locationList = this.locationService.findByCountrySeq(countrySeq);
                }
            } else {
                if (countrySeq.equals(-1)) {
                    locationList = this.locationService.findByLocationNameContainingIgnoreCase(locationName);
                } else {
                    locationList = this.locationService.findByLocationNameContainingIgnoreCaseAndCountrySeq(locationName, countrySeq);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationList;
    }
}
