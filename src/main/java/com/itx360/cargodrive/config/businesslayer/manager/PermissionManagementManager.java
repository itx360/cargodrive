package com.itx360.cargodrive.config.businesslayer.manager;


import com.itx360.cargodrive.config.datalayer.model.Authority;
import com.itx360.cargodrive.config.datalayer.model.Group;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.util.List;

/**
 * Created by shanakajay on 9/2/2016.
 */
public interface PermissionManagementManager {

    List<Group> getAssignedGroupList(Integer userSeq, Integer companyProfileSeq, Integer moduleSeq);

    List<Authority> getAssignedAuthorityList(Integer groupSeq, Integer moduleSeq);

    ResponseObject saveUserGroups(Integer userSeq, Integer companyProfileSeq, Integer moduleSeq, List<Integer> groupSeqList, String username);

    ResponseObject saveGroupAuthorities(Integer groupSeq, Integer moduleSeq, List<Integer> authoritySeqList, String username);
}
