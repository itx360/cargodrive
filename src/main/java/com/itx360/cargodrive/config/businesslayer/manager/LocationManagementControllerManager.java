package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Location;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.util.List;

/**
 * Created by Thilangaj on 9/20/2016 2:50 PM).
 */
public interface LocationManagementControllerManager {

    ResponseObject createLocation(Location location, String username);

    ResponseObject updateLocation(Location location, String username);

    List<Location> searchLocation(String locationName, Integer countrySeq);
}
