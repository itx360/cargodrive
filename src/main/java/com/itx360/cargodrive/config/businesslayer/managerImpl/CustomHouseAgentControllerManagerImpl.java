package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.CustomHouseAgentControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.CustomHouseAgent;
import com.itx360.cargodrive.config.datalayer.service.CustomHouseAgentService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 10/6/2016.
 */
@Service
public class CustomHouseAgentControllerManagerImpl implements CustomHouseAgentControllerManager {

    private final CustomHouseAgentService customHouseAgentService;

    @Autowired
    public CustomHouseAgentControllerManagerImpl(CustomHouseAgentService customHouseAgentService) {
        this.customHouseAgentService = customHouseAgentService;
    }


    @Override
    public ResponseObject saveCustomHouseAgent(AddressBook addressBook, CustomHouseAgent customHouseAgent, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Custom House Agent Saved Successfully", true);
        try {
            CustomHouseAgent customHouseAgentDB = this.customHouseAgentService.findByCustomHouseAgentNameContainingIgnoreCase(customHouseAgent.getCustomHouseAgentName());
            if (customHouseAgentDB == null) {
                customHouseAgent.setCreatedBy(principal.getName());
                customHouseAgent.setCreatedDate(new Date());
                customHouseAgent.setLastModifiedBy(principal.getName());
                customHouseAgent.setLastModifiedDate(new Date());

                AddressBook dbAddressBook = addressBook;
                dbAddressBook.setCreatedBy(principal.getName());
                dbAddressBook.setCreatedDate(new Date());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                customHouseAgent.setAddressBook(dbAddressBook);

                customHouseAgent = this.customHouseAgentService.save(customHouseAgent);
                responseObject.setObject(customHouseAgent);
            } else {
                responseObject.setMessage("Custom House Agent Already Exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Custom House Agent Already Exist!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateCustomHouseAgent(AddressBook addressBook, CustomHouseAgent customHouseAgent, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Custom House Agent Successfully Updated", true);
        try {
            CustomHouseAgent customHouseAgentDB = this.customHouseAgentService.findOne(customHouseAgent.getCustomHouseAgentSeq());
            if (customHouseAgentDB != null) {
                customHouseAgentDB.setCustomHouseAgentName(customHouseAgent.getCustomHouseAgentName());
                customHouseAgentDB.setTaxRegistrationNo(customHouseAgent.getTaxRegistrationNo());
                customHouseAgentDB.setDescription(customHouseAgent.getDescription());
                customHouseAgentDB.setTaxTypeSeq(customHouseAgent.getTaxTypeSeq());
                customHouseAgentDB.setCountrySeq(customHouseAgent.getCountrySeq());
                customHouseAgentDB.setStatus(customHouseAgent.getStatus());
                customHouseAgentDB.setLastModifiedBy(principal.getName());
                customHouseAgentDB.setLastModifiedDate(new Date());

                AddressBook dbAddressBook = customHouseAgentDB.getAddressBook();
                dbAddressBook.setAddress1(addressBook.getAddress1());
                dbAddressBook.setAddress2(addressBook.getAddress2());
                dbAddressBook.setCity(addressBook.getCity());
                dbAddressBook.setZip(addressBook.getZip());
                dbAddressBook.setTelephone(addressBook.getTelephone());
                dbAddressBook.setTelephoneExtension(addressBook.getTelephoneExtension());
                dbAddressBook.setMobile(addressBook.getMobile());
                dbAddressBook.setFax(addressBook.getFax());
                dbAddressBook.setEmail(addressBook.getEmail());
                dbAddressBook.setWebsite(addressBook.getWebsite());
                dbAddressBook.setState(addressBook.getState());
                dbAddressBook.setCountrySeq(addressBook.getCountrySeq());
                dbAddressBook.setChaLicenseNo(addressBook.getChaLicenseNo());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                customHouseAgentDB.setAddressBook(dbAddressBook);

                customHouseAgentDB = this.customHouseAgentService.save(customHouseAgentDB);
                responseObject.setObject(customHouseAgentDB);
            } else {
                responseObject.setMessage("Custom House Agent Not Found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Custom House Agent Already Exist!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }

        return responseObject;
    }

    @Override
    public List<CustomHouseAgent> searchCustomHouseAgent(String customHouseAgentName, Integer taxTypeSeq, String taxRegistrationNo, String description) {
        List<CustomHouseAgent> customHouseAgentList = null;
        try {
            if (taxTypeSeq.equals(-1)) {
                if (customHouseAgentName.equals("") && taxRegistrationNo.equals("") && description.equals("")) {
                    customHouseAgentList = this.customHouseAgentService.findAll();
                } else {
                    customHouseAgentList = this.customHouseAgentService.findByCustomHouseAgentNameContainingIgnoreCaseAndTaxRegistrationNoContainingIgnoreCaseAndDescriptionContainingIgnoreCase(customHouseAgentName,taxRegistrationNo,description);
                }
            } else {
                if (customHouseAgentName.equals("") && taxRegistrationNo.equals("") && description.equals("")) {
                    customHouseAgentList = this.customHouseAgentService.findByTaxTypeSeq(taxTypeSeq);
                } else {
                    customHouseAgentList = this.customHouseAgentService.findByCustomHouseAgentNameContainingIgnoreCaseAndTaxTypeSeqAndTaxRegistrationNoContainingIgnoreCaseAndDescriptionContainingIgnoreCase(customHouseAgentName,taxTypeSeq,taxRegistrationNo,description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customHouseAgentList;
    }
}
