package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.FinalDestinationManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.FinalDestination;
import com.itx360.cargodrive.config.datalayer.service.FinalDestinationService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 10/5/2016.
 */
@Service
public class FinalDestinationManagementControllerManagerImpl implements FinalDestinationManagementControllerManager {

    private final FinalDestinationService finalDestinationService;

    @Autowired
    public FinalDestinationManagementControllerManagerImpl(FinalDestinationService finalDestinationService) {
        this.finalDestinationService = finalDestinationService;
    }

    @Override
    public ResponseObject saveDestination(AddressBook addressBook, FinalDestination finalDestination, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Final Destination Saved Successfully", true);
        try {
            FinalDestination finalDestinationDB = this.finalDestinationService.findByFinalDestinationCodeIgnoreCaseAndCountrySeq(finalDestination.getFinalDestinationCode(), finalDestination.getCountrySeq());
            if (finalDestinationDB == null) {
                finalDestination.setCreatedBy(principal.getName());
                finalDestination.setCreatedDate(new Date());
                finalDestination.setLastModifiedBy(principal.getName());
                finalDestination.setLastModifiedDate(new Date());
                finalDestination.setStatus(1);

                AddressBook dbAddressBook = addressBook;
                dbAddressBook.setCreatedBy(principal.getName());
                dbAddressBook.setCreatedDate(new Date());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                finalDestination.setAddressBook(dbAddressBook);

                finalDestination = this.finalDestinationService.save(finalDestination);
                responseObject.setObject(finalDestination);
            } else {
                responseObject.setMessage("Final Destination Already Exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Saving Branch");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateDestination(AddressBook addressBook, FinalDestination finalDestination, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Final Destination Successfully Updated", true);
        try {
            FinalDestination finalDestinationDB = this.finalDestinationService.findOne(finalDestination.getFinalDestinationSeq());

            if (finalDestinationDB != null) {
                finalDestinationDB.setFinalDestinationCode(finalDestination.getFinalDestinationCode());
                finalDestinationDB.setCountrySeq(finalDestination.getCountrySeq());
                finalDestinationDB.setStatus(1);
                finalDestinationDB.setLastModifiedBy(principal.getName());
                finalDestinationDB.setLastModifiedDate(new Date());

                AddressBook dbAddressBook = finalDestinationDB.getAddressBook();

                dbAddressBook.setCity(addressBook.getCity());
                dbAddressBook.setZip(addressBook.getZip());
                dbAddressBook.setState(addressBook.getState());
                dbAddressBook.setCountrySeq(addressBook.getCountrySeq());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                finalDestinationDB.setAddressBook(dbAddressBook);

                finalDestinationDB = this.finalDestinationService.save(finalDestinationDB);
                responseObject.setObject(finalDestinationDB);
            } else {
                responseObject.setMessage("Final Destination Not Found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseObject;
    }

    @Override
    public List<FinalDestination> searchDestination(String finalDestinationCode, Integer countrySeq, String city, String state, String zip) {
        List<FinalDestination> finalDestinationList = null;
        try {
            if (countrySeq.equals(-1)) {
                if (finalDestinationCode.equals("") && city.equals("") && state.equals("") && zip.equals("")) {
                    finalDestinationList = this.finalDestinationService.findAll();
                } else {
                    finalDestinationList = this.finalDestinationService.findByFinalDestinationCodeContainingIgnoreCaseAndAddressBookCityContainingIgnoreCaseAndAddressBookStateContainingIgnoreCaseAndAddressBookZipContainingIgnoreCase(finalDestinationCode, city, state, zip);
                }
            } else {
                if (finalDestinationCode.equals("") && city.equals("") && state.equals("") && zip.equals("")) {
                    finalDestinationList = this.finalDestinationService.findByCountrySeq(countrySeq);
                } else {
                    finalDestinationList = this.finalDestinationService.findByFinalDestinationCodeContainingIgnoreCaseAndCountryCountrySeqAndAddressBookCityContainingIgnoreCaseAndAddressBookStateContainingIgnoreCaseAndAddressBookZipContainingIgnoreCase(finalDestinationCode, countrySeq, city, state, zip);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalDestinationList;
    }
}
