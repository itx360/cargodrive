package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.UnitManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Unit;
import com.itx360.cargodrive.config.datalayer.service.UnitService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shanakajay on 10/4/2016.
 */
@Service
public class UnitManagementControllerManagerImpl implements UnitManagementControllerManager {

    private final UnitService unitService;

    public UnitManagementControllerManagerImpl(UnitService unitService) {
        this.unitService = unitService;
    }

    @Override
    public ResponseObject createUnit(Unit unit, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Unit Successfully Saved", true);
        try {
            if (this.unitService.findByUnitName(unit.getUnitName()) == null) {
                unit.setCreatedBy(principal.getName());
                unit.setCreatedDate(new Date());
                unit.setLastModifiedBy(principal.getName());
                unit.setLastModifiedDate(new Date());
                unit = this.unitService.save(unit);
                responseObject.setObject(unit);
            } else {
                responseObject.setMessage("Unit already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateDeliveryType(Unit unit, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Unit Successfully Updated", true);
        try {
            Unit dbUnit = this.unitService.findOne(unit.getUnitSeq());
            if (dbUnit != null) {
                dbUnit.setUnitCode(unit.getUnitCode());
                dbUnit.setUnitName(unit.getUnitName());
                dbUnit.setUsedFor(unit.getUsedFor());
                dbUnit.setDescription(unit.getDescription());
                dbUnit.setStatus(unit.getStatus());
                dbUnit.setLastModifiedBy(principal.getName());
                dbUnit.setLastModifiedDate(new Date());
                this.unitService.save(dbUnit);
            } else {
                responseObject.setMessage("Unit not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<Unit> searchUnit(String unitName, String unitCode, String usedFor) {
        List<Unit> unitList = new ArrayList<>();
        try {
            if (unitName.equals("") && unitCode.equals("") && usedFor.equals("None")) {//null,null,none
                unitList = this.unitService.findAll();
            } else if (unitName.equals("") && unitCode.equals("") && !usedFor.equals("None")) {//null,null,value
                unitList = this.unitService.findByUsedForContainingIgnoreCase(usedFor);
            } else if (unitName.equals("") && !unitCode.equals("") && usedFor.equals("None")) {//null,value,none
                unitList = this.unitService.findByUnitCodeContainingIgnoreCase(unitCode);
            } else if (unitName.equals("") && !unitCode.equals("") && !usedFor.equals("None")) {//null,value,value
                unitList = this.unitService.findByUnitCodeContainingIgnoreCaseAndUsedForContainingIgnoreCase(unitCode, usedFor);
            } else if (!unitName.equals("") && unitCode.equals("") && usedFor.equals("None")) {//value,null,none
                unitList = this.unitService.findByUnitNameContainingIgnoreCase(unitName);
            } else if (!unitName.equals("") && unitCode.equals("") && !usedFor.equals("None")) {//value,null,value
                unitList = this.unitService.findByUnitNameContainingIgnoreCaseAndUsedForContainingIgnoreCase(unitName, usedFor);
            } else if (!unitName.equals("") && !unitCode.equals("") && usedFor.equals("None")) {//value,value,none
                unitList = this.unitService.findByUnitNameContainingIgnoreCaseAndUnitCodeContainingIgnoreCase(unitName, unitCode);
            } else {//value,value,value
                unitList = this.unitService.findByUnitNameContainingIgnoreCaseAndUnitCodeContainingIgnoreCaseAndUsedForContainingIgnoreCase(unitName, unitCode, usedFor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unitList;
    }
}
