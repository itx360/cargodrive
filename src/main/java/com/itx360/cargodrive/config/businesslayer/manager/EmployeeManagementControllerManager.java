package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Employee;
import com.itx360.cargodrive.master.utility.ResponseObject;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 10/4/2016.
 */
public interface EmployeeManagementControllerManager {

    ResponseObject saveEmployee(AddressBook addressBook,Employee employee,Principal principal,HttpServletRequest request);

    ResponseObject updateEmployee(AddressBook addressBook,Employee employee,Principal principal);


    List<Employee> searchEmployee(String employeeName, Integer status, Integer employeeDesignationSeq);
}
