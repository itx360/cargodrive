package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.TradeLane;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 9/20/2016.
 */
public interface TradeLaneControllerManager {

    ResponseObject saveTradeLane(TradeLane tradeLane, Principal principal);

    ResponseObject updateTradeLane(TradeLane tradeLane, Principal principal);

    List<TradeLane> searchTradeLane(String tradeLaneCode,String tradeLaneName);
}
