package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.CustomerGroupManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.CustomerGroup;
import com.itx360.cargodrive.config.datalayer.service.CustomerGroupService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 9/28/2016.
 */
@Service
public class CustomerGroupManagementControllerManagerImpl implements CustomerGroupManagementControllerManager {

    private final CustomerGroupService customerGroupService;

    @Autowired
    public CustomerGroupManagementControllerManagerImpl(CustomerGroupService customerGroupService) {
        this.customerGroupService = customerGroupService;
    }

    @Override
    public ResponseObject saveCustomerGroup(CustomerGroup customerGroup, Principal principal,HttpServletRequest request) {
        ResponseObject responseObject = new ResponseObject("Customer Group Saved Successfully", true);
        try {
            CustomerGroup dbCustomerGroup = this.customerGroupService.findByCustomerGroupNameIgnoreCase(customerGroup.getCustomerGroupName());
            Integer companyProfileSeq = Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString());

            if (dbCustomerGroup == null) {
                customerGroup.setCreatedBy(principal.getName());
                customerGroup.setCreatedDate(new Date());
                customerGroup.setLastModifiedBy(principal.getName());
                customerGroup.setLastModifiedDate(new Date());
                customerGroup.setStatus(1);
                customerGroup.setCompanyProfileSeq(companyProfileSeq);
                customerGroup = this.customerGroupService.save(customerGroup);
                responseObject.setObject(customerGroup);
            } else {
                responseObject.setMessage("Customer Group Already Exist!!");
                responseObject.setStatus(false);
            }


        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Saving Customer Group");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateCustomerGroup(CustomerGroup customerGroup, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Customer Group Successfully Updated", true);
        try {
            CustomerGroup dbCustomerGroup = this.customerGroupService.findOne(customerGroup.getCustomerGroupSeq());
            if(dbCustomerGroup !=null){
                dbCustomerGroup.setLastModifiedBy(principal.getName());
                dbCustomerGroup.setLastModifiedDate(new Date());
                dbCustomerGroup.setCustomerGroupCode(customerGroup.getCustomerGroupCode());
                dbCustomerGroup.setCustomerGroupName(customerGroup.getCustomerGroupName());
                dbCustomerGroup.setStatus(customerGroup.getStatus());
                dbCustomerGroup = this.customerGroupService.save(dbCustomerGroup);
                responseObject.setObject(dbCustomerGroup);
            }else{
                responseObject.setMessage("Customer Group Not Found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Updating Customer Group");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<CustomerGroup> searchCustomerGroup(String customerGroupCode, String customerGroupName) {
        List<CustomerGroup> customerGroupList = null;
        try{
            if (customerGroupCode.equals("") && customerGroupName.equals("")) {
                customerGroupList = customerGroupService.findAll();
            }else if(!customerGroupCode.equals("") && customerGroupName.equals("")){
                customerGroupList = customerGroupService.findByCustomerGroupCodeContainingIgnoreCase(customerGroupCode);
            }else if(customerGroupCode.equals("") && !customerGroupName.equals("")){
                customerGroupList = customerGroupService.findByCustomerGroupNameContainingIgnoreCase(customerGroupName);
            }else if(!customerGroupCode.equals("") && !customerGroupName.equals("")){
                customerGroupList = customerGroupService.findByCustomerGroupCodeContainingIgnoreCaseAndCustomerGroupNameContainingIgnoreCase(customerGroupCode,customerGroupName);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return customerGroupList;

    }
}