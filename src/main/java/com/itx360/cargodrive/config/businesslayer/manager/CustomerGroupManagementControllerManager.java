package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.CustomerGroup;
import com.itx360.cargodrive.master.utility.ResponseObject;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 9/28/2016.
 */
public interface CustomerGroupManagementControllerManager {
    ResponseObject saveCustomerGroup(CustomerGroup customerGroup, Principal principal,HttpServletRequest request);

    ResponseObject updateCustomerGroup(CustomerGroup customerGroup, Principal principal);

    List<CustomerGroup> searchCustomerGroup(String customerGroupCode, String customerGroupName);
}
