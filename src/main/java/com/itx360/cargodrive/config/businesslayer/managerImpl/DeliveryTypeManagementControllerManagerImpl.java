package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.DeliveryTypeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.DeliveryType;
import com.itx360.cargodrive.config.datalayer.service.DeliveryTypeService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shanakajay on 10/5/2016.
 */
@Service
public class DeliveryTypeManagementControllerManagerImpl implements DeliveryTypeManagementControllerManager {

    private final DeliveryTypeService deliveryTypeService;

    public DeliveryTypeManagementControllerManagerImpl(DeliveryTypeService deliveryTypeService) {
        this.deliveryTypeService = deliveryTypeService;
    }

    @Override
    public ResponseObject createDeliveryType(DeliveryType deliveryType, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Delivery Type Successfully Saved", true);
        try {
            if (this.deliveryTypeService.findByDeliveryTypeCode(deliveryType.getDeliveryTypeCode()) == null) {
                deliveryType.setCreatedBy(principal.getName());
                deliveryType.setCreatedDate(new Date());
                deliveryType.setLastModifiedBy(principal.getName());
                deliveryType.setLastModifiedDate(new Date());
                deliveryType = this.deliveryTypeService.save(deliveryType);
                responseObject.setObject(deliveryType);
            } else {
                responseObject.setMessage("Delivery Type already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateDeliveryType(DeliveryType deliveryType, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Delivery Type Successfully Updated", true);
        try {
            DeliveryType dbDeliveryType = this.deliveryTypeService.findOne(deliveryType.getDeliveryTypeSeq());
            if (dbDeliveryType != null) {
                dbDeliveryType.setDeliveryTypeCode(deliveryType.getDeliveryTypeCode());
                dbDeliveryType.setDescription(deliveryType.getDescription());
                dbDeliveryType.setDeliveryTypeMode(deliveryType.getDeliveryTypeMode());
                dbDeliveryType.setStatus(deliveryType.getStatus());
                dbDeliveryType.setLastModifiedBy(principal.getName());
                dbDeliveryType.setLastModifiedDate(new Date());
                this.deliveryTypeService.save(dbDeliveryType);
            } else {
                responseObject.setMessage("Delivery Type not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<DeliveryType> searchDeliveryType(String deliveryTypeCode, String description, String deliveryTypeMode) {
        List<DeliveryType> deliveryTypeList = new ArrayList<>();
        try {
            if (deliveryTypeCode.equals("") && description.equals("") && deliveryTypeMode.equals("None")) {//null,null,none
                deliveryTypeList = this.deliveryTypeService.findAll();
            } else if (deliveryTypeCode.equals("") && description.equals("") && !deliveryTypeMode.equals("None")) {//null,null,value
                deliveryTypeList = this.deliveryTypeService.findByDeliveryTypeModeContainingIgnoreCase(deliveryTypeMode);
            } else if (deliveryTypeCode.equals("") && !description.equals("") && deliveryTypeMode.equals("None")) {//null,value,none
                deliveryTypeList = this.deliveryTypeService.findByDescriptionContainingIgnoreCase(description);
            } else if (deliveryTypeCode.equals("") && !description.equals("") && !deliveryTypeMode.equals("None")) {//null,value,value
                deliveryTypeList = this.deliveryTypeService.findByDescriptionContainingIgnoreCaseAndDeliveryTypeModeContainingIgnoreCase(description, deliveryTypeMode);
            } else if (!deliveryTypeCode.equals("") && description.equals("") && deliveryTypeMode.equals("None")) {//value,null,none
                deliveryTypeList = this.deliveryTypeService.findByDeliveryTypeCodeContainingIgnoreCase(deliveryTypeCode);
            } else if (!deliveryTypeCode.equals("") && description.equals("") && !deliveryTypeMode.equals("None")) {//value,null,value
                deliveryTypeList = this.deliveryTypeService.findByDeliveryTypeCodeContainingIgnoreCaseAndDeliveryTypeModeContainingIgnoreCase(deliveryTypeCode, deliveryTypeMode);
            } else if (!deliveryTypeCode.equals("") && !description.equals("") && deliveryTypeMode.equals("None")) {//value,value,none
                deliveryTypeList = this.deliveryTypeService.findByDeliveryTypeCodeContainingIgnoreCaseAndDescriptionContainingIgnoreCase(deliveryTypeCode, description);
            } else {//value,value,value
                deliveryTypeList = this.deliveryTypeService.findByDeliveryTypeCodeContainingIgnoreCaseAndDescriptionContainingIgnoreCaseAndDeliveryTypeModeContainingIgnoreCase(deliveryTypeCode, description, deliveryTypeMode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveryTypeList;
    }
}
