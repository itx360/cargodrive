package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Port;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by Harshaa on 9/21/2016.
 */
public interface PortManagementControllerManager {
    ResponseObject createPort(Port port, Principal principal);

    ResponseObject updatePort(Port port, Principal principal);

    List<Port> searchPorts(String portName, Integer transportMode, Integer locationSeq);
}
