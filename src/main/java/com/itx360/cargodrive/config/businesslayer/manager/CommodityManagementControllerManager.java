package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Commodity;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 8:15 PM).
 */
public interface CommodityManagementControllerManager {
    ResponseObject createCommodity(Commodity commodity, String username);

    ResponseObject updateCommodity(Commodity commodity, String username);

    List<Commodity> searchCommodity(Integer commodityTypeSeq, Integer hsCodeSeq, String description);
}
