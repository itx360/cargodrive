package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.CommodityCategory;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/26/2016
 * Time: 11:57 AM
 * To change this template use File | Settings | File Templates.
 */
public interface CommodityCategoryManagementControllerManager {
    ResponseObject saveCommodityCategory(CommodityCategory commodityCategory, Principal principal);

    ResponseObject updateCommodityCategory(CommodityCategory commodityCategory, Principal principal);

    List<CommodityCategory> searchCommodityCategoryByCommodityCodeAndCommodityName(String commodityCode, String commodityName);
}
