package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.CommodityTypeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.CommodityType;
import com.itx360.cargodrive.config.datalayer.service.CommodityTypeService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 8:14 PM).
 */
@Service
public class CommodityTypeManagementControllerManagerImpl implements CommodityTypeManagementControllerManager {

    private final CommodityTypeService commodityTypeService;

    @Autowired
    public CommodityTypeManagementControllerManagerImpl(CommodityTypeService commodityTypeService) {
        this.commodityTypeService = commodityTypeService;
    }

    @Override
    public ResponseObject createCommodityType(CommodityType commodityType, String username) {
        ResponseObject responseObject = new ResponseObject("Commodity Type Saved Successfully", true);
        try {
            CommodityType dbCommodityType = this.commodityTypeService.findByCommodityTypeCode(commodityType.getCommodityTypeCode());
            if (dbCommodityType == null) {
                commodityType.setCreatedBy(username);
                commodityType.setCreatedDate(new Date());
                commodityType.setLastModifiedBy(username);
                commodityType.setLastModifiedDate(new Date());
                commodityType = this.commodityTypeService.save(commodityType);
                responseObject.setObject(commodityType);
            } else {
                responseObject.setMessage("Commodity Type already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateCommodityType(CommodityType commodityType, String username) {
        ResponseObject responseObject = new ResponseObject("Commodity Type Updated Successfully", true);
        try {
            CommodityType dbCommodityType = this.commodityTypeService.findOne(commodityType.getCommodityTypeSeq());
            if (dbCommodityType != null) {
                dbCommodityType.setCommodityTypeCode(commodityType.getCommodityTypeCode());
                dbCommodityType.setCommodityCategorySeq(commodityType.getCommodityCategorySeq());
                dbCommodityType.setDescription(commodityType.getDescription());
                dbCommodityType.setRemarks(commodityType.getRemarks());
                dbCommodityType.setLastModifiedBy(username);
                dbCommodityType.setLastModifiedDate(new Date());
                dbCommodityType.setStatus(commodityType.getStatus());
                dbCommodityType = this.commodityTypeService.save(dbCommodityType);
                responseObject.setObject(dbCommodityType);
            } else {
                responseObject.setMessage("Commodity Type Not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Commodity Type Already Exists");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<CommodityType> searchCommodityType(String commodityTypeCode, String description) {
        List<CommodityType> commodityTypeList = null;
        try {
            if (commodityTypeCode == null) {
                if (description == null) {
                    commodityTypeList = this.commodityTypeService.findAll();
                } else {
                    commodityTypeList = this.commodityTypeService.findByDescriptionContainingIgnoreCase(description);
                }
            } else {
                if (description == null) {
                    commodityTypeList = this.commodityTypeService.findByCommodityTypeCodeContainingIgnoreCase(commodityTypeCode);
                } else {
                    commodityTypeList = this.commodityTypeService.findByCommodityTypeCodeContainingIgnoreCaseAndDescriptionContainingIgnoreCase(commodityTypeCode, description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commodityTypeList;
    }
}
