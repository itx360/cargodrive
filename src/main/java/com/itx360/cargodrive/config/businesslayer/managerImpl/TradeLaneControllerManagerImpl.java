package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.TradeLaneControllerManager;
import com.itx360.cargodrive.config.datalayer.model.TradeLane;
import com.itx360.cargodrive.config.datalayer.service.TradeLaneService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 9/20/2016.
 */
@Service
public class TradeLaneControllerManagerImpl implements TradeLaneControllerManager {

    private final TradeLaneService tradeLaneService;

    @Autowired
    public TradeLaneControllerManagerImpl(TradeLaneService tradeLaneService) {
        this.tradeLaneService = tradeLaneService;
    }

    @Override
    public ResponseObject saveTradeLane(TradeLane tradeLane, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Trade Lane Saved Successfully", true);
        try {
            TradeLane dbTradeLane = this.tradeLaneService.findByTradeLaneNameIgnoreCase(tradeLane.getTradeLaneName());
            if(dbTradeLane == null){
                tradeLane.setCreatedBy(principal.getName());
                tradeLane.setCreatedDate(new Date());
                tradeLane.setModifiedBy(principal.getName());
                tradeLane.setModifiedDate(new Date());
                tradeLane = this.tradeLaneService.save(tradeLane);
                responseObject.setObject(tradeLane);
            }else {
                responseObject.setMessage("Trade Lane already exist!!");
                responseObject.setStatus(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Saving Trade Lane");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateTradeLane(TradeLane tradeLane, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Trade Lane Successfully Updated", true);
        try {
            TradeLane dbTradeLane = this.tradeLaneService.findOne(tradeLane.getTradeLaneSeq());

            if (dbTradeLane != null) {
                dbTradeLane.setTradeLaneName(tradeLane.getTradeLaneName());
                dbTradeLane.setTradeLaneCode(tradeLane.getTradeLaneCode());
                dbTradeLane.setDescription(tradeLane.getDescription());
                dbTradeLane.setStatus(tradeLane.getStatus());
                dbTradeLane.setModifiedBy(principal.getName());
                dbTradeLane.setModifiedDate(new Date());
                dbTradeLane= this.tradeLaneService.save(dbTradeLane);
                responseObject.setObject(dbTradeLane);
            }else {
                responseObject.setMessage("Trade Lane Not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Update Trade Lane");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<TradeLane> searchTradeLane(String tradeLaneCode, String tradeLaneName) {
        List<TradeLane> tradeLaneList = null;
        try {
            if (tradeLaneCode.equals("") && tradeLaneName.equals("")) {
                tradeLaneList = tradeLaneService.findAll();
            }else if(!tradeLaneCode.equals("") && tradeLaneName.equals("")){
                tradeLaneList = tradeLaneService.findByTradeLaneCodeContainingIgnoreCase(tradeLaneCode);
            }else if(tradeLaneCode.equals("") && !tradeLaneName.equals("")){
                tradeLaneList = tradeLaneService.findByTradeLaneNameContainingIgnoreCase(tradeLaneName);
            }else if(!tradeLaneCode.equals("") && !tradeLaneName.equals("")){
                tradeLaneList = tradeLaneService.findByTradeLaneCodeContainingIgnoreCaseAndTradeLaneNameContainingIgnoreCase(tradeLaneCode,tradeLaneName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tradeLaneList;

    }


}

