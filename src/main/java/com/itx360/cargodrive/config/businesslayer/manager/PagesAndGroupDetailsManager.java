package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.*;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;

/**
 * Created by Sachithrac on 9/1/2016.
 */
public interface PagesAndGroupDetailsManager {
    ResponseObject saveModule(Module module, Principal principal);

    ResponseObject saveSubModule(SubModule subModule, Principal principal);

    ResponseObject saveActionGroup(ActionGroup actionGroup, Principal principal);

    ResponseObject saveGroup(Group group, Principal principal);

    ResponseObject saveDocumentLink(DocumentLink documentLink, Integer create, Integer view, Integer update, Integer remove, Principal principal);

    ResponseObject updateModule(Module module, Principal principal);

    ResponseObject updateSubModule(SubModule subModule, Principal principal);

    ResponseObject updateActionGroup(ActionGroup actionGroup, Principal principal);

    ResponseObject updateGroup(Group group, Principal principal);

    ResponseObject saveCustomRole(Integer documentLinkSeq, String authority, Principal principal);
}
