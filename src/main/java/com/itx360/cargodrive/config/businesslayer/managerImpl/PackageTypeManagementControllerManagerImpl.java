package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.PackageTypeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.PackageType;
import com.itx360.cargodrive.config.datalayer.service.PackageTypeService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/4/2016
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class PackageTypeManagementControllerManagerImpl implements PackageTypeManagementControllerManager {

    private final PackageTypeService packageTypeService;

    @Autowired
    public PackageTypeManagementControllerManagerImpl(PackageTypeService packageTypeService) {
        this.packageTypeService = packageTypeService;
    }

    @Override
    public ResponseObject savePackageType(PackageType packageType, Principal principal) {
        ResponseObject responseObject;
        try {
            List<PackageType> dbPackageTypeList = this.packageTypeService.findByPackageTypeNameContainingIgnoreCase(packageType.getPackageTypeName());
            if (dbPackageTypeList.size() == 0) {
                packageType.setCreatedDate(new Date());
                packageType.setCreatedBy(principal.getName());
                packageType.setLastModifiedDate(new Date());
                packageType.setLastModifiedBy(principal.getName());
                packageType.setPackageTypeName(packageType.getPackageTypeName().toUpperCase());
                packageType.setPackageTypeCode(packageType.getPackageTypeCode().toUpperCase());
                packageType.setDescription(packageType.getDescription().toUpperCase());
                this.packageTypeService.save(packageType);
                responseObject = new ResponseObject("Package type saved successfully", true);
                responseObject.setObject(packageType);
            } else {
                responseObject = new ResponseObject("Package Type Already Exists !!", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject = new ResponseObject(e.getMessage(), false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updatePackageType(PackageType packageType, Principal principal) {
        ResponseObject responseObject;
        try {
            PackageType dbPackageType = this.packageTypeService.findOne(packageType.getPackageTypeSeq());
            if (dbPackageType != null) {
                dbPackageType.setLastModifiedBy(principal.getName());
                dbPackageType.setLastModifiedDate(new Date());
                dbPackageType.setPackageTypeCode(packageType.getPackageTypeCode());
                dbPackageType.setPackageTypeName(packageType.getPackageTypeName());
                dbPackageType.setAsycudaCode(packageType.getAsycudaCode());
                dbPackageType.setDescription(packageType.getDescription());
                dbPackageType.setStatus(packageType.getStatus());
                this.packageTypeService.save(dbPackageType);
                responseObject = new ResponseObject("Package Type Updated Successfully", true);
                responseObject.setObject(dbPackageType);
            } else {
                responseObject = new ResponseObject("Package Type Already Exists !!", false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject = new ResponseObject("Package Type Already Exists !!", false);
            } else {
                e.printStackTrace();
                responseObject = new ResponseObject(ExceptionUtils.getRootCauseMessage(e), false);
            }
        }
        return responseObject;
    }

    @Override
    public List<PackageType> searchPackageType(String packageTypeName, String packageTypeCode, String description) {
        List<PackageType> packageTypeList;
        if (packageTypeName.equals("") && packageTypeCode.equals("") && description.equals("")) {
            packageTypeList = this.packageTypeService.findAll();
        } else {
            packageTypeList = this.packageTypeService.findByPackageTypeNameContainingIgnoreCaseAndPackageTypeCodeContainingIgnoreCaseAndDescriptionContainingIgnoreCase(packageTypeName,
                    packageTypeCode,
                    description);
        }
        return packageTypeList;
    }
}
