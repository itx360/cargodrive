package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.ContainerType;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/5/2016
 * Time: 2:36 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ContainerTypeManagementControllerManager {
    ResponseObject saveContainerType(ContainerType containerType, Principal principal);

    ResponseObject updateContainerType(ContainerType containerType, Principal principal);

    List<ContainerType> searchContainerType(Integer containerSizeSeq, Integer containerCategorySeq, String description);
}
