package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.TaxRegistration;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.util.List;

/**
 * Created by Thilangaj on 10/4/2016 4:53 PM).
 */
public interface TaxRegistrationManagementControllerManager {

    ResponseObject createTaxRegistration(TaxRegistration taxRegistration, String username);

    ResponseObject updateTaxRegistration(TaxRegistration taxRegistration, String username);

    List<TaxRegistration> searchTaxRegistration(String taxName, String remarks, Integer countrySeq, Integer customerProfileSeq);
}
