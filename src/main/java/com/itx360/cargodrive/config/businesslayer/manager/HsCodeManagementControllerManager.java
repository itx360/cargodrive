package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.HsCode;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 9:59 PM).
 */
public interface HsCodeManagementControllerManager {
    ResponseObject createHsCode(HsCode hsCode, String username);

    ResponseObject updateHsCode(HsCode hsCode, String username);

    List<HsCode> searchHsCode(String hsCodeName, String description, Integer hsCategorySeq);
}
