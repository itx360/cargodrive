package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.FinalDestination;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 10/5/2016.
 */
public interface FinalDestinationManagementControllerManager {

    ResponseObject saveDestination(AddressBook addressBook, FinalDestination finalDestination, Principal principal);

    ResponseObject updateDestination(AddressBook addressBook,FinalDestination finalDestination, Principal principal);

    List<FinalDestination> searchDestination(String finalDestinationCode, Integer countrySeq, String city,String state,String zip);
}
