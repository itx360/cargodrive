package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Bank;
import com.itx360.cargodrive.config.datalayer.model.Charge;
import com.itx360.cargodrive.config.datalayer.model.CompanyModule;
import com.itx360.cargodrive.master.utility.ResponseObject;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 9/27/2016.
 */
public interface ChargeManagementControllerManager {
    ResponseObject createCharge(Charge charge, List<Integer> modeSeqList, Principal principal);

    ResponseObject updateCharge(Charge charge, List<Integer> modeSeqList, Principal principal);

    List<Charge> searchCharge(String chargeName, String description, Integer customerProfileSeq);

    List<CompanyModule> getFinanceEnabledCompanyModuleList(HttpServletRequest request);
}
