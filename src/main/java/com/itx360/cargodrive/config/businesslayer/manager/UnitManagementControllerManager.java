package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Unit;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 10/4/2016.
 */
public interface UnitManagementControllerManager {

    ResponseObject createUnit(Unit unit, Principal principal);

    ResponseObject updateDeliveryType(Unit unit, Principal principal);

    List<Unit> searchUnit(String unitName, String unitCode, String usedFor);
}
