package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Country;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 9/20/2016.
 */

public interface CountryManagementControllerManager {

    ResponseObject createCountry(Country country, Principal principal);

    ResponseObject updateCountry(Country country, Principal principal);

    List<Country> searchCountry(String countryName, String countryCode);
}
