package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Module;
import com.itx360.cargodrive.config.datalayer.model.User;
import com.itx360.cargodrive.config.utility.ResponseMessage;

import java.security.Principal;

/**
 * Created by shanakajay on 8/22/2016.
 */
public interface UserDetailsManager {
    public User getUserByUserSeq(int id);


}
