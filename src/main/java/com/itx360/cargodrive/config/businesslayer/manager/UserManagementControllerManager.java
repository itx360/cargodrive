package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Module;
import com.itx360.cargodrive.config.datalayer.model.User;
import com.itx360.cargodrive.config.datalayer.model.UserModule;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.List;

/**
 * Created by Thilanga-Ehl on 9/5/2016 7:51 PM).
 */
public interface UserManagementControllerManager {

    ResponseObject createUser(User user, MultipartFile multipartFile, String username);

    ResponseObject updateUser(User user, MultipartFile multipartFile, String username);

    ResponseObject assignUserModuleList(Integer userSeq, Integer companyProfileSeq,List<Integer> moduleSeqList, String username);

    ResponseObject removeUserModuleList(Integer userSeq, Integer companyProfileSeq, List<Integer> moduleSeqList, String username);
}
