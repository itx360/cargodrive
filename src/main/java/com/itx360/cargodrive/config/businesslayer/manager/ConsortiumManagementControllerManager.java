package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.Consortium;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by Nuwanr on 9/29/2016.
 */
public interface ConsortiumManagementControllerManager {
    ResponseObject saveConsortium(Consortium consortium, Principal principal);

    ResponseObject updateConsortium(Consortium consortium, Principal principal);

    List<Consortium> searchConsortiumByConsortiumName(String consortiumName);
}
