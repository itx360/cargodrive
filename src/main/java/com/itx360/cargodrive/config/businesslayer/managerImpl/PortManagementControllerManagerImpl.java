package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.PortManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Port;
import com.itx360.cargodrive.config.datalayer.service.PortsService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Harshaa on 9/21/2016.
 */
@Service
public class PortManagementControllerManagerImpl implements PortManagementControllerManager {

    private PortsService portsService;

    @Autowired
    public PortManagementControllerManagerImpl(PortsService portsService) {
        this.portsService = portsService;
    }

    @Override
    public ResponseObject createPort(Port port, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Port Creation Failed", false);
        try {
                /*
                    Default status set to Open, any changes need to be made through the update
                 */
            port.setStatus(1);
            port.setCreatedBy(principal.getName());
            port.setCreatedDate(new Date());
            port.setModifiedBy(principal.getName());
            port.setModifiedDate(new Date());

            port = this.portsService.save(port);
            responseObject.setObject(port);

            responseObject.setMessage("Port Creation Successful");
            responseObject.setStatus(true);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseObject;
    }

    @Override
    public ResponseObject updatePort(Port port, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Port update failed", false);
        try {
            Port dbPort = this.portsService.findByPortSeq(port.getPortSeq());
            if (dbPort != null || !dbPort.equals(port)) {
                dbPort.setStatus(port.getStatus());
                dbPort.setPortName(port.getPortName());
                dbPort.setPortCode(port.getPortCode());
                dbPort.setLocation(port.getLocation());
                dbPort.setTradeLane(port.getTradeLane());
                dbPort.setTransportMode(port.getTransportMode());
                dbPort.setModifiedBy(principal.getName());
                dbPort.setModifiedDate(new Date());

                this.portsService.save(dbPort);
                responseObject.setMessage("Port update successful !");
                responseObject.setStatus(true);
            } else {
                responseObject.setMessage("Port not found in database !");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            responseObject.setObject(ex);
            responseObject.setMessage(ex.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }


    @Override
    public List<Port> searchPorts(String portName, Integer transportMode, Integer locationSeq) {
        List<Port> portsList = null;
        if (portName.equals("*") && transportMode.equals(-1) && locationSeq.equals(-1)) {
            portsList = this.portsService.findAll();
        } else if (portName.equals("*") && transportMode.equals(-1) && !locationSeq.equals(-1)) {
            portsList = this.portsService.findByLocationSeq(locationSeq);
        } else if (portName.equals("*") && !transportMode.equals(-1) && locationSeq.equals(-1)) {
            portsList = this.portsService.findByTransportMode(transportMode);
        } else if (portName.equals("*") && !transportMode.equals(-1) && !locationSeq.equals(-1)) {
            portsList = this.portsService.findByLocationSeqAndTransportMode(transportMode, locationSeq);
        } else if (!portName.equals("*") && transportMode.equals(-1) && locationSeq.equals(-1)) {
            portsList = this.portsService.findByPortNameIsContainingIgnoreCase(portName);
        } else if (!portName.equals("*") && transportMode.equals(-1) && !locationSeq.equals(-1)) {
            portsList = this.portsService.findByPortNameIsContainingIgnoreCaseAndLocationSeq(portName, locationSeq);
        } else if (!portName.equals("*") && !transportMode.equals(-1) && locationSeq.equals(-1)) {
            portsList = this.portsService.findByPortNameIsContainingIgnoreCaseAndTransportMode(portName, transportMode);
        } else if (!portName.equals("*") && !transportMode.equals(-1) && !locationSeq.equals(-1)) {
            portsList = this.portsService.findByPortNameIsContainingIgnoreCaseAndLocationSeqAndTransportMode(portName, locationSeq, transportMode);
        }

        return portsList;
    }
}
