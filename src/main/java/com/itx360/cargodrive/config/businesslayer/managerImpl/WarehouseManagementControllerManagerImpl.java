package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.WarehouseManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Warehouse;
import com.itx360.cargodrive.config.datalayer.service.WarehouseService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 9/29/2016.
 */
@Service
public class WarehouseManagementControllerManagerImpl implements WarehouseManagementControllerManager {

    private final WarehouseService warehouseService;

    @Autowired
    public WarehouseManagementControllerManagerImpl(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @Override
    public ResponseObject saveWarehouse(AddressBook addressBook, Warehouse warehouse, Principal principal, HttpServletRequest request) {
        ResponseObject responseObject = new ResponseObject("Warehouse Saved Successfully", true);
        try {
            Warehouse dbWarehouse = this.warehouseService.findByWarehouseNameIgnoreCase(warehouse.getWarehouseName());
            Integer companyProfileSeq = Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString());

            if (dbWarehouse == null) {
                warehouse.setCreatedBy(principal.getName());
                warehouse.setCreatedDate(new Date());
                warehouse.setLastModifiedBy(principal.getName());
                warehouse.setLastModifiedDate(new Date());
                warehouse.setStatus(1);
                warehouse.setCompanyProfileSeq(companyProfileSeq);

                AddressBook dbAddressBook = addressBook;
                dbAddressBook.setCreatedBy(principal.getName());
                dbAddressBook.setCreatedDate(new Date());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                warehouse.setAddressBook(dbAddressBook);

                warehouse = this.warehouseService.save(warehouse);
                responseObject.setObject(warehouse);
            } else {
                responseObject.setMessage("Warehouse Already Exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Warehouse Already Exist!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateWarehouse(AddressBook addressBook, Warehouse warehouse, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Warehouse Successfully Updated", true);
        try {
            Warehouse warehouseDB = this.warehouseService.findOne(warehouse.getWarehouseSeq());
            if (warehouseDB != null) {
                warehouseDB.setWarehouseCode(warehouse.getWarehouseCode());
                warehouseDB.setWarehouseName(warehouse.getWarehouseName());
                warehouseDB.setDescription(warehouse.getDescription());
                warehouseDB.setStatus(warehouse.getStatus());
                warehouseDB.setCountrySeq(warehouse.getCountrySeq());
                warehouseDB.setLastModifiedBy(principal.getName());
                warehouseDB.setLastModifiedDate(new Date());

                AddressBook dbAddressBook = warehouseDB.getAddressBook();
                dbAddressBook.setAddress1(addressBook.getAddress1());
                dbAddressBook.setAddress2(addressBook.getAddress2());
                dbAddressBook.setCity(addressBook.getCity());
                dbAddressBook.setZip(addressBook.getZip());
                dbAddressBook.setTelephone(addressBook.getTelephone());
                dbAddressBook.setTelephoneExtension(addressBook.getTelephoneExtension());
                dbAddressBook.setMobile(addressBook.getMobile());
                dbAddressBook.setFax(addressBook.getFax());
                dbAddressBook.setEmail(addressBook.getEmail());
                dbAddressBook.setWebsite(addressBook.getWebsite());
                dbAddressBook.setState(addressBook.getState());
                dbAddressBook.setCountrySeq(addressBook.getCountrySeq());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                warehouse.setAddressBook(dbAddressBook);

                warehouseDB = this.warehouseService.save(warehouseDB);
                responseObject.setObject(warehouseDB);
            } else {
                responseObject.setMessage("Warehouse Not Found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Warehouse Already Exist!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<Warehouse> searchWarehouse(String warehouseCode, String warehouseName, Integer countrySeq) {
        List<Warehouse> warehouseList = null;

        try {
            if (warehouseCode.equals("") && warehouseName.equals("") && countrySeq.equals(-1)) {
                warehouseList = warehouseService.findAll();
            } else if (!warehouseCode.equals("") && warehouseName.equals("") && countrySeq.equals(-1)) {
                warehouseList = warehouseService.findByWarehouseCodeContainingIgnoreCase(warehouseCode);
            } else if (warehouseCode.equals("") && !warehouseName.equals("") && countrySeq.equals(-1)) {
                warehouseList = warehouseService.findByWarehouseNameContainingIgnoreCase(warehouseName);
            } else if (warehouseCode.equals("") && warehouseName.equals("") && !countrySeq.equals(-1)) {
                warehouseList = warehouseService.findByCountrySeq(countrySeq);
            } else if (!warehouseCode.equals("") && warehouseName.equals("") && !countrySeq.equals(-1)) {
                warehouseList = warehouseService.findByWarehouseCodeContainingIgnoreCaseAndCountrySeq(warehouseCode, countrySeq);
            } else if (warehouseCode.equals("") && !warehouseName.equals("") && !countrySeq.equals(-1)) {
                warehouseList = warehouseService.findByWarehouseNameContainingIgnoreCaseAndCountrySeq(warehouseName, countrySeq);
            } else if (!warehouseCode.equals("") && !warehouseName.equals("") && countrySeq.equals(-1)) {
                warehouseList = warehouseService.findByWarehouseCodeContainingIgnoreCaseAndWarehouseNameContainingIgnoreCase(warehouseCode, warehouseName);
            } else if (!warehouseCode.equals("") && !warehouseName.equals("") && !countrySeq.equals(-1)) {
                warehouseList = warehouseService.findByWarehouseCodeContainingIgnoreCaseAndWarehouseNameContainingIgnoreCaseAndCountrySeq(warehouseCode, warehouseName, countrySeq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return warehouseList;
    }
}
