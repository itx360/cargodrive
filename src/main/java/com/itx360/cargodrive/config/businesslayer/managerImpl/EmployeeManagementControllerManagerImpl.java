package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.EmployeeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Employee;
import com.itx360.cargodrive.config.datalayer.service.EmployeeService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 10/4/2016.
 */
@Service
public class EmployeeManagementControllerManagerImpl implements EmployeeManagementControllerManager {

    private final EmployeeService employeeService;


    @Autowired
    public EmployeeManagementControllerManagerImpl(EmployeeService employeeService) {
        this.employeeService = employeeService;

    }


    @Override
    public ResponseObject saveEmployee(AddressBook addressBook, Employee employee, Principal principal, HttpServletRequest request) {
        ResponseObject responseObject = new ResponseObject("Employee Saved Successfully", true);
        try {
            Integer companyProfileSeq = Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString());

            employee.setCreatedBy(principal.getName());
            employee.setCreatedDate(new Date());
            employee.setLastModifiedBy(principal.getName());
            employee.setLastModifiedDate(new Date());
            employee.setCompanyProfileSeq(companyProfileSeq);

            AddressBook dbAddressBook = addressBook;
            dbAddressBook.setCreatedBy(principal.getName());
            dbAddressBook.setCreatedDate(new Date());
            dbAddressBook.setLastModifiedBy(principal.getName());
            dbAddressBook.setLastModifiedDate(new Date());
            employee.setAddressBook(dbAddressBook);

            employee = this.employeeService.save(employee);
            responseObject.setObject(employee);
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Saving Branch");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateEmployee(AddressBook addressBook, Employee employee, Principal principal) {
        ResponseObject responseObject = new ResponseObject("Employee Successfully Updated", true);
        try {
            Employee employeeDB = this.employeeService.findOne(employee.getEmployeeSeq());
            if (employeeDB != null) {
                employeeDB.setEmployeeName(employee.getEmployeeName());
                employeeDB.setEmployeeDesignationSeq(employeeDB.getEmployeeDesignationSeq());
                employeeDB.setStatus(employee.getStatus());
                employeeDB.setLastModifiedBy(principal.getName());
                employeeDB.setLastModifiedDate(new Date());

                AddressBook dbAddressBook = employeeDB.getAddressBook();
                dbAddressBook.setAddress1(addressBook.getAddress1());
                dbAddressBook.setAddress2(addressBook.getAddress2());
                dbAddressBook.setCity(addressBook.getCity());
                dbAddressBook.setZip(addressBook.getZip());
                dbAddressBook.setTelephone(addressBook.getTelephone());
                dbAddressBook.setTelephoneExtension(addressBook.getTelephoneExtension());
                dbAddressBook.setMobile(addressBook.getMobile());
                dbAddressBook.setFax(addressBook.getFax());
                dbAddressBook.setEmail(addressBook.getEmail());
                dbAddressBook.setWebsite(addressBook.getWebsite());
                dbAddressBook.setState(addressBook.getState());
                dbAddressBook.setCountrySeq(addressBook.getCountrySeq());
                dbAddressBook.setNicNo(addressBook.getNicNo());
                dbAddressBook.setChaLicenseNo(addressBook.getChaLicenseNo());
                dbAddressBook.setLastModifiedBy(principal.getName());
                dbAddressBook.setLastModifiedDate(new Date());
                employeeDB.setAddressBook(dbAddressBook);

                employeeDB = this.employeeService.save(employeeDB);
                responseObject.setObject(employeeDB);
            } else {
                responseObject.setMessage("Employee Not Found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Saving Employee");
            responseObject.setStatus(false);
        }

        return responseObject;

    }

    @Override
    public List<Employee> searchEmployee(String employeeName, Integer status, Integer employeeDesignationSeq) {
        List<Employee> employeeList = null;

        try {
            if (employeeName.equals("") && status.equals(-1) && employeeDesignationSeq.equals(-1)) {
                employeeList = this.employeeService.findAll();
            } else if (!employeeName.equals("") && status.equals(-1) && employeeDesignationSeq.equals(-1)) {
                employeeList = this.employeeService.findByEmployeeNameContainingIgnoreCase(employeeName);
            } else if (employeeName.equals("") && !status.equals(-1) && employeeDesignationSeq.equals(-1)) {
                employeeList = this.employeeService.findByStatus(status);
            } else if (employeeName.equals("") && status.equals(-1) && !employeeDesignationSeq.equals(-1)) {
                employeeList = this.employeeService.findByEmployeeDesignationSeq(employeeDesignationSeq);
            } else if (!employeeName.equals("") && !status.equals(-1) && employeeDesignationSeq.equals(-1)) {
                employeeList = this.employeeService.findByEmployeeNameContainingIgnoreCaseAndStatus(employeeName, status);
            } else if (!employeeName.equals("") && status.equals(-1) && !employeeDesignationSeq.equals(-1)) {
                employeeList = this.employeeService.findByEmployeeNameContainingIgnoreCaseAndEmployeeDesignationSeq(employeeName, employeeDesignationSeq);
            } else if (employeeName.equals("") && !status.equals(-1) && !employeeDesignationSeq.equals(-1)) {
                employeeList = this.employeeService.findByStatusAndEmployeeDesignationSeq(status, employeeDesignationSeq);
            } else if (!employeeName.equals("") && !status.equals(-1) && !employeeDesignationSeq.equals(-1)) {
                employeeList = this.employeeService.findByEmployeeNameContainingIgnoreCaseAndStatusAndEmployeeDesignationSeq(employeeName, status, employeeDesignationSeq);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employeeList;
    }

}
