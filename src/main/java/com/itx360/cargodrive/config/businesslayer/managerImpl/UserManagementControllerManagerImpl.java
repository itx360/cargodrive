package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.UserManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.CompanyModule;
import com.itx360.cargodrive.config.datalayer.model.User;
import com.itx360.cargodrive.config.datalayer.model.UserModule;
import com.itx360.cargodrive.config.datalayer.service.CompanyModuleService;
import com.itx360.cargodrive.config.datalayer.service.UserModuleService;
import com.itx360.cargodrive.config.datalayer.service.UserService;
import com.itx360.cargodrive.master.datalayer.model.UploadedDocument;
import com.itx360.cargodrive.master.datalayer.service.UploadedDocumentService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import com.itx360.cargodrive.util.DateFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.security.MessageDigest;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilanga-Ehl on 9/5/2016 7:52 PM).
 */
@Service
public class UserManagementControllerManagerImpl implements UserManagementControllerManager {

    private final UserService userService;
    private final CompanyModuleService companyModuleService;
    private final UploadedDocumentService uploadedDocumentService;
    private final UserModuleService userModuleService;

    @Autowired
    public UserManagementControllerManagerImpl(UserModuleService userModuleService, CompanyModuleService companyModuleService,
                                               UploadedDocumentService uploadedDocumentService, UserService userService) {
        this.userService = userService;
        this.companyModuleService = companyModuleService;
        this.uploadedDocumentService = uploadedDocumentService;
        this.userModuleService = userModuleService;
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte aData : data) {
            int halfbyte = (aData >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = aData & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    @Override
    @Transactional
    public ResponseObject createUser(User user, MultipartFile multipartFile, String username) {
        ResponseObject responseObject = new ResponseObject("User Successfully Saved", true);
        try {
            if (this.userService.findByUsername(user.getUsername()) == null) {
                UploadedDocument file = this.uploadedDocumentService.save(multipartFile, username);
                user.setUploadDocumentSeq(file.getUploadedDocumentSeq());
                //Date was coming with time from the front end
                user.setDateOfBirth(DateFormatter.getStartOfTheDay(user.getDateOfBirth()));
                //Password Encryption
                String encryptedPassword = this.SHA1(user.getUsername());
                user.setPassword(encryptedPassword);
                //Password Encryption
                user.setEnabled(1);
                user.setCreatedBy(username);
                user.setCreatedDate(new Date());
                user = this.userService.save(user);
                responseObject.setObject(user);
            } else {
                responseObject.setMessage("Username already exist!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage(e.getMessage());
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    @Transactional
    public ResponseObject updateUser(User user, MultipartFile multipartFile, String username) {
        ResponseObject responseObject = new ResponseObject("User Successfully Updated", true);
        try {
            User dbUser = this.userService.findOne(user.getUserSeq());
            if (dbUser != null) {
                dbUser.setFirstName(user.getFirstName());
                dbUser.setSecondName(user.getSecondName());
                dbUser.setEnabled(user.getEnabled() == null ? 0 : 1);
                dbUser.setDateOfBirth(DateFormatter.getStartOfTheDay(user.getDateOfBirth()));
                dbUser.setContactNo(user.getContactNo());
                dbUser.setEmail(user.getEmail());
                dbUser.setModifiedBy(username);
                dbUser.setModifiedDate(new Date());
                UploadedDocument file = this.uploadedDocumentService.save(multipartFile, username);
                if (file.getUploadedDocumentSeq() != null) {
                    dbUser.setUploadDocumentSeq(file.getUploadedDocumentSeq());
                }
                this.userService.save(dbUser);
            } else {
                responseObject.setMessage("User not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("Error Updating user");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject assignUserModuleList(Integer userSeq, Integer companyProfileSeq, List<Integer> moduleSeqList, String username) {
        ResponseObject responseObject = new ResponseObject("User Module Successfully Saved", true);
        try {
            if (moduleSeqList.size() > 0) {
                for (Integer moduleSeq : moduleSeqList) {
                    UserModule userModule = new UserModule();
                    userModule.setUserSeq(userSeq);
                    CompanyModule companyModule = this.companyModuleService.findByCompanyProfileSeqAndModuleSeq(companyProfileSeq, moduleSeq);
                    userModule.setCompanyModuleSeq(companyModule.getCompanyModuleSeq());
                    userModule.setCreatedBy(username);
                    userModule.setCreatedDate(new Date());
                    userModule.setStatus(1);
                    this.userModuleService.save(userModule);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("User Module Save failed");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject removeUserModuleList(Integer userSeq, Integer companyProfileSeq, List<Integer> moduleSeqList, String username) {
        ResponseObject responseObject = new ResponseObject("User Module Successfully Removed", true);
        try {
            for (Integer moduleSeq : moduleSeqList) {
                CompanyModule companyModule = this.companyModuleService.findByCompanyProfileSeqAndModuleSeq(companyProfileSeq, moduleSeq);
                UserModule userModule = this.userModuleService.findByUserSeqAndCompanyModuleSeq(userSeq, companyModule.getCompanyModuleSeq());
                userModule.setStatus(0);
                userModule.setModifiedDate(new Date());
                userModule.setModifiedBy(username);
                this.userModuleService.save(userModule);
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseObject.setMessage("User Module Save failed");
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    private String SHA256(String username) {
        try {
            MessageDigest md;
            md = MessageDigest.getInstance("SHA-256");
            byte[] sha1hash;
            md.update(username.getBytes("iso-8859-1"), 0, username.length());
            sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (Exception e) {
            return "";
        }
    }

    private String SHA1(String username) {
        try {
            MessageDigest md;
            md = MessageDigest.getInstance("SHA-1");
            byte[] sha1hash;
            md.update(username.getBytes("iso-8859-1"), 0, username.length());
            sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (Exception e) {
            return "";
        }
    }
}
