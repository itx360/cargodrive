package com.itx360.cargodrive.config.businesslayer.managerImpl;

import com.itx360.cargodrive.config.businesslayer.manager.CommodityManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Commodity;
import com.itx360.cargodrive.config.datalayer.service.CommodityService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 8:15 PM).
 */
@Service
public class CommodityManagementControllerManagerImpl implements CommodityManagementControllerManager {

    private final CommodityService commodityService;

    @Autowired
    public CommodityManagementControllerManagerImpl(CommodityService commodityService) {
        this.commodityService = commodityService;
    }

    @Override
    public ResponseObject createCommodity(Commodity commodity, String username) {
        ResponseObject responseObject = new ResponseObject("Commodity Saved Successfully", true);
        try {
            commodity.setCreatedBy(username);
            commodity.setCreatedDate(new Date());
            commodity.setLastModifiedBy(username);
            commodity.setLastModifiedDate(new Date());
            commodity = this.commodityService.save(commodity);
            responseObject.setObject(commodity);
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Commodity Already Exists for HS Code and Commodity Type!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public ResponseObject updateCommodity(Commodity commodity, String username) {
        ResponseObject responseObject = new ResponseObject("Commodity Updated Successfully", true);
        try {
            Commodity dbCommodity = this.commodityService.findOne(commodity.getCommoditySeq());
            if (dbCommodity != null) {
                dbCommodity.setLastModifiedBy(username);
                dbCommodity.setLastModifiedDate(new Date());
                dbCommodity.setDescription(commodity.getDescription());
                dbCommodity.setCommodityTypeSeq(commodity.getCommodityTypeSeq());
                dbCommodity.setHsCodeSeq(commodity.getHsCodeSeq());
                dbCommodity.setStatus(commodity.getStatus());
                dbCommodity = this.commodityService.save(dbCommodity);
                responseObject.setObject(dbCommodity);
            } else {
                responseObject.setMessage("Commodity Not found!!");
                responseObject.setStatus(false);
            }
        } catch (Exception e) {
            Throwable throwable = ExceptionUtils.getRootCause(e);
            if (throwable instanceof SQLIntegrityConstraintViolationException) {
                responseObject.setMessage("Commodity Already Exists for HS Code and Commodity Type!!");
            } else {
                e.printStackTrace();
                responseObject.setMessage(ExceptionUtils.getRootCauseMessage(e));
            }
            responseObject.setStatus(false);
        }
        return responseObject;
    }

    @Override
    public List<Commodity> searchCommodity(Integer commodityTypeSeq, Integer hsCodeSeq, String description) {
        List<Commodity> commodityList = null;
        try {
            if (description.equals("")) {
                if (commodityTypeSeq.equals(-1)) {
                    if (hsCodeSeq.equals(-1)) {
                        commodityList = this.commodityService.findAll();
                    } else {
                        commodityList = this.commodityService.findByHsCodeSeq(hsCodeSeq);
                    }
                } else {
                    if (hsCodeSeq.equals(-1)) {
                        commodityList = this.commodityService.findByCommodityTypeSeq(commodityTypeSeq);
                    } else {
                        commodityList = this.commodityService.findByHsCodeSeqAndCommodityTypeSeq(hsCodeSeq, commodityTypeSeq);
                    }
                }
            } else {
                if (commodityTypeSeq.equals(-1)) {
                    if (hsCodeSeq.equals(-1)) {
                        commodityList = this.commodityService.findByDescriptionContainingIgnoreCase(description);
                    } else {
                        commodityList = this.commodityService.findByHsCodeSeqAndDescriptionContainingIgnoreCase(hsCodeSeq, description);
                    }
                } else {
                    if (hsCodeSeq.equals(-1)) {
                        commodityList = this.commodityService.findByCommodityTypeSeqAndDescriptionContainingIgnoreCase(commodityTypeSeq, description);
                    } else {
                        commodityList = this.commodityService.findByHsCodeSeqAndCommodityTypeSeqAndDescriptionContainingIgnoreCase(hsCodeSeq, commodityTypeSeq, description);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commodityList;
    }
}
