package com.itx360.cargodrive.config.businesslayer.manager;

import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.CustomHouseAgent;
import com.itx360.cargodrive.master.utility.ResponseObject;

import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 10/6/2016.
 */
public interface CustomHouseAgentControllerManager {

    ResponseObject saveCustomHouseAgent(AddressBook addressBook, CustomHouseAgent customHouseAgent, Principal principal);

    ResponseObject updateCustomHouseAgent(AddressBook addressBook,CustomHouseAgent customHouseAgent,Principal principal);

    List<CustomHouseAgent> searchCustomHouseAgent(String customHouseAgentName, Integer taxTypeSeq, String taxRegistrationNo,String description);
}
