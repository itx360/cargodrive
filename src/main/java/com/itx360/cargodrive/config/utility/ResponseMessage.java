package com.itx360.cargodrive.config.utility;

/**
 * Created by Sachithrac on 8/31/2016.
 */
public class ResponseMessage {

    private String messageType;
    private String message;

    public ResponseMessage(String messageType, String message) {
        this.messageType = messageType;
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
