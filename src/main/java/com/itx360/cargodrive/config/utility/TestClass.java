package com.itx360.cargodrive.config.utility;

/**
 * Created by Thilangaj on 9/20/2016 1:11 PM).
 */
public class TestClass {

    public static void main(String[] args) {

        System.out.println(MasterDataStatus.APPROVED.getStatusSeq());
        System.out.println(MasterDataStatus.values());
        MasterDataStatus[] masterDataStatuses = MasterDataStatus.values();
        for (MasterDataStatus masterDataStatuse : masterDataStatuses) {
            System.out.println(masterDataStatuse.getStatus());
            System.out.println(masterDataStatuse.getStatusSeq());
        }
    }
}
