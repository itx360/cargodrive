package com.itx360.cargodrive.config.utility;

import java.util.regex.Pattern;

/**
 * Created by Sachithrac on 9/8/2016.
 */
public class CheckSpecialCharacters {

    public boolean isSpecialCharacterInclude(String string){
        Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
        return pattern.matcher(string).find();
    }
}
