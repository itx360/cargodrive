package com.itx360.cargodrive.config.utility;

/**
 * Created by shanakajay on 10/4/2016.
 */
public enum UnitCategory {
    VOLUME(1, "Volume"),
    WEIGHT(2, "Weight"),
    PIECES(3, "Pieces");

    private Integer unitCategoryCode;
    private String unitCategory;

    UnitCategory(Integer unitCategoryCode, String unitCategory) {
        this.unitCategory = unitCategory;
        this.unitCategoryCode = unitCategoryCode;
    }

    public Integer getUnitCategoryCode() {
        return unitCategoryCode;
    }

    public String getUnitCategory() {
        return unitCategory;
    }
}
