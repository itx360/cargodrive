package com.itx360.cargodrive.config.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Thilangaj on 9/20/2016 1:06 PM).
 */
public enum MasterDataStatus {

    DELETED(0, "Deleted"),
    OPEN(1, "Open"),
    APPROVED(2, "Approved");

    private final Integer statusSeq;
    private final String status;

    MasterDataStatus(Integer statusSeq, String status) {
        this.statusSeq = statusSeq;
        this.status = status;
    }

    public static List<MasterDataStatus> getStatusListForCreate() {
        MasterDataStatus[] masterDataStatuses = MasterDataStatus.values();
        List<MasterDataStatus> masterDataStatusList = new ArrayList<>();
        for (MasterDataStatus masterDataStatus : masterDataStatuses) {
            if (!masterDataStatus.getStatusSeq().equals(0)) {
                masterDataStatusList.add(masterDataStatus);
            }
        }
        return masterDataStatusList;
    }

    public Integer getStatusSeq() {
        return statusSeq;
    }

    public String getStatus() {
        return status;
    }
}
