package com.itx360.cargodrive.config.utility;

/**
 * Created by Harshaa on 9/20/2016.
 */
public enum Modes {
    AIR(0, "Air"),
    OCEAN(1, "Ocean");

    private Integer modeCode;
    private String mode;

    Modes(Integer modeCode, String mode) {
        this.mode = mode;
        this.modeCode = modeCode;
    }

    public Integer getModeCode() {
        return modeCode;
    }

    public String getMode() {
        return mode;
    }
}
