package com.itx360.cargodrive.config.utility;

/**
 * Created by shanakajay on 10/6/2016.
 */
public enum DeliveryTypeMode {
    AIR(1, "Air"),
    OCEAN(2, "Ocean");

    private Integer deliveryTypeModeCode;
    private String deliveryTypeMode;

    DeliveryTypeMode(Integer deliveryTypeModeCode, String deliveryTypeMode) {
        this.deliveryTypeMode = deliveryTypeMode;
        this.deliveryTypeModeCode = deliveryTypeModeCode;
    }

    public Integer getDeliveryTypeModeCode() {
        return deliveryTypeModeCode;
    }

    public String getDeliveryTypeMode() {
        return deliveryTypeMode;
    }
}
