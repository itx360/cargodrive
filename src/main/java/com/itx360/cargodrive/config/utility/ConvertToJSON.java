package com.itx360.cargodrive.config.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by IntelliJ IDEA.
 * User: Thilanga-Ehl
 * Date: 9/12/12
 * Time: 9:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConvertToJSON {


    public ResponseEntity<String> createJsonResponse(Object o) {
        HttpHeaders headers = new HttpHeaders();
        Gson gson = new Gson();
        headers.set("Content-Type", "application/json");
        String json = gson.toJson(o);
        return new ResponseEntity<String>(json, headers, HttpStatus.CREATED);
    }

    public String convertToBasicJson(Object o) {
        Gson gson =  new GsonBuilder().serializeNulls().create();
        return gson.toJson(o);
    }
}
