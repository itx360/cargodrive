package com.itx360.cargodrive.config.utility;

/**
 * Created by Sachithrac on 10/6/2016.
 */
public enum EmployeeDataStatus {

    ENABLED(1, "ENABLED"),
    DISABLED(0, "DISABLED");

    private final Integer statusSeq;
    private final String status;


    EmployeeDataStatus(Integer statusSeq, String status) {
        this.statusSeq = statusSeq;
        this.status = status;
    }

    public Integer getStatusSeq() {
        return statusSeq;
    }

    public String getStatus() {
        return status;
    }
}
