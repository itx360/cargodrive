package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by shanakajay on 9/27/2016.
 */
@Entity
@Table(name = "CHARGE_MODE", schema = "NEWCARGODRIVE")
public class ChargeMode {
    private Integer chargeModeSeq;
    private Integer chargeSeq;
    private Integer companyModuleSeq;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Charge charge;
   /* private CompanyModule companyModule;*/

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "CHARGE_MODE_SEQ", allocationSize = 1)
    @Column(name = "CHARGE_MODE_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getChargeModeSeq() {
        return chargeModeSeq;
    }

    public void setChargeModeSeq(Integer chargeModeSeq) {
        this.chargeModeSeq = chargeModeSeq;
    }

    @Basic
    @Column(name = "CHARGE_SEQ", nullable = true, insertable = false, updatable = false)
    public Integer getChargeSeq() {
        return chargeSeq;
    }

    public void setChargeSeq(Integer chargeSeq) {
        this.chargeSeq = chargeSeq;
    }

    @Basic
    @Column(name = "COMPANY_MODULE_SEQ", nullable = false)//, insertable = false, updatable = false
    public Integer getCompanyModuleSeq() {
        return companyModuleSeq;
    }

    public void setCompanyModuleSeq(Integer companyModuleSeq) {
        this.companyModuleSeq = companyModuleSeq;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = true, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CHARGE_SEQ", insertable = false, updatable = false)
    @JsonIgnore
    public Charge getCharge() {
        return charge;
    }

    public void setCharge(Charge charge) {
        this.charge = charge;
    }

/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY_MODULE_SEQ", insertable = false, updatable = false)
    @JsonIgnore
    public CompanyModule getCompanyModule() {
        return companyModule;
    }

    public void setCompanyModule(CompanyModule companyModule) {
        this.companyModule = companyModule;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChargeMode that = (ChargeMode) o;

        if (chargeModeSeq != null ? !chargeModeSeq.equals(that.chargeModeSeq) : that.chargeModeSeq != null)
            return false;
        if (chargeSeq != null ? !chargeSeq.equals(that.chargeSeq) : that.chargeSeq != null) return false;
        if (companyModuleSeq != null ? !companyModuleSeq.equals(that.companyModuleSeq) : that.companyModuleSeq != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(that.lastModifiedBy) : that.lastModifiedBy != null)
            return false;
        if (lastModifiedDate != null ? !lastModifiedDate.equals(that.lastModifiedDate) : that.lastModifiedDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = chargeModeSeq != null ? chargeModeSeq.hashCode() : 0;
        result = 31 * result + (chargeSeq != null ? chargeSeq.hashCode() : 0);
        result = 31 * result + (companyModuleSeq != null ? companyModuleSeq.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        return result;
    }
}
