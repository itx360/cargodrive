package com.itx360.cargodrive.config.datalayer.service;


import com.itx360.cargodrive.config.datalayer.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by IntelliJ IDEA.
 * User: Thilanga-Ehl
 * Date: 9/11/12
 * Time: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
public interface UserService extends JpaRepository<User, Integer> {

    User getUserByUserSeq(Integer userSeq);

    User findByUserSeq(Integer userSeq);

    User findByUsername(String username);
}
