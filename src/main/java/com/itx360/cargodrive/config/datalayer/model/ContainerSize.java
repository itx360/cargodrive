package com.itx360.cargodrive.config.datalayer.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/5/2016
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "CONTAINER_SIZE", schema = "NEWCARGODRIVE", catalog = "")
public class ContainerSize {
    private Integer containerSizeSeq;
    private Integer containerSizeValue;
    private Integer status;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "CONTAINER_SIZE_SEQ", allocationSize = 1)
    @Column(name = "CONTAINER_SIZE_SEQ", nullable = false, precision = 0)
    public Integer getContainerSizeSeq() {
        return containerSizeSeq;
    }

    public void setContainerSizeSeq(Integer containerSizeSeq) {
        this.containerSizeSeq = containerSizeSeq;
    }

    @Basic
    @Column(name = "CONTAINER_SIZE_VALUE", nullable = true, precision = 0)
    public Integer getContainerSizeValue() {
        return containerSizeValue;
    }

    public void setContainerSizeValue(Integer containerSizeValue) {
        this.containerSizeValue = containerSizeValue;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContainerSize that = (ContainerSize) o;
        return containerSizeSeq == that.containerSizeSeq &&
                Objects.equals(containerSizeValue, that.containerSizeValue) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerSizeSeq, containerSizeValue, status);
    }
}
