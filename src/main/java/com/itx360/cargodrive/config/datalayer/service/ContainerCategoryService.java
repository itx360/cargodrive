package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.ContainerCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/5/2016
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface ContainerCategoryService extends JpaRepository<ContainerCategory, Integer> {
}
