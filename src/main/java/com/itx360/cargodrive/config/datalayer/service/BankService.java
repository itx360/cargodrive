package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by shanakajay on 9/26/2016.
 */
@Repository
public interface BankService extends JpaRepository<Bank, Integer> {

    Bank findByBankName(String bankName);

    Bank findByBankCodeIgnoreCase(String bankCode);

    List<Bank> findByBankNameContainingIgnoreCase(String bankName);

    List<Bank> findByBankCodeContainingIgnoreCase(String bankCode);

    List<Bank> findByBankNameContainingIgnoreCaseAndBankCodeContainingIgnoreCase(String bankName,String bankCode);
}
