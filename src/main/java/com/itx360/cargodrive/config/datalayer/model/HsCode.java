package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by Thilangaj on 9/26/2016 9:03 PM).
 */
@Entity
@Table(name = "HS_CODE", schema = "NEWCARGODRIVE", catalog = "")
public class HsCode {
    private Integer hsCodeSeq;
    private Integer hsCategorySeq;
    private String hsCodeName;
    private String description;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer status;
    private HsCategory hsCategory;

    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "HS_CODE_SEQ", allocationSize = 1)
    @Column(name = "HS_CODE_SEQ", nullable = false, precision = 0)
    public Integer getHsCodeSeq() {
        return hsCodeSeq;
    }

    public void setHsCodeSeq(Integer hsCodeSeq) {
        this.hsCodeSeq = hsCodeSeq;
    }

    @Basic
    @Column(name = "HS_CATEGORY_SEQ", nullable = false, precision = 0)
    public Integer getHsCategorySeq() {
        return hsCategorySeq;
    }

    public void setHsCategorySeq(Integer hsCategorySeq) {
        this.hsCategorySeq = hsCategorySeq;
    }

    @Basic
    @Column(name = "HS_CODE_NAME", nullable = false, length = 100)
    public String getHsCodeName() {
        return hsCodeName;
    }

    public void setHsCodeName(String hsCodeName) {
        this.hsCodeName = hsCodeName;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = false, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HsCode)) return false;

        HsCode hsCode = (HsCode) o;

        if (getHsCodeSeq() != null ? !getHsCodeSeq().equals(hsCode.getHsCodeSeq()) : hsCode.getHsCodeSeq() != null)
            return false;
        if (getHsCategorySeq() != null ? !getHsCategorySeq().equals(hsCode.getHsCategorySeq()) : hsCode.getHsCategorySeq() != null)
            return false;
        if (getHsCodeName() != null ? !getHsCodeName().equals(hsCode.getHsCodeName()) : hsCode.getHsCodeName() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(hsCode.getDescription()) : hsCode.getDescription() != null)
            return false;
        if (getCreatedBy() != null ? !getCreatedBy().equals(hsCode.getCreatedBy()) : hsCode.getCreatedBy() != null)
            return false;
        if (getCreatedDate() != null ? !getCreatedDate().equals(hsCode.getCreatedDate()) : hsCode.getCreatedDate() != null)
            return false;
        if (getLastModifiedBy() != null ? !getLastModifiedBy().equals(hsCode.getLastModifiedBy()) : hsCode.getLastModifiedBy() != null)
            return false;
        if (getLastModifiedDate() != null ? !getLastModifiedDate().equals(hsCode.getLastModifiedDate()) : hsCode.getLastModifiedDate() != null)
            return false;
        if (getStatus() != null ? !getStatus().equals(hsCode.getStatus()) : hsCode.getStatus() != null) return false;
        if (getHsCategory() != null ? !getHsCategory().equals(hsCode.getHsCategory()) : hsCode.getHsCategory() != null)
            return false;
        return getStatusDescription() != null ? getStatusDescription().equals(hsCode.getStatusDescription()) : hsCode.getStatusDescription() == null;

    }

    @Override
    public int hashCode() {
        int result = getHsCodeSeq() != null ? getHsCodeSeq().hashCode() : 0;
        result = 31 * result + (getHsCategorySeq() != null ? getHsCategorySeq().hashCode() : 0);
        result = 31 * result + (getHsCodeName() != null ? getHsCodeName().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getCreatedBy() != null ? getCreatedBy().hashCode() : 0);
        result = 31 * result + (getCreatedDate() != null ? getCreatedDate().hashCode() : 0);
        result = 31 * result + (getLastModifiedBy() != null ? getLastModifiedBy().hashCode() : 0);
        result = 31 * result + (getLastModifiedDate() != null ? getLastModifiedDate().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getHsCategory() != null ? getHsCategory().hashCode() : 0);
        result = 31 * result + (getStatusDescription() != null ? getStatusDescription().hashCode() : 0);
        return result;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "HS_CATEGORY_SEQ", insertable = false, updatable = false)
    public HsCategory getHsCategory() {
        return hsCategory;
    }

    public void setHsCategory(HsCategory hsCategory) {
        this.hsCategory = hsCategory;
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
