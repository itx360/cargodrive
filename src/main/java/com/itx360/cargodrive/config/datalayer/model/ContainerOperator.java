package com.itx360.cargodrive.config.datalayer.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/7/2016
 * Time: 11:41 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "CONTAINER_OPERATOR", schema = "NEWCARGODRIVE", catalog = "")
public class ContainerOperator {
    private Integer containerOperatorSeq;
    private Integer vesselSeq;
    private Integer freightLineSeq;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "CONTAINER_OPERATOR_SEQ", allocationSize = 1)
    @Column(name = "CONTAINER_OPERATOR_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getContainerOperatorSeq() {
        return containerOperatorSeq;
    }

    public void setContainerOperatorSeq(Integer containerOperatorSeq) {
        this.containerOperatorSeq = containerOperatorSeq;
    }

    @Basic
    @Column(name = "VESSEL_SEQ", nullable = true, precision = 0)
    public Integer getVesselSeq() {
        return vesselSeq;
    }

    public void setVesselSeq(Integer vesselSeq) {
        this.vesselSeq = vesselSeq;
    }

    @Basic
    @Column(name = "FREIGHT_LINE_SEQ", nullable = true, precision = 0)
    public Integer getFreightLineSeq() {
        return freightLineSeq;
    }

    public void setFreightLineSeq(Integer freightLineSeq) {
        this.freightLineSeq = freightLineSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContainerOperator that = (ContainerOperator) o;
        return containerOperatorSeq == that.containerOperatorSeq &&
                Objects.equals(vesselSeq, that.vesselSeq) &&
                Objects.equals(freightLineSeq, that.freightLineSeq);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerOperatorSeq, vesselSeq, freightLineSeq);
    }
}
