package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Thilangaj on 9/26/2016 9:11 PM).
 */
@Entity
@Table(name = "COMMODITY_TYPE", schema = "NEWCARGODRIVE", catalog = "")
public class CommodityType {
    private Integer commodityTypeSeq;
    private String commodityTypeCode;
    private String description;
    private String remarks;
    private Integer commodityCategorySeq;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer status;
    private CommodityCategory commodityCategory;

    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "COMMODITY_TYPE_SEQ", allocationSize = 1)
    @Column(name = "COMMODITY_TYPE_SEQ", nullable = false, precision = 0)
    public Integer getCommodityTypeSeq() {
        return commodityTypeSeq;
    }

    public void setCommodityTypeSeq(Integer commodityTypeSeq) {
        this.commodityTypeSeq = commodityTypeSeq;
    }

    @Basic
    @Column(name = "COMMODITY_TYPE_CODE", nullable = false, length = 100)
    public String getCommodityTypeCode() {
        return commodityTypeCode;
    }

    public void setCommodityTypeCode(String commodityTypeCode) {
        this.commodityTypeCode = commodityTypeCode;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = false, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "REMARKS", nullable = true, length = 500)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "COMMODITY_CATEGORY_SEQ", nullable = false, precision = 0)
    public Integer getCommodityCategorySeq() {
        return commodityCategorySeq;
    }

    public void setCommodityCategorySeq(Integer commodityCategorySeq) {
        this.commodityCategorySeq = commodityCategorySeq;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommodityType that = (CommodityType) o;

        if (commodityTypeSeq != null ? !commodityTypeSeq.equals(that.commodityTypeSeq) : that.commodityTypeSeq != null)
            return false;
        if (commodityTypeCode != null ? !commodityTypeCode.equals(that.commodityTypeCode) : that.commodityTypeCode != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (remarks != null ? !remarks.equals(that.remarks) : that.remarks != null) return false;
        if (commodityCategorySeq != null ? !commodityCategorySeq.equals(that.commodityCategorySeq) : that.commodityCategorySeq != null)
            return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(that.lastModifiedBy) : that.lastModifiedBy != null)
            return false;
        if (lastModifiedDate != null ? !lastModifiedDate.equals(that.lastModifiedDate) : that.lastModifiedDate != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = commodityTypeSeq != null ? commodityTypeSeq.hashCode() : 0;
        result = 31 * result + (commodityTypeCode != null ? commodityTypeCode.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        result = 31 * result + (commodityCategorySeq != null ? commodityCategorySeq.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COMMODITY_CATEGORY_SEQ", insertable = false, updatable = false)
    public CommodityCategory getCommodityCategory() {
        return commodityCategory;
    }

    public void setCommodityCategory(CommodityCategory commodityCategory) {
        this.commodityCategory = commodityCategory;
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
