package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/19/2016
 * Time: 4:57 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface CurrencyService extends JpaRepository<Currency, Integer> {

    List<Currency> findByCurrencyNameContainingIgnoreCase(String currencyName);

    List<Currency> findByCurrencyDisplayContainingIgnoreCase(String currencyDisplay);



    List<Currency> findByCurrencyNameContainingIgnoreCaseAndCurrencyDisplayContainingIgnoreCase(String currencyName, String currencyDisplay);

    Currency findByCurrencyCodeIgnoreCase(String currencyCode);
}
