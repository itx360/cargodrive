package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.FinalDestination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Sachithrac on 10/5/2016.
 */
@Repository
public interface FinalDestinationService extends JpaRepository<FinalDestination,Integer> {

    FinalDestination findByFinalDestinationCodeIgnoreCaseAndCountrySeq(String finalDestinationCode,Integer countrySeq);

    List<FinalDestination> findByFinalDestinationCodeContainingIgnoreCaseAndCountryCountrySeqAndAddressBookCityContainingIgnoreCaseAndAddressBookStateContainingIgnoreCaseAndAddressBookZipContainingIgnoreCase(String finalDestinationCode,Integer countrySeq,String city,String state,String zip);

    List<FinalDestination> findByFinalDestinationCodeContainingIgnoreCaseAndAddressBookCityContainingIgnoreCaseAndAddressBookStateContainingIgnoreCaseAndAddressBookZipContainingIgnoreCase(String finalDestinationCode, String city, String state, String zip);

    List<FinalDestination> findByCountrySeq(Integer countrySeq);
}
