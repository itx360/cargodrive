package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.CommodityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 9:18 PM).
 */
@Repository
public interface CommodityTypeService extends JpaRepository<CommodityType, Integer> {

    CommodityType findByCommodityTypeCode(String commodityTypeCode);

    List<CommodityType> findByDescriptionContainingIgnoreCase(String description);

    List<CommodityType> findByCommodityTypeCodeContainingIgnoreCase(String commodityTypeCode);

    List<CommodityType> findByCommodityTypeCodeContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String commodityTypeCode, String description);

}
