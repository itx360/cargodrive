package com.itx360.cargodrive.config.datalayer.serviceImpl;

import com.itx360.cargodrive.config.datalayer.model.Group;
import com.itx360.cargodrive.config.datalayer.model.Module;
import com.itx360.cargodrive.config.datalayer.model.UserModule;
import com.itx360.cargodrive.config.datalayer.service.UserModuleService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/9/2016
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class UserModuleServiceImpl implements UserModuleService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public UserModule getUserModuleByUserModuleSeq(Integer userModuleSeq) {
        UserModule userModule = null;
        try {
            userModule = this.entityManager.find(UserModule.class, userModuleSeq);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userModule;
    }
}
