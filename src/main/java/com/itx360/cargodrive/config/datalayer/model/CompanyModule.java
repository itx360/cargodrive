package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/9/2016
 * Date: 5:59 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "COMPANY_MODULE", schema = "NEWCARGODRIVE", catalog = "")
public class CompanyModule {
    private Integer companyModuleSeq;
    private Integer companyProfileSeq;
    private Integer moduleSeq;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private Integer status;
    private Module module;
/*    private Set<ChargeMode> chargeModes = new HashSet<ChargeMode>(0);*/

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "COMPANY_MODULE_SEQ", allocationSize = 1)
    @Column(name = "COMPANY_MODULE_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getCompanyModuleSeq() {
        return companyModuleSeq;
    }

    public void setCompanyModuleSeq(Integer companyModuleSeq) {
        this.companyModuleSeq = companyModuleSeq;
    }

    @Basic
    @Column(name = "COMPANY_PROFILE_SEQ", nullable = false, precision = 0)
    public Integer getCompanyProfileSeq() {
        return companyProfileSeq;
    }

    public void setCompanyProfileSeq(Integer companyProfileSeq) {
        this.companyProfileSeq = companyProfileSeq;
    }

    @Basic
    @Column(name = "MODULE_SEQ", nullable = false, precision = 0)
    public Integer getModuleSeq() {
        return moduleSeq;
    }

    public void setModuleSeq(Integer moduleSeq) {
        this.moduleSeq = moduleSeq;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY", nullable = true, length = 50)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFIED_DATE", nullable = true)
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODULE_SEQ", insertable = false, updatable = false)
    @JsonIgnore
    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

/*    @OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "COMPANY_PROFILE_SEQ", nullable = false)
    public Set<ChargeMode> getChargeModes() {
        return chargeModes;
    }

    public void setChargeModes(Set<ChargeMode> chargeModes) {
        this.chargeModes = chargeModes;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyModule that = (CompanyModule) o;
        return java.util.Objects.equals(companyModuleSeq, that.companyModuleSeq) &&
                java.util.Objects.equals(companyProfileSeq, that.companyProfileSeq) &&
                java.util.Objects.equals(moduleSeq, that.moduleSeq) &&
                java.util.Objects.equals(createdBy, that.createdBy) &&
                java.util.Objects.equals(createdDate, that.createdDate) &&
                java.util.Objects.equals(modifiedBy, that.modifiedBy) &&
                java.util.Objects.equals(modifiedDate, that.modifiedDate) &&
                java.util.Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(companyModuleSeq, companyProfileSeq, moduleSeq, createdBy, createdDate, modifiedBy, modifiedDate, status);
    }
}
