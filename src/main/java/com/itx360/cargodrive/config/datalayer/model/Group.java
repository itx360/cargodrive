package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Thilanga-Ehl on 8/31/2016 1:05 PM).
 */
@Entity
@Table(name = "GROUPS", schema = "NEWCARGODRIVE", catalog = "")
public class Group {
    private Integer groupSeq;
    private Integer moduleSeq;
    private String groupName;
    private String description;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private Integer status;
    private Set<User> users = new HashSet<User>(0);
    private Set<Authority> authorities = new HashSet<Authority>(0);

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "GROUP_SEQ", allocationSize = 1)
    @Column(name = "GROUP_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getGroupSeq() {
        return groupSeq;
    }

    public void setGroupSeq(Integer groupSeq) {
        this.groupSeq = groupSeq;
    }

    @Basic
    @Column(name = "GROUP_NAME", nullable = false, length = 100)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 200)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY", nullable = true, length = 50)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFIED_DATE", nullable = true)
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "MODULE_SEQ", nullable = false)
    public Integer getModuleSeq() {
        return moduleSeq;
    }

    public void setModuleSeq(Integer moduleSeq) {
        this.moduleSeq = moduleSeq;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "assignedGroups")
    @JsonIgnore
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "GROUP_AUTHORITIES", catalog = "", joinColumns = {
            @JoinColumn(name = "GROUP_SEQ", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "AUTHORITY_SEQ",
                    nullable = false, updatable = false)})
    @JsonIgnore
    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (groupSeq != null ? !groupSeq.equals(group.groupSeq) : group.groupSeq != null) return false;
        if (moduleSeq != null ? !moduleSeq.equals(group.moduleSeq) : group.moduleSeq != null) return false;
        if (groupName != null ? !groupName.equals(group.groupName) : group.groupName != null) return false;
        if (description != null ? !description.equals(group.description) : group.description != null) return false;
        if (createdBy != null ? !createdBy.equals(group.createdBy) : group.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(group.createdDate) : group.createdDate != null) return false;
        if (modifiedBy != null ? !modifiedBy.equals(group.modifiedBy) : group.modifiedBy != null) return false;
        if (modifiedDate != null ? !modifiedDate.equals(group.modifiedDate) : group.modifiedDate != null) return false;
        if (status != null ? !status.equals(group.status) : group.status != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = groupSeq != null ? groupSeq.hashCode() : 0;
        result = 31 * result + (moduleSeq != null ? moduleSeq.hashCode() : 0);
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (modifiedBy != null ? modifiedBy.hashCode() : 0);
        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
