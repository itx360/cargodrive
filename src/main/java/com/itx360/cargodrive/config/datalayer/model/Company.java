package com.itx360.cargodrive.config.datalayer.model;

/**
 * Created by Harshaa on 9/7/2016.
 */


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "COMPANY_PROFILE", schema = "NEWCARGODRIVE", catalog = "")
public class Company {
    private Integer companyProfileSeq;
    private String companyName;
    private String shortCode;
    private String description;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private Integer status;
    private Integer uploadDocumentSeq;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "COMPANY_PROFILE_SEQ", allocationSize = 1)
    @Column(name = "COMPANY_PROFILE_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getCompanyProfileSeq() {
        return companyProfileSeq;
    }

    public void setCompanyProfileSeq(Integer companySeq) {
        this.companyProfileSeq = companySeq;
    }

    @Basic
    @Column(name = "COMPANY_NAME", nullable = false)
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Basic
    @Column(name = "SHORT_CODE", unique = true)
    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    @Basic
    @Column(name = "DESCRIPTION", length = 512)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, updatable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false, updatable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY")
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFIED_DATE")
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = false)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "UPLOAD_DOCUMENT_SEQ")
    public Integer getUploadDocumentSeq() {
        return uploadDocumentSeq;
    }

    public void setUploadDocumentSeq(Integer uploadDocumentSeq) {
        this.uploadDocumentSeq = uploadDocumentSeq;
    }

}


