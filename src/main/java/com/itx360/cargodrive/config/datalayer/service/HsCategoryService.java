package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.HsCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 9:19 PM).
 */
@Repository
public interface HsCategoryService extends JpaRepository<HsCategory, Integer> {

    HsCategory findByHsCategoryName(String hsCategoryName);

    List<HsCategory> findByDescriptionContainingIgnoreCase(String description);

    List<HsCategory> findByHsCategoryNameContainingIgnoreCase(String hsCategoryName);

    List<HsCategory> findByHsCategoryNameContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String hsCategoryName, String description);

}
