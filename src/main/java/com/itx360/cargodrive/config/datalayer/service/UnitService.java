package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by shanakajay on 10/4/2016.
 */
@Repository
public interface UnitService extends JpaRepository<Unit, Integer> {

    Unit findByUnitName(String unitName);

    List<Unit> findByUnitNameContainingIgnoreCase(String unitName);

    List<Unit> findByUnitCodeContainingIgnoreCase(String unitCode);

    List<Unit> findByUsedForContainingIgnoreCase(String usedFor);

    List<Unit> findByUnitCodeContainingIgnoreCaseAndUsedForContainingIgnoreCase(String unitCode, String usedFor);

    List<Unit> findByUnitNameContainingIgnoreCaseAndUsedForContainingIgnoreCase(String unitName, String usedFor);

    List<Unit> findByUnitNameContainingIgnoreCaseAndUnitCodeContainingIgnoreCase(String unitName, String unitCode);

    List<Unit> findByUnitNameContainingIgnoreCaseAndUnitCodeContainingIgnoreCaseAndUsedForContainingIgnoreCase(String unitName, String unitCode,String usedFor);
}
