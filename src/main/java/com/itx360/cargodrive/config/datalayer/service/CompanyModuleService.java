package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.CompanyModule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/9/2016
 * Time: 5:52 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CompanyModuleService extends JpaRepository<CompanyModule, Integer> {
    List<CompanyModule> findByCompanyProfileSeq(Integer companyProfileSeq);

    CompanyModule findByCompanyProfileSeqAndModuleSeq(Integer companyProfileSeq, Integer moduleSeq);
}
