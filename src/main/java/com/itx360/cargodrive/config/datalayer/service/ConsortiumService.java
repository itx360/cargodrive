package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Consortium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Nuwanr on 9/29/2016.
 */
@Repository
public interface ConsortiumService extends JpaRepository<Consortium, Integer> {
    List<Consortium>findByConsortiumNameContainingIgnoreCase(String consortiumName);
}
