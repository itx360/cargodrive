package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Vessel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/7/2016
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface VesselService extends JpaRepository<Vessel, Integer> {
}
