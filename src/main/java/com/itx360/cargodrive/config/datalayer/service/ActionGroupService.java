package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.ActionGroup;
import com.itx360.cargodrive.config.datalayer.model.Module;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sachithrac on 9/1/2016.
 */
public interface ActionGroupService extends JpaRepository<ActionGroup, Integer>{

}
