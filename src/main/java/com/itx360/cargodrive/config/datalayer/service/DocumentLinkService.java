package com.itx360.cargodrive.config.datalayer.service;
import com.itx360.cargodrive.config.datalayer.model.DocumentLink;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Sachithrac on 9/1/2016.
 */
public interface DocumentLinkService extends JpaRepository<DocumentLink, Integer> {

    List<DocumentLink> findByModuleSeq(Integer moduleSeq);
}
