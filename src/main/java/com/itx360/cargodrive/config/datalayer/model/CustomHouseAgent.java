package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

/**
 * Created by Sachithrac on 10/6/2016.
 */
@Entity
@Table(name = "CUSTOM_HOUSE_AGENT", schema = "NEWCARGODRIVE", catalog = "")
public class CustomHouseAgent {
    private Integer customHouseAgentSeq;
    private String customHouseAgentName;
    private String taxRegistrationNo;
    private String description;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private AddressBook addressBook;
    private Country country;
    private TaxType taxType;
    private Integer addressBookSeq;
    private Integer countrySeq;
    private Integer taxTypeSeq;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "CUSTOM_HOUSE_AGENT_SEQ", allocationSize = 1)
    @Column(name = "CUSTOM_HOUSE_AGENT_SEQ")

    public Integer getCustomHouseAgentSeq() {
        return customHouseAgentSeq;
    }

    public void setCustomHouseAgentSeq(Integer customHouseAgentSeq) {
        this.customHouseAgentSeq = customHouseAgentSeq;
    }

    @Basic
    @Column(name = "CUSTOM_HOUSE_AGENT_NAME",length = 50,nullable = false)
    public String getCustomHouseAgentName() {
        return customHouseAgentName;
    }

    public void setCustomHouseAgentName(String customHouseAgentName) {
        this.customHouseAgentName = customHouseAgentName;
    }

    @Basic
    @Column(name = "TAX_REGISTRATION_NO",length = 15)
    public String getTaxRegistrationNo() {
        return taxRegistrationNo;
    }

    public void setTaxRegistrationNo(String taxRegistrationNo) {
        this.taxRegistrationNo = taxRegistrationNo;
    }


    @Basic
    @Column(name = "DESCRIPTION",length = 150,nullable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "STATUS")

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED_BY",nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ADDRESS_BOOK_SEQ", nullable = false)
    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    @Basic
    @Column(name = "ADDRESS_BOOK_SEQ", insertable = false, updatable = false, nullable = false)
    public Integer getAddressBookSeq() {
        return addressBookSeq;
    }

    public void setAddressBookSeq(Integer addressBookSeq) {
        this.addressBookSeq = addressBookSeq;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "COUNTRY_SEQ", insertable = false, updatable = false, nullable = false)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Basic
    @Column(name = "COUNTRY_SEQ", nullable = false)
    public Integer getCountrySeq() {
        return countrySeq;
    }

    public void setCountrySeq(Integer countrySeq) {
        this.countrySeq = countrySeq;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "TAX_TYPE_SEQ", insertable = false, updatable = false, nullable = false)
    public TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxType taxType) {
        this.taxType = taxType;
    }

    @Basic
    @Column(name = "TAX_TYPE_SEQ", nullable = false)
    public Integer getTaxTypeSeq() {
        return taxTypeSeq;
    }

    public void setTaxTypeSeq(Integer taxTypeSeq) {
        this.taxTypeSeq = taxTypeSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomHouseAgent)) return false;

        CustomHouseAgent that = (CustomHouseAgent) o;

        if (!getCustomHouseAgentSeq().equals(that.getCustomHouseAgentSeq())) return false;
        if (!getCustomHouseAgentName().equals(that.getCustomHouseAgentName())) return false;
        if (!getTaxRegistrationNo().equals(that.getTaxRegistrationNo())) return false;
        if (!getDescription().equals(that.getDescription())) return false;
        if (!getStatus().equals(that.getStatus())) return false;
        if (!getCreatedBy().equals(that.getCreatedBy())) return false;
        if (!getCreatedDate().equals(that.getCreatedDate())) return false;
        if (!getLastModifiedBy().equals(that.getLastModifiedBy())) return false;
        if (!getLastModifiedDate().equals(that.getLastModifiedDate())) return false;
        if (!getAddressBook().equals(that.getAddressBook())) return false;
        if (!getAddressBookSeq().equals(that.getAddressBookSeq())) return false;
        if (!getCountrySeq().equals(that.getCountrySeq())) return false;
        return getTaxTypeSeq().equals(that.getTaxTypeSeq());

    }

    @Override
    public int hashCode() {
        int result = getCustomHouseAgentSeq().hashCode();
        result = 31 * result + getCustomHouseAgentName().hashCode();
        result = 31 * result + getTaxRegistrationNo().hashCode();
        result = 31 * result + getDescription().hashCode();
        result = 31 * result + getStatus().hashCode();
        result = 31 * result + getCreatedBy().hashCode();
        result = 31 * result + getCreatedDate().hashCode();
        result = 31 * result + getLastModifiedBy().hashCode();
        result = 31 * result + getLastModifiedDate().hashCode();
        result = 31 * result + getAddressBook().hashCode();
        result = 31 * result + getAddressBookSeq().hashCode();
        result = 31 * result + getCountrySeq().hashCode();
        result = 31 * result + getTaxTypeSeq().hashCode();
        return result;
    }
}
