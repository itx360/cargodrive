package com.itx360.cargodrive.config.datalayer.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USER_LOGIN_AUDIT")
public class UserLogInAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "USER_LOGIN_AUDIT_SEQ", allocationSize = 1)
    @Column(name = "USER_LOGIN_AUDIT_SEQ", unique = true)
    private Integer userLogInAuditSeq;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "LOGIN_DATE")
    private Date loginDate;

    @Column(name = "LOGOUT_DATE")
    private Date logoutDate;

    @Column(name = "REMOTE_ADDRESS")
    private String remoteAddress;

    public Integer getUserLogInAuditSeq() {
        return userLogInAuditSeq;
    }

    public void setUserLogInAuditSeq(Integer userLogInAuditSeq) {
        this.userLogInAuditSeq = userLogInAuditSeq;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLogoutDate() {
        return logoutDate;
    }

    public void setLogoutDate(Date logoutDate) {
        this.logoutDate = logoutDate;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }
}
