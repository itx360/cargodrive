package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.CommodityCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/26/2016
 * Time: 11:27 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface CommodityCategoryService extends JpaRepository<CommodityCategory, Integer> {

    List<CommodityCategory> findByCommodityCodeContainingIgnoreCase(String commodityCode);

    List<CommodityCategory> findByCommodityNameContainingIgnoreCase(String commodityCode);

    List<CommodityCategory> findByCommodityCodeContainingIgnoreCaseAndCommodityNameContainingIgnoreCase(String commodityCode, String commodityName);

    CommodityCategory findByCommodityCodeIgnoreCase(String commodityCode);
}
