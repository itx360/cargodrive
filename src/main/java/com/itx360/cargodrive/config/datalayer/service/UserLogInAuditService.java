package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.UserLogInAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Thilangaj on 9/14/2016 10:02 AM).
 */
public interface UserLogInAuditService extends JpaRepository<UserLogInAudit, Integer> {

    @Query("select u from UserLogInAudit u where u.userLogInAuditSeq=(select max(userLogInAuditSeq) from UserLogInAudit where username=:USERNAME)")
    UserLogInAudit lastLogInAudit(@Param("USERNAME") String username);
}
