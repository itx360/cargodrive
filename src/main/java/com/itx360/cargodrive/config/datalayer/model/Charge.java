package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.*;

/**
 * Created by shanakajay on 9/27/2016.
 */
@Entity
@Table(name = "CHARGE", schema = "NEWCARGODRIVE")
public class Charge {
    private Integer chargeSeq;
    private String chargeName;
    private String description;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer companyProfileSeq;
    private Set<ChargeMode> chargeModes = new HashSet<ChargeMode>(0);
    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "CHARGE_SEQ", allocationSize = 1)
    @Column(name = "CHARGE_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getChargeSeq() {
        return chargeSeq;
    }

    public void setChargeSeq(Integer chargeSeq) {
        this.chargeSeq = chargeSeq;
    }

    @Basic
    @Column(name = "CHARGE_NAME", nullable = false, length = 100)
    public String getChargeName() {
        return chargeName;
    }

    public void setChargeName(String chargeName) {
        this.chargeName = chargeName;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = false, length = 150)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "CHARGE_SEQ", nullable = false)
    public Set<ChargeMode> getChargeModes() {
        return chargeModes;
    }

    public void setChargeModes(Set<ChargeMode> chargeModes) {
        this.chargeModes = chargeModes;
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Basic
    @Column(name = "COMPANY_PROFILE_SEQ", nullable = false, precision = 0)
    public Integer getCompanyProfileSeq() {
        return companyProfileSeq;
    }

    public void setCompanyProfileSeq(Integer companyProfileSeq) {
        this.companyProfileSeq = companyProfileSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Charge)) return false;

        Charge charge = (Charge) o;

        if (!getChargeSeq().equals(charge.getChargeSeq())) return false;
        if (!getChargeName().equals(charge.getChargeName())) return false;
        if (!getDescription().equals(charge.getDescription())) return false;
        if (!getStatus().equals(charge.getStatus())) return false;
        if (!getCreatedBy().equals(charge.getCreatedBy())) return false;
        if (!getCreatedDate().equals(charge.getCreatedDate())) return false;
        if (!getLastModifiedBy().equals(charge.getLastModifiedBy())) return false;
        if (!getLastModifiedDate().equals(charge.getLastModifiedDate())) return false;
        if (getChargeModes() != null ? !getChargeModes().equals(charge.getChargeModes()) : charge.getChargeModes() != null)
            return false;
        if (getStatusDescription() != null ? !getStatusDescription().equals(charge.getStatusDescription()) : charge.getStatusDescription() != null)
            return false;
        return getCompanyProfileSeq().equals(charge.getCompanyProfileSeq());

    }

    @Override
    public int hashCode() {
        int result = getChargeSeq().hashCode();
        result = 31 * result + getChargeName().hashCode();
        result = 31 * result + getDescription().hashCode();
        result = 31 * result + getStatus().hashCode();
        result = 31 * result + getCreatedBy().hashCode();
        result = 31 * result + getCreatedDate().hashCode();
        result = 31 * result + getLastModifiedBy().hashCode();
        result = 31 * result + getLastModifiedDate().hashCode();
        result = 31 * result + (getChargeModes() != null ? getChargeModes().hashCode() : 0);
        result = 31 * result + (getStatusDescription() != null ? getStatusDescription().hashCode() : 0);
        result = 31 * result + getCompanyProfileSeq().hashCode();
        return result;
    }
}
