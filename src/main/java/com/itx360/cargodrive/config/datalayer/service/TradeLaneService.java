package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.TradeLane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Sachithrac on 9/20/2016.
 */
public interface TradeLaneService extends JpaRepository<TradeLane, Integer> {

    TradeLane findByTradeLaneNameIgnoreCase(String tradeLaneName);

    List<TradeLane> findByTradeLaneCodeContainingIgnoreCase(String tradeLaneCode);

    List<TradeLane> findByTradeLaneNameContainingIgnoreCase(String tradeLaneName);

    List<TradeLane> findByTradeLaneCodeContainingIgnoreCaseAndTradeLaneNameContainingIgnoreCase(String tradeLaneCode, String tradeLaneName);


}
