package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sachithrac on 9/29/2016.
 */
@Entity
public class Warehouse {
    private Integer warehouseSeq;
    private String warehouseCode;
    private String warehouseName;
    private String description;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private AddressBook addressBook;
    private CompanyProfile companyProfile;
    private Integer addressBookSeq;
    private Country country;
    private Integer countrySeq;
    private Integer companyProfileSeq;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "WAREHOUSE_SEQ", allocationSize = 1)
    @Column(name = "WAREHOUSE_SEQ")
    public Integer getWarehouseSeq() {
        return warehouseSeq;
    }

    public void setWarehouseSeq(Integer warehouseSeq) {
        this.warehouseSeq = warehouseSeq;
    }

    @Basic
    @Column(name = "WAREHOUSE_CODE", length = 100, nullable = false)
    public String getWarehouseCode() {
        return warehouseCode;
    }

    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    @Basic
    @Column(name = "WAREHOUSE_NAME", length = 100, nullable = false)
    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    @Basic
    @Column(name = "DESCRIPTION", length = 200, nullable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "STATUS", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    @Basic
    @Column(name = "CREATED_BY", length = 50, nullable = true)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ADDRESS_BOOK_SEQ", nullable = false)
    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "COUNTRY_SEQ", insertable = false, updatable = false, nullable = false)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "COMPANY_PROFILE_SEQ",insertable = false, updatable = false, nullable = false)
    public CompanyProfile getCompanyProfile() {
        return companyProfile;
    }

    public void setCompanyProfile(CompanyProfile companyProfile) {
        this.companyProfile = companyProfile;
    }

    @Basic
    @Column(name = "ADDRESS_BOOK_SEQ", insertable = false, updatable = false, nullable = false)
    public Integer getAddressBookSeq() {
        return addressBookSeq;
    }

    public void setAddressBookSeq(Integer addressBookSeq) {
        this.addressBookSeq = addressBookSeq;
    }

    @Basic
    @Column(name = "COUNTRY_SEQ", nullable = false)
    public Integer getCountrySeq() {
        return countrySeq;
    }

    public void setCountrySeq(Integer countrySeq) {
        this.countrySeq = countrySeq;
    }

    @Basic
    @Column(name = "COMPANY_PROFILE_SEQ", updatable = false,nullable = false)
    public Integer getCompanyProfileSeq() {
        return companyProfileSeq;
    }

    public void setCompanyProfileSeq(Integer companyProfileSeq) {
        this.companyProfileSeq = companyProfileSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Warehouse)) return false;

        Warehouse warehouse = (Warehouse) o;

        if (!getWarehouseSeq().equals(warehouse.getWarehouseSeq())) return false;
        if (!getWarehouseCode().equals(warehouse.getWarehouseCode())) return false;
        if (!getWarehouseName().equals(warehouse.getWarehouseName())) return false;
        if (!getDescription().equals(warehouse.getDescription())) return false;
        if (!getStatus().equals(warehouse.getStatus())) return false;
        if (!getCreatedBy().equals(warehouse.getCreatedBy())) return false;
        if (!getCreatedDate().equals(warehouse.getCreatedDate())) return false;
        if (!getLastModifiedBy().equals(warehouse.getLastModifiedBy())) return false;
        if (!getLastModifiedDate().equals(warehouse.getLastModifiedDate())) return false;
        if (!getAddressBook().equals(warehouse.getAddressBook())) return false;
        if (!getAddressBookSeq().equals(warehouse.getAddressBookSeq())) return false;
        if (!getCountry().equals(warehouse.getCountry())) return false;
        return getCountrySeq().equals(warehouse.getCountrySeq());

    }

    @Override
    public int hashCode() {
        int result = getWarehouseSeq().hashCode();
        result = 31 * result + getWarehouseCode().hashCode();
        result = 31 * result + getWarehouseName().hashCode();
        result = 31 * result + getDescription().hashCode();
        result = 31 * result + getStatus().hashCode();
        result = 31 * result + getCreatedBy().hashCode();
        result = 31 * result + getCreatedDate().hashCode();
        result = 31 * result + getLastModifiedBy().hashCode();
        result = 31 * result + getLastModifiedDate().hashCode();
        result = 31 * result + getAddressBook().hashCode();
        result = 31 * result + getAddressBookSeq().hashCode();
        result = 31 * result + getCountry().hashCode();
        result = 31 * result + getCountrySeq().hashCode();
        return result;
    }
}
