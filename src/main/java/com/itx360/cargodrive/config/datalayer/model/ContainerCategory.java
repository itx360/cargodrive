package com.itx360.cargodrive.config.datalayer.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/5/2016
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "CONTAINER_CATEGORY", schema = "NEWCARGODRIVE", catalog = "")
public class ContainerCategory {
    private Integer containerCategorySeq;
    private String containerCategoryName;
    private Integer status;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "CONTAINER_CATEGORY_SEQ", allocationSize = 1)
    @Column(name = "CONTAINER_CATEGORY_SEQ", nullable = false, precision = 0)
    public Integer getContainerCategorySeq() {
        return containerCategorySeq;
    }

    public void setContainerCategorySeq(Integer containerCategorySeq) {
        this.containerCategorySeq = containerCategorySeq;
    }

    @Basic
    @Column(name = "CONTAINER_CATEGORY_NAME", nullable = true, length = 50)
    public String getContainerCategoryName() {
        return containerCategoryName;
    }

    public void setContainerCategoryName(String containerCategoryName) {
        this.containerCategoryName = containerCategoryName;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContainerCategory that = (ContainerCategory) o;
        return containerCategorySeq == that.containerCategorySeq &&
                Objects.equals(containerCategoryName, that.containerCategoryName) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerCategorySeq, containerCategoryName, status);
    }
}
