package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.ContainerType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/5/2016
 * Time: 5:07 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface ContainerTypeService extends JpaRepository<ContainerType, Integer> {
    ContainerType findByAsycudaCodeContainingIgnoreCase(String asycudaCode);

    List<ContainerType> findByDescriptionContainingIgnoreCase(String description);

    List<ContainerType> findByContainerSizeContainerSizeSeq(Integer containerSizeSeq);

    List<ContainerType> findByContainerCategoryContainerCategorySeq(Integer containerCategorySeq);

    List<ContainerType> findByContainerSizeContainerSizeSeqAndContainerCategoryContainerCategorySeq(Integer containerSizeSeq,Integer containerCategorySeq);

    List<ContainerType> findByContainerSizeContainerSizeSeqAndDescriptionContainingIgnoreCase(Integer containerSizeSeq,String description);

    List<ContainerType> findByContainerCategoryContainerCategorySeqAndDescriptionContainingIgnoreCase(Integer containerCategorySeq,String description);

    List<ContainerType> findByContainerSizeContainerSizeSeqAndContainerCategoryContainerCategorySeqAndDescriptionContainingIgnoreCase(Integer containerSizeSeq,
                                                                                                        Integer ContainerCategorySeq,
                                                                                                        String description);
}
