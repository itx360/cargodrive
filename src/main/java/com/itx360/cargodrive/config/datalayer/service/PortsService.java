package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Port;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Harshaa on 9/21/2016.
 */
public interface PortsService extends JpaRepository<Port, Integer> {

    Port findByPortSeq(Integer portSeq);

    List<Port> findByLocationSeq(Integer locationSeq);

    List<Port> findByTransportMode(Integer transportMode);

    List<Port> findByLocationSeqAndTransportMode(Integer transportMode, Integer locationSeq);


    List<Port> findByPortNameIsContainingIgnoreCase(String portName);

    List<Port> findByPortNameIsContainingIgnoreCaseAndLocationSeq(String portName, Integer locationSeq);

    List<Port> findByPortNameIsContainingIgnoreCaseAndTransportMode(String portName, Integer transportMode);

    List<Port> findByPortNameIsContainingIgnoreCaseAndLocationSeqAndTransportMode(String portName, Integer locationSeq, Integer transportMode);
}

