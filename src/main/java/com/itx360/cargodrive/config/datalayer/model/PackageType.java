package com.itx360.cargodrive.config.datalayer.model;

import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/4/2016
 * Date: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "PACKAGE_TYPE", schema = "NEWCARGODRIVE", catalog = "")
public class PackageType {
    private Integer packageTypeSeq;
    private String packageTypeName;
    private String packageTypeCode;
    private String asycudaCode;
    private String description;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer status;

    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "PACKAGE_TYPE_SEQ", allocationSize = 1)
    @Column(name = "PACKAGE_TYPE_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getPackageTypeSeq() {
        return packageTypeSeq;
    }

    public void setPackageTypeSeq(Integer packageTypeSeq) {
        this.packageTypeSeq = packageTypeSeq;
    }

    @Basic
    @Column(name = "PACKAGE_TYPE_NAME", nullable = true, length = 50, unique = true)
    public String getPackageTypeName() {
        return packageTypeName;
    }

    public void setPackageTypeName(String packageTypeName) {
        this.packageTypeName = packageTypeName;
    }

    @Basic
    @Column(name = "PACKAGE_TYPE_CODE", nullable = true, length = 50)
    public String getPackageTypeCode() {
        return packageTypeCode;
    }

    public void setPackageTypeCode(String packageTypeCode) {
        this.packageTypeCode = packageTypeCode;
    }

    @Basic
    @Column(name = "ASYCUDA_CODE", nullable = true, length = 50)
    public String getAsycudaCode() {
        return asycudaCode;
    }

    public void setAsycudaCode(String asycudaCode) {
        this.asycudaCode = asycudaCode;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 50)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackageType that = (PackageType) o;
        return Objects.equals(packageTypeSeq, that.packageTypeSeq) &&
                Objects.equals(packageTypeName, that.packageTypeName) &&
                Objects.equals(packageTypeCode, that.packageTypeCode) &&
                Objects.equals(asycudaCode, that.asycudaCode) &&
                Objects.equals(description, that.description) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
                Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageTypeSeq, packageTypeName, packageTypeCode, asycudaCode, description, createdBy, createdDate, lastModifiedBy, lastModifiedDate, status);
    }
}
