package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Module;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Sachithrac on 9/1/2016.
 */
public interface ModuleService extends JpaRepository<Module, Integer> {

    Module getModuleByModuleSeq(Integer moduleSeq);

    @Query("select m from Module m, CompanyModule cm where cm.companyProfileSeq = :companySeq and cm.moduleSeq = m.moduleSeq")
    List<Module> getModuleListByCompanySeq(@Param("companySeq") Integer companySeq);

    @Query("select " +
            "  distinct m " +
            "from " +
            "   Module m, " +
            "   CompanyModule cm," +
            "   User u, " +
            "   UserModule um " +
            "where " +
            "   u.username = :USERNAME " +
            "   and u.userSeq = um.userSeq " +
            "   and um.companyModuleSeq = cm.companyModuleSeq " +
            "   and cm.companyProfileSeq = :COMPANY " +
            "   and cm.moduleSeq = m.moduleSeq " +
            "order by " +
            "   m.moduleSeq ")
    List<Module> getModuleListByCompanySeqAndUsername(@Param("COMPANY") Integer companyProfileSeq,
                                                      @Param("USERNAME") String username);

    Module findByModuleName(String moduleName);

    @Query("select " +
            "  distinct m " +
            "from " +
            "   Module m, " +
            "   CompanyModule cm," +
            "   UserModule um " +
            "where " +
            "   um.userSeq = :USER " +
            "   and um.companyModuleSeq = cm.companyModuleSeq " +
            "   and cm.companyProfileSeq = :COMPANY " +
            "   and cm.moduleSeq = m.moduleSeq " +
            "order by " +
            "   m.moduleSeq ")
    List<Module> getModuleListByCompanySeqAndUserSeq(@Param("COMPANY") Integer companySeq,
                                                     @Param("USER") Integer userSeq);

    List<Module> findByFinanceEnabled(Integer financeEnabled);
}
