package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.TaxRegistration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Thilangaj on 10/4/2016 12:16 PM).
 */
@Repository
public interface TaxRegistrationService extends JpaRepository<TaxRegistration, Integer> {

    TaxRegistration findByTaxNameAndCompanyProfileSeq(String taxName, Integer companyProfileSeq);

}
