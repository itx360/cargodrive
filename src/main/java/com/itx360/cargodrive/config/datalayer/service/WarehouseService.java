package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Sachithrac on 9/29/2016.
 */
@Repository
public interface WarehouseService extends JpaRepository<Warehouse, Integer> {

    Warehouse findByWarehouseNameIgnoreCase(String warehouseName);

    List<Warehouse> findByWarehouseCodeContainingIgnoreCase(String warehouseCode);

    List<Warehouse> findByWarehouseNameContainingIgnoreCase(String warehouseName);

    List<Warehouse> findByCountrySeq(Integer countrySeq);

    List<Warehouse> findByWarehouseCodeContainingIgnoreCaseAndCountrySeq(String warehouseCode, Integer countrySeq);

    List<Warehouse> findByWarehouseNameContainingIgnoreCaseAndCountrySeq(String warehouseName, Integer countrySeq);

    List<Warehouse> findByWarehouseCodeContainingIgnoreCaseAndWarehouseNameContainingIgnoreCase(String warehouseCode, String warehouseName);

    List<Warehouse> findByWarehouseCodeContainingIgnoreCaseAndWarehouseNameContainingIgnoreCaseAndCountrySeq(String warehouseCode, String warehouseName, Integer countrySeq);
}


