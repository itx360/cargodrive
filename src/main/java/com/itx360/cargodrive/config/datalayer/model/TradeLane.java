package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/19/2016
 * Date: 6:34 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TRADE_LANE", schema = "NEWCARGODRIVE", catalog = "")
public class TradeLane {
    private Integer tradeLaneSeq;
    private String tradeLaneCode;
    private String tradeLaneName;
    private String description;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "TRADE_LANE_SEQ", allocationSize = 1)
    @Column(name = "TRADE_LANE_SEQ", nullable = false, precision = 0,unique = true)
    public Integer getTradeLaneSeq() {
        return tradeLaneSeq;
    }

    public void setTradeLaneSeq(Integer tradeLaneSeq) {
        this.tradeLaneSeq = tradeLaneSeq;
    }

    @Basic
    @Column(name = "TRADE_LANE_CODE", nullable = true, length = 100)
    public String getTradeLaneCode() {
        return tradeLaneCode;
    }

    public void setTradeLaneCode(String tradeLaneCode) {
        this.tradeLaneCode = tradeLaneCode;
    }

    @Basic
    @Column(name = "TRADE_LANE_NAME", nullable = true, length = 100)
    public String getTradeLaneName() {
        return tradeLaneName;
    }

    public void setTradeLaneName(String tradeLaneName) {
        this.tradeLaneName = tradeLaneName;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false,updatable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false,updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY", nullable = true, length = 50)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFIED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TradeLane)) return false;

        TradeLane tradeLane = (TradeLane) o;

        if (getTradeLaneSeq() != null ? !getTradeLaneSeq().equals(tradeLane.getTradeLaneSeq()) : tradeLane.getTradeLaneSeq() != null)
            return false;
        if (getTradeLaneCode() != null ? !getTradeLaneCode().equals(tradeLane.getTradeLaneCode()) : tradeLane.getTradeLaneCode() != null)
            return false;
        if (getTradeLaneName() != null ? !getTradeLaneName().equals(tradeLane.getTradeLaneName()) : tradeLane.getTradeLaneName() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(tradeLane.getDescription()) : tradeLane.getDescription() != null)
            return false;
        if (getStatus() != null ? !getStatus().equals(tradeLane.getStatus()) : tradeLane.getStatus() != null)
            return false;
        if (getCreatedBy() != null ? !getCreatedBy().equals(tradeLane.getCreatedBy()) : tradeLane.getCreatedBy() != null)
            return false;
        if (getCreatedDate() != null ? !getCreatedDate().equals(tradeLane.getCreatedDate()) : tradeLane.getCreatedDate() != null)
            return false;
        if (getModifiedBy() != null ? !getModifiedBy().equals(tradeLane.getModifiedBy()) : tradeLane.getModifiedBy() != null)
            return false;
        return getModifiedDate() != null ? getModifiedDate().equals(tradeLane.getModifiedDate()) : tradeLane.getModifiedDate() == null;

    }

    @Override
    public int hashCode() {
        int result = getTradeLaneSeq() != null ? getTradeLaneSeq().hashCode() : 0;
        result = 31 * result + (getTradeLaneCode() != null ? getTradeLaneCode().hashCode() : 0);
        result = 31 * result + (getTradeLaneName() != null ? getTradeLaneName().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getCreatedBy() != null ? getCreatedBy().hashCode() : 0);
        result = 31 * result + (getCreatedDate() != null ? getCreatedDate().hashCode() : 0);
        result = 31 * result + (getModifiedBy() != null ? getModifiedBy().hashCode() : 0);
        result = 31 * result + (getModifiedDate() != null ? getModifiedDate().hashCode() : 0);
        return result;
    }
}
