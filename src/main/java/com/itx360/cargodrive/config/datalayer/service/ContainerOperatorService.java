package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.ContainerOperator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/7/2016
 * Time: 11:46 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface ContainerOperatorService extends JpaRepository<ContainerOperator, Integer> {
}
