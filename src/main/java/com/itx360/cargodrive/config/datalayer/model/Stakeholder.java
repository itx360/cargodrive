package com.itx360.cargodrive.config.datalayer.model;

import javax.persistence.*;

/**
 * Created by Harshaa on 10/6/2016.
 */
@Entity
public class Stakeholder {
    private Long stakeholderSeq;
    private String stakeholderName;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "STAKEHOLDER_SEQ", allocationSize = 1)
    @Column(name = "STAKEHOLDER_SEQ")
    public Long getStakeholderSeq() {
        return stakeholderSeq;
    }

    public void setStakeholderSeq(Long stakeholderSeq) {
        this.stakeholderSeq = stakeholderSeq;
    }

    @Basic
    @Column(name = "STAKEHOLDER_NAME")
    public String getStakeholderName() {
        return stakeholderName;
    }

    public void setStakeholderName(String stakeholderName) {
        this.stakeholderName = stakeholderName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stakeholder that = (Stakeholder) o;

        if (stakeholderSeq != null ? !stakeholderSeq.equals(that.stakeholderSeq) : that.stakeholderSeq != null)
            return false;
        if (stakeholderName != null ? !stakeholderName.equals(that.stakeholderName) : that.stakeholderName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = stakeholderSeq != null ? stakeholderSeq.hashCode() : 0;
        result = 31 * result + (stakeholderName != null ? stakeholderName.hashCode() : 0);
        return result;
    }
}
