package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.SubModule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Sachithrac on 9/1/2016.
 */
public interface SubModuleService extends JpaRepository<SubModule, Integer> {

    SubModule findBySubModuleName(String subModuleName);

    @Query("select " +
            "   sm " +
            "from " +
            "   SubModule sm, " +
            "   Module m " +
            "where " +
            "   sm.moduleSeq = m.moduleSeq " +
            "   and sm.subModuleName = :SUB_MODULE " +
            "   and m.moduleName = :MODULE ")
    SubModule findBySubModuleNameAndModuleName(@Param("SUB_MODULE") String subModuleName,
                                               @Param("MODULE") String moduleName);
}
