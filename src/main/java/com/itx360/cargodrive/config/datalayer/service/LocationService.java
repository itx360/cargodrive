package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Thilangaj on 9/20/2016 1:01 PM).
 */
public interface LocationService extends JpaRepository<Location, Integer> {

    Location findByLocationName(String locationName);

    List<Location> findByLocationNameContainingIgnoreCase(String locationName);

    List<Location> findByLocationNameContainingIgnoreCaseAndCountrySeq(String locationName, Integer countrySeq);

    List<Location> findByCountrySeq(Integer countrySeq);

}
