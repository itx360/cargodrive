package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sachithrac on 9/26/2016.
 */
@Entity
public class Branch {
    private Integer branchSeq;
    private String branchCode;
    private String branchName;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer bankSeq;
    private AddressBook addressBook;
    private Bank bank;
    private Integer addressBookSeq;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "BRANCH_SEQ", allocationSize = 1)
    @Column(name = "BRANCH_SEQ", unique = true)
    public Integer getBranchSeq() {
        return branchSeq;
    }

    public void setBranchSeq(Integer branchSeq) {
        this.branchSeq = branchSeq;
    }

    @Basic
    @Column(name = "BRANCH_CODE", length = 5, nullable = false)
    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    @Basic
    @Column(name = "BRANCH_NAME", length = 30, nullable = false)
    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Basic
    @Column(name = "STATUS", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED_BY", length = 50, nullable = true,updatable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    @Basic
    @Column(name = "CREATED_DATE", nullable = true, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY",length = 50,nullable = true)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ADDRESS_BOOK_SEQ",nullable = false)
    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "BANK_SEQ",insertable = false,updatable = false)
    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Basic
    @Column(name = "ADDRESS_BOOK_SEQ",insertable = false,updatable = false,nullable = false)
    public Integer getAddressBookSeq() {
        return addressBookSeq;
    }

    public void setAddressBookSeq(Integer addressBookSeq) {
        this.addressBookSeq = addressBookSeq;
    }

    @Basic
    @Column(name = "BANK_SEQ",nullable = false)
    public Integer getBankSeq() {
        return bankSeq;
    }

    public void setBankSeq(Integer bankSeq) {
        this.bankSeq = bankSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Branch)) return false;

        Branch branch = (Branch) o;

        if (!getBranchSeq().equals(branch.getBranchSeq())) return false;
        if (!getBranchCode().equals(branch.getBranchCode())) return false;
        if (!getBranchName().equals(branch.getBranchName())) return false;
        if (!getStatus().equals(branch.getStatus())) return false;
        if (!getCreatedBy().equals(branch.getCreatedBy())) return false;
        if (!getCreatedDate().equals(branch.getCreatedDate())) return false;
        if (!getLastModifiedBy().equals(branch.getLastModifiedBy())) return false;
        if (!getLastModifiedDate().equals(branch.getLastModifiedDate())) return false;
        if (!getBankSeq().equals(branch.getBankSeq())) return false;
        return getAddressBookSeq().equals(branch.getAddressBookSeq());

    }

    @Override
    public int hashCode() {
        int result = getBranchSeq().hashCode();
        result = 31 * result + getBranchCode().hashCode();
        result = 31 * result + getBranchName().hashCode();
        result = 31 * result + getStatus().hashCode();
        result = 31 * result + getCreatedBy().hashCode();
        result = 31 * result + getCreatedDate().hashCode();
        result = 31 * result + getLastModifiedBy().hashCode();
        result = 31 * result + getLastModifiedDate().hashCode();
        result = 31 * result + getBankSeq().hashCode();
        result = 31 * result + getAddressBookSeq().hashCode();
        return result;
    }
}
