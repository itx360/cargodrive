package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by shanakajay on 9/20/2016.
 */
@Repository
public interface RegionService extends JpaRepository<Region, Integer> {
}
