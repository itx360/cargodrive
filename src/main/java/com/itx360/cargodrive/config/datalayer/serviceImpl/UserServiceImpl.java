package com.itx360.cargodrive.config.datalayer.serviceImpl;


import com.itx360.cargodrive.config.datalayer.model.User;
import com.itx360.cargodrive.config.datalayer.service.UserService;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by IntelliJ IDEA.
 * User: Thilanga-Ehl
 * Date: 9/11/12
 * Time: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public abstract class UserServiceImpl implements UserService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User getUserByUserSeq(Integer userSeq) {
        User user = this.entityManager.find(User.class, userSeq);
        return user;
    }
}
