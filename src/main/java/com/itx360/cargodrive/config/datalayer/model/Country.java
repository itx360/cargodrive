package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/20/2016
 * Date: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "COUNTRY", schema = "NEWCARGODRIVE")
public class Country {
    private Integer countrySeq;
    private String countryCode;
    private String countryName;
    private Integer regionSeq;
    private Integer currencySeq;
    private String iataCode;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Currency currency;
    private Region region;
    private String statusDescription;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "COUNTRY_SEQ", allocationSize = 1)
    @Column(name = "COUNTRY_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getCountrySeq() {
        return countrySeq;
    }

    public void setCountrySeq(Integer countrySeq) {
        this.countrySeq = countrySeq;
    }

    @Basic
    @Column(name = "COUNTRY_CODE", nullable = false, length = 15)
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Basic
    @Column(name = "COUNTRY_NAME", nullable = false, length = 100)
    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Basic
    @Column(name = "REGION_SEQ", nullable = true, precision = 0)
    public Integer getRegionSeq() {
        return regionSeq;
    }

    public void setRegionSeq(Integer regionSeq) {
        this.regionSeq = regionSeq;
    }

    @Basic
    @Column(name = "CURRENCY_SEQ", nullable = true, precision = 0)
    public Integer getCurrencySeq() {
        return currencySeq;
    }

    public void setCurrencySeq(Integer currencySeq) {
        this.currencySeq = currencySeq;
    }

    @Basic
    @Column(name = "IATA_CODE", nullable = true, length = 20)
    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_Date", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }


    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CURRENCY_SEQ", insertable = false, updatable = false)
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "REGION_SEQ", insertable = false, updatable = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }


    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;

        Country country = (Country) o;

        if (getCountrySeq() != null ? !getCountrySeq().equals(country.getCountrySeq()) : country.getCountrySeq() != null)
            return false;
        if (getCountryCode() != null ? !getCountryCode().equals(country.getCountryCode()) : country.getCountryCode() != null)
            return false;
        if (getCountryName() != null ? !getCountryName().equals(country.getCountryName()) : country.getCountryName() != null)
            return false;
        if (getRegionSeq() != null ? !getRegionSeq().equals(country.getRegionSeq()) : country.getRegionSeq() != null)
            return false;
        if (getCurrencySeq() != null ? !getCurrencySeq().equals(country.getCurrencySeq()) : country.getCurrencySeq() != null)
            return false;
        if (getIataCode() != null ? !getIataCode().equals(country.getIataCode()) : country.getIataCode() != null)
            return false;
        if (getStatus() != null ? !getStatus().equals(country.getStatus()) : country.getStatus() != null) return false;
        if (getCreatedBy() != null ? !getCreatedBy().equals(country.getCreatedBy()) : country.getCreatedBy() != null)
            return false;
        if (getCreatedDate() != null ? !getCreatedDate().equals(country.getCreatedDate()) : country.getCreatedDate() != null)
            return false;
        if (getLastModifiedBy() != null ? !getLastModifiedBy().equals(country.getLastModifiedBy()) : country.getLastModifiedBy() != null)
            return false;
        return getLastModifiedDate() != null ? getLastModifiedDate().equals(country.getLastModifiedDate()) : country.getLastModifiedDate() == null;

    }

    @Override
    public int hashCode() {
        int result = getCountrySeq() != null ? getCountrySeq().hashCode() : 0;
        result = 31 * result + (getCountryCode() != null ? getCountryCode().hashCode() : 0);
        result = 31 * result + (getCountryName() != null ? getCountryName().hashCode() : 0);
        result = 31 * result + (getRegionSeq() != null ? getRegionSeq().hashCode() : 0);
        result = 31 * result + (getCurrencySeq() != null ? getCurrencySeq().hashCode() : 0);
        result = 31 * result + (getIataCode() != null ? getIataCode().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getCreatedBy() != null ? getCreatedBy().hashCode() : 0);
        result = 31 * result + (getCreatedDate() != null ? getCreatedDate().hashCode() : 0);
        result = 31 * result + (getLastModifiedBy() != null ? getLastModifiedBy().hashCode() : 0);
        result = 31 * result + (getLastModifiedDate() != null ? getLastModifiedDate().hashCode() : 0);
        return result;
    }
}
