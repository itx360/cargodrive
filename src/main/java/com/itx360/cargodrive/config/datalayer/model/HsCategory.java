package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Thilangaj on 9/26/2016 9:03 PM).
 */
@Entity
@Table(name = "HS_CATEGORY", schema = "NEWCARGODRIVE", catalog = "")
public class HsCategory {
    private Integer hsCategorySeq;
    private String hsCategoryName;
    private String description;
    private String createdBy;
    private Date createdDate;
    private Date lastModifiedDate;
    private String lastModifiedBy;
    private Integer status;

    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "HS_CATEGORY_SEQ", allocationSize = 1)
    @Column(name = "HS_CATEGORY_SEQ", nullable = false, precision = 0)
    public Integer getHsCategorySeq() {
        return hsCategorySeq;
    }

    public void setHsCategorySeq(Integer hsCategorySeq) {
        this.hsCategorySeq = hsCategorySeq;
    }

    @Basic
    @Column(name = "HS_CATEGORY_NAME", nullable = false, length = 200)
    public String getHsCategoryName() {
        return hsCategoryName;
    }

    public void setHsCategoryName(String hsCategoryName) {
        this.hsCategoryName = hsCategoryName == null ? null : hsCategoryName.toUpperCase();
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = false, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HsCategory)) return false;

        HsCategory that = (HsCategory) o;

        if (getHsCategorySeq() != null ? !getHsCategorySeq().equals(that.getHsCategorySeq()) : that.getHsCategorySeq() != null)
            return false;
        if (getHsCategoryName() != null ? !getHsCategoryName().equals(that.getHsCategoryName()) : that.getHsCategoryName() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(that.getDescription()) : that.getDescription() != null)
            return false;
        if (getCreatedBy() != null ? !getCreatedBy().equals(that.getCreatedBy()) : that.getCreatedBy() != null)
            return false;
        if (getCreatedDate() != null ? !getCreatedDate().equals(that.getCreatedDate()) : that.getCreatedDate() != null)
            return false;
        if (getLastModifiedDate() != null ? !getLastModifiedDate().equals(that.getLastModifiedDate()) : that.getLastModifiedDate() != null)
            return false;
        if (getLastModifiedBy() != null ? !getLastModifiedBy().equals(that.getLastModifiedBy()) : that.getLastModifiedBy() != null)
            return false;
        if (getStatus() != null ? !getStatus().equals(that.getStatus()) : that.getStatus() != null) return false;
        return getStatusDescription() != null ? getStatusDescription().equals(that.getStatusDescription()) : that.getStatusDescription() == null;

    }

    @Override
    public int hashCode() {
        int result = getHsCategorySeq() != null ? getHsCategorySeq().hashCode() : 0;
        result = 31 * result + (getHsCategoryName() != null ? getHsCategoryName().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getCreatedBy() != null ? getCreatedBy().hashCode() : 0);
        result = 31 * result + (getCreatedDate() != null ? getCreatedDate().hashCode() : 0);
        result = 31 * result + (getLastModifiedDate() != null ? getLastModifiedDate().hashCode() : 0);
        result = 31 * result + (getLastModifiedBy() != null ? getLastModifiedBy().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getStatusDescription() != null ? getStatusDescription().hashCode() : 0);
        return result;
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
