package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.TaxType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Thilangaj on 10/4/2016 1:05 PM).
 */
@Repository
public interface TaxTypeService extends JpaRepository<TaxType, Integer> {
}
