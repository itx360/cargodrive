package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Sachithrac on 9/26/2016.
 */
@Repository
public interface BranchService extends JpaRepository<Branch, Integer> {

    Branch findByBranchCodeIgnoreCaseAndBranchNameIgnoreCaseAndBankBankSeq(String branchCode,String branchName,Integer bankSeq);

    List<Branch> findByBranchCodeContainingIgnoreCase(String branchCode);

    List<Branch> findByBranchNameContainingIgnoreCase(String branchName);

    List<Branch> findByBankBankNameContainingIgnoreCase(String bankName);

    List<Branch> findByBranchCodeContainingIgnoreCaseAndBranchNameContainingIgnoreCase(String branchCode, String branchName);

    List<Branch> findByBranchCodeContainingIgnoreCaseAndBankBankNameContainingIgnoreCase(String branchCode, String bankName);

    List<Branch> findByBranchNameContainingIgnoreCaseAndBankBankNameContainingIgnoreCase(String branchName, String bankName);

    List<Branch> findByBranchCodeContainingIgnoreCaseAndBranchNameContainingIgnoreCaseAndBankBankNameContainingIgnoreCase(String branchCode, String branchName, String bankName);

}
