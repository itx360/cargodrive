package com.itx360.cargodrive.config.datalayer.model;

import javax.persistence.*;
import java.sql.Time;

/**
 * Created by Harshaa on 10/6/2016.
 */
@Entity
@Table(name = "FREIGHT_LINE", schema = "NEWCARGODRIVE", catalog = "")
public class FreightLine {
    private Long freightLineSeq;
    private String freightLineName;
    private Long consortiumSeq;
    private Long localAgentSeq;
    private String prefix;
    private Long referenceNumber;
    private Long currencySeq;
    private Long countrySeq;
    private Long addressBookSeq;
    private String createdBy;
    private Time createdDate;
    private String lastModifiedBy;
    private Time lsatModifiedDate;
    private Long status;

    @Id
    @Column(name = "FREIGHT_LINE_SEQ")
    public Long getFreightLineSeq() {
        return freightLineSeq;
    }

    public void setFreightLineSeq(Long freightLineSeq) {
        this.freightLineSeq = freightLineSeq;
    }

    @Basic
    @Column(name = "FREIGHT_LINE_NAME")
    public String getFreightLineName() {
        return freightLineName;
    }

    public void setFreightLineName(String freightLineName) {
        this.freightLineName = freightLineName;
    }

    @Basic
    @Column(name = "CONSORTIUM_SEQ")
    public Long getConsortiumSeq() {
        return consortiumSeq;
    }

    public void setConsortiumSeq(Long consortiumSeq) {
        this.consortiumSeq = consortiumSeq;
    }

    @Basic
    @Column(name = "LOCAL_AGENT_SEQ")
    public Long getLocalAgentSeq() {
        return localAgentSeq;
    }

    public void setLocalAgentSeq(Long localAgentSeq) {
        this.localAgentSeq = localAgentSeq;
    }

    @Basic
    @Column(name = "PREFIX")
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Basic
    @Column(name = "REFERENCE_NUMBER")
    public Long getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(Long referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    @Basic
    @Column(name = "CURRENCY_SEQ")
    public Long getCurrencySeq() {
        return currencySeq;
    }

    public void setCurrencySeq(Long currencySeq) {
        this.currencySeq = currencySeq;
    }

    @Basic
    @Column(name = "COUNTRY_SEQ")
    public Long getCountrySeq() {
        return countrySeq;
    }

    public void setCountrySeq(Long countrySeq) {
        this.countrySeq = countrySeq;
    }

    @Basic
    @Column(name = "ADDRESS_BOOK_SEQ")
    public Long getAddressBookSeq() {
        return addressBookSeq;
    }

    public void setAddressBookSeq(Long addressBookSeq) {
        this.addressBookSeq = addressBookSeq;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE")
    public Time getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Time createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LSAT_MODIFIED_DATE")
    public Time getLsatModifiedDate() {
        return lsatModifiedDate;
    }

    public void setLsatModifiedDate(Time lsatModifiedDate) {
        this.lsatModifiedDate = lsatModifiedDate;
    }

    @Basic
    @Column(name = "STATUS")
    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FreightLine that = (FreightLine) o;

        if (freightLineSeq != null ? !freightLineSeq.equals(that.freightLineSeq) : that.freightLineSeq != null)
            return false;
        if (freightLineName != null ? !freightLineName.equals(that.freightLineName) : that.freightLineName != null)
            return false;
        if (consortiumSeq != null ? !consortiumSeq.equals(that.consortiumSeq) : that.consortiumSeq != null)
            return false;
        if (localAgentSeq != null ? !localAgentSeq.equals(that.localAgentSeq) : that.localAgentSeq != null)
            return false;
        if (prefix != null ? !prefix.equals(that.prefix) : that.prefix != null) return false;
        if (referenceNumber != null ? !referenceNumber.equals(that.referenceNumber) : that.referenceNumber != null)
            return false;
        if (currencySeq != null ? !currencySeq.equals(that.currencySeq) : that.currencySeq != null) return false;
        if (countrySeq != null ? !countrySeq.equals(that.countrySeq) : that.countrySeq != null) return false;
        if (addressBookSeq != null ? !addressBookSeq.equals(that.addressBookSeq) : that.addressBookSeq != null)
            return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(that.lastModifiedBy) : that.lastModifiedBy != null)
            return false;
        if (lsatModifiedDate != null ? !lsatModifiedDate.equals(that.lsatModifiedDate) : that.lsatModifiedDate != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = freightLineSeq != null ? freightLineSeq.hashCode() : 0;
        result = 31 * result + (freightLineName != null ? freightLineName.hashCode() : 0);
        result = 31 * result + (consortiumSeq != null ? consortiumSeq.hashCode() : 0);
        result = 31 * result + (localAgentSeq != null ? localAgentSeq.hashCode() : 0);
        result = 31 * result + (prefix != null ? prefix.hashCode() : 0);
        result = 31 * result + (referenceNumber != null ? referenceNumber.hashCode() : 0);
        result = 31 * result + (currencySeq != null ? currencySeq.hashCode() : 0);
        result = 31 * result + (countrySeq != null ? countrySeq.hashCode() : 0);
        result = 31 * result + (addressBookSeq != null ? addressBookSeq.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lsatModifiedDate != null ? lsatModifiedDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
