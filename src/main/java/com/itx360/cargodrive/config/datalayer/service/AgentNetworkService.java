package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.AgentNetwork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Sachithrac on 10/7/2016.
 */
@Repository
public interface AgentNetworkService extends JpaRepository<AgentNetwork,Integer> {

    List<AgentNetwork> findByAgentNetworkCodeContainingIgnoreCase(String agentNetworkCode);

    List<AgentNetwork> findByAgentNetworkNameContainingIgnoreCase(String agentNetworkName);

    List<AgentNetwork> findByAgentNetworkCodeContainingIgnoreCaseAndAgentNetworkNameContainingIgnoreCase(String agentNetworkCode,String agentNetworkName);
}
