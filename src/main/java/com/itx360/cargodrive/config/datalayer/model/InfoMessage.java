package com.itx360.cargodrive.config.datalayer.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Udaya-Ehl on 1/8/16.
 */
@Entity
@Table(name = "INFO_MESSAGE")
public class InfoMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "INFO_MESSAGE_SEQ", allocationSize = 1)
    @Column(name = "INFO_MESSAGE_SEQ", unique = true)
    private Integer infoMessageSeq;

    @Column(name = "LINK_SEQ")
    private Integer linkSeq;

    @Column(name = "KEY_NAME")
    private String keyName;

    @Column(name = "MESSAGE_VALUE")
    private String messageValue;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "STATUS")
    private Integer status;

    public Integer getInfoMessageSeq() {
        return infoMessageSeq;
    }

    public void setInfoMessageSeq(Integer infoMessageSeq) {
        this.infoMessageSeq = infoMessageSeq;
    }

    public void setLinkSeq(Integer linkSeq) {
        this.linkSeq = linkSeq;
    }

    public Integer getLinkSeq() {
        return linkSeq;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setMessageValue(String messageValue) {
        this.messageValue = messageValue;
    }

    public String getMessageValue() {
        return messageValue;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }
}
