package com.itx360.cargodrive.config.datalayer.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Thilangaj on 10/4/2016 12:58 PM).
 */
@Entity
@Table(name = "TAX_TYPE", schema = "NEWCARGODRIVE", catalog = "")
public class TaxType {
    private Integer taxTypeSeq;
    private String taxTypeName;
    private String remarks;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer status;

    @Id
    @Column(name = "TAX_TYPE_SEQ", nullable = false, precision = 0)
    public Integer getTaxTypeSeq() {
        return taxTypeSeq;
    }

    public void setTaxTypeSeq(Integer taxTypeSeq) {
        this.taxTypeSeq = taxTypeSeq;
    }

    @Basic
    @Column(name = "TAX_TYPE_NAME", nullable = false, length = 50)
    public String getTaxTypeName() {
        return taxTypeName;
    }

    public void setTaxTypeName(String taxTypeName) {
        this.taxTypeName = taxTypeName;
    }

    @Basic
    @Column(name = "REMARKS", nullable = true, length = 150)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaxType taxType = (TaxType) o;

        if (taxTypeSeq != null ? !taxTypeSeq.equals(taxType.taxTypeSeq) : taxType.taxTypeSeq != null) return false;
        if (taxTypeName != null ? !taxTypeName.equals(taxType.taxTypeName) : taxType.taxTypeName != null) return false;
        if (remarks != null ? !remarks.equals(taxType.remarks) : taxType.remarks != null) return false;
        if (createdBy != null ? !createdBy.equals(taxType.createdBy) : taxType.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(taxType.createdDate) : taxType.createdDate != null) return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(taxType.lastModifiedBy) : taxType.lastModifiedBy != null)
            return false;
        if (lastModifiedDate != null ? !lastModifiedDate.equals(taxType.lastModifiedDate) : taxType.lastModifiedDate != null)
            return false;
        if (status != null ? !status.equals(taxType.status) : taxType.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taxTypeSeq != null ? taxTypeSeq.hashCode() : 0;
        result = 31 * result + (taxTypeName != null ? taxTypeName.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
