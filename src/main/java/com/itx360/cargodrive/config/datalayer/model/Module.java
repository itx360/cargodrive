package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Thilanga-Ehl on 8/31/2016 1:05 PM).
 */
@Entity
@Table(name = "MODULES", schema = "NEWCARGODRIVE", catalog = "")
public class Module {
    private Integer moduleSeq;
    private String moduleName;
    private String description;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private String urlPattern;
    private Integer financeEnabled;

    private List<SubModule> subModuleList;
    private Set<CompanyModule> companyModules = new HashSet<CompanyModule>();

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "MODULE_SEQ", allocationSize = 1)
    @Column(name = "MODULE_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getModuleSeq() {
        return moduleSeq;
    }

    public void setModuleSeq(Integer moduleSeq) {
        this.moduleSeq = moduleSeq;
    }

    @Basic
    @Column(name = "MODULE_NAME", nullable = true, length = 100)
    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY", nullable = true, length = 50)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFIED_DATE", nullable = true)
    public Date getModifiedDate() {
        return modifiedDate;
    }

    @Basic
    @Column(name = "URL_PATTERN", nullable = false)
    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "FINANCE_ENABLED", nullable = true, precision = 0)
    public Integer getFinanceEnabled() {
        return financeEnabled;
    }

    public void setFinanceEnabled(Integer financeEnabled) {
        this.financeEnabled = financeEnabled;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODULE_SEQ", insertable = false, updatable = false)
    @JsonIgnore
    public List<SubModule> getSubModuleList() {
        return subModuleList;
    }

    public void setSubModuleList(List<SubModule> subModuleList) {
        this.subModuleList = subModuleList;
    }

    @OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "MODULE_SEQ",insertable = false, updatable = false, nullable = false)
    public Set<CompanyModule> getCompanyModules() {
        return companyModules;
    }

    public void setCompanyModules(Set<CompanyModule> companyModules) {
        this.companyModules = companyModules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Module)) return false;

        Module module = (Module) o;

        if (getModuleSeq() != null ? !getModuleSeq().equals(module.getModuleSeq()) : module.getModuleSeq() != null)
            return false;
        if (getModuleName() != null ? !getModuleName().equals(module.getModuleName()) : module.getModuleName() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(module.getDescription()) : module.getDescription() != null)
            return false;
        if (getStatus() != null ? !getStatus().equals(module.getStatus()) : module.getStatus() != null) return false;
        if (getCreatedBy() != null ? !getCreatedBy().equals(module.getCreatedBy()) : module.getCreatedBy() != null)
            return false;
        if (getCreatedDate() != null ? !getCreatedDate().equals(module.getCreatedDate()) : module.getCreatedDate() != null)
            return false;
        if (getModifiedBy() != null ? !getModifiedBy().equals(module.getModifiedBy()) : module.getModifiedBy() != null)
            return false;
        if (getModifiedDate() != null ? !getModifiedDate().equals(module.getModifiedDate()) : module.getModifiedDate() != null)
            return false;
        if (getUrlPattern() != null ? !getUrlPattern().equals(module.getUrlPattern()) : module.getUrlPattern() != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = getModuleSeq() != null ? getModuleSeq().hashCode() : 0;
        result = 31 * result + (getModuleName() != null ? getModuleName().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getCreatedBy() != null ? getCreatedBy().hashCode() : 0);
        result = 31 * result + (getCreatedDate() != null ? getCreatedDate().hashCode() : 0);
        result = 31 * result + (getModifiedBy() != null ? getModifiedBy().hashCode() : 0);
        result = 31 * result + (getModifiedDate() != null ? getModifiedDate().hashCode() : 0);
        result = 31 * result + (getUrlPattern()!= null ? getUrlPattern().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Module{" +
                "moduleSeq=" + moduleSeq +
                ", moduleName='" + moduleName + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", modifiedDate=" + modifiedDate +
                ", urlPattern='" + urlPattern + '\'' +
                ", subModuleList=" + subModuleList +
                '}';
    }
}
