package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Harshaa on 9/20/2016.
 */
@Entity
@Table(name = "PORTS", schema = "NEWCARGODRIVE")
public class Port {

    private Integer portSeq;
    private String portName;
    private String portCode;
    private Integer countrySeq;
    private Integer locationSeq;
    private Integer tradeLaneSeq;
    private Integer transportMode;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;

    private Location location;
    private TradeLane tradeLane;

//    @Transient
//    private String statusDesc = MasterDataStatus.values()[];

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "PORT_SEQ", allocationSize = 1)
    @Column(name = "PORT_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getPortSeq() {
        return portSeq;
    }

    public void setPortSeq(Integer portSeq) {
        this.portSeq = portSeq;
    }

    @Basic
    @Column(name = "PORT_NAME", nullable = false, length = 512)
    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    @Basic
    @Column(name = "PORT_CODE", nullable = false, length = 3, unique = true)
    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    @Basic
    @Column(name = "COUNTRY_SEQ", nullable = false)
    public Integer getCountrySeq() {
        return countrySeq;
    }

    public void setCountrySeq(Integer countrySeq) {
        this.countrySeq = countrySeq;
    }

    @Basic
    @Column(name = "LOCATION_SEQ", nullable = false)
    public Integer getLocationSeq() {
        return locationSeq;
    }

    public void setLocationSeq(Integer locationSeq) {
        this.locationSeq = locationSeq;
    }

    @Basic
    @Column(name = "TRADE_LANE_SEQ")
    public Integer getTradeLaneSeq() {
        return tradeLaneSeq;
    }

    public void setTradeLaneSeq(Integer tradeLaneSeq) {
        this.tradeLaneSeq = tradeLaneSeq;
    }

    @Basic
    @Column(name = "TRANSPORT_MODE", length = 128)
    public Integer getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(Integer transportMode) {
        this.transportMode = transportMode;
    }

    @Basic
    @Column(name = "STATUS", nullable = false)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY", nullable = false, length = 128)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "LOCATION_SEQ", insertable = false, updatable = false)
    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TRADE_LANE_SEQ", insertable = false, updatable = false)
    public TradeLane getTradeLane() {
        return this.tradeLane;
    }

    public void setTradeLane(TradeLane tradeLane) {
        this.tradeLane = tradeLane;
    }


//    public void setStatus(MasterDataStatus statusDesc) {
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Port that = (Port) o;

        if (portSeq != that.getPortSeq()) return false;
        if (status != that.getStatus()) return false;
        if (portName != null ? !portName.equals(that.getPortName()) : that.getPortName() != null) return false;
        if (portCode != null ? !portCode.equals(that.getPortCode()) : that.getPortCode() != null) return false;
        if (transportMode != null ? !transportMode.equals(that.getTransportMode()) : that.getTransportMode() != null)
            return false;
        if (createdBy != null ? !createdBy.equals(that.getCreatedBy()) : that.getCreatedBy() != null) return false;
        if (createdDate != null ? !createdDate.equals(that.getCreatedDate()) : that.getCreatedDate() != null)
            return false;
        if (modifiedBy != null ? !modifiedBy.equals(that.getModifiedBy()) : that.getModifiedBy() != null) return false;
        if (modifiedDate != null ? !modifiedDate.equals(that.getModifiedDate()) : that.getModifiedDate() != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (portSeq ^ (portSeq >>> 32));
        result = 31 * result + (portName != null ? portName.hashCode() : 0);
        result = 31 * result + (portCode != null ? portCode.hashCode() : 0);
        result = 31 * result + (transportMode != null ? transportMode.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (modifiedBy != null ? modifiedBy.hashCode() : 0);
        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result + (int) (status ^ (status >>> 32));
        return result;
    }
}
