package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Charge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by shanakajay on 9/27/2016.
 */
@Repository
public interface ChargeService extends JpaRepository<Charge, Integer> {
    Charge findByChargeName(String chargeName);

    List<Charge> findByChargeNameContainingIgnoreCase(String chargeName);

    List<Charge> findByDescriptionContainingIgnoreCase(String description);

    List<Charge> findByChargeNameContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String chargeName,String description);

}
