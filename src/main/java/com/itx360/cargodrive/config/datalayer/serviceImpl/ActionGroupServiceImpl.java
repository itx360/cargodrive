package com.itx360.cargodrive.config.datalayer.serviceImpl;

import com.itx360.cargodrive.config.datalayer.service.ActionGroupService;
import com.itx360.cargodrive.config.datalayer.service.GroupService;
import org.springframework.stereotype.Repository;

/**
 * Created by Sachithrac on 9/1/2016.
 */
@Repository
public abstract class ActionGroupServiceImpl implements ActionGroupService{
}
