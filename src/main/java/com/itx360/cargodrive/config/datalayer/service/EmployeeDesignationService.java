package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.EmployeeDesignation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Sachithrac on 10/4/2016.
 */
@Repository
public interface EmployeeDesignationService extends JpaRepository<EmployeeDesignation, Integer> {
}
