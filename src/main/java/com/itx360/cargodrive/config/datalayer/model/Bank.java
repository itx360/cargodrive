package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

/**
 * Created by shanakajay on 9/26/2016.
 */
@Entity
public class Bank {
    private Integer bankSeq;
    private String bankCode;
    private Integer slipsCode;
    private String bankName;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "BANK_SEQ", allocationSize = 1)
    @Column(name = "BANK_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getBankSeq() {
        return bankSeq;
    }

    public void setBankSeq(Integer bankSeq) {
        this.bankSeq = bankSeq;
    }

    @Basic
    @Column(name = "BANK_CODE", nullable = false, unique = true, length = 10)
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "SLIPS_CODE", nullable = false, unique = true, precision = 0)
    public Integer getSlipsCode() {
        return slipsCode;
    }

    public void setSlipsCode(Integer slipsCode) {
        this.slipsCode = slipsCode;
    }

    @Basic
    @Column(name = "BANK_NAME", nullable = false, unique = true, length = 200)
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bank bank = (Bank) o;

        if (bankSeq != bank.bankSeq) return false;
        if (bankCode != null ? !bankCode.equals(bank.bankCode) : bank.bankCode != null) return false;
        if (slipsCode != null ? !slipsCode.equals(bank.slipsCode) : bank.slipsCode != null) return false;
        if (bankName != null ? !bankName.equals(bank.bankName) : bank.bankName != null) return false;
        if (status != null ? !status.equals(bank.status) : bank.status != null) return false;
        if (createdBy != null ? !createdBy.equals(bank.createdBy) : bank.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(bank.createdDate) : bank.createdDate != null) return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(bank.lastModifiedBy) : bank.lastModifiedBy != null)
            return false;
        if (lastModifiedDate != null ? !lastModifiedDate.equals(bank.lastModifiedDate) : bank.lastModifiedDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (bankSeq ^ (bankSeq >>> 32));
        result = 31 * result + (bankCode != null ? bankCode.hashCode() : 0);
        result = 31 * result + (slipsCode != null ? slipsCode.hashCode() : 0);
        result = 31 * result + (bankName != null ? bankName.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        return result;
    }
}
