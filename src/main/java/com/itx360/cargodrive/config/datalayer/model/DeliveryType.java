package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by shanakajay on 10/6/2016.
 */
@Entity
@Table(name = "DELIVERY_TYPE", schema = "NEWCARGODRIVE", catalog = "")
public class DeliveryType {
    private Integer deliveryTypeSeq;
    private String deliveryTypeCode;
    private String description;
    private String deliveryTypeMode;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "CHARGE_SEQ", allocationSize = 1)
    @Column(name = "DELIVERY_TYPE_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getDeliveryTypeSeq() {
        return deliveryTypeSeq;
    }

    public void setDeliveryTypeSeq(Integer deliveryTypeSeq) {
        this.deliveryTypeSeq = deliveryTypeSeq;
    }

    @Basic
    @Column(name = "DELIVERY_TYPE_CODE", nullable = false, length = 20)
    public String getDeliveryTypeCode() {
        return deliveryTypeCode;
    }

    public void setDeliveryTypeCode(String deliveryTypeCode) {
        this.deliveryTypeCode = deliveryTypeCode;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = false, length = 50)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "DELIVERY_TYPE_MODE", nullable = false, length = 20)
    public String getDeliveryTypeMode() {
        return deliveryTypeMode;
    }

    public void setDeliveryTypeMode(String deliveryTypeMode) {
        this.deliveryTypeMode = deliveryTypeMode;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeliveryType that = (DeliveryType) o;

        if (deliveryTypeSeq != that.deliveryTypeSeq) return false;
        if (status != that.status) return false;
        if (deliveryTypeCode != null ? !deliveryTypeCode.equals(that.deliveryTypeCode) : that.deliveryTypeCode != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (deliveryTypeMode != null ? !deliveryTypeMode.equals(that.deliveryTypeMode) : that.deliveryTypeMode != null)
            return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(that.lastModifiedBy) : that.lastModifiedBy != null)
            return false;
        if (lastModifiedDate != null ? !lastModifiedDate.equals(that.lastModifiedDate) : that.lastModifiedDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deliveryTypeSeq ^ (deliveryTypeSeq >>> 32));
        result = 31 * result + (deliveryTypeCode != null ? deliveryTypeCode.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (deliveryTypeMode != null ? deliveryTypeMode.hashCode() : 0);
        result = 31 * result + (int) (status ^ (status >>> 32));
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        return result;
    }
}
