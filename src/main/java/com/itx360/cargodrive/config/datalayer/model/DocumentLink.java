package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilanga-Ehl on 8/31/2016 1:05 PM).
 */
@Entity
@Table(name = "DOCUMENT_LINKS", schema = "NEWCARGODRIVE", catalog = "")
public class DocumentLink {
    private Integer documentLinkSeq;
    private String linkName;
    private String icon;
    private String pageName;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private Integer status;
    private Integer moduleSeq;
    private Integer subModuleSeq;
    private Integer actionGroupSeq;
    private List<Authority> authorities = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "DOCUMENT_LINK_SEQ", allocationSize = 1)
    @Column(name = "DOCUMENT_LINK_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getDocumentLinkSeq() {
        return documentLinkSeq;
    }

    public void setDocumentLinkSeq(Integer documentLinkSeq) {
        this.documentLinkSeq = documentLinkSeq;
    }

    @Basic
    @Column(name = "LINK_NAME", nullable = false, length = 200)
    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    @Basic
    @Column(name = "PAGE_NAME", nullable = false, length = 200)
    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY", nullable = true, length = 50)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFIED_DATE", nullable = true)
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Column(name = "MODULE_SEQ", nullable = false)
    public Integer getModuleSeq() {
        return moduleSeq;
    }

    public void setModuleSeq(Integer moduleSeq) {
        this.moduleSeq = moduleSeq;
    }

    @Basic
    @Column(name = "SUB_MODULE_SEQ", nullable = false)
    public Integer getSubModuleSeq() {
        return subModuleSeq;
    }

    public void setSubModuleSeq(Integer subModuleSeq) {
        this.subModuleSeq = subModuleSeq;
    }

    @Basic
    @Column(name = "ACTION_GROUP_SEQ", nullable = false)
    public Integer getActionGroupSeq() {
        return actionGroupSeq;
    }

    public void setActionGroupSeq(Integer actionGroupSeq) {
        this.actionGroupSeq = actionGroupSeq;
    }

    @Basic
    @Column(name = "ICON", nullable = false, length = 50)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "DOCUMENT_LINK_SEQ", nullable = false)
    @JsonIgnore
    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocumentLink that = (DocumentLink) o;

        if (documentLinkSeq != null ? !documentLinkSeq.equals(that.documentLinkSeq) : that.documentLinkSeq != null)
            return false;
        if (linkName != null ? !linkName.equals(that.linkName) : that.linkName != null) return false;
        if (icon != null ? !icon.equals(that.icon) : that.icon != null) return false;
        if (pageName != null ? !pageName.equals(that.pageName) : that.pageName != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (modifiedBy != null ? !modifiedBy.equals(that.modifiedBy) : that.modifiedBy != null) return false;
        if (modifiedDate != null ? !modifiedDate.equals(that.modifiedDate) : that.modifiedDate != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (moduleSeq != null ? !moduleSeq.equals(that.moduleSeq) : that.moduleSeq != null)
            return false;
        if (subModuleSeq != null ? !subModuleSeq.equals(that.subModuleSeq) : that.subModuleSeq != null)
            return false;
        if (actionGroupSeq != null ? !actionGroupSeq.equals(that.actionGroupSeq) : that.actionGroupSeq != null)
            return false;


        return true;
    }

    @Override
    public int hashCode() {
        int result = documentLinkSeq != null ? documentLinkSeq.hashCode() : 0;
        result = 31 * result + (linkName != null ? linkName.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (pageName != null ? pageName.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (modifiedBy != null ? modifiedBy.hashCode() : 0);
        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (moduleSeq != null ? moduleSeq.hashCode() : 0);
        result = 31 * result + (subModuleSeq != null ? subModuleSeq.hashCode() : 0);
        result = 31 * result + (actionGroupSeq != null ? actionGroupSeq.hashCode() : 0);

        return result;
    }

    @Override
    public String toString() {
        return "DocumentLink{" +
                "documentLinkSeq=" + documentLinkSeq +
                ", linkName='" + linkName + '\'' +
                ", description='" + pageName + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", modifiedDate=" + modifiedDate +
                ", status=" + status +
                ", moduleSeq=" + moduleSeq +
                ", subModuleSeq=" + subModuleSeq +
                ", actionGroupSeq=" + actionGroupSeq +
                ", authorities=" + authorities +
                '}';
    }
}
