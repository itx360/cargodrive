package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Sachithrac on 10/4/2016.
 */
@Repository
public interface EmployeeService extends JpaRepository<Employee, Integer> {

    List<Employee> findByEmployeeNameContainingIgnoreCase(String employeeName);

    List<Employee> findByStatus(Integer status);

    List<Employee> findByEmployeeDesignationSeq(Integer employeeDesignationSeq);

    List<Employee> findByEmployeeNameContainingIgnoreCaseAndStatus(String employeeName,Integer status);

    List<Employee> findByEmployeeNameContainingIgnoreCaseAndEmployeeDesignationSeq(String employeeName,Integer employeeDesignationSeq);

    List<Employee> findByStatusAndEmployeeDesignationSeq(Integer status,Integer employeeDesignationSeq);

    List<Employee> findByEmployeeNameContainingIgnoreCaseAndStatusAndEmployeeDesignationSeq(String employeeName,Integer status,Integer employeeDesignationSeq);
}
