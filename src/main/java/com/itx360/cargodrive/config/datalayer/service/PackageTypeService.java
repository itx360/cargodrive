package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.PackageType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/4/2016
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface PackageTypeService extends JpaRepository<PackageType, Integer> {
    List<PackageType> findByPackageTypeNameContainingIgnoreCase(String packageType);

    List<PackageType> findByPackageTypeNameContainingIgnoreCaseAndPackageTypeCodeContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String packageTypeName,
                                                                                                                                        String packageCode,
                                                                                                                                        String description);
}
