package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.CustomerGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Sachithrac on 9/28/2016.
 */
@Repository
public interface CustomerGroupService extends JpaRepository<CustomerGroup,Integer> {

    CustomerGroup findByCustomerGroupNameIgnoreCase(String customerGroupName);

    List<CustomerGroup> findByCustomerGroupCodeContainingIgnoreCase(String customerGroupCode);

    List<CustomerGroup> findByCustomerGroupNameContainingIgnoreCase(String customerGroupName);

    List<CustomerGroup> findByCustomerGroupCodeContainingIgnoreCaseAndCustomerGroupNameContainingIgnoreCase(String customerGroupCode,String customerGroupName);
}
