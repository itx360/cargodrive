package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sachithrac on 9/26/2016.
 */
@Entity
@Table(name = "ADDRESS_BOOK", schema = "NEWCARGODRIVE", catalog = "")
public class AddressBook {
    private Integer addressBookSeq;
    private String firstName;
    private Integer countrySeq;
    private String middleInitials;
    private String lastName;
    private String title;
    private String designation;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String fax;
    private String email;
    private String telephone;
    private String mobile;
    private String pager;
    private String telephoneExtension;
    private String website;
    private String epfNo;
    private String nicNo;
    private String altEMail;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private String chaLicenseNo;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "ADDRESS_BOOK_SEQ", allocationSize = 1)
    @Column(name = "ADDRESS_BOOK_SEQ",nullable = false, precision = 0, unique = true)
    public Integer getAddressBookSeq() {
        return addressBookSeq;
    }

    public void setAddressBookSeq(Integer addressBookSeq) {
        this.addressBookSeq = addressBookSeq;
    }

    @Basic
    @Column(name = "FIRST_NAME")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "COUNTRY_SEQ",nullable = true)
    public Integer getCountrySeq() {
        return countrySeq;
    }

    public void setCountrySeq(Integer countrySeq) {
        this.countrySeq = countrySeq;
    }


    @Basic
    @Column(name = "MIDDLE_INITIALS")
    public String getMiddleInitials() {
        return middleInitials;
    }

    public void setMiddleInitials(String middleInitials) {
        this.middleInitials = middleInitials;
    }

    @Basic
    @Column(name = "LAST_NAME")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "DESIGNATION")
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Basic
    @Column(name = "ADDRESS1")
    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Basic
    @Column(name = "ADDRESS2")
    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Basic
    @Column(name = "CITY")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "STATE")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "ZIP",length = 10)
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Basic
    @Column(name = "FAX")
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Basic
    @Column(name = "E_MAIL")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "TELEPHONE")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "MOBILE")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "PAGER")
    public String getPager() {
        return pager;
    }

    public void setPager(String pager) {
        this.pager = pager;
    }

    @Basic
    @Column(name = "TELEPHONE_EXTENSION")
    public String getTelephoneExtension() {
        return telephoneExtension;
    }

    public void setTelephoneExtension(String telephoneExtension) {
        this.telephoneExtension = telephoneExtension;
    }

    @Basic
    @Column(name = "WEBSITE")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Basic
    @Column(name = "EPF_NO")
    public String getEpfNo() {
        return epfNo;
    }

    public void setEpfNo(String epfNo) {
        this.epfNo = epfNo;
    }

    @Basic
    @Column(name = "NIC_NO")
    public String getNicNo() {
        return nicNo;
    }

    public void setNicNo(String nicNo) {
        this.nicNo = nicNo;
    }

    @Basic
    @Column(name = "ALT_E_MAIL")
    public String getAltEMail() {
        return altEMail;
    }

    public void setAltEMail(String altEMail) {
        this.altEMail = altEMail;
    }


    @Basic
    @Column(name = "CREATED_BY", length = 50, updatable = false,nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true, updatable = false,unique = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY",length = 50,nullable = false)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "CHA_LICENSE_NO",length = 50,nullable = true)
    public String getChaLicenseNo() {
        return chaLicenseNo;
    }

    public void setChaLicenseNo(String chaLicenseNo) {
        this.chaLicenseNo = chaLicenseNo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressBook)) return false;

        AddressBook that = (AddressBook) o;

        if (!getAddressBookSeq().equals(that.getAddressBookSeq())) return false;
        if (!getCountrySeq().equals(that.getCountrySeq())) return false;
        if (!getAddress1().equals(that.getAddress1())) return false;
        if (!getAddress2().equals(that.getAddress2())) return false;
        if (!getCity().equals(that.getCity())) return false;
        if (!getState().equals(that.getState())) return false;
        if (!getZip().equals(that.getZip())) return false;
        if (!getFax().equals(that.getFax())) return false;
        if (!getEmail().equals(that.getEmail())) return false;
        if (!getTelephone().equals(that.getTelephone())) return false;
        if (!getMobile().equals(that.getMobile())) return false;
        if (!getTelephoneExtension().equals(that.getTelephoneExtension())) return false;
        return getWebsite().equals(that.getWebsite());

    }

    @Override
    public int hashCode() {
        int result = getAddressBookSeq().hashCode();
        result = 31 * result + getCountrySeq().hashCode();
        result = 31 * result + getAddress1().hashCode();
        result = 31 * result + getAddress2().hashCode();
        result = 31 * result + getCity().hashCode();
        result = 31 * result + getState().hashCode();
        result = 31 * result + getZip().hashCode();
        result = 31 * result + getFax().hashCode();
        result = 31 * result + getEmail().hashCode();
        result = 31 * result + getTelephone().hashCode();
        result = 31 * result + getMobile().hashCode();
        result = 31 * result + getTelephoneExtension().hashCode();
        result = 31 * result + getWebsite().hashCode();
        return result;
    }
}
