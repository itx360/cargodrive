package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.HsCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 9:20 PM).
 */
@Repository
public interface HsCodeService extends JpaRepository<HsCode, Integer> {
    HsCode findByHsCodeName(String hsCodeName);

    List<HsCode> findByDescriptionContainingIgnoreCase(String description);

    List<HsCode> findByHsCategorySeq(Integer hsCategorySeq);

    List<HsCode> findByHsCategorySeqAndDescriptionContainingIgnoreCase(Integer hsCategorySeq, String description);

    List<HsCode> findByHsCodeNameContainingIgnoreCase(String hsCodeName);

    List<HsCode> findByHsCodeNameContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String hsCodeName, String description);

    List<HsCode> findByHsCategorySeqAndHsCodeNameContainingIgnoreCase(Integer hsCategorySeq, String hsCodeName);

    List<HsCode> findByHsCategorySeqAndHsCodeNameContainingIgnoreCaseAndDescriptionContainingIgnoreCase(Integer hsCategorySeq, String hsCodeName, String description);
}
