package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sachithrac on 10/4/2016.
 */
@Entity
@Table(name = "FINAL_DESTINATION", schema = "NEWCARGODRIVE", catalog = "")
public class FinalDestination {
    private Integer finalDestinationSeq;
    private String finalDestinationCode;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private AddressBook addressBook;
    private Country country;
    private Integer addressBookSeq;
    private Integer countrySeq;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "FINAL_DESTINATION_SEQ", allocationSize = 1)
    @Column(name = "FINAL_DESTINATION_SEQ",unique = true)
    public Integer getFinalDestinationSeq() {
        return finalDestinationSeq;
    }

    public void setFinalDestinationSeq(Integer finalDestinationSeq) {
        this.finalDestinationSeq = finalDestinationSeq;
    }

    @Basic
    @Column(name = "FINAL_DESTINATION_CODE",length = 10,nullable = false)
    public String getFinalDestinationCode() {
        return finalDestinationCode;
    }

    public void setFinalDestinationCode(String finalDestinationCode) {
        this.finalDestinationCode = finalDestinationCode;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ADDRESS_BOOK_SEQ", nullable = false)
    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }

    @Basic
    @Column(name = "ADDRESS_BOOK_SEQ", insertable = false, updatable = false, nullable = false)
    public Integer getAddressBookSeq() {
        return addressBookSeq;
    }

    public void setAddressBookSeq(Integer addressBookSeq) {
        this.addressBookSeq = addressBookSeq;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "COUNTRY_SEQ",insertable = false, updatable = false, nullable = false)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Basic
    @Column(name = "COUNTRY_SEQ", nullable = false)
    public Integer getCountrySeq() {
        return countrySeq;
    }

    public void setCountrySeq(Integer countrySeq) {
        this.countrySeq = countrySeq;
    }

    @Basic
    @Column(name = "STATUS", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    @Basic
    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinalDestination that = (FinalDestination) o;

        if (finalDestinationSeq != that.finalDestinationSeq) return false;
        if (finalDestinationCode != null ? !finalDestinationCode.equals(that.finalDestinationCode) : that.finalDestinationCode != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (finalDestinationSeq ^ (finalDestinationSeq >>> 32));
        result = 31 * result + (finalDestinationCode != null ? finalDestinationCode.hashCode() : 0);
        return result;
    }
}
