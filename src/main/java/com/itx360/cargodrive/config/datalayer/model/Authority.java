package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Thilanga-Ehl on 8/31/2016 1:05 PM).
 */
@Entity
@Table(name = "AUTHORITIES", schema = "NEWCARGODRIVE", catalog = "")
public class Authority {
    private Integer authoritySeq;
    private Integer documentLinkSeq;
    private String authority;
    private String createdBy;
    private Date createdDate;
    private DocumentLink documentLink;
    private Set<Group> groups;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "AUTHORITY_SEQ", allocationSize = 1)
    @Column(name = "AUTHORITY_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getAuthoritySeq() {
        return authoritySeq;
    }

    public void setAuthoritySeq(Integer authoritySeq) {
        this.authoritySeq = authoritySeq;
    }

    @Basic
    @Column(name = "AUTHORITY", nullable = false, length = 200)
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "DOCUMENT_LINK_SEQ", updatable = false, insertable = false, nullable = false)
    public Integer getDocumentLinkSeq() {
        return documentLinkSeq;
    }

    public void setDocumentLinkSeq(Integer documentLinkSeq) {
        this.documentLinkSeq = documentLinkSeq;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DOCUMENT_LINK_SEQ", insertable = false, updatable = false)
    @JsonIgnore
    public DocumentLink getDocumentLink() {
        return documentLink;
    }

    public void setDocumentLink(DocumentLink documentLink) {
        this.documentLink = documentLink;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "authorities")
    @JsonIgnore
    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Authority)) return false;

        Authority authority1 = (Authority) o;

        if (getAuthoritySeq() != null ? !getAuthoritySeq().equals(authority1.getAuthoritySeq()) : authority1.getAuthoritySeq() != null)
            return false;
        if (getDocumentLinkSeq() != null ? !getDocumentLinkSeq().equals(authority1.getDocumentLinkSeq()) : authority1.getDocumentLinkSeq() != null)
            return false;
        if (getAuthority() != null ? !getAuthority().equals(authority1.getAuthority()) : authority1.getAuthority() != null)
            return false;
        if (getCreatedBy() != null ? !getCreatedBy().equals(authority1.getCreatedBy()) : authority1.getCreatedBy() != null)
            return false;
        if (getCreatedDate() != null ? !getCreatedDate().equals(authority1.getCreatedDate()) : authority1.getCreatedDate() != null)
            return false;
        return getGroups() != null ? getGroups().equals(authority1.getGroups()) : authority1.getGroups() == null;

    }

    @Override
    public int hashCode() {
        int result = getAuthoritySeq() != null ? getAuthoritySeq().hashCode() : 0;
        result = 31 * result + (getDocumentLinkSeq() != null ? getDocumentLinkSeq().hashCode() : 0);
        result = 31 * result + (getAuthority() != null ? getAuthority().hashCode() : 0);
        result = 31 * result + (getCreatedBy() != null ? getCreatedBy().hashCode() : 0);
        result = 31 * result + (getCreatedDate() != null ? getCreatedDate().hashCode() : 0);
        result = 31 * result + (getGroups() != null ? getGroups().hashCode() : 0);
        return result;
    }
}
