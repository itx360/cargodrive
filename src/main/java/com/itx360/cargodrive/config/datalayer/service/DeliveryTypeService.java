package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.DeliveryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by shanakajay on 10/5/2016.
 */
@Repository
public interface DeliveryTypeService extends JpaRepository<DeliveryType, Integer> {

    DeliveryType findByDeliveryTypeCode(String deliveryTypeCode);

    List<DeliveryType> findByDeliveryTypeCodeContainingIgnoreCase(String deliveryTypeCode);

    List<DeliveryType> findByDescriptionContainingIgnoreCase(String description);

    List<DeliveryType> findByDeliveryTypeModeContainingIgnoreCase(String deliveryTypeMode);

    List<DeliveryType> findByDescriptionContainingIgnoreCaseAndDeliveryTypeModeContainingIgnoreCase(String description, String deliveryTypeMode);

    List<DeliveryType> findByDeliveryTypeCodeContainingIgnoreCaseAndDeliveryTypeModeContainingIgnoreCase(String deliveryTypeCode, String deliveryTypeMode);

    List<DeliveryType> findByDeliveryTypeCodeContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String deliveryTypeCode, String description);

    List<DeliveryType> findByDeliveryTypeCodeContainingIgnoreCaseAndDescriptionContainingIgnoreCaseAndDeliveryTypeModeContainingIgnoreCase(String deliveryTypeCode, String description, String deliveryTypeMode);

}
