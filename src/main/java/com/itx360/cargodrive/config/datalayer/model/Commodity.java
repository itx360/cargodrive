package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Thilangaj on 9/26/2016 9:14 PM).
 */
@Entity
@Table(name = "COMMODITY", schema = "NEWCARGODRIVE", catalog = "")
public class Commodity {
    private Integer commoditySeq;
    private Integer commodityTypeSeq;
    private Integer hsCodeSeq;
    private String description;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer status;

    private CommodityType commodityType;
    private HsCode hsCode;

    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "COMMODITY_SEQ", allocationSize = 1)
    @Column(name = "COMMODITY_SEQ", nullable = false, precision = 0)
    public Integer getCommoditySeq() {
        return commoditySeq;
    }

    public void setCommoditySeq(Integer commoditySeq) {
        this.commoditySeq = commoditySeq;
    }

    @Basic
    @Column(name = "COMMODITY_TYPE_SEQ", nullable = false, precision = 0)
    public Integer getCommodityTypeSeq() {
        return commodityTypeSeq;
    }

    public void setCommodityTypeSeq(Integer commodityTypeSeq) {
        this.commodityTypeSeq = commodityTypeSeq;
    }

    @Basic
    @Column(name = "HS_CODE_SEQ", nullable = false, precision = 0)
    public Integer getHsCodeSeq() {
        return hsCodeSeq;
    }

    public void setHsCodeSeq(Integer hsCodeSeq) {
        this.hsCodeSeq = hsCodeSeq;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = false, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.toUpperCase();
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Commodity commodity = (Commodity) o;

        if (commoditySeq != null ? !commoditySeq.equals(commodity.commoditySeq) : commodity.commoditySeq != null)
            return false;
        if (commodityTypeSeq != null ? !commodityTypeSeq.equals(commodity.commodityTypeSeq) : commodity.commodityTypeSeq != null)
            return false;
        if (hsCodeSeq != null ? !hsCodeSeq.equals(commodity.hsCodeSeq) : commodity.hsCodeSeq != null) return false;
        if (description != null ? !description.equals(commodity.description) : commodity.description != null)
            return false;
        if (createdBy != null ? !createdBy.equals(commodity.createdBy) : commodity.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(commodity.createdDate) : commodity.createdDate != null)
            return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(commodity.lastModifiedBy) : commodity.lastModifiedBy != null)
            return false;
        if (lastModifiedDate != null ? !lastModifiedDate.equals(commodity.lastModifiedDate) : commodity.lastModifiedDate != null)
            return false;
        if (status != null ? !status.equals(commodity.status) : commodity.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = commoditySeq != null ? commoditySeq.hashCode() : 0;
        result = 31 * result + (commodityTypeSeq != null ? commodityTypeSeq.hashCode() : 0);
        result = 31 * result + (hsCodeSeq != null ? hsCodeSeq.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COMMODITY_TYPE_SEQ", insertable = false, updatable = false)
    public CommodityType getCommodityType() {
        return commodityType;
    }

    public void setCommodityType(CommodityType commodityType) {
        this.commodityType = commodityType;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "HS_CODE_SEQ", insertable = false, updatable = false)
    public HsCode getHsCode() {
        return hsCode;
    }

    public void setHsCode(HsCode hsCode) {
        this.hsCode = hsCode;
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
