package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by shanakajay on 9/20/2016.
 */
@Repository
public interface CountryService extends JpaRepository<Country, Integer> {

    Country findByCountryName(String countryName);

    List<Country> findByCountryNameContainingIgnoreCase(String countryName);

    List<Country> findByCountryCodeContainingIgnoreCase(String countryCode);

    List<Country> findByCountryNameContainingIgnoreCaseAndCountryCodeContainingIgnoreCase(String countryName,String countryCode);
}
