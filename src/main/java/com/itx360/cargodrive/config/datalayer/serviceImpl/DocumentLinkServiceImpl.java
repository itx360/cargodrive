package com.itx360.cargodrive.config.datalayer.serviceImpl;

import com.itx360.cargodrive.config.datalayer.model.DocumentLink;
import com.itx360.cargodrive.config.datalayer.model.Group;
import com.itx360.cargodrive.config.datalayer.service.DocumentLinkService;
import com.itx360.cargodrive.config.datalayer.service.GroupService;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Sachithrac on 9/1/2016.
 */
@Repository
public abstract class DocumentLinkServiceImpl implements DocumentLinkService {


}
