package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.GroupAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by shanakajay on 9/14/2016.
 */
@Repository
public interface GroupAuthorityService extends JpaRepository<GroupAuthority, Integer> {
    GroupAuthority findByGroupSeqAndAuthoritySeq(Integer groupSeq, Integer authoritySeq);

    List<GroupAuthority> findAllByGroupSeq(Integer groupSeq);
}
