package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/7/2016
 * Date: 11:11 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "VESSEL", schema = "VESSEL_SEQ", catalog = "")
public class Vessel {
    private Integer vesselSeq;
    private String vesselName;
    private String vesselFlag;
    private Integer freightLineSeq;
    private Integer vesselOperatorSeq;
    private Integer countrySeq;
    private Integer containerOperatorSeq;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer status;
    private FreightLine freightLine;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "VESSEL_SEQ", allocationSize = 1)
    @Column(name = "VESSEL_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getVesselSeq() {
        return vesselSeq;
    }

    public void setVesselSeq(Integer vesselSeq) {
        this.vesselSeq = vesselSeq;
    }

    @Basic
    @Column(name = "VESSEL_NAME", nullable = true, length = 50)
    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    @Basic
    @Column(name = "VESSEL_FLAG", nullable = true, length = 50,unique = true)
    public String getVesselFlag() {
        return vesselFlag;
    }

    public void setVesselFlag(String vesselFlag) {
        this.vesselFlag = vesselFlag;
    }

    @Basic
    @Column(name = "FREIGHT_LINE_SEQ", nullable = true, precision = 0)
    public Integer getFreightLineSeq() {
        return freightLineSeq;
    }

    public void setFreightLineSeq(Integer freightLineSeq) {
        this.freightLineSeq = freightLineSeq;
    }

    @Basic
    @Column(name = "VESSEL_OPERATOR_SEQ", nullable = true, precision = 0)
    public Integer getVesselOperatorSeq() {
        return vesselOperatorSeq;
    }

    public void setVesselOperatorSeq(Integer vesselOperatorSeq) {
        this.vesselOperatorSeq = vesselOperatorSeq;
    }

    @Basic
    @Column(name = "COUNTRY_SEQ", nullable = true, precision = 0)
    public Integer getCountrySeq() {
        return countrySeq;
    }

    public void setCountrySeq(Integer countrySeq) {
        this.countrySeq = countrySeq;
    }

    @Basic
    @Column(name = "CONTAINER_OPERATOR_SEQ", nullable = true, precision = 0)
    public Integer getContainerOperatorSeq() {
        return containerOperatorSeq;
    }

    public void setContainerOperatorSeq(Integer containerOperatorSeq) {
        this.containerOperatorSeq = containerOperatorSeq;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FREIGHT_LINE_SEQ", insertable = false, updatable = false)
    public FreightLine getFreightLine() {
        return freightLine;
    }

    public void setFreightLine(FreightLine freightLine) {
        this.freightLine = freightLine;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vessel vessel = (Vessel) o;
        return vesselSeq == vessel.vesselSeq &&
                Objects.equals(vesselName, vessel.vesselName) &&
                Objects.equals(vesselFlag, vessel.vesselFlag) &&
                Objects.equals(freightLineSeq, vessel.freightLineSeq) &&
                Objects.equals(vesselOperatorSeq, vessel.vesselOperatorSeq) &&
                Objects.equals(countrySeq, vessel.countrySeq) &&
                Objects.equals(containerOperatorSeq, vessel.containerOperatorSeq) &&
                Objects.equals(createdBy, vessel.createdBy) &&
                Objects.equals(createdDate, vessel.createdDate) &&
                Objects.equals(lastModifiedBy, vessel.lastModifiedBy) &&
                Objects.equals(lastModifiedDate, vessel.lastModifiedDate) &&
                Objects.equals(status, vessel.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vesselSeq, vesselName, vesselFlag, freightLineSeq, vesselOperatorSeq, countrySeq, containerOperatorSeq, createdBy, createdDate, lastModifiedBy, lastModifiedDate, status);
    }
}
