package com.itx360.cargodrive.config.datalayer.serviceImpl;

import com.itx360.cargodrive.config.datalayer.model.SubModule;
import com.itx360.cargodrive.config.datalayer.service.ModuleService;
import com.itx360.cargodrive.config.datalayer.service.SubModuleService;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Sachithrac on 9/1/2016.
 */
@Repository
public abstract class SubModuleServiceImpl implements SubModuleService {


}
