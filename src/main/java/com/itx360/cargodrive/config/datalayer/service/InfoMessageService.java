package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.InfoMessage;


import java.util.List;

/**
 * Created by Udaya-Ehl on 1/8/16.
 */
public interface InfoMessageService {

    public InfoMessage getInfoMessageByInfoMessageSeq(Integer infoMessageSeq);

    public List<InfoMessage> getInfoMessagesList();

    public List<InfoMessage> getInfoMessagesListByLinkSeq(Integer linkSeq);

    public boolean save(InfoMessage infoMessage);

    public boolean update(InfoMessage infoMessage);

    public String getInfoMessageValue(String keyName, String viewName);

}
