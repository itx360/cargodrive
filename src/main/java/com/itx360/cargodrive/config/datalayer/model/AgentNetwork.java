package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sachithrac on 10/7/2016.
 */
@Entity
@Table(name = "AGENT_NETWORK", schema = "NEWCARGODRIVE", catalog = "")
public class AgentNetwork {
    private Integer agentNetworkSeq;
    private String agentNetworkName;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private String agentNetworkCode;
    private CompanyProfile companyProfile;
    private Integer companyProfileSeq;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "AGENT_NETWORK_SEQ", allocationSize = 1)
    @Column(name = "AGENT_NETWORK_SEQ",unique = true)
    public Integer getAgentNetworkSeq() {
        return agentNetworkSeq;
    }

    public void setAgentNetworkSeq(Integer agentNetworkSeq) {
        this.agentNetworkSeq = agentNetworkSeq;
    }

    @Basic
    @Column(name = "AGENT_NETWORK_NAME",length = 40,nullable = false)
    public String getAgentNetworkName() {
        return agentNetworkName;
    }

    public void setAgentNetworkName(String agentNetworkName) {
        this.agentNetworkName = agentNetworkName;
    }

    @Basic
    @Column(name = "STATUS")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    @Basic
    @Column(name = "CREATED_DATE", nullable = true, updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY")
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "AGENT_NETWORK_CODE",length = 40,nullable = false)
    public String getAgentNetworkCode() {
        return agentNetworkCode;
    }

    public void setAgentNetworkCode(String agentNetworkCode) {
        this.agentNetworkCode = agentNetworkCode;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "COMPANY_PROFILE_SEQ", insertable = false, updatable = false, nullable = false)
    public CompanyProfile getCompanyProfile() {
        return companyProfile;
    }

    public void setCompanyProfile(CompanyProfile companyProfile) {
        this.companyProfile = companyProfile;
    }

    @Basic
    @Column(name = "COMPANY_PROFILE_SEQ", updatable = false, nullable = false)
    public Integer getCompanyProfileSeq() {
        return companyProfileSeq;
    }

    public void setCompanyProfileSeq(Integer companyProfileSeq) {
        this.companyProfileSeq = companyProfileSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgentNetwork that = (AgentNetwork) o;

        if (agentNetworkSeq != that.agentNetworkSeq) return false;
        if (agentNetworkName != null ? !agentNetworkName.equals(that.agentNetworkName) : that.agentNetworkName != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(that.lastModifiedBy) : that.lastModifiedBy != null)
            return false;
        if (lastModifiedDate != null ? !lastModifiedDate.equals(that.lastModifiedDate) : that.lastModifiedDate != null)
            return false;
        if (agentNetworkCode != null ? !agentNetworkCode.equals(that.agentNetworkCode) : that.agentNetworkCode != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (agentNetworkSeq ^ (agentNetworkSeq >>> 32));
        result = 31 * result + (agentNetworkName != null ? agentNetworkName.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lastModifiedDate != null ? lastModifiedDate.hashCode() : 0);
        result = 31 * result + (agentNetworkCode != null ? agentNetworkCode.hashCode() : 0);
        return result;
    }
}
