package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.ChargeMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by shanakajay on 9/29/2016.
 */
@Repository
public interface ChargeModeService extends JpaRepository<ChargeMode, Integer> {
}
