package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/5/2016
 * Time: 4:46 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "CONTAINER_TYPE", schema = "NEWCARGODRIVE", catalog = "")
public class ContainerType {
    private Integer containerTypeSeq;
    private ContainerSize containerSize;
    private ContainerCategory containerCategory;
    private String asycudaCode;
    private String description;
    private Double length;
    private Double height;
    private Double width;
    private Integer teu;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer status;
    private String statusDescription;
    private Integer containerSizeSeq;
    private Integer containerCategorySeq;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "CONTAINER_TYPE_SEQ", allocationSize = 1)
    @Column(name = "CONTAINER_TYPE_SEQ", nullable = false, precision = 0)
    public Integer getContainerTypeSeq() {
        return containerTypeSeq;
    }

    public void setContainerTypeSeq(Integer containerTypeSeq) {
        this.containerTypeSeq = containerTypeSeq;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CONTAINER_SIZE_SEQ", insertable = false, updatable = false)
    public ContainerSize getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(ContainerSize containerSize) {
        this.containerSize = containerSize;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CONTAINER_CATEGORY_SEQ", insertable = false, updatable = false)
    public ContainerCategory getContainerCategory() {
        return containerCategory;
    }

    public void setContainerCategory(ContainerCategory containerCategory) {
        this.containerCategory = containerCategory;
    }

    @Basic
    @Column(name = "ASYCUDA_CODE", nullable = true, length = 50)
    public String getAsycudaCode() {
        return asycudaCode;
    }

    public void setAsycudaCode(String asycudaCode) {
        this.asycudaCode = asycudaCode;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 200)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "LENGTH", nullable = true, length = 50)
    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    @Basic
    @Column(name = "HEIGHT", nullable = true, length = 50)
    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    @Basic
    @Column(name = "WIDTH", nullable = true, length = 50)
    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    @Basic
    @Column(name = "TEU", nullable = true, precision = 0)
    public Integer getTeu() {
        return teu;
    }

    public void setTeu(Integer teu) {
        this.teu = teu;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = true, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm a")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Basic
    @Column(name = "CONTAINER_SIZE_SEQ", nullable = false)
    public Integer getContainerSizeSeq() {
        return containerSizeSeq;
    }

    public void setContainerSizeSeq(Integer containerSizeSeq) {
        this.containerSizeSeq = containerSizeSeq;
    }

    @Basic
    @Column(name = "CONTAINER_CATEGORY_SEQ", nullable = false)
    public Integer getContainerCategorySeq() {
        return containerCategorySeq;
    }

    public void setContainerCategorySeq(Integer containerCategorySeq) {
        this.containerCategorySeq = containerCategorySeq;
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContainerType that = (ContainerType) o;
        return Objects.equals(containerTypeSeq, that.containerTypeSeq) &&
                Objects.equals(containerSize, that.containerSize) &&
                Objects.equals(containerCategory, that.containerCategory) &&
                Objects.equals(asycudaCode, that.asycudaCode) &&
                Objects.equals(description, that.description) &&
                Objects.equals(length, that.length) &&
                Objects.equals(height, that.height) &&
                Objects.equals(width, that.width) &&
                Objects.equals(teu, that.teu) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
                Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
                Objects.equals(status, that.status) &&
                Objects.equals(statusDescription, that.statusDescription) &&
                Objects.equals(containerSizeSeq, that.containerSizeSeq) &&
                Objects.equals(containerCategorySeq, that.containerCategorySeq);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerTypeSeq, containerSize, containerCategory, asycudaCode, description, length, height, width, teu, createdBy, createdDate, lastModifiedBy, lastModifiedDate, status, statusDescription, containerSizeSeq, containerCategorySeq);
    }
}
