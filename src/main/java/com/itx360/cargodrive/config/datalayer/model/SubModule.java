package com.itx360.cargodrive.config.datalayer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilanga-Ehl on 8/31/2016 1:05 PM).
 */
@Entity
@Table(name = "SUB_MODULES", schema = "NEWCARGODRIVE", catalog = "")
public class SubModule {
    private Integer subModuleSeq;
    private Integer moduleSeq;
    private String subModuleName;
    private String description;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private Integer status;
    private String icon;

    private List<ActionGroup> actionGroupList;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "SUB_MODULE_SEQ", allocationSize = 1)
    @Column(name = "SUB_MODULE_SEQ", nullable = false, precision = 0, unique = true)
    public Integer getSubModuleSeq() {
        return subModuleSeq;
    }

    public void setSubModuleSeq(Integer subModuleSeq) {
        this.subModuleSeq = subModuleSeq;
    }

    @Basic
    @Column(name = "SUB_MODULE_NAME", nullable = true, length = 100)
    public String getSubModuleName() {
        return subModuleName;
    }

    public void setSubModuleName(String subModuleName) {
        this.subModuleName = subModuleName;
    }

    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = true)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY", nullable = true, length = 50)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFIED_DATE", nullable = true)
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "MODULE_SEQ", nullable = false)
    public Integer getModuleSeq() {
        return moduleSeq;
    }

    public void setModuleSeq(Integer moduleSeq) {
        this.moduleSeq = moduleSeq;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "SUB_MODULE_SEQ", insertable = false, updatable = false)
    @JsonIgnore
    public List<ActionGroup> getActionGroupList() {
        return actionGroupList;
    }

    public void setActionGroupList(List<ActionGroup> actionGroupList) {
        this.actionGroupList = actionGroupList;
    }

    @Basic
    @Column(name = "ICON", nullable = false, length = 50)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubModule subModule = (SubModule) o;

        if (subModuleSeq != null ? !subModuleSeq.equals(subModule.subModuleSeq) : subModule.subModuleSeq != null)
            return false;
        if (moduleSeq != null ? !moduleSeq.equals(subModule.moduleSeq) : subModule.moduleSeq != null)
            return false;
        if (subModuleName != null ? !subModuleName.equals(subModule.subModuleName) : subModule.subModuleName != null)
            return false;
        if (description != null ? !description.equals(subModule.description) : subModule.description != null)
            return false;
        if (createdBy != null ? !createdBy.equals(subModule.createdBy) : subModule.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(subModule.createdDate) : subModule.createdDate != null)
            return false;
        if (modifiedBy != null ? !modifiedBy.equals(subModule.modifiedBy) : subModule.modifiedBy != null) return false;
        if (modifiedDate != null ? !modifiedDate.equals(subModule.modifiedDate) : subModule.modifiedDate != null)
            return false;
        if (status != null ? !status.equals(subModule.status) : subModule.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = subModuleSeq != null ? subModuleSeq.hashCode() : 0;
        result = 31 * result + (subModuleName != null ? subModuleName.hashCode() : 0);
        result = 31 * result + (moduleSeq != null ? moduleSeq.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (modifiedBy != null ? modifiedBy.hashCode() : 0);
        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SubModule{" +
                "subModuleSeq=" + subModuleSeq +
                ", moduleSeq=" + moduleSeq +
                ", subModuleName='" + subModuleName + '\'' +
                ", description='" + description + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", modifiedDate=" + modifiedDate +
                ", status=" + status +
                ", actionGroupList=" + actionGroupList +
                '}';
    }
}
