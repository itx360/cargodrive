package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.CustomHouseAgent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Sachithrac on 10/6/2016.
 */
@Repository
public interface CustomHouseAgentService extends JpaRepository<CustomHouseAgent,Integer> {

    CustomHouseAgent findByCustomHouseAgentNameContainingIgnoreCase(String customHouseAgentName);

    List<CustomHouseAgent> findByCustomHouseAgentNameContainingIgnoreCaseAndTaxTypeSeqAndTaxRegistrationNoContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String customHouseAgentName, Integer taxTypeSeq, String taxRegistrationNo, String description);

    List<CustomHouseAgent> findByCustomHouseAgentNameContainingIgnoreCaseAndTaxRegistrationNoContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String customHouseAgentName, String taxRegistrationNo, String description);

    List<CustomHouseAgent> findByTaxTypeSeq(Integer taxTypeSeq);
}
