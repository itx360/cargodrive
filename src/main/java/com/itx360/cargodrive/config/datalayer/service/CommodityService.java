package com.itx360.cargodrive.config.datalayer.service;

import com.itx360.cargodrive.config.datalayer.model.Commodity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Thilangaj on 9/26/2016 9:16 PM).
 */
@Repository
public interface CommodityService extends JpaRepository<Commodity, Integer> {

    Commodity findByDescription(String description);

    List<Commodity> findByHsCodeSeq(Integer hsCodeSeq);

    List<Commodity> findByCommodityTypeSeq(Integer commodityTypeSeq);

    List<Commodity> findByHsCodeSeqAndCommodityTypeSeq(Integer hsCodeSeq, Integer commodityTypeSeq);

    List<Commodity> findByDescriptionContainingIgnoreCase(String description);

    List<Commodity> findByHsCodeSeqAndDescriptionContainingIgnoreCase(Integer hsCodeSeq, String description);

    List<Commodity> findByCommodityTypeSeqAndDescriptionContainingIgnoreCase(Integer commodityTypeSeq, String description);

    List<Commodity> findByHsCodeSeqAndCommodityTypeSeqAndDescriptionContainingIgnoreCase(Integer hsCodeSeq, Integer commodityTypeSeq, String description);
}
