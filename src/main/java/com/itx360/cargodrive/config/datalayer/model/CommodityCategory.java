package com.itx360.cargodrive.config.datalayer.model;

import com.itx360.cargodrive.config.utility.MasterDataStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/26/2016
 * Date: 11:21 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "COMMODITY_CATEGORY", schema = "NEWCARGODRIVE", catalog = "")
public class CommodityCategory {
    private Integer commodityCategorySeq;
    private String commodityCode;
    private String commodityName;
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
    private Integer status;

    private String statusDescription;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "COMMODITY_CATEGORY_SEQ", allocationSize = 1)
    @Column(name = "COMMODITY_CATEGORY_SEQ", unique = true, nullable = false, precision = 0)
    public Integer getCommodityCategorySeq() {
        return commodityCategorySeq;
    }

    public void setCommodityCategorySeq(Integer commodityCategorySeq) {
        this.commodityCategorySeq = commodityCategorySeq;
    }

    @Basic
    @Column(name = "COMMODITY_CODE", unique = true, nullable = true, length = 50)
    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    @Basic
    @Column(name = "COMMODITY_NAME", nullable = true, length = 50)
    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = false, length = 50)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATED_DATE", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_BY", nullable = false, length = 50)
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Basic
    @Column(name = "LAST_MODIFIED_DATE", nullable = false)
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
        if (status != null) {
            this.setStatusDescription(MasterDataStatus.values()[status].getStatus());
        }
    }

    @Transient
    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommodityCategory that = (CommodityCategory) o;
        return commodityCategorySeq == that.commodityCategorySeq &&
                Objects.equals(commodityCode, that.commodityCode) &&
                Objects.equals(commodityName, that.commodityName) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
                Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commodityCategorySeq, commodityCode, commodityName, createdBy, createdDate, lastModifiedBy, lastModifiedDate, status);
    }
}
