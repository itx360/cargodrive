package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.UserManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.CompanyProfile;
import com.itx360.cargodrive.config.datalayer.model.Module;
import com.itx360.cargodrive.config.datalayer.model.User;
import com.itx360.cargodrive.config.datalayer.service.CompanyProfileService;
import com.itx360.cargodrive.config.datalayer.service.ModuleService;
import com.itx360.cargodrive.config.datalayer.service.UserService;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Thilanga-Ehl on 9/5/2016 1:08 PM).
 */
@Controller
@RequestMapping("/config/userManagement")
public class UserManagementController {

    private final UserService userService;

    private final ModuleService moduleService;

    private final CompanyProfileService companyProfileService;

    private final UserManagementControllerManager userManagementControllerManager;

    @Autowired
    public UserManagementController(UserService userService, ModuleService moduleService, CompanyProfileService companyProfileService, UserManagementControllerManager userManagementControllerManager) {
        this.userService = userService;
        this.moduleService = moduleService;
        this.companyProfileService = companyProfileService;
        this.userManagementControllerManager = userManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/userManagement";
    }

    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createUser(@RequestPart(value = "data") User user,
                              @RequestPart(value = "userPhoto", required = false) MultipartFile multipartFile,
                              Principal principal) {
        return this.userManagementControllerManager.createUser(user, multipartFile, principal.getName());
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateUser(@Valid @RequestPart(value = "data") User user,
                              @RequestPart(value = "userPhoto", required = false) MultipartFile multipartFile,
                              Principal principal) {
        return this.userManagementControllerManager.updateUser(user, multipartFile, principal.getName());
    }

    @RequestMapping(value = "/assignModuleToUser", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject assignModuleToUser(@RequestParam(value = "userSeq") Integer userSeq,
                                      @RequestParam(value = "companyProfileSeq") Integer companyProfileSeq,
                                      @RequestParam(value = "moduleSeq[]") List<Integer> moduleSeqList,
                                      Principal principal) {
        return this.userManagementControllerManager.assignUserModuleList(userSeq, companyProfileSeq, moduleSeqList, principal.getName());
    }

    @RequestMapping(value = "/removeModuleFromUser", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject removeModuleFromUser(@RequestParam(value = "userSeq") Integer userSeq,
                                        @RequestParam(value = "companyProfileSeq") Integer companyProfileSeq,
                                        @RequestParam(value = "moduleSeq[]") List<Integer> moduleSeqList,
                                        Principal principal) {
        return this.userManagementControllerManager.removeUserModuleList(userSeq, companyProfileSeq, moduleSeqList, principal.getName());
    }

    @RequestMapping(value = "/getModuleListByUserSeqAndCompanySeq/{userSeq}/{companySeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_VIEW')")
    public
    @ResponseBody
    List<Module> getModuleListByUserSeqAndCompanySeq(@PathVariable("userSeq") Integer userSeq,
                                                     @PathVariable("companySeq") Integer companySeq) {
        return this.moduleService.getModuleListByCompanySeqAndUserSeq(companySeq, userSeq);
    }

    @RequestMapping(value = "/getModuleListByCompanySeq/{companySeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_VIEW')")
    public
    @ResponseBody
    List<Module> getModuleListByCompanySeq(@PathVariable("companySeq") Integer companySeq) {
        return this.moduleService.getModuleListByCompanySeq(companySeq);
    }

    @RequestMapping(value = "/getCompanyListByUserSeq/{userSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_VIEW')")
    public
    @ResponseBody
    List<CompanyProfile> getCompanyListByUserSeq(@PathVariable("userSeq") Integer userSeq) {
        return this.companyProfileService.getActiveCompanyListByUserSeq(userSeq);
    }

    @RequestMapping(value = "/findUserByUserSeq/{userSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@userManagement_VIEW')")
    public
    @ResponseBody
    User findUserByUserSeq(@PathVariable("userSeq") Integer userId) {
        return this.userService.findOne(userId);
    }

    private Model pageLoad(Model model) {
        model.addAttribute("userList", this.userService.findAll());
        model.addAttribute("companyProfileList", this.companyProfileService.findAll());
        return model;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
