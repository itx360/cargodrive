package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.WarehouseManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Warehouse;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.datalayer.service.WarehouseService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 9/29/2016.
 */
@Controller
@RequestMapping("/config/warehouseManagement")
public class WarehouseManagementController {

    private final CountryService countryService;
    private final WarehouseService warehouseService;
    private final WarehouseManagementControllerManager warehouseManagementControllerManager;

    @Autowired
    public WarehouseManagementController(CountryService countryService,
                                         WarehouseService warehouseService, WarehouseManagementControllerManager warehouseManagementControllerManager) {
        this.countryService = countryService;
        this.warehouseService = warehouseService;
        this.warehouseManagementControllerManager = warehouseManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@warehouseManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/warehouseManagement";
    }

    @RequestMapping(value = "/addWarehouse", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@warehouseManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject addWarehouse(@Valid @ModelAttribute AddressBook addressBook,@ModelAttribute Warehouse warehouse,
                                                          Principal principal,HttpServletRequest request) {
        return this.warehouseManagementControllerManager.saveWarehouse(addressBook,warehouse,principal,request);
    }

    @RequestMapping(value = "/updateWarehouse", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@warehouseManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateWarehouse(@Valid @ModelAttribute AddressBook addressBook, @ModelAttribute Warehouse warehouse,
                                Principal principal) {
        return this.warehouseManagementControllerManager.updateWarehouse(addressBook,warehouse,principal);
    }

    @RequestMapping(value = "/findWarehouseByWarehouseSeq/{warehouseSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@warehouseManagement_VIEW')")
    public
    @ResponseBody
    Warehouse findWarehouseByWarehouseSeq(@PathVariable("warehouseSeq") Integer warehouseSeq) {
        return this.warehouseService.findOne(warehouseSeq);
    }

    @RequestMapping(value = "/searchWarehouseData", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@warehouseManagement_VIEW')")
    public String searchWarehouseData(@RequestParam(value = "warehouseCode", required = false) String warehouseCode,@RequestParam(value = "warehouseName", required = false) String warehouseName,@RequestParam(value = "countrySeq") Integer countrySeq,
                                    Model model) {
        List<Warehouse> warehouses = this.warehouseManagementControllerManager.searchWarehouse(warehouseCode,warehouseName,countrySeq);
        model.addAttribute("warehouseListDB", warehouses);
        return "config/content/warehouseData";
    }

    @RequestMapping(value = "/getWarehouseDetails/{warehouseSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@warehouseManagement_VIEW')")
    @ResponseBody
    public Warehouse getWarehouseDetails(@PathVariable("warehouseSeq") Integer warehouseSeq) {
        return this.warehouseService.findOne(warehouseSeq);
    }

    public Model pageLoad(Model model) {
        model.addAttribute("warehouseList", warehouseService.findAll());
        model.addAttribute("countryList", countryService.findAll());
        model.addAttribute("statusList", MasterDataStatus.values());
        return model;
    }
}
