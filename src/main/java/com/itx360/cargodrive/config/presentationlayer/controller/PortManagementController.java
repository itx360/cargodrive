package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.PortManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Location;
import com.itx360.cargodrive.config.datalayer.model.Port;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.datalayer.service.LocationService;
import com.itx360.cargodrive.config.datalayer.service.PortsService;
import com.itx360.cargodrive.config.datalayer.service.TradeLaneService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.config.utility.Modes;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;

import java.security.Principal;
import java.util.List;

/**
 * Created by Harshaa on 9/20/2016.
 */
@Controller
@RequestMapping("/config/portManagement")
public class PortManagementController {

    private final CountryService countryService;
    private final LocationService locationService;
    private final TradeLaneService tradeLaneService;
    private final PortsService portsService;
    private final PortManagementControllerManager portManagementControllerManager;


    @Autowired
    public PortManagementController(CountryService countryService,
                                    LocationService locationService,
                                    TradeLaneService tradeLaneService,
                                    PortsService portsService,
                                    PortManagementControllerManager portManagementControllerManager) {
        this.countryService = countryService;
        this.locationService = locationService;
        this.tradeLaneService = tradeLaneService;
        this.portsService = portsService;
        this.portManagementControllerManager = portManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@portManagement_VIEW')")
    public String getPage(Model model, Principal principal) {
        this.prepareModel(model, principal);
        return "config/portManagement";
    }

    private Model prepareModel(Model model, Principal principal) {
        model.addAttribute("username", principal.getName());
        model.addAttribute("countryList", this.countryService.findAll());
        model.addAttribute("locationList", this.locationService.findAll());
        model.addAttribute("tradeLaneList", this.tradeLaneService.findAll());
        model.addAttribute("portList", this.portsService.findAll());
        model.addAttribute("modes", Modes.values());
        model.addAttribute("status", MasterDataStatus.values());
        return model;
    }

    @RequestMapping("/getLocationsByCountry/{countrySeq}")
    @PreAuthorize("hasRole('ROLE_Config@portManagement_VIEW')")
    public
    @ResponseBody
    List<Location> getLocationsByCountry(@PathVariable("countrySeq") Integer countrySeq, Principal principal) {
        return this.locationService.findByCountrySeq(countrySeq);
    }

    @RequestMapping(value = "/createPort", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@portManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createPort(@RequestPayload Port port, Principal principal) {
        return this.portManagementControllerManager.createPort(port, principal);
    }


    @RequestMapping(value = "/updatePort", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@portManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updatePort(@RequestPayload Port port, Principal principal) {
        return this.portManagementControllerManager.updatePort(port, principal);
    }

    @RequestMapping("/findPortByPortSeq/{portSeq}")
    @PreAuthorize("hasRole('ROLE_Config@portManagement_VIEW')")
    public
    @ResponseBody
    Port findPortByPortSeq(@PathVariable("portSeq") Integer portSeq) {
        return this.portsService.findByPortSeq(portSeq);
    }

    @RequestMapping(value = "/searchPorts", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@portManagement_VIEW')")
    public String searchPorts(@RequestParam(value = "portName", defaultValue = "*") String portName,
                              @RequestParam("transportMode") Integer transportMode,
                              @RequestParam("locationSeq") Integer locationSeq,
                              Model model) {
        model.addAttribute("portList", this.portManagementControllerManager.searchPorts(portName, transportMode, locationSeq));
        return "config/content/portData";
    }


}
