package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.BankManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Bank;
import com.itx360.cargodrive.config.datalayer.service.BankService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 9/26/2016.
 */
@Controller
@RequestMapping(value = "/config/bankManagement")
public class BankManagementController {

    private final BankManagementControllerManager bankManagementControllerManager;
    private final BankService bankService;

    @Autowired
    public BankManagementController(BankManagementControllerManager bankManagementControllerManager, BankService bankService) {
        this.bankManagementControllerManager = bankManagementControllerManager;
        this.bankService = bankService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/bankManagement";
    }

    @RequestMapping(value = "/createBank", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@bankManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createBank(@ModelAttribute Bank bank,
                              Principal principal) {
        return this.bankManagementControllerManager.createBank(bank, principal);
    }

    @RequestMapping(value = "/updateBank", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@bankManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateBank(@ModelAttribute Bank bank,
                              Principal principal) {
        return this.bankManagementControllerManager.updateBank(bank, principal);
    }

    @RequestMapping(value = "/searchBank", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@bankManagement_VIEW')")
    public String searchBank(@RequestParam(value = "bankName") String bankName,
                             @RequestParam(value = "bankCode") String bankCode,
                             Model model) {
        model.addAttribute("bankList", this.bankManagementControllerManager.searchBank(bankName, bankCode));
        return "config/content/bankData";
    }

    @RequestMapping(value = "/findBankByBankSeq/{bankSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@bankManagement_VIEW')")
    public
    @ResponseBody
    Bank findBankByBankSeq(@PathVariable("bankSeq") Integer bankSeq) {
        return this.bankService.findOne(bankSeq);
    }

    @RequestMapping(value = "/getBankDetails/{bankSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@bankManagement_VIEW')")
    @ResponseBody
    public Bank getBankDetails(@PathVariable("bankSeq") Integer bankSeq) {
        return this.bankService.findOne(bankSeq);
    }

    private Model pageLoad(Model model) {
        model.addAttribute("bankList", this.bankService.findAll());
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        return model;
    }

}
