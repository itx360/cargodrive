package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.ConsortiumManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Consortium;
import com.itx360.cargodrive.config.datalayer.service.ConsortiumService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by Nuwanr on 9/28/2016.
 */
@Controller
@RequestMapping("/config/consortiumManagement")
public class ConsortiumManagementController {

    private final ConsortiumManagementControllerManager consortiumManagementControllerManager;
    private final ConsortiumService consortiumService;

    @Autowired
    public ConsortiumManagementController(ConsortiumManagementControllerManager consortiumManagementControllerManager,
                                          ConsortiumService consortiumService) {
        this.consortiumManagementControllerManager = consortiumManagementControllerManager;
        this.consortiumService = consortiumService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@consortiumManagement_VIEW')")   //page view after get permission
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/consortiumManagement";
    }

    @RequestMapping(value = "/createConsortium", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@consortiumManagement_CREATE')")   // not given permission
    public
    @ResponseBody
    ResponseObject createConsortium(@ModelAttribute Consortium consortium, Principal principal) {
        return this.consortiumManagementControllerManager.saveConsortium(consortium, principal);
    }

    @RequestMapping(value = "/updateConsortium", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@consortiumManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCurrency(@ModelAttribute Consortium consortium, Principal principal) {
        return this.consortiumManagementControllerManager.updateConsortium(consortium, principal);
    }

    @RequestMapping(value = "/searchConsortium", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@consortiumManagement_VIEW')")
    public String searchConsortium(@RequestParam(value = "consortiumName", required = false) String consortiumName,
                                   Model model) {
        List<Consortium> consortiumList = this.consortiumManagementControllerManager.searchConsortiumByConsortiumName(consortiumName);
        model.addAttribute("consortiumManagementList", consortiumList);
        return "config/content/consortiumData";
    }

    @RequestMapping(value = "/getConsortiumDetails/{consortiumSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@consortiumManagement_VIEW')")
    @ResponseBody
    public Consortium getConsortiumDetails(@PathVariable("consortiumSeq") Integer consortiumSeq) {
        return this.consortiumService.findOne(consortiumSeq);
    }

    private Model pageLoad(Model model) {
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("consortiumList", this.consortiumService.findAll());
        return model;
    }

}
