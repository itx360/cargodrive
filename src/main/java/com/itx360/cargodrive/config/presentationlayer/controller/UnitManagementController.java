package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.UnitManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Unit;
import com.itx360.cargodrive.config.datalayer.service.UnitService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.config.utility.UnitCategory;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 10/4/2016.
 */
@Controller
@RequestMapping(value = "/config/unitManagement")
public class UnitManagementController {

    private final UnitManagementControllerManager unitManagementControllerManager;
    private final UnitService unitService;

    public UnitManagementController(UnitManagementControllerManager unitManagementControllerManager, UnitService unitService) {
        this.unitManagementControllerManager = unitManagementControllerManager;
        this.unitService = unitService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/unitManagement";
    }

    @RequestMapping(value = "/createUnit", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@unitManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createUnit(@ModelAttribute Unit unit,
                              Principal principal) {
        return this.unitManagementControllerManager.createUnit(unit, principal);
    }

    @RequestMapping(value = "/updateUnit", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@unitManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateUnit(@ModelAttribute Unit unit,
                                      Principal principal) {
        return this.unitManagementControllerManager.updateDeliveryType(unit, principal);
    }

    @RequestMapping(value = "/searchUnit", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@unitManagement_VIEW')")
    public String searchUnit(@RequestParam(value = "unitName") String unitName,
                             @RequestParam(value = "unitCode") String unitCode,
                             @RequestParam(value = "usedFor", required = false) String usedFor,
                             Model model) {
        model.addAttribute("unitList", this.unitManagementControllerManager.searchUnit(unitName, unitCode, usedFor));
        return "config/content/unitData";
    }

    @RequestMapping(value = "/findByUnitSeq/{unitSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@unitManagement_VIEW')")
    public
    @ResponseBody
    Unit findByDeliveryTypeSeq(@PathVariable("unitSeq") Integer unitSeq) {
        return this.unitService.findOne(unitSeq);
    }

    private Model pageLoad(Model model) {
        model.addAttribute("usedForList", UnitCategory.values());
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        return model;
    }
}
