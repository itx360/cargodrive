package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.FinalDestinationManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.FinalDestination;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.datalayer.service.FinalDestinationService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 10/5/2016.
 */
@Controller
@RequestMapping("/config/finalDestinationManagement")
public class FinalDestinationManagementController {

    private final FinalDestinationService finalDestinationService;
    private final CountryService countryService;
    private final FinalDestinationManagementControllerManager finalDestinationManagementControllerManager;

    @Autowired
    public FinalDestinationManagementController(FinalDestinationService finalDestinationService,
                                                CountryService countryService,
                                                FinalDestinationManagementControllerManager finalDestinationManagementControllerManager) {
        this.finalDestinationService = finalDestinationService;
        this.countryService = countryService;
        this.finalDestinationManagementControllerManager = finalDestinationManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@finalDestinationManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/finalDestinationManagement";
    }

    @RequestMapping(value = "/addDestination", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@finalDestinationManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject addDestination(@Valid @ModelAttribute AddressBook addressBook, @ModelAttribute FinalDestination finalDestination,
                                  Principal principal) {
        return this.finalDestinationManagementControllerManager.saveDestination(addressBook, finalDestination, principal);
    }

    @RequestMapping(value = "/updateDestination", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@finalDestinationManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateDestination(@Valid @ModelAttribute AddressBook addressBook, @ModelAttribute FinalDestination finalDestination,
                                  Principal principal) {
        return this.finalDestinationManagementControllerManager.updateDestination(addressBook, finalDestination, principal);
    }

    @RequestMapping(value = "/searchDestinationData", method = RequestMethod.POST)
     @PreAuthorize("hasRole('ROLE_Config@finalDestinationManagement_VIEW')")
    public String searchDestinationData(@RequestParam(value = "finalDestinationCode", required = false) String finalDestinationCode, @RequestParam(value = "countrySeq", required = false) Integer countrySeq,
                                     @RequestParam(value = "city", required = false) String city,
                                     @RequestParam(value = "state", required = false) String state, @RequestParam(value = "zip", required = false) String zip,
                                     Model model) {
        List<FinalDestination> finalDestinationList = this.finalDestinationManagementControllerManager.searchDestination(finalDestinationCode, countrySeq, city, state, zip);
        model.addAttribute("destinationListDB", finalDestinationList);
        return "config/content/finalDestinationData";
    }


    @RequestMapping(value = "/findByFinalDestinationSeq/{finalDestinationSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@finalDestinationManagement_VIEW')")
    public
    @ResponseBody
    FinalDestination findByFinalDestinationSeq(@PathVariable("finalDestinationSeq") Integer finalDestinationSeq) {
        return this.finalDestinationService.findOne(finalDestinationSeq);
    }

    public Model pageLoad(Model model) {
        model.addAttribute("finalDestinationList", this.finalDestinationService.findAll());
        model.addAttribute("countryList", countryService.findAll());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        return model;
    }
}
