package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.CurrencyManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Currency;
import com.itx360.cargodrive.config.datalayer.service.CurrencyService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/19/2016
 * Time: 4:55 PM
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("/config/currencyManagement")
public class CurrencyManagementController {

    private final CurrencyManagementControllerManager currencyManagementControllerManager;
    private final CurrencyService currencyService;

    @Autowired
    public CurrencyManagementController(CurrencyManagementControllerManager currencyManagementControllerManager,
                                        CurrencyService currencyService) {
        this.currencyManagementControllerManager = currencyManagementControllerManager;
        this.currencyService = currencyService;
    }

    /**
     * request for view currency management page
     *
     * @param model Model
     * @return currency management page
     */
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@currencyManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/currencyManagement";
    }

    /**
     * request for create new currency
     *
     * @param currency  Currency
     * @param principal Principal
     * @return ResponseObject
     */
    @RequestMapping(value = "/createCurrency", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@currencyManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createCurrency(@ModelAttribute Currency currency, Principal principal) {
        return this.currencyManagementControllerManager.saveCurrency(currency, principal);
    }

    /**
     * request for update existing currency details
     *
     * @param currency  Currency
     * @param principal principal
     * @return ResponseObject
     */
    @RequestMapping(value = "/updateCurrency", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@currencyManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCurrency(@ModelAttribute Currency currency, Principal principal) {
        return this.currencyManagementControllerManager.updateCurrency(currency, principal);
    }

    /**
     * request for search currency details
     *
     * @param currencyName    currencyName default value - *
     * @param currencyDisplay currencyDisplay default - *
     * @param model           Modal
     * @return ResponseObject
     */
    @RequestMapping(value = "/searchCurrency", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@currencyManagement_VIEW')")
    public String searchCurrency(@RequestParam(value = "currencyName", required = false) String currencyName,
                                 @RequestParam(value = "currencyDisplay", required = false) String currencyDisplay,
                                 Model model) {
        List<Currency> currencyList = this.currencyManagementControllerManager.searchCurrencyByCurrencyNameAndCurrencyDisplay(currencyName,
                currencyDisplay);
        model.addAttribute("currencyManagementList", currencyList);
        return "config/content/currencyData";
    }

    /**
     * request for getting currency details by currencySeq
     *
     * @param currencySeq currencySeq
     * @return Currency
     */
    @RequestMapping(value = "/getCurrencyDetails/{currencySeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@currencyManagement_VIEW')")
    @ResponseBody
    public Currency getCurrencyDetails(@PathVariable("currencySeq") Integer currencySeq) {
        return this.currencyService.findOne(currencySeq);
    }

    /**
     * Common objects are defining here
     *
     * @param model Modal
     * @return Modal
     */
    private Model pageLoad(Model model) {
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("currencyList", this.currencyService.findAll());
        return model;
    }
}
