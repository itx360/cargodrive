package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.CommodityManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Commodity;
import com.itx360.cargodrive.config.datalayer.service.CommodityService;
import com.itx360.cargodrive.config.datalayer.service.CommodityTypeService;
import com.itx360.cargodrive.config.datalayer.service.HsCodeService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by Thilangaj on 9/26/2016 7:59 PM).
 */
@Controller
@RequestMapping("/config/commodityManagement")
public class CommodityManagementController {

    private final CommodityTypeService commodityTypeService;
    private final HsCodeService hsCodeService;
    private final CommodityService commodityService;
    private final CommodityManagementControllerManager commodityManagementControllerManager;

    @Autowired
    public CommodityManagementController(CommodityTypeService commodityTypeService, HsCodeService hsCodeService, CommodityService commodityService, CommodityManagementControllerManager commodityManagementControllerManager) {
        this.commodityTypeService = commodityTypeService;
        this.hsCodeService = hsCodeService;
        this.commodityService = commodityService;
        this.commodityManagementControllerManager = commodityManagementControllerManager;
    }

    /**
     * Request mapping for viewing the page
     *
     * @param model Spring MVC Model Object to Add Objects
     * @return Page name as a String
     */
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@commodityManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/commodityManagement";
    }

    @RequestMapping(value = "/createCommodity", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createCommodity(@ModelAttribute Commodity commodity,
                                   Principal principal) {
        return this.commodityManagementControllerManager.createCommodity(commodity, principal.getName());
    }

    @RequestMapping(value = "/updateCommodity", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCommodity(@ModelAttribute Commodity commodity,
                                   Principal principal) {
        return this.commodityManagementControllerManager.updateCommodity(commodity, principal.getName());
    }

    @RequestMapping(value = "/searchCommodity", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityManagement_VIEW')")
    public String searchCommodity(@RequestParam(value = "commodityTypeSeq") Integer commodityTypeSeq,
                                  @RequestParam(value = "hsCodeSeq") Integer hsCodeSeq,
                                  @RequestParam(value = "description", required = false) String description,
                                  Model model) {
        model.addAttribute("commodityList", this.commodityManagementControllerManager.searchCommodity(commodityTypeSeq, hsCodeSeq, description));
        return "config/content/commodityData";
    }

    @RequestMapping(value = "/findCommodityByCommoditySeq/{commoditySeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@commodityManagement_VIEW')")
    public
    @ResponseBody
    Commodity findCommodityByCommoditySeq(@PathVariable("commoditySeq") Integer commoditySeq) {
        return this.commodityService.findOne(commoditySeq);
    }

    /**
     * Object that are dependant for form loading and functionality
     *
     * @param model Model object from GET request
     * @return Objects Assigned Model Object
     */
    private Model pageLoad(Model model) {
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("hsCodeList", this.hsCodeService.findAll());
        model.addAttribute("commodityTypeList", this.commodityTypeService.findAll());
        model.addAttribute("commodityList", this.commodityService.findAll());
        return model;
    }
}
