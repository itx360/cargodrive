package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.HsCodeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.HsCode;
import com.itx360.cargodrive.config.datalayer.service.HsCategoryService;
import com.itx360.cargodrive.config.datalayer.service.HsCodeService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by Thilangaj on 9/26/2016 7:59 PM).
 */
@Controller
@RequestMapping("/config/hsCodeManagement")
public class HsCodeManagementController {

    private final HsCategoryService hsCategoryService;
    private final HsCodeService hsCodeService;
    private final HsCodeManagementControllerManager hsCodeManagementControllerManager;

    @Autowired
    public HsCodeManagementController(HsCategoryService hsCategoryService, HsCodeService hsCodeService, HsCodeManagementControllerManager hsCodeManagementControllerManager) {
        this.hsCategoryService = hsCategoryService;
        this.hsCodeService = hsCodeService;
        this.hsCodeManagementControllerManager = hsCodeManagementControllerManager;
    }

    /**
     * Request mapping for viewing the page
     *
     * @param model Spring MVC Model Object to Add Objects
     * @return Page name as a String
     */
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@hsCodeManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/hsCodeManagement";
    }

    @RequestMapping(value = "/createHsCode", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@hsCodeManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createHsCode(@ModelAttribute HsCode hsCode,
                                Principal principal) {
        return this.hsCodeManagementControllerManager.createHsCode(hsCode, principal.getName());
    }

    @RequestMapping(value = "/updateHsCode", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@hsCodeManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateHsCode(@ModelAttribute HsCode hsCode,
                                Principal principal) {
        return this.hsCodeManagementControllerManager.updateHsCode(hsCode, principal.getName());
    }

    @RequestMapping(value = "/searchHsCode", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@hsCodeManagement_VIEW')")
    public String searchHsCode(@RequestParam(value = "hsCodeName", required = false) String hsCodeName,
                               @RequestParam(value = "description", required = false) String description,
                               @RequestParam(value = "hsCategorySeq") Integer hsCategorySeq,
                               Model model) {
        model.addAttribute("hsCodeList", this.hsCodeManagementControllerManager.searchHsCode(hsCodeName, description, hsCategorySeq));
        return "config/content/hsCodeData";
    }

    @RequestMapping(value = "/findHsCodeByHsCodeSeq/{hsCodeSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@hsCodeManagement_VIEW')")
    public
    @ResponseBody
    HsCode findHsCodeByHsCodeSeq(@PathVariable("hsCodeSeq") Integer hsCodeSeq) {
        return this.hsCodeService.findOne(hsCodeSeq);
    }

    /**
     * Object that are dependant for form loading and functionality
     *
     * @param model Model object from GET request
     * @return Objects Assigned Model Object
     */
    private Model pageLoad(Model model) {
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("hsCodeList", this.hsCodeService.findAll());
        model.addAttribute("hsCategoryList", this.hsCategoryService.findAll());
        return model;
    }
}
