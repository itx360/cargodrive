package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.LocationManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Location;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.datalayer.service.LocationService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by Thilangaj on 9/20/2016 12:46 PM).
 */
@Controller
@RequestMapping("/config/locationManagement")
public class LocationManagementController {

    private final LocationManagementControllerManager locationManagementControllerManager;

    private final LocationService locationService;

    private final CountryService countryService;

    @Autowired
    public LocationManagementController(LocationManagementControllerManager locationManagementControllerManager, LocationService locationService, CountryService countryService) {
        this.locationManagementControllerManager = locationManagementControllerManager;
        this.locationService = locationService;
        this.countryService = countryService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@locationManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/locationManagement";
    }

    @RequestMapping(value = "/createLocation", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@locationManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createLocation(@ModelAttribute Location location,
                                  Principal principal) {
        return this.locationManagementControllerManager.createLocation(location, principal.getName());
    }

    @RequestMapping(value = "/updateLocation", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@locationManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateLocation(@ModelAttribute Location location,
                                  Principal principal) {
        return this.locationManagementControllerManager.updateLocation(location, principal.getName());
    }

    @RequestMapping(value = "/searchLocation", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@locationManagement_VIEW')")
    public String searchLocation(@RequestParam(value = "locationName", required = false) String locationName,
                                 @RequestParam(value = "countrySeq") Integer countrySeq,
                                 Model model) {
        model.addAttribute("locationList", this.locationManagementControllerManager.searchLocation(locationName, countrySeq));
        return "config/content/locationData";
    }

    @RequestMapping(value = "/findLocationByLocationSeq/{locationSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@locationManagement_VIEW')")
    public
    @ResponseBody
    Location findLocationByLocationSeq(@PathVariable("locationSeq") Integer locationSeq) {
        return this.locationService.findOne(locationSeq);
    }

    private Model pageLoad(Model model) {
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("countryList", this.countryService.findAll());
        model.addAttribute("locationList", this.locationService.findAll());
        return model;
    }
}
