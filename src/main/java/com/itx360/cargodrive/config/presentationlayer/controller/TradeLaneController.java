package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.TradeLaneControllerManager;
import com.itx360.cargodrive.config.datalayer.model.TradeLane;
import com.itx360.cargodrive.config.datalayer.service.TradeLaneService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 9/20/2016.
 */
@Controller
@RequestMapping("/config/tradeLane")
public class TradeLaneController {

    private final TradeLaneService tradeLaneService;
    private final TradeLaneControllerManager tradeLaneControllerManager;

    @Autowired
    public TradeLaneController(TradeLaneService tradeLaneService, TradeLaneControllerManager tradeLaneControllerManager) {
        this.tradeLaneService = tradeLaneService;
        this.tradeLaneControllerManager = tradeLaneControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@tradeLane_VIEW')")
    public String getPage(Model model,
                          Principal principal) {
        this.pageLoad(model, principal);
        return "config/tradeLane";
    }

    @RequestMapping(value = "/addTradeLane", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@tradeLane_CREATE')")
    public
    @ResponseBody
    ResponseObject addTradeLane(@Valid @ModelAttribute TradeLane tradeLane,
                                Principal principal) {
        return this.tradeLaneControllerManager.saveTradeLane(tradeLane, principal);
    }

    @RequestMapping(value = "/updateTradeLane", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@tradeLane_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateTradeLane(@Valid @ModelAttribute TradeLane tradeLane,
                                   Principal principal) {
        return this.tradeLaneControllerManager.updateTradeLane(tradeLane, principal);
    }

    @RequestMapping(value = "/searchTradeLaneData", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@tradeLane_VIEW')")
    public String loadTradeLaneList(@RequestParam(value = "tradeLaneCode", required = false) String tradeLaneCode,
                                    @RequestParam(value = "tradeLaneName", required = false) String tradeLaneName,
                                    Model model) {
        List<TradeLane> tradeLanes = this.tradeLaneControllerManager.searchTradeLane(tradeLaneCode, tradeLaneName);
        model.addAttribute("tradeLaneListDB", tradeLanes);
        return "config/content/tradeLaneData";
    }

    @RequestMapping(value = "/findTradeLaneByTradeLaneSeq/{tradeLaneSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@tradeLane_VIEW')")
    public
    @ResponseBody
    TradeLane findTradeLaneByTradeLaneSeq(@PathVariable("tradeLaneSeq") Integer tradeLaneId) {
        return this.tradeLaneService.findOne(tradeLaneId);
    }

    @RequestMapping(value = "/getTradeLaneDetails/{tradeLaneSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@tradeLane_VIEW')")
    @ResponseBody
    public TradeLane getTradeLaneDetails(@PathVariable("tradeLaneSeq") Integer tradeLaneSeq) {
        return this.tradeLaneService.findOne(tradeLaneSeq);
    }

    public Model pageLoad(Model model, Principal principal) {
        model.addAttribute("tradeLaneList", this.tradeLaneService.findAll());
        model.addAttribute("statusList", MasterDataStatus.values());
        return model;
    }


}
