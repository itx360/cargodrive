package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.PackageTypeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.PackageType;
import com.itx360.cargodrive.config.datalayer.service.PackageTypeService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/4/2016
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/config/packageTypeManagement")
public class PackageTypeManagementController {

    private final PackageTypeManagementControllerManager packageTypeManagementControllerManager;
    private final PackageTypeService packageTypeService;

    @Autowired
    public PackageTypeManagementController(PackageTypeManagementControllerManager packageTypeManagementControllerManager,
                                           PackageTypeService packageTypeService) {
        this.packageTypeManagementControllerManager = packageTypeManagementControllerManager;
        this.packageTypeService = packageTypeService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@packageTypeManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/packageTypeManagement";
    }

    @RequestMapping(value = "/createPackageType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@packageTypeManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createPackageType(@ModelAttribute PackageType packageType, Principal principal) {
        return this.packageTypeManagementControllerManager.savePackageType(packageType, principal);
    }

    @RequestMapping(value = "/updatePackageType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@packageTypeManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updatePackageType(@ModelAttribute PackageType packageType,
                                       Principal principal) {
        return this.packageTypeManagementControllerManager.updatePackageType(packageType, principal);
    }

    @RequestMapping(value = "/searchPackageType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@packageTypeManagement_VIEW')")
    public String searchCurrency(@RequestParam(value = "packageTypeName", required = false) String packageTypeName,
                                 @RequestParam(value = "packageTypeCode", required = false) String packageTypeCode,
                                 @RequestParam(value = "description", required = false) String description,
                                 Model model) {
        List<PackageType> packageTypeList = this.packageTypeManagementControllerManager.searchPackageType(packageTypeName,
                packageTypeCode,
                description);
        model.addAttribute("packageTypeManagementList", packageTypeList);
        return "config/content/packageTypeData";
    }

    @RequestMapping(value = "/findByPackageTypeSeq/{packageTypeSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@packageTypeManagement_VIEW')")
    public
    @ResponseBody
    PackageType findPackageTypeByPackageTypeSeq(@PathVariable("packageTypeSeq") Integer packageTypeSeq) {
        return this.packageTypeService.findOne(packageTypeSeq);
    }

    private Model pageLoad(Model model) {
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        return model;
    }
}
