package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.BranchManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Branch;
import com.itx360.cargodrive.config.datalayer.service.BankService;
import com.itx360.cargodrive.config.datalayer.service.BranchService;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 9/26/2016.
 */
@Controller
@RequestMapping("/config/branchManagement")
public class BranchManagementController {

    private final BranchService branchService;
    private final CountryService countryService;
    private final BankService bankService;
    private final BranchManagementControllerManager branchManagementControllerManager;

    @Autowired
    public BranchManagementController(BranchService branchService, CountryService countryService, BankService bankService, BranchManagementControllerManager branchManagementControllerManager) {
        this.branchService = branchService;
        this.countryService = countryService;
        this.bankService = bankService;
        this.branchManagementControllerManager = branchManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@branchManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/branchManagement";
    }

    @RequestMapping(value = "/addBranch", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@branchManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject addBranch(@Valid @ModelAttribute AddressBook addressBook, @ModelAttribute Branch branch,
                             Principal principal) {
        return this.branchManagementControllerManager.saveBranch(addressBook,branch,principal);
    }



    @RequestMapping(value = "/updateBranch", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@branchManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateBranch(@Valid @ModelAttribute AddressBook addressBook,@ModelAttribute Branch branch,
                                Principal principal) {
        return this.branchManagementControllerManager.updateBranch(addressBook,branch,principal);
    }

    @RequestMapping(value = "/findBranchByBranchSeq/{branchSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@branchManagement_VIEW')")
    public
    @ResponseBody
    Branch findBranchByBranchSeq(@PathVariable("branchSeq") Integer branchSeq) {
        return this.branchService.findOne(branchSeq);
    }

    @RequestMapping(value = "/searchBranchData", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@branchManagement_VIEW')")
    public String searchBranchData(@RequestParam(value = "bankName", required = false) String bankName,@RequestParam(value = "branchCode", required = false) String branchCode,@RequestParam(value = "branchName", required = false) String branchName,
                                    Model model) {
        List<Branch> branches = this.branchManagementControllerManager.searchBranch(branchCode,branchName,bankName);
        model.addAttribute("branchListDB", branches);
        return "config/content/branchData";
    }


    @RequestMapping(value = "/getBranchDetails/{branchSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@branchManagement_VIEW')")
    @ResponseBody
    public Branch getBranchDetails(@PathVariable("branchSeq") Integer branchSeq) {
        return this.branchService.findOne(branchSeq);
    }

    public Model pageLoad(Model model) {
        model.addAttribute("bankList", this.bankService.findAll());
        model.addAttribute("branchList", this.branchService.findAll());
        model.addAttribute("countryList", countryService.findAll());
        model.addAttribute("statusList", MasterDataStatus.values());
        return model;
    }

}
