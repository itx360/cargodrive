package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.CustomHouseAgentControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.CustomHouseAgent;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.datalayer.service.CustomHouseAgentService;
import com.itx360.cargodrive.config.datalayer.service.TaxTypeService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 10/6/2016.
 */
@Controller
@RequestMapping("/config/customHouseAgentManagement")
public class CustomHouseAgentManagementController {

    private final CustomHouseAgentService customHouseAgentService;
    private final CountryService countryService;
    private final TaxTypeService taxTypeService;
    private final CustomHouseAgentControllerManager customHouseAgentControllerManager;

    @Autowired
    public CustomHouseAgentManagementController(CustomHouseAgentService customHouseAgentService,
                                                CountryService countryService,
                                                TaxTypeService taxTypeService,
                                                CustomHouseAgentControllerManager customHouseAgentControllerManager) {
        this.customHouseAgentService = customHouseAgentService;
        this.countryService = countryService;
        this.taxTypeService = taxTypeService;
        this.customHouseAgentControllerManager = customHouseAgentControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@customHouseAgentManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/customHouseAgentManagement";
    }

    @RequestMapping(value = "/addCustomHouseAgent", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@customHouseAgentManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject addCustomHouseAgent(@Valid @ModelAttribute AddressBook addressBook, @ModelAttribute CustomHouseAgent customHouseAgent,
                                       Principal principal) {
        return this.customHouseAgentControllerManager.saveCustomHouseAgent(addressBook, customHouseAgent, principal);
    }

    @RequestMapping(value = "/updateCustomHouseAgent", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@customHouseAgentManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCustomHouseAgent(@Valid @ModelAttribute AddressBook addressBook, @ModelAttribute CustomHouseAgent customHouseAgent,
                                          Principal principal) {
        return this.customHouseAgentControllerManager.updateCustomHouseAgent(addressBook, customHouseAgent, principal);
    }

    @RequestMapping(value = "/searchCustomHouseAgentData", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@customHouseAgentManagement_VIEW')")
    public String searchCustomHouseAgentData(@RequestParam(value = "customHouseAgentName", required = false) String customHouseAgentName,
                                             @RequestParam(value = "taxTypeSeq", required = false) Integer taxTypeSeq,
                                             @RequestParam(value = "taxRegistrationNo", required = false) String taxRegistrationNo,
                                             @RequestParam(value = "description", required = false) String description,
                                             Model model) {
        List<CustomHouseAgent> customHouseAgentList = this.customHouseAgentControllerManager.searchCustomHouseAgent(customHouseAgentName, taxTypeSeq, taxRegistrationNo, description);
        model.addAttribute("customHouseAgentListDB", customHouseAgentList);
        return "config/content/customHouseAgentData";
    }

    @RequestMapping(value = "/findByCustomHouseAgentSeq/{customHouseAgentSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@customHouseAgentManagement_VIEW')")
    public
    @ResponseBody
    CustomHouseAgent findByCustomHouseAgentSeq(@PathVariable("customHouseAgentSeq") Integer customHouseAgentSeq) {
        return this.customHouseAgentService.findOne(customHouseAgentSeq);
    }

    public Model pageLoad(Model model) {
        model.addAttribute("taxTypeList", this.taxTypeService.findAll());
        model.addAttribute("countryList", countryService.findAll());
        model.addAttribute("statusList", MasterDataStatus.getStatusListForCreate());
        return model;
    }
}
