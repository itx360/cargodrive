package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.CountryManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Country;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.datalayer.service.CurrencyService;
import com.itx360.cargodrive.config.datalayer.service.RegionService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 9/19/2016.
 */
@Controller
@RequestMapping(value = "/config/countryManagement")
public class CountryManagementController {

    private final CountryManagementControllerManager countryManagementControllerManager;
    private final CurrencyService currencyService;
    private final RegionService regionService;
    private final CountryService countryService;

    @Autowired
    public CountryManagementController(CountryManagementControllerManager countryManagementControllerManager,
                                       CurrencyService currencyService,
                                       RegionService regionService,
                                       CountryService countryService) {
        this.countryManagementControllerManager = countryManagementControllerManager;
        this.currencyService = currencyService;
        this.regionService = regionService;
        this.countryService = countryService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(Model model,
                          Principal principal) {
        this.pageLoad(model, principal);
        return "config/countryManagement";
    }

    @RequestMapping(value = "/createCountry", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@countryManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createCountry(@ModelAttribute Country country,
                                 Principal principal) {
        return this.countryManagementControllerManager.createCountry(country, principal);
    }

    @RequestMapping(value = "/updateCountry", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@countryManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCountry(@ModelAttribute Country country,
                              Principal principal) {
        return this.countryManagementControllerManager.updateCountry(country, principal);
    }

    @RequestMapping(value = "/searchCountry", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@countryManagement_VIEW')")
    public String searchCountry(@RequestParam(value = "countryName") String countryName,
                                @RequestParam(value = "countryCode") String countryCode,
                                Model model) {
        List<Country> countryList = this.countryManagementControllerManager.searchCountry(countryName, countryCode);
        model.addAttribute("countryList", countryList);
        return "config/content/countryData";
    }

    @RequestMapping(value = "/findCountryByCountrySeq/{countrySeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@countryManagement_VIEW')")
    public
    @ResponseBody
    Country findCountryByCountrySeq(@PathVariable("countrySeq") Integer countrySeq) {
        return this.countryService.findOne(countrySeq);
    }

    @RequestMapping(value = "/getCountryDetails/{countrySeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@countryManagement_VIEW')")
    @ResponseBody
    public Country getCountryDetails(@PathVariable("countrySeq") Integer countrySeq) {
        return this.countryService.findOne(countrySeq);
    }

    private Model pageLoad(Model model,
                           Principal principal) {
        model.addAttribute("username", principal.getName());
        model.addAttribute("currencyList", this.currencyService.findAll());
        model.addAttribute("regionList", this.regionService.findAll());
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("countryList", this.countryService.findAll());
        return model;
    }
}
