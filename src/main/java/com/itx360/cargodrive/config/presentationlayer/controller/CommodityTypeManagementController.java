package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.CommodityTypeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.CommodityType;
import com.itx360.cargodrive.config.datalayer.service.CommodityCategoryService;
import com.itx360.cargodrive.config.datalayer.service.CommodityTypeService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by Thilangaj on 9/26/2016 7:59 PM).
 */
@Controller
@RequestMapping("/config/commodityTypeManagement")
public class CommodityTypeManagementController {

    private final CommodityCategoryService commodityCategoryService;
    private final CommodityTypeService commodityTypeService;
    private final CommodityTypeManagementControllerManager commodityTypeManagementControllerManager;

    @Autowired
    public CommodityTypeManagementController(CommodityCategoryService commodityCategoryService, CommodityTypeService commodityTypeService, CommodityTypeManagementControllerManager commodityTypeManagementControllerManager) {
        this.commodityCategoryService = commodityCategoryService;
        this.commodityTypeService = commodityTypeService;
        this.commodityTypeManagementControllerManager = commodityTypeManagementControllerManager;
    }

    /**
     * Request mapping for viewing the page
     *
     * @param model
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@commodityTypeManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/commodityTypeManagement";
    }

    @RequestMapping(value = "/createCommodityType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityTypeManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createCommodityType(@ModelAttribute CommodityType commodityType,
                                Principal principal) {
        return this.commodityTypeManagementControllerManager.createCommodityType(commodityType, principal.getName());
    }

    @RequestMapping(value = "/updateCommodityType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityTypeManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCommodityType(@ModelAttribute CommodityType commodityType,
                                Principal principal) {
        return this.commodityTypeManagementControllerManager.updateCommodityType(commodityType, principal.getName());
    }

    @RequestMapping(value = "/searchCommodityType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityTypeManagement_VIEW')")
    public String searchCommodityType(@RequestParam(value = "commodityTypeCode", required = false) String commodityTypeCode,
                               @RequestParam(value = "description", required = false) String description,
                               Model model) {
        model.addAttribute("commodityTypeList", this.commodityTypeManagementControllerManager.searchCommodityType(commodityTypeCode, description));
        return "config/content/commodityTypeData";
    }

    @RequestMapping(value = "/findCommodityTypeByCommodityTypeSeq/{commodityTypeSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@commodityTypeManagement_VIEW')")
    public
    @ResponseBody
    CommodityType findCommodityTypeByCommodityTypeSeq(@PathVariable("commodityTypeSeq") Integer commodityTypeSeq) {
        return this.commodityTypeService.findOne(commodityTypeSeq);
    }
    
    /**
     * Object that are dependant for form loading and functionality
     *
     * @param model
     * @return
     */
    private Model pageLoad(Model model) {
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("commodityCategoryList", this.commodityCategoryService.findAll());
        model.addAttribute("commodityTypeList", this.commodityTypeService.findAll());
        return model;
    }
}
