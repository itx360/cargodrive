package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.ChargeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.Charge;
import com.itx360.cargodrive.config.datalayer.service.ChargeService;
import com.itx360.cargodrive.config.datalayer.service.ModuleService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 9/27/2016.
 */
@Controller
@RequestMapping(value = "/config/chargeManagement")
public class ChargeManagementController {

    private final ChargeService chargeService;
    private final ModuleService moduleService;
    private final ChargeManagementControllerManager chargeManagementControllerManager;

    @Autowired
    public ChargeManagementController(ChargeService chargeService, ModuleService moduleService, ChargeManagementControllerManager chargeManagementControllerManager) {
        this.chargeService = chargeService;
        this.moduleService = moduleService;
        this.chargeManagementControllerManager = chargeManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(Model model,
                          HttpServletRequest request) {
        this.pageLoad(model,request);
        return "config/chargeManagement";
    }

    @RequestMapping(value = "/createCharge", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@chargeManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createCharge(@ModelAttribute Charge charge,
                                @RequestParam(value = "modeSeq[]") List<Integer> modeSeqList,
                                HttpServletRequest request,
                                Principal principal) {
        charge.setCompanyProfileSeq(Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString()));
        return this.chargeManagementControllerManager.createCharge(charge, modeSeqList, principal);
    }

    @RequestMapping(value = "/updateCharge", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@chargeManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCharge(@ModelAttribute Charge charge,
                                @RequestParam(value = "modeSeq[]") List<Integer> modeSeqList,
                                HttpServletRequest request,
                                Principal principal) {
        charge.setCompanyProfileSeq(Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString()));
        return this.chargeManagementControllerManager.updateCharge(charge,modeSeqList, principal);
    }

    @RequestMapping(value = "/searchCharge", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@chargeManagement_VIEW')")
    public String searchCharge(@RequestParam(value = "chargeName") String chargeName,
                               @RequestParam(value = "description") String description,
                               HttpServletRequest request,
                               Model model) {
        Integer customerProfileSeq = Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString());
        model.addAttribute("chargeList", this.chargeManagementControllerManager.searchCharge(chargeName, description, customerProfileSeq));
        return "config/content/chargeData";
    }

    @RequestMapping(value = "/findChargeByChargeSeq/{chargeSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@chargeManagement_VIEW')")
    public
    @ResponseBody
    Charge findChargeByChargeSeq(@PathVariable("chargeSeq") Integer chargeSeq) {
        return this.chargeService.findOne(chargeSeq);
    }

    @RequestMapping(value = "/getChargeDetails/{chargeSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@chargeManagement_VIEW')")
    @ResponseBody
    public Charge getChargeDetails(@PathVariable("chargeSeq") Integer chargeSeq) {
        return this.chargeService.findOne(chargeSeq);
    }

    private Model pageLoad(Model model, HttpServletRequest request) {
        model.addAttribute("modeList", this.chargeManagementControllerManager.getFinanceEnabledCompanyModuleList(request));
        model.addAttribute("chargeList", this.chargeService.findAll());
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        return model;
    }
}
