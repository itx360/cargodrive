package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.HsCategoryManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.HsCategory;
import com.itx360.cargodrive.config.datalayer.service.HsCategoryService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by Thilangaj on 9/26/2016 7:59 PM).
 */
@Controller
@RequestMapping("/config/hsCategoryManagement")
public class HsCategoryManagementController {

    private final HsCategoryService hsCategoryService;
    private final HsCategoryManagementControllerManager hsCategoryManagementControllerManager;

    @Autowired
    public HsCategoryManagementController(HsCategoryService hsCategoryService, HsCategoryManagementControllerManager hsCategoryManagementControllerManager) {
        this.hsCategoryService = hsCategoryService;
        this.hsCategoryManagementControllerManager = hsCategoryManagementControllerManager;
    }

    /**
     * Request mapping for viewing the page
     *
     * @param model Spring MVC Model Object to Add Objects
     * @return Page name as a String
     */
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@hsCategoryManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/hsCategoryManagement";
    }

    @RequestMapping(value = "/createHsCategory", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@hsCategoryManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createHsCategory(@ModelAttribute HsCategory hsCategory,
                                    Principal principal) {
        return this.hsCategoryManagementControllerManager.createHsCategory(hsCategory, principal.getName());
    }

    @RequestMapping(value = "/updateHsCategory", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@hsCategoryManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateHsCategory(@ModelAttribute HsCategory hsCategory,
                                    Principal principal) {
        return this.hsCategoryManagementControllerManager.updateHsCategory(hsCategory, principal.getName());
    }

    @RequestMapping(value = "/searchHsCategory", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@hsCategoryManagement_VIEW')")
    public String searchHsCategory(@RequestParam(value = "hsCategoryName", required = false) String hsCategoryName,
                                   @RequestParam(value = "description", required = false) String description,
                                   Model model) {
        model.addAttribute("hsCategoryList", this.hsCategoryManagementControllerManager.searchHsCategoryName(hsCategoryName, description));
        return "config/content/hsCategoryData";
    }

    @RequestMapping(value = "/findHsCategoryByHsCategorySeq/{hsCategorySeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@hsCategoryManagement_VIEW')")
    public
    @ResponseBody
    HsCategory findHsCategoryByHsCategorySeq(@PathVariable("hsCategorySeq") Integer hsCategorySeq) {
        return this.hsCategoryService.findOne(hsCategorySeq);
    }

    /**
     * Object that are dependant for form loading and functionality
     *
     * @param model Model object from GET request
     * @return Objects Assigned Model Object
     */
    private Model pageLoad(Model model) {
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("hsCategoryList", this.hsCategoryService.findAll());
        return model;
    }
}
