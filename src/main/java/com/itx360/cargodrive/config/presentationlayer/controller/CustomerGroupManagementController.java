package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.CustomerGroupManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.CustomerGroup;
import com.itx360.cargodrive.config.datalayer.model.SubModule;
import com.itx360.cargodrive.config.datalayer.service.CustomerGroupService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by Sachithrac on 9/28/2016.
 */
@Controller
@RequestMapping("/config/customerGroupManagement")
public class CustomerGroupManagementController {

    private final CustomerGroupService customerGroupService;
    private final CustomerGroupManagementControllerManager customerGroupManagementControllerManager;

    public CustomerGroupManagementController(CustomerGroupService customerGroupService,
                                             CustomerGroupManagementControllerManager customerGroupManagementControllerManager) {
        this.customerGroupService = customerGroupService;
        this.customerGroupManagementControllerManager = customerGroupManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@customerGroupManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/customerGroupManagement";
    }

    @RequestMapping(value = "/addCustomerGroup", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@customerGroupManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject addCustomerGroup(@Valid @ModelAttribute CustomerGroup customerGroup, Principal principal,HttpServletRequest request) {
        return this.customerGroupManagementControllerManager.saveCustomerGroup(customerGroup, principal,request);
    }

    @RequestMapping(value = "/updateCustomerGroup", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@customerGroupManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCustomerGroup(@Valid @ModelAttribute CustomerGroup customerGroup, Principal principal) {
        return this.customerGroupManagementControllerManager.updateCustomerGroup(customerGroup, principal);
    }

    @RequestMapping(value = "/searchCustomerGroupData", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@customerGroupManagement_VIEW')")
    public String searchCustomerGroup(
            @RequestParam(value = "customerGroupCode", required = false) String customerGroupCode,
            @RequestParam(value = "customerGroupName") String customerGroupName,
            Model model) {
        model.addAttribute("customerGroupListDB", this.customerGroupManagementControllerManager.searchCustomerGroup(customerGroupCode, customerGroupName));
        return "config/content/customerGroupData";
    }

    @RequestMapping(value = "/getCustomerGroupDetails/{customerGroupSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@customerGroupManagement_VIEW')")
    @ResponseBody
    public CustomerGroup getCustomerGroupDetails(@PathVariable("customerGroupSeq") Integer customerGroupSeq) {
        return this.customerGroupService.findOne(customerGroupSeq);
    }

    @RequestMapping(value = "/findCustomerGroupByCustomerGroupSeq/{customerGroupSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@customerGroupManagement_VIEW')")
    public
    @ResponseBody
    CustomerGroup findCustomerGroupByCustomerGroupSeq(@PathVariable("customerGroupSeq") Integer customerGroupSeq) {
        return this.customerGroupService.findOne(customerGroupSeq);
    }

    public Model pageLoad(Model model) {
        model.addAttribute("customerGroupList", this.customerGroupService.findAll());
        model.addAttribute("statusList", MasterDataStatus.values());
        return model;
    }
}
