package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.AgentNetworkManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AgentNetwork;
import com.itx360.cargodrive.config.datalayer.service.AgentNetworkService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 10/7/2016.
 */
@Controller
@RequestMapping("/config/agentNetworkManagement")
public class AgentNetworkManagementController {

    private final AgentNetworkService agentNetworkService;
    private final AgentNetworkManagementControllerManager agentNetworkManagementControllerManager;

    @Autowired
    public AgentNetworkManagementController(AgentNetworkService agentNetworkService, AgentNetworkManagementControllerManager agentNetworkManagementControllerManager) {
        this.agentNetworkService = agentNetworkService;
        this.agentNetworkManagementControllerManager = agentNetworkManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@agentNetworkManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/agentNetworkManagement";
    }

    @RequestMapping(value = "/createAgentNetwork", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@agentNetworkManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createAgentNetwork(@Valid @ModelAttribute AgentNetwork agentNetwork, HttpServletRequest request,
                                      Principal principal) {
        return this.agentNetworkManagementControllerManager.saveAgentNetwork(agentNetwork, principal, request);
    }

    @RequestMapping(value = "/updateAgentNetwork", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@agentNetworkManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateAgentNetwork(@Valid @ModelAttribute AgentNetwork agentNetwork,
                                      Principal principal) {
        return this.agentNetworkManagementControllerManager.updateAgentNetwork(agentNetwork, principal);
    }

    @RequestMapping(value = "/searchAgentNetworkData", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@agentNetworkManagement_VIEW')")
    public String searchAgentNetworkData(@RequestParam(value = "agentNetworkCode", required = false) String agentNetworkCode,
                                         @RequestParam(value = "agentNetworkName", required = false) String agentNetworkName,
                                         Model model) {
        List<AgentNetwork> agentNetworkList = this.agentNetworkManagementControllerManager.searchAgentNetwork(agentNetworkCode,agentNetworkName);
        model.addAttribute("agentNetworkListDB", agentNetworkList);
        return "config/content/agentNetworkData";
    }

    @RequestMapping(value = "/findByAgentNetworkSeq/{agentNetworkSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@agentNetworkManagement_VIEW')")
    public
    @ResponseBody
    AgentNetwork findByAgentNetworkSeq(@PathVariable("agentNetworkSeq") Integer agentNetworkSeq) {
        return this.agentNetworkService.findOne(agentNetworkSeq);
    }

    public Model pageLoad(Model model) {
        model.addAttribute("StatusList", MasterDataStatus.getStatusListForCreate());
        return model;
    }
}
