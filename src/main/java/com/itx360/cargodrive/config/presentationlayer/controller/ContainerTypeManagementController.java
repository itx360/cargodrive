package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.ContainerTypeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.ContainerType;
import com.itx360.cargodrive.config.datalayer.service.ContainerCategoryService;
import com.itx360.cargodrive.config.datalayer.service.ContainerSizeService;
import com.itx360.cargodrive.config.datalayer.service.ContainerTypeService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 10/5/2016
 * Time: 2:36 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("config/containerTypeManagement")
public class ContainerTypeManagementController {

    private final ContainerTypeManagementControllerManager containerTypeManagementControllerManager;
    private final ContainerSizeService containerSizeService;
    private final ContainerCategoryService containerCategoryService;
    private final ContainerTypeService containerTypeService;

    @Autowired
    public ContainerTypeManagementController(ContainerTypeManagementControllerManager containerTypeManagementControllerManager,
                                             ContainerSizeService containerSizeService,
                                             ContainerCategoryService containerCategoryService,
                                             ContainerTypeService containerTypeService) {
        this.containerTypeManagementControllerManager = containerTypeManagementControllerManager;
        this.containerSizeService = containerSizeService;
        this.containerCategoryService = containerCategoryService;
        this.containerTypeService = containerTypeService;
    }


    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@containerTypeManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/containerTypeManagement";
    }

    @RequestMapping(value = "/createContainerType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@containerTypeManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createContainerType(@ModelAttribute ContainerType containerType,
                                       Principal principal) {
        return this.containerTypeManagementControllerManager.saveContainerType(containerType, principal);
    }

    @RequestMapping(value = "/updateContainerType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@containerTypeManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateContainerType(@ModelAttribute ContainerType containerType,
                                       Principal principal) {
        return this.containerTypeManagementControllerManager.updateContainerType(containerType, principal);
    }

    @RequestMapping(value = "/searchContainerType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@containerTypeManagement_VIEW')")
    public String searchContainerType(@RequestParam(value = "containerSizeSeq") Integer containerSizeSeq,
                                      @RequestParam(value = "containerCategorySeq") Integer containerCategorySeq,
                                      @RequestParam(value = "description", required = false) String description,
                                      Model model) {
        List<ContainerType> containerTypeList = this.containerTypeManagementControllerManager.searchContainerType(containerSizeSeq,
                containerCategorySeq,
                description);
        model.addAttribute("containerTypeManagementList", containerTypeList);
        return "config/content/containerTypeData";
    }

    @RequestMapping(value = "/findByContainerTypeSeq/{containerTypeSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@containerTypeManagement_VIEW')")
    public
    @ResponseBody
    ContainerType findContainerTypeByContainerTypeSeq(@PathVariable("containerTypeSeq") Integer containerTypeSeq) {
        return this.containerTypeService.findOne(containerTypeSeq);
    }

    private Model pageLoad(Model model) {
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("containerSizeList", this.containerSizeService.findAll());
        model.addAttribute("containerCategoryList", this.containerCategoryService.findAll());
        return model;
    }
}
