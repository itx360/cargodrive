package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.DeliveryTypeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.DeliveryType;
import com.itx360.cargodrive.config.datalayer.model.TaxRegistration;
import com.itx360.cargodrive.config.datalayer.service.DeliveryTypeService;
import com.itx360.cargodrive.config.utility.DeliveryTypeMode;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by shanakajay on 10/5/2016.
 */
@Controller
@RequestMapping("/config/deliveryTypeManagement")
public class DeliveryTypeManagementController {

    private final DeliveryTypeManagementControllerManager deliveryTypeManagementControllerManager;
    private final DeliveryTypeService deliveryTypeService;

    public DeliveryTypeManagementController(DeliveryTypeManagementControllerManager deliveryTypeManagementControllerManager, DeliveryTypeService deliveryTypeService) {
        this.deliveryTypeManagementControllerManager = deliveryTypeManagementControllerManager;
        this.deliveryTypeService = deliveryTypeService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/deliveryTypeManagement";
    }

    @RequestMapping(value = "/createDeliveryType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@deliveryTypeManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createDeliveryType(@ModelAttribute DeliveryType deliveryType,
                                      Principal principal) {
        return this.deliveryTypeManagementControllerManager.createDeliveryType(deliveryType, principal);
    }

    @RequestMapping(value = "/updateDeliveryType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@deliveryTypeManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateDeliveryType(@ModelAttribute DeliveryType deliveryType,
                              Principal principal) {
        return this.deliveryTypeManagementControllerManager.updateDeliveryType(deliveryType, principal);
    }

    @RequestMapping(value = "/searchDeliveryType", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@deliveryTypeManagement_VIEW')")
    public String searchDeliveryType(@RequestParam(value = "deliveryTypeCode") String deliveryTypeCode,
                             @RequestParam(value = "description") String description,
                             @RequestParam(value = "deliveryTypeMode", required = false) String deliveryTypeMode,
                             Model model) {
        model.addAttribute("deliveryTypeList", this.deliveryTypeManagementControllerManager.searchDeliveryType(deliveryTypeCode, description, deliveryTypeMode));
        return "config/content/deliveryTypeData";
    }

    @RequestMapping(value = "/findByDeliveryTypeSeq/{deliveryTypeSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@deliveryTypeManagement_VIEW')")
    public
    @ResponseBody
    DeliveryType findByDeliveryTypeSeq(@PathVariable("deliveryTypeSeq") Integer deliveryTypeSeq) {
        return this.deliveryTypeService.findOne(deliveryTypeSeq);
    }

    private Model pageLoad(Model model) {
        model.addAttribute("deliveryTypeModeList", DeliveryTypeMode.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        return model;
    }
}
