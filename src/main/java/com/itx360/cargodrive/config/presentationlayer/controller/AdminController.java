package com.itx360.cargodrive.config.presentationlayer.controller;


import com.itx360.cargodrive.config.datalayer.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

;

/**
 * Created by shanakajay on 8/22/2016.
 */
@Controller
public class AdminController {

//    @RequestMapping(value = "/config/pagesAndGroups", method = RequestMethod.GET)
//    @PreAuthorize("hasRole('ROLE_config-admin_VIEW')")
    public String loadPageAndGroups() {
        return "config/pagesAndGroups";
    }

   /* @RequestMapping(value = "/config/userManagement/findByUserSeq/{userId}", method = RequestMethod.GET)
    public ResponseEntity<User> findUserById(@PathVariable Integer userId) {
        return new ResponseEntity<User>(userDetailsManager.getUserByUserSeq(userId), HttpStatus.OK);
    }*/

}
