package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.CommodityCategoryManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.CommodityCategory;
import com.itx360.cargodrive.config.datalayer.service.CommodityCategoryService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya-Ehl
 * Date: 9/26/2016
 * Time: 10:58 AM
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("/config/commodityCategoryManagement")
public class CommodityCategoryManagementController {

    private final CommodityCategoryManagementControllerManager commodityCategoryManagementControllerManager;
    private final CommodityCategoryService commodityCategoryService;

    @Autowired
    public CommodityCategoryManagementController(CommodityCategoryManagementControllerManager commodityCategoryManagementControllerManager,
                                                 CommodityCategoryService commodityCategoryService) {
        this.commodityCategoryManagementControllerManager = commodityCategoryManagementControllerManager;
        this.commodityCategoryService = commodityCategoryService;
    }

    /**
     * request for view commodity category management page
     *
     * @param model Model
     * @return commodity category management page
     */
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@commodityCategoryManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/commodityCategoryManagement";
    }

    /**
     * request for create new commodity category
     *
     * @param commodityCategory CommodityCategory
     * @param principal         Principal
     * @return ResponseObject
     */
    @RequestMapping(value = "/createCommodityCategory", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityCategoryManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createCommodityCategory(@ModelAttribute CommodityCategory commodityCategory, Principal principal) {
        return this.commodityCategoryManagementControllerManager.saveCommodityCategory(commodityCategory, principal);
    }

    /**
     * request for update existing commodity category
     *
     * @param commodityCategory CommodityCategory
     * @param principal         Principal
     * @return ResponseObject
     */
    @RequestMapping(value = "/updateCommodityCategory", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityCategoryManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateCommodityCategory(@ModelAttribute CommodityCategory commodityCategory, Principal principal) {
        return this.commodityCategoryManagementControllerManager.updateCommodityCategory(commodityCategory, principal);
    }

    /**
     * request for search commodity category
     *
     * @param commodityCode commodityCode
     * @param commodityName commodityName
     * @param model         Model
     * @return String
     */
    @RequestMapping(value = "/searchCommodityCategory", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@commodityCategoryManagement_VIEW')")
    public String searchCommodityCategory(@RequestParam(value = "commodityCode", required = false) String commodityCode,
                                          @RequestParam(value = "commodityName", required = false) String commodityName,
                                          Model model) {
        List<CommodityCategory> commodityCategoryList = this.commodityCategoryManagementControllerManager.searchCommodityCategoryByCommodityCodeAndCommodityName(commodityCode,
                commodityName);
        model.addAttribute("commodityCategoryList", commodityCategoryList);
        return "config/content/commodityCategoryData";
    }

    /**
     * request for getting commodity category details by commodityCategorySeq
     *
     * @param commodityCategorySeq commodityCategorySeq
     * @return CommodityCategory
     */
    @RequestMapping(value = "/getCommodityCategoryDetails/{commodityCategorySeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@commodityCategoryManagement_VIEW')")
    @ResponseBody
    public CommodityCategory getCommodityCategoryDetails(@PathVariable("commodityCategorySeq") Integer commodityCategorySeq) {
        return this.commodityCategoryService.findOne(commodityCategorySeq);
    }

    /**
     * Common objects are defining here
     *
     * @param model Model
     * @return Model
     */
    private Model pageLoad(Model model) {
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("commodityCategoryList", this.commodityCategoryService.findAll());
        return model;
    }

}
