package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.EmployeeManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.AddressBook;
import com.itx360.cargodrive.config.datalayer.model.Employee;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.datalayer.service.EmployeeDesignationService;
import com.itx360.cargodrive.config.datalayer.service.EmployeeService;
import com.itx360.cargodrive.config.utility.EmployeeDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Sachithrac on 10/4/2016.
 */
@Controller
@RequestMapping("/config/employeeManagement")
public class EmployeeManagementController {

    private final CountryService countryService;
    private final EmployeeDesignationService employeeDesignationService;
    private final EmployeeService employeeService;
    private final EmployeeManagementControllerManager employeeManagementControllerManager;

    public EmployeeManagementController(CountryService countryService, EmployeeDesignationService employeeDesignationService, EmployeeService employeeService, EmployeeManagementControllerManager employeeManagementControllerManager) {
        this.countryService = countryService;
        this.employeeDesignationService = employeeDesignationService;
        this.employeeService = employeeService;
        this.employeeManagementControllerManager = employeeManagementControllerManager;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@employeeManagement_VIEW')")
    public String getPage(Model model) {
        this.pageLoad(model);
        return "config/employeeManagement";
    }

    @RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@employeeManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject addEmployee(@Valid @ModelAttribute AddressBook addressBook, @ModelAttribute Employee employee,
                               Principal principal, HttpServletRequest request) {
        return this.employeeManagementControllerManager.saveEmployee(addressBook, employee, principal, request);
    }

    @RequestMapping(value = "/updateEmployee", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@employeeManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateEmployee(@Valid @ModelAttribute AddressBook addressBook, @ModelAttribute Employee employee,
                                  Principal principal) {
        return this.employeeManagementControllerManager.updateEmployee(addressBook, employee, principal);
    }

    @RequestMapping(value = "/findByEmployeeSeq/{employeeSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@employeeManagement_VIEW')")
    public
    @ResponseBody
    Employee findByEmployeeSeq(@PathVariable("employeeSeq") Integer employeeSeq) {
        return this.employeeService.findOne(employeeSeq);
    }

    @RequestMapping(value = "/searchEmployeeData", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@employeeManagement_VIEW')")
    public String searchEmployeeData(@RequestParam(value = "employeeName", required = false) String employeeName, @RequestParam(value = "status", required = false) Integer status, @RequestParam(value = "employeeDesignationSeq", required = false) Integer employeeDesignationSeq,
                                     Model model) {
        List<Employee> employees = this.employeeManagementControllerManager.searchEmployee(employeeName, status, employeeDesignationSeq);
        model.addAttribute("employeeListDB", employees);
        return "config/content/employeeData";
    }


    public Model pageLoad(Model model) {
        model.addAttribute("designationList", this.employeeDesignationService.findAll());
        model.addAttribute("countryList", countryService.findAll());
        model.addAttribute("statusList", EmployeeDataStatus.values());
        return model;
    }

}
