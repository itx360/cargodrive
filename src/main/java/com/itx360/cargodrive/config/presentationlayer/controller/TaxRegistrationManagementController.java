package com.itx360.cargodrive.config.presentationlayer.controller;

import com.itx360.cargodrive.config.businesslayer.manager.TaxRegistrationManagementControllerManager;
import com.itx360.cargodrive.config.datalayer.model.TaxRegistration;
import com.itx360.cargodrive.config.datalayer.service.CountryService;
import com.itx360.cargodrive.config.datalayer.service.TaxRegistrationService;
import com.itx360.cargodrive.config.datalayer.service.TaxTypeService;
import com.itx360.cargodrive.config.utility.MasterDataStatus;
import com.itx360.cargodrive.master.utility.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Created by Thilangaj on 10/4/2016 12:41 PM).
 */
@Controller
@RequestMapping("/config/taxRegistrationManagement")
public class TaxRegistrationManagementController {

    private final CountryService countryService;

    private final TaxTypeService taxTypeService;

    private final TaxRegistrationManagementControllerManager taxRegistrationManagementControllerManager;

    private final TaxRegistrationService taxRegistrationService;

    @Autowired
    public TaxRegistrationManagementController(CountryService countryService, TaxTypeService taxTypeService, TaxRegistrationManagementControllerManager taxRegistrationManagementControllerManager, TaxRegistrationService taxRegistrationService) {
        this.countryService = countryService;
        this.taxTypeService = taxTypeService;
        this.taxRegistrationManagementControllerManager = taxRegistrationManagementControllerManager;
        this.taxRegistrationService = taxRegistrationService;
    }

    /**
     * Request mapping for viewing the page
     *
     * @param model Spring MVC Model Object to Add Objects
     * @return Page name as a String
     */
    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@taxRegistrationManagement_VIEW')")
    public String getPage(Model model, HttpServletRequest request) {
        this.pageLoad(model, request);
        return "config/taxRegistrationManagement";
    }

    @RequestMapping(value = "/createTaxRegistration", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@taxRegistrationManagement_CREATE')")
    public
    @ResponseBody
    ResponseObject createTaxRegistration(@ModelAttribute TaxRegistration taxRegistration,
                                         HttpServletRequest request,
                                         Principal principal) {
        taxRegistration.setCompanyProfileSeq(Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString()));
        return this.taxRegistrationManagementControllerManager.createTaxRegistration(taxRegistration, principal.getName());
    }

    @RequestMapping(value = "/updateTaxRegistration", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@taxRegistrationManagement_UPDATE')")
    public
    @ResponseBody
    ResponseObject updateTaxRegistration(@ModelAttribute TaxRegistration taxRegistration,
                                         HttpServletRequest request,
                                         Principal principal) {
        taxRegistration.setCompanyProfileSeq(Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString()));
        return this.taxRegistrationManagementControllerManager.updateTaxRegistration(taxRegistration, principal.getName());
    }

    @RequestMapping(value = "/searchTaxRegistration", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_Config@taxRegistrationManagement_VIEW')")
    public String searchTaxRegistration(@RequestParam(value = "taxName", required = false, defaultValue = "") String taxName,
                                        @RequestParam(value = "remarks", required = false, defaultValue = "") String remarks,
                                        @RequestParam(value = "countrySeq") Integer countrySeq,
                                        HttpServletRequest request,
                                        Model model) {
        Integer customerProfileSeq = Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString());
        model.addAttribute("taxRegistrationList", this.taxRegistrationManagementControllerManager.searchTaxRegistration(taxName, remarks, countrySeq, customerProfileSeq));
        return "config/content/taxRegistrationData";
    }

    @RequestMapping(value = "/findByTaxRegistrationSeq/{taxRegistrationSeq}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Config@taxRegistrationManagement_VIEW')")
    public
    @ResponseBody
    TaxRegistration findTaxRegistrationByTaxRegistrationSeq(@PathVariable("taxRegistrationSeq") Integer taxRegistrationSeq) {
        return this.taxRegistrationService.findOne(taxRegistrationSeq);
    }


    /**
     * Object that are dependant for form loading and functionality
     *
     * @param model Model object from GET request
     * @return Objects Assigned Model Object
     */
    private Model pageLoad(Model model, HttpServletRequest request) {
        model.addAttribute("statusList", MasterDataStatus.values());
        model.addAttribute("createStatusList", MasterDataStatus.getStatusListForCreate());
        model.addAttribute("taxTypeList", this.taxTypeService.findAll());
        model.addAttribute("countryList", this.countryService.findAll());
        return model;
    }
}
