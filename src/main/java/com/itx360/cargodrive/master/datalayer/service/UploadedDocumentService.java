package com.itx360.cargodrive.master.datalayer.service;

import com.itx360.cargodrive.master.datalayer.model.UploadedDocument;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Thilanga-Ehl on 9/5/2016 8:32 PM).
 */
public interface UploadedDocumentService {

    UploadedDocument save(MultipartFile multipartFile, String username);

    UploadedDocument findUploadedDocumentByUploadedDocumentSeq(Integer uploadedDocumentSeq);

    List<UploadedDocument> findUploadedDocumentUploadedSeq(List<Integer> uploadedDocumentSeq);

    void findAndWriteToResponse(Integer uploadedDocumentSeq, HttpServletResponse httpServletResponse);

    ResponseEntity<byte[]> findAndDownload(Integer uploadedDocumentSeq);

}
