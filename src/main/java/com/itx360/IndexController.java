package com.itx360;

import com.itx360.cargodrive.config.datalayer.model.SubModule;
import com.itx360.cargodrive.config.datalayer.service.ModuleService;
import com.itx360.cargodrive.config.datalayer.service.SubModuleService;
import com.itx360.cargodrive.config.datalayer.service.UserService;
import com.itx360.cargodrive.master.datalayer.service.UploadedDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

/**
 * Created by Thilanga-Ehl on 8/24/2016 4:17 PM) 4:17 PM) 4:19 PM).
 */
@Controller
public class IndexController {

    private final ModuleService moduleService;

    private final SubModuleService subModuleService;

    private final UserService userService;

    private final UploadedDocumentService uploadedDocumentService;

    @Autowired
    public IndexController(ModuleService moduleService, SubModuleService subModuleService, UserService userService, UploadedDocumentService uploadedDocumentService) {
        this.moduleService = moduleService;
        this.subModuleService = subModuleService;
        this.userService = userService;
        this.uploadedDocumentService = uploadedDocumentService;
    }

    @PreAuthorize("hasRole('ROLE_Home@index_VIEW')")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewPage(Model model, HttpServletRequest request, Principal principal) {
        if (principal == null) {
            return "login";
        }
        this.pageLoad(model, principal, request);
        return "index";
    }

    @RequestMapping(value = "/loadLeftNavi/{moduleName}/{subModuleName}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_Home@index_VIEW')")
    public String loadLeftNavi(@PathVariable String moduleName,
                               @PathVariable String subModuleName,
                               Model model) {
        SubModule subModule = this.subModuleService.findBySubModuleNameAndModuleName(subModuleName, moduleName);
        model.addAttribute("module", this.moduleService.findOne(subModule.getModuleSeq()));
        model.addAttribute("subModule", subModule);
        return "/sidebar";
    }

    private Model pageLoad(Model model, Principal principal, HttpServletRequest request) {
        model.addAttribute("username", principal.getName());
        Integer companyProfileSeq = Integer.parseInt(request.getSession().getAttribute("companyProfileSeq").toString());
        model.addAttribute("companyProfileSeq", companyProfileSeq);
        model.addAttribute("moduleList", this.moduleService.getModuleListByCompanySeqAndUsername(companyProfileSeq, principal.getName()));
        model.addAttribute("user", this.userService.findByUsername(principal.getName()));
        return model;
    }

    @RequestMapping(value = "/getLogo/{uploadDocumentSeq}", method = RequestMethod.GET)
    public void getLogo(@PathVariable Integer uploadDocumentSeq, HttpServletResponse httpServletResponse) throws IOException {
        this.uploadedDocumentService.findAndWriteToResponse(uploadDocumentSeq, httpServletResponse);
    }
}
