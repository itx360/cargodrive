/**
 * Created by Thilanga-Ehl on 9/5/2016.
 */

var _tc = $("meta[name='_csrf']").attr("content");
var _hc = $("meta[name='_csrf_header']").attr("content");

// Header
var headersStomp = {};
headersStomp[_hc] = _tc;

$(document).ajaxSend(function (e, xhr, options) {
    xhr.setRequestHeader(_hc, _tc);
});

function saveFormDataWithAttachments(url, formId, reset) {
    var entityJson = $('#' + formId).serializeJSON(); // js library to get json from form
    var entityJsonStr = JSON.stringify(entityJson);

    var formData = new FormData();
    formData.append("data", new Blob([entityJsonStr], {
        type: "application/json"  // ** specify that this is JSON**
    }));

    // append files, if there are any
    $.each($("#" + formId).find("input[type='file']"), function (i, tag) {
        $.each($(tag)[0].files, function (i, file) {
            formData.append(tag.name, file);
        });
    });

    var returnObject = '';
    $.ajax({
        url: url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        async: false
    }).done(function (data) {
        returnObject = data;
        if (returnObject != '') {
            if (returnObject.status) {
                noty({text: returnObject.message, layout: 'topRight', type: 'success'});
                if (reset == 'reset') {
                    $('#' + formId).trigger("reset");
                }
            } else {
                noty({text: returnObject.message, layout: 'topRight', type: 'error'});
            }
        } else {
            noty({text: 'Critical Error', layout: 'topRight', type: 'error'});
        }
    }).fail(function (jqXHR, textStatus) {
        alert('File upload failed ...');
    });
    return returnObject;
}

function saveFormData(url, formId, reset) {
    var returnObject = '';
    $.ajax({
        url: url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: new FormData(document.getElementById(formId)),
        async: false
    }).done(function (data) {
        returnObject = data;
        if (returnObject != '') {
            if (returnObject.status) {
                noty({text: returnObject.message, layout: 'topCenter', type: 'success'});
                if (reset == 'reset') {
                    $('#' + formId).trigger("reset");
                    $('#' + formId + ' .select').selectpicker('refresh');
                }
            } else {
                noty({text: returnObject.message, layout: 'topCenter', type: 'error'});
            }
        } else {
            noty({text: 'Critical Error', layout: 'topCenter', type: 'error'});
        }
    }).fail(function (jqXHR, textStatus) {
        console.log('Error Saving ...');
        noty({text: 'Critical Error', layout: 'topCenter', type: 'error'});
    });
    return returnObject;
}

function loadDataPost(url, data) {
    var returnObject = '';
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        async: false,
    }).done(function (data) {
        returnObject = data;
    }).fail(function (jqXHR, textStatus) {
        console.log('Error executing Ajax');
    });
    return returnObject;
}

function loadObjectData(url, data, type) {
    var returnObject = null;
    $.ajax({
        url: url + data,
        type: type,
        dataType: 'json',
        async: false,
    }).done(function (data) {
        returnObject = data;
    }).fail(function (jqXHR, textStatus) {
        console.log('Error executing Ajax');
    });
    return returnObject;
}

function loadPageData(url, data, type) {
    var returnObject = '';
    $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: 'text',
        async: false,
    }).done(function (data) {
        returnObject = data;
    }).fail(function (jqXHR, textStatus) {
        console.log('Error executing Ajax');
    });
    return returnObject;
}

function form_validate(formId) {
    var result = true;
    $('#' + formId).validator('validate');
    $('#' + formId + ' .form-group').each(function () {
        if ($(this).hasClass('has-error')) {
            result = false;
            return false;
        }
    });
    return result;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

(function ($) {
    $.fn.BindJson = function (fieldData, idPrefix) {
        var selectorPrefix = " #" + idPrefix,
            key = [],
            ctl = null,
            isTextField = false,
            fill = "";
        for (key in fieldData) {
            if (fieldData.hasOwnProperty(key)) {
                ctl = $(selectorPrefix + '_' + key, this);
                fill = fieldData[key];
                if (ctl.length != 0) {
                    $(ctl).each(function () {
                        isTextField = ($(this).is("DIV") || $(this).is("SPAN") || $(this).is("LABEL"));
                        if (isTextField) {
                            $(this).text(fill);
                        } else if ($(this).prop('type') == 'checkbox') {
                            $(this).prop('checked', true);
                        } else if ($(this).is('SELECT')) {
                            $(this).val(fill);
                            $(this).selectpicker('refresh');
                        } else {
                            $(this).val(fill);
                        }
                    });
                }
            }
        }
    };

    $.fn.BindJsonByForm = function (fieldData, formId) {
        var selectorPrefix = "#" + formId,
            key = [],
            ctl = null,
            isTextField = false,
            fill = "";
        for (key in fieldData) {
            if (fieldData.hasOwnProperty(key)) {
                ctl = $(selectorPrefix + ' :input[name=' + key + ']');
                if (ctl.length == 0) {
                    ctl = $(selectorPrefix + ' .' + key);
                }
                fill = fieldData[key];
                if (ctl.length != 0) {
                    $(ctl).each(function () {
                        isTextField = ($(this).is("DIV") || $(this).is("SPAN") || $(this).is("LABEL"));
                        if (isTextField) {
                            $(this).text(fill);
                        } else if ($(this).prop('type') == 'checkbox') {
                            $(this).prop('checked', true);
                        } else if ($(this).is('SELECT')) {
                            $(this).val(fill);
                            $(this).selectpicker('refresh');
                        } else {
                            $(this).val(fill);
                        }
                    });
                }
            }
        }
    };
})(jQuery);

function populate_dropdown(elementId, data, key, value) {
    $("#" + elementId).empty();
    $.each(data, function (i, datax) {
        var o = new Option("option " + datax[value], datax[key]);
        $(o).html(datax[value]);
        $("#" + elementId).append(o);
    });
    $("#" + elementId).selectpicker('refresh');
}

function add_option_to_dropdown(elementId, datax, key, value) {
    var o = new Option("option " + datax[value], datax[key]);
    $(o).html(datax[value]);
    $("#" + elementId).append(o);
    $("#" + elementId).selectpicker('refresh');
}

function dataTableSubmit(formId, tableId) {
    var $form = $("#" + formId);
    var table = $('#' + tableId).DataTable();
    // Iterate over all checkboxes in the table
    table.$('input[type="checkbox"]').each(function () {
        // If checkbox doesn't exist in DOM
        if (!$.contains(document, this)) {
            // If checkbox is checked
            if (this.checked) {
                // Create a hidden element
                $form.append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', this.name)
                        .val(this.value)
                );
            }
        }
    });
}

function display_update_modal(formContent, modalId, formId, object) {
    $('#' + modalId + ' .modal-body').html(formContent);
    $('#' + formId + ' .bootstrap-select').remove();
    $('#' + formId).find('.removeFromModal').remove();
    $('#' + formId + ' .with-errors').text('');
    $('#' + formId).trigger("reset");
    $('#' + formId + ' input[type=hidden]').val('');
    $('#' + formId).BindJsonByForm(object, 'modal');
    $('#' + formId + ' .select').selectpicker('refresh');
    $('#' + formId + ' .operations').hide();
    $('#' + formId + ' .createOperation').hide();
    $('#' + formId + ' .updateOperation').show();
    $('#' + formId + ' .has-error .has-danger').removeClass("has-error has-danger");
    $('#' + modalId).modal("show");
}

function transform_form(initFormId, newFormId, responseObject) {
    $("#" + initFormId).attr('id', newFormId);
    $('#' + newFormId).trigger('reset');
    $('#' + newFormId + ' input[type=hidden]').val('');
    $('#' + newFormId + ' .select').selectpicker('refresh');
    $('#' + newFormId).BindJsonByForm(responseObject.object, newFormId);
    $('#' + newFormId + ' .createOperation').hide();
    $('#' + newFormId + ' .updateOperation').show();
    $('#' + newFormId + ' .operations').show();
}

function new_form(initFormId, newFormId) {
    $("#" + initFormId).attr('id', newFormId);
    $('#' + newFormId).trigger('reset');
    $('#' + newFormId + ' input[type=hidden]').val('');
    $('#' + newFormId + ' .select').selectpicker('refresh');
    $('#' + newFormId + ' .createOperation').show();
    $('#' + newFormId + ' .updateOperation').hide();
    $('#' + newFormId + ' .operations').hide();
}

