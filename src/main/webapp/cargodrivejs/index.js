/**
 * Created by Thilanga-Ehl on 8/31/2016.
 */

function resizeDiv() {
    var window_height = $("#pageContent").height(),
        content_height = window_height + 100;
    $('.page-content').height(content_height);
}

$(function () {
    loadLeftNavi('Config/Master Settings');
    loadPageContent('config/bankManagement', 'Bank');
});

function loadPageContent(pageRefInput, linkDescription) {
    $.ajax({
        url: pageRefInput,
        type: "GET",
        dataType: 'text',
        success: function (response) {
            $('#pageContent').html(response);
            var selectedListId = pageRefInput.replace("/", "-");
            $('#' + selectedListId).toggleClass("active");
            $('#' + selectedListId).siblings().removeClass("active");
            $('#breadcrumbDocumentLink').html(linkDescription);
            resizeDiv();
        },
        error: function (error) {
            console.log('the page was NOT loaded', error);
        }
    });
}

function loadLeftNavi(pageRefInput) {
    $.ajax({
        url: '/loadLeftNavi/' + pageRefInput,
        type: 'GET',
        dataType: 'text',
        success: function (response) {
            $("#sideBarData").html(response);
            $("#breadcrumbSubModule").html(pageRefInput);
            $("#x-navigation").mCustomScrollbar("update");
            initializeSideBar();
        },
        error: function (error) {
            console.log('the page was NOT loaded', error);
        }
    });
}
