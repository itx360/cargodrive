/**
 * Created by Thilangaj on 9/20/2016.
 */
$(function () {
    $(".select").selectpicker();
});

function create_commodityType() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/commodityTypeManagement/createCommodityType', 'create','reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_commodityTypeSeq', responseObject.object, 'commodityTypeSeq', 'commodityTypeCode');
            $('#update').BindJson(responseObject.object, 'update');
        }
    }
}

function load_commodityType_by_commodityType_seq_to_update() {
    var commodityTypeSeq = $("select#update_commodityTypeSeq").val();
    if (commodityTypeSeq != '-1') {
        var commodityType = loadObjectData('config/commodityTypeManagement/findCommodityTypeByCommodityTypeSeq/', commodityTypeSeq, 'GET');
        $('#update').BindJson(commodityType, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function update_commodityType() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/commodityTypeManagement/updateCommodityType', 'update');
        $('#update_commodityTypeSeq option[value=' + responseObject.object.commodityTypeSeq + ']').text(responseObject.object.commodityTypeCode);
        $('#update .select').selectpicker('refresh');
    }
}

function update_commodityType_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/commodityTypeManagement/updateCommodityType', 'modal');
        $('#modal_commodityTypeSeq option[value=' + responseObject.object.commodityTypeSeq + ']').text(responseObject.object.commodityTypeCode);
        $('#modal').BindJson(responseObject.object, 'modal');
        $('#modal .select').selectpicker('refresh');
    }
}

function load_commodityType_data_to_modal(commodityTypeSeq) {
    var commodityType = loadObjectData('config/commodityTypeManagement/findCommodityTypeByCommodityTypeSeq/', commodityTypeSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(commodityType, 'modal');
    $("#myModal").modal("show");
}

function search_commodityType() {
    var commodityTypeCode = $('input#search_commodityTypeCode').val();
    var description = $('input#search_description').val();
    var data = {'commodityTypeCode': commodityTypeCode, 'description': description};
    var responseObject = loadPageData('config/commodityTypeManagement/searchCommodityType', data, "POST");
    $("#loadCommodityTypeData").html(responseObject);
    $('.datatable').DataTable();
}