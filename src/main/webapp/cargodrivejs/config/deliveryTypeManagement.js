/**
 * Created by shanakajay on 10/5/2016.
 */
$(function () {
    $(".select").selectpicker();
});

function create_delivery_type() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/deliveryTypeManagement/createDeliveryType', 'create');
        if (responseObject.status == true) {
            transform_form('create','update',responseObject);
        }
    }
}

function update_delivery_type() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/deliveryTypeManagement/updateDeliveryType', 'update');
        if (responseObject.status == true) {
            $('#update').BindJsonByForm(responseObject.object, 'update');
        }
    }
}

function delete_delivery_type() {

}

function search_delivery_type() {
    var deliveryTypeCode = $('input#search_deliveryTypeCode').val();
    var description = $('input#search_description ').val();
    var deliveryTypeMode = $('select#search_deliveryTypeMode').val();
    var data = {'deliveryTypeCode': deliveryTypeCode, 'description': description, 'deliveryTypeMode': deliveryTypeMode};
    var responseObject = loadPageData('config/deliveryTypeManagement/searchDeliveryType', data, "POST");
    $("#loadDeliveryTypeData").html(responseObject);
    $('.datatable').DataTable();
}

function update_delivery_type_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/deliveryTypeManagement/updateDeliveryType', 'modal');
        $('#modal').BindJsonByForm(responseObject.object, 'modal');
    }
}

function load_delivery_type_data_to_modal(deliveryTypeSeq) {
    var deliveryType = loadObjectData('config/deliveryTypeManagement/findByDeliveryTypeSeq/', deliveryTypeSeq, 'GET');
    var formContent = $(".deliveryTypeForm").html();
    display_update_modal(formContent, 'myModal', 'modal', deliveryType);
}
