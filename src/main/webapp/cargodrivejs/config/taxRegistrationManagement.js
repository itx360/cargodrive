/**
 * Created by Thilangaj on 10/4/2016.
 */
$(function () {
    $(".select").selectpicker();
});

function create_tax_registration() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/taxRegistrationManagement/createTaxRegistration', 'create');
        if (responseObject.status == true) {
            transform_form('create','update',responseObject);
        }
    }
}

function update_tax_registration() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/taxRegistrationManagement/updateTaxRegistration', 'update');
        if (responseObject.status == true) {
            $('#update').BindJsonByForm(responseObject.object, 'update');
        }
    }
}

function delete_tax_registration() {

}

function search_tax_registration() {
    var taxName = $('input#search_taxName').val();
    var countrySeq = $('select#search_countrySeq').val();
    var remarks = $('input#search_remarks').val();
    var data = {'taxName': taxName, 'countrySeq': countrySeq, 'remarks': remarks};
    var responseObject = loadPageData('config/taxRegistrationManagement/searchTaxRegistration', data, "POST");
    $("#loadTaxRegistrationData").html(responseObject);
    $('.datatable').DataTable();
}

function update_tax_registration_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/taxRegistrationManagement/updateTaxRegistration', 'modal');
        $('#modal').BindJsonByForm(responseObject.object, 'modal');
    }
}

function load_tax_registration_data_to_modal(taxRegistrationSeq) {
    var taxRegistration = loadObjectData('config/taxRegistrationManagement/findByTaxRegistrationSeq/', taxRegistrationSeq, 'GET');
    var formContent = $(".taxRegistrationForm").html();
    display_update_modal(formContent, 'myModal', 'modal', taxRegistration);
}
