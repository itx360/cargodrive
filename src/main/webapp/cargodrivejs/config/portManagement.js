/**
 * Created by Harshaa on 9/21/2016.
 */

$(function () {

    $(".select").selectpicker();

});


function create_port() {
    if (form_validate("create")) {
        var responseObject = saveFormData("/config/portManagement/createPort", "create");
        if (responseObject.status == true) {
            add_option_to_dropdown("update_portSeq", responseObject.object, 'portSeq', 'portName');
        }
    }
}

function update_port(formId, formName) {
    if (form_validate(formId)) {
        saveFormData("/config/portManagement/updatePort", formName);
    }
}


function load_locations_by_country(formPrefix) {
    var countrySeq = $("select#" + formPrefix + "_countrySeq").val();
    var locationsList = loadObjectData("/config/portManagement/getLocationsByCountry/", countrySeq, "GET");
    populate_dropdown(formPrefix + "_locationSeq", locationsList, "locationSeq", "locationName");
}

function load_port_to_update() {
    var portSeq = $("select#update_port").val();
    if (portSeq != null || portSeq != -1) {
        var port = loadObjectData("/config/portManagement/findPortByPortSeq/", portSeq, "GET");
        $("#update").BindJson(port, 'update');
    } else {
        $("#update").reset();
    }
    var locationsList = load_locations_by_country('update');
    populate_dropdown("update_locationSeq", locationsList, "locationSeq", "locationName");
}

function search_port() {
    var portName = $("input#searchPorts_portName").val();
    var mode = $("select#searchPorts_transportMode").val();
    var locationSeq = $("select#searchPorts_locationSeq").val();

    var searchParams = {"portName": portName, "transportMode": mode, "locationSeq": locationSeq};

    if (form_validate("searchPorts")) {
        var responseObject = loadPageData("/config/portManagement/searchPorts", searchParams, "POST");
    }
    $("#loadPortSearchResult").html(responseObject);
    $(".datatable").DataTable();
}

function load_port_data_to_modal(portSeq) {
    if (portSeq != '-1') {
        var port = loadObjectData('config/portManagement/findPortByPortSeq/', portSeq, 'GET');
        $('#portDetailsModal').BindJson(port, 'modal');
        $("#portDetailsModal").modal("show");
    }
}
