/**
 * Created by Thilangaj on 9/20/2016.
 */
$(function () {
    $(".select").selectpicker();
});

function create_HsCategory() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/hsCategoryManagement/createHsCategory', 'create','reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_hsCategorySeq', responseObject.object, 'hsCategorySeq', 'hsCategoryName');
            $('#update').BindJson(responseObject.object, 'update');
        }
    }
}

function load_hsCategory_by_hsCategory_seq_to_update() {
    var hsCategorySeq = $("select#update_hsCategorySeq").val();
    if (hsCategorySeq != '-1') {
        var hsCategory = loadObjectData('config/hsCategoryManagement/findHsCategoryByHsCategorySeq/', hsCategorySeq, 'GET');
        $('#update').BindJson(hsCategory, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function update_HsCategory() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/hsCategoryManagement/updateHsCategory', 'update');
        $('#update_hsCategorySeq option[value=' + responseObject.object.hsCategorySeq + ']').text(responseObject.object.hsCategoryName);
        $('#update .select').selectpicker('refresh');
    }
}

function update_hsCategory_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/hsCategoryManagement/updateHsCategory', 'modal');
        $('#modal_hsCategorySeq option[value=' + responseObject.object.hsCategorySeq + ']').text(responseObject.object.hsCategoryName);
        $('#modal').BindJson(responseObject.object, 'modal');
        $('#modal .select').selectpicker('refresh');
    }
}

function load_hsCategory_data_to_modal(hsCategorySeq) {
    var hsCategory = loadObjectData('config/hsCategoryManagement/findHsCategoryByHsCategorySeq/', hsCategorySeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(hsCategory, 'modal');
    $("#myModal").modal("show");
}

function search_HsCategory() {
    var hsCategoryName = $('input#search_hsCategoryName').val();
    var description = $('input#search_description').val();
    var data = {'hsCategoryName': hsCategoryName, 'description': description};
    var responseObject = loadPageData('config/hsCategoryManagement/searchHsCategory', data, "POST");
    $("#loadHsCategoryData").html(responseObject);
    $('.datatable').DataTable();
}