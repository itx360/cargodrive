/**
 * Created by Udaya-Ehl on 10/4/2016.
 */

$(function () {
    $(".select").selectpicker();
});


function create_new_packageType() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/packageTypeManagement/createPackageType', 'create', 'reset');
        if (responseObject.status == true) {
            transform_form('create', 'update', responseObject);
        }
    }
}

function update_package_type() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/packageTypeManagement/updatePackageType', 'update');
        if (responseObject.status == true) {
            $('#update').BindJsonByForm(responseObject.object, 'update');
        }
    }
}

function search_package_type() {
    var packageTypeName = $('input#search_packageTypeName').val();
    var packageTypeCode = $('input#search_packageTypeCode').val();
    var description = $('input#search_description').val();
    var data = {
        'packageTypeName': packageTypeName,
        'packageTypeCode': packageTypeCode,
        'description': description
    };
    if (form_validate("search")) {
        var responseObject = loadPageData('config/packageTypeManagement/searchPackageType', data, "POST");
    }
    $("#loadPackageTypeSearchResult").html(responseObject);
    $('.datatable').DataTable();
}

function update_package_type_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/packageTypeManagement/updatePackageType', 'modal');
        if (responseObject.status == true) {
            $('#update').BindJsonByForm(responseObject.object, 'update');
        }
    }
}

function load_package_type_data_to_modal(packageTypeSeq) {
    var packageType = loadObjectData('config/packageTypeManagement/findByPackageTypeSeq/', packageTypeSeq, 'GET');
    var formContent = $(".packageTypeForm").html();
    display_update_modal(formContent, 'myModal', 'modal', packageType);
}

function delete_package_type() {

}