$(function () {
    $(".select").selectpicker();
});

function create_customer_group() {
    if (form_validate("create")) {
        var customerGroup = saveFormData('config/customerGroupManagement/addCustomerGroup', 'create', 'reset');
        add_option_to_dropdown('update_customerGroupSeq', customerGroup.object, 'customerGroupSeq', 'customerGroupName');
        $('#update').BindJson(customerGroup.object, 'update');
    }
}

function load_customer_group_customer_group_seq_to_update() {
    var customerGroupSeq = $("#update_customerGroupSeq").val();
    if (customerGroupSeq != '-1') {
        var customerGroup = loadObjectData('config/customerGroupManagement/findCustomerGroupByCustomerGroupSeq/', customerGroupSeq, 'GET');
        $('#update').BindJson(customerGroup, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function update_customer_group() {
    if (form_validate("update")) {
        var customerGroup = saveFormData('config/customerGroupManagement/updateCustomerGroup', 'update');
        $('#update_customerGroupSeq option[value='+customerGroup.object.customerGroupSeq+']').text(customerGroup.object.customerGroupName);
        $('#update_customerGroupSeq').selectpicker('refresh');
    }
}

function update_customer_group_popup() {
    if (form_validate("modal")) {
        var customerGroup = saveFormData('config/customerGroupManagement/updateCustomerGroup', 'modal');
        $('#update_customerGroupSeq option[value='+customerGroup.object.customerGroupSeq+']').text(customerGroup.object.customerGroupName);
        $('#update_customerGroupSeq').selectpicker('refresh');

    }
}


function load_customer_group_table_data() {
    var customerGroupCode = $('input#search_customerGroupCode').val();
    var customerGroupName = $('input#search_customerGroupName').val();
    var data = {'customerGroupCode': customerGroupCode, 'customerGroupName': customerGroupName};
    var pageData = loadPageData('config/customerGroupManagement/searchCustomerGroupData', data, "POST");
    $("#loadCustomerGroupListData").html(pageData);
    $('.datatable').DataTable();

}

function load__customer_group_popup(customerGroupSeq) {
    var data = {'customerGroupSeq': customerGroupSeq};
    var customerGroup = loadObjectData('config/customerGroupManagement/getCustomerGroupDetails/', customerGroupSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(customerGroup, 'modal');
    $("#myModal").modal("show");
}