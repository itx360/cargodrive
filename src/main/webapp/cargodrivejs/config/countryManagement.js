/**
 * Created by shanakajay on 9/19/2016.
 */
$(function () {
    $('.select').selectpicker();
});

function create_country() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/countryManagement/createCountry', 'create');
        add_option_to_dropdown('update_countrySeq',responseObject.object,'countrySeq','countryName')
    }
}

function update_country() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/countryManagement/updateCountry', 'update');
    }
}

function search_country() {
    var countryName = $('input#search_countryName').val();
    var countryCode = $('input#search_countryCode').val();
    var data = {'countryName': countryName,'countryCode':countryCode};
    var responseObject = loadPageData('config/countryManagement/searchCountry', data, "POST");
    $("#loadCountryList").html(responseObject);
    $('.datatable').DataTable();
}

function load_country_by_country_seq_to_update() {
    var countrySeq = $("select#update_countrySeq").val();
    if (countrySeq != '-1') {
        var country = loadObjectData('config/countryManagement/findCountryByCountrySeq/', countrySeq, 'GET');
        $('#update').BindJson(country, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function update_country_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/countryManagement/updateCountry', 'modal');
    }
}

function load_country_data_to_modal(countrySeq) {
    var country = loadObjectData('config/countryManagement/getCountryDetails/', countrySeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(country, 'modal');
    $("#myModal").modal("show");
}
