$(function () {
    $(".select").selectpicker();
});

function create_warehouse() {
    if (form_validate("create")) {
        var warehouse = saveFormData('config/warehouseManagement/addWarehouse', 'create', 'reset');
        add_option_to_dropdown('update_warehouseSeq',warehouse.object,'warehouseSeq','warehouseName');
        $('#update').BindJson(warehouse.object, 'update');
        $('#update').BindJson(warehouse.object.addressBook, 'update');

    }
}

function update_warehouse() {
    if (form_validate("update")) {
        var warehouse = saveFormData('config/warehouseManagement/updateWarehouse', 'update');
        $('#update_warehouseSeq option[value='+warehouse.object.warehouseSeq+']').text(warehouse.object.warehouseName);
        $('#update_warehouseSeq').selectpicker('refresh');

    }
}

function update_warehouse_popup() {
    if (form_validate("modal")) {
        var warehouse = saveFormData('config/warehouseManagement/updateWarehouse', 'modal');
        $('#update_warehouseSeq option[value='+warehouse.object.warehouseSeq+']').text(warehouse.object.warehouseName);
        $('#update_warehouseSeq').selectpicker('refresh');
    }
}

function load_warehouse_by_warehouse_seq_to_update() {
    var warehouseSeq = $("select#update_warehouseSeq").val();
    if (warehouseSeq != '-1') {
        var warehouse = loadObjectData('config/warehouseManagement/findWarehouseByWarehouseSeq/', warehouseSeq, 'GET');
        $('#update').BindJson(warehouse, 'update');
        $('#update').BindJson(warehouse.addressBook, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function load_warehouse_table_data() {
    var warehouseCode = $('input#search_warehouseCode').val();
    var warehouseName = $('input#search_warehouseName').val();
    var countrySeq = $('select#search_countrySeq').val();
    var data = {'warehouseCode': warehouseCode,'warehouseName': warehouseName,'countrySeq': countrySeq};
    var pageData = loadPageData('config/warehouseManagement/searchWarehouseData', data, "POST");
    $("#loadWarehouseData").html(pageData);
    $('.datatable').DataTable();


}


function load__warehouse_popup(warehouseSeq) {
    var data = {'warehouseSeq': warehouseSeq};
    var warehouse = loadObjectData('config/warehouseManagement/getWarehouseDetails/', warehouseSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(warehouse, 'modal');
    $('#modal').BindJson(warehouse.addressBook, 'modal');
    $("#myModal").modal("show");


}
