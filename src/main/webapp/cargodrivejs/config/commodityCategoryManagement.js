/**
 * Created by Udaya-Ehl on 9/26/2016.
 */

$(function () {
    $(".select").selectpicker();
});

/**
 * function used to create new commodity category
 */
function create_commodity_category() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/commodityCategoryManagement/createCommodityCategory', 'create', 'reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_commodityCategorySeq', responseObject.object, 'commodityCategorySeq', 'commodityCode');
            $('#update').BindJson(responseObject.object, 'update');
        }
    }
}

/**
 * function used to update existing commodity category
 */
function update_commodity_category() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/commodityCategoryManagement/updateCommodityCategory', 'update');
    }
}

/**
 * function used to search commodity category by commodity code and commodity name
 */
function search_Commodity_category() {
    var commodityCode = $('input#search_commodityCode').val();
    var commodityName = $('input#search_commodityName').val();
    var data = {
        'commodityCode': commodityCode,
        'commodityName': commodityName
    };
    if (form_validate("search")) {
        var responseObject = loadPageData('config/commodityCategoryManagement/searchCommodityCategory', data, "POST");
    }
    $("#loadCommodityCategorySearchResult").html(responseObject);
    $('.datatable').DataTable();
}

/**
 * function used to update commodity category modal data
 */
function update_commodity_category_modal_data() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/commodityCategoryManagement/updateCommodityCategory', 'modal');
    }
}

/**
 * function used to load commodity category details by commodity category sequence
 */
function load_commodity_category_by_commodity_category_seq_to_update() {
    var commodityCategorySeq = $("select#update_commodityCategorySeq").val();
    if (commodityCategorySeq != '') {
        var commodityCategory = loadObjectData('config/commodityCategoryManagement/getCommodityCategoryDetails/', commodityCategorySeq, 'GET');
        $('#update').BindJson(commodityCategory, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

/**
 * function used to load commodity category data to the modal
 * @param commodityCategorySeq commodityCategorySeq
 */
function load_commodity_category_data_to_modal(commodityCategorySeq) {
    var responseObject = loadObjectData('config/commodityCategoryManagement/getCommodityCategoryDetails/', commodityCategorySeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(responseObject, 'modal');
    $("#myModal").modal("show");
}