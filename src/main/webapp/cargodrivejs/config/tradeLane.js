$(function () {
    $(".select").selectpicker();
});

function create_trade_lane() {
    if (form_validate("create")) {
        var tradeLane = saveFormData('config/tradeLane/addTradeLane', 'create', 'reset');
        if (tradeLane.status == true) {
            add_option_to_dropdown('update_tradeLaneSeq', tradeLane.object, 'tradeLaneSeq', 'tradeLaneName');
            $('#update').BindJson(tradeLane.object, 'update');
        }
    }
}

function update_trade_lane() {
    if (form_validate("update")) {
        var tradeLane = saveFormData('config/tradeLane/updateTradeLane', 'update');
        $('#update_tradeLaneSeq option[value='+tradeLane.object.tradeLaneSeq+']').text(tradeLane.object.tradeLaneName);
        $('#update_tradeLaneSeq').selectpicker('refresh');
    }
}

function update_trade_lane_popup() {
    if (form_validate("modal")) {
        var tradeLane = saveFormData('config/tradeLane/updateTradeLane', 'modal');
        $('#update_tradeLaneSeq option[value='+tradeLane.object.tradeLaneSeq+']').text(tradeLane.object.tradeLaneName);
        $('#update_tradeLaneSeq').selectpicker('refresh');
    }
}

function load__trade_lane_popup(tradeLaneSeq) {
    var data = {'tradeLaneSeq': tradeLaneSeq};
    var tradeLane = loadObjectData('config/tradeLane/getTradeLaneDetails/', tradeLaneSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(tradeLane, 'modal');
    $("#myModal").modal("show");


}

function load_trade_lane_trade_lane_seq_to_update() {
    var tradeLaneSeq = $("#update_tradeLaneSeq").val();
    if (tradeLaneSeq != '-1') {
        var tradeLane = loadObjectData('config/tradeLane/findTradeLaneByTradeLaneSeq/', tradeLaneSeq, 'GET');
        $('#update').BindJson(tradeLane, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function load_trade_lane_table_data() {
    var tradeLaneCode = $('input#search_tradeLaneCode').val();
    var tradeLaneName = $('input#search_tradeLaneName').val();
    var data = {'tradeLaneCode': tradeLaneCode, 'tradeLaneName': tradeLaneName};
    var pageData = loadPageData('config/tradeLane/searchTradeLaneData', data, "POST");
    $("#loadTradeLaneListData").html(pageData);
    $('.datatable').DataTable();


}


