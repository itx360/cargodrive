/**
 * Created by Harshaa on 9/9/2016.
 */

$(function () {
    $("#create_companyLogo").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-danger",
        fileType: "any"
    });

    $("#update_companyLogo").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-danger",
        fileType: "any"
    });

    $(".select").selectpicker();

});

function create_company() {
    if (form_validate("create")) {
        var response = saveFormDataWithAttachments('config/companyProfileManagement/createCompanyProfile', 'create');
        if (response.status != false){
            add_option_to_dropdown('updateCompanyProfileFromCompanySeq', response.object, 'companyProfileSeq', 'companyProfileName')
        }
    }
}

function update_company() {
    if(form_validate("update")){
        saveFormDataWithAttachments('config/companyProfileManagement/updateCompanyProfile', 'update')
    }
}

function load_company_profiles_by_company_seq_to_update(){
    var companyProfileSeq = $("#updateCompanyProfileFromCompanySeq").val();
    if (companyProfileSeq != '-1') {
        var companyProfile = loadObjectData('config/companyProfileManagement/findCompanyProfileByCompanyProfileSeq/', companyProfileSeq, 'GET');
        $('#update').BindJson(companyProfile, 'update');
        if (companyProfile.addressBook != null){
            $('#update #update_telephoneNumber').val(companyProfile.addressBook.telephone);
            $('#update #update_email').val(companyProfile.addressBook.email);
        }
    } else {
        $('#update').rest();
    }
}

function load_assigned_module_list_by_company() {
    var companyProfileSeq = $('select#companyToRemove').val();
    console.log(companyProfileSeq);
    if (companyProfileSeq != null) {
        var moduleList = loadObjectData('config/companyProfileManagement/getModuleListByCompanyProfileSeq/', companyProfileSeq, "GET");
        console.log(moduleList);
        populate_dropdown('companyModulesToRemove', moduleList, 'moduleSeq', 'moduleName');
    }
}

function assign_modules_to_company() {
    if (form_validate("assignModulesToCompanyProfile")) {
        saveFormData("/config/companyProfileManagement/assignModules", "assignModulesToCompanyProfile");
    }
}

function remove_modules_from_company() {
    if (form_validate("removeModulesFromCompanyProfile")) {
        saveFormData("/config/companyProfileManagement/removeModules", "removeModulesFromCompanyProfile");
    }
}