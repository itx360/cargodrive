/**
 * Created by Sachithrac on 10/7/2016.
 */
$(function () {
    $(".select").selectpicker();
});

function create_agent_network() {
    if (form_validate("create")) {
        var agentNetwork = saveFormData('config/agentNetworkManagement/createAgentNetwork', 'create');
        if (agentNetwork.status == true) {
            transform_form('create', 'update', agentNetwork);
        }
    }
}

function update_agent_network() {
    if (form_validate("update")) {
        var agentNetwork = saveFormData('config/agentNetworkManagement/updateAgentNetwork', 'update');
        if (agentNetwork.status == true) {
            $('#update').BindJsonByForm(agentNetwork.object, 'update');
        }
    }
}

function search_agent_network() {
    var agentNetworkCode = $('input#search_agentNetworkCode').val();
    var agentNetworkName = $('input#search_agentNetworkName').val();
    var data = {'agentNetworkCode': agentNetworkCode,'agentNetworkName': agentNetworkName};
    var agentNetwork = loadPageData('config/agentNetworkManagement/searchAgentNetworkData', data, "POST");
    $("#loadAgentNetworkData").html(agentNetwork);
    $('.datatable').DataTable();
}

function update_agent_network_modal() {
    if (form_validate("modal")) {
        var agentNetwork = saveFormData('config/agentNetworkManagement/updateAgentNetwork', 'modal');
        $('#modal').BindJsonByForm(agentNetwork.object, 'modal');
    }
}
function delete_agent_network() {

}

function load_agent_network_data_to_modal(agentNetworkSeq) {
    var agentNetwork = loadObjectData('config/agentNetworkManagement/findByAgentNetworkSeq/',agentNetworkSeq, 'GET');
    var formContent = $(".agentNetworkForm").html();
    display_update_modal(formContent, 'myModal', 'modal', agentNetwork);

}
