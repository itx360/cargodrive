/**
 * Created by Thilangaj on 9/20/2016.
 */
$(function () {
    $(".select").selectpicker();
});

function create_location() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/locationManagement/createLocation', 'create');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_locationSeq', responseObject.object, 'locationSeq', 'locationName');
        }
    }
}

function load_location_by_location_seq_to_update() {
    var locationSeq = $("select#update_locationSeq").val();
    if (locationSeq != '-1') {
        var location = loadObjectData('config/locationManagement/findLocationByLocationSeq/', locationSeq, 'GET');
        $('#update').BindJson(location, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function update_location() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/locationManagement/updateLocation', 'update');
    }
}

function update_location_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/locationManagement/updateLocation', 'modal');
    }
}

function load_location_data_to_modal(locationSeq) {
    var location = loadObjectData('config/locationManagement/findLocationByLocationSeq/', locationSeq, 'GET');
    $('#modal').BindJson(location, 'modal');
    $("#myModal").modal("show");
}

function search_location() {
    var locationName = $('input#search_locationName').val();
    var countrySeq = $('select#search_countrySeq').val();
    var data = {'locationName': locationName, 'countrySeq': countrySeq};
    var responseObject = loadPageData('config/locationManagement/searchLocation', data, "POST");
    $("#loadLocationData").html(responseObject);
    $('.datatable').DataTable();
}