$(function () {
    $(".select").selectpicker();
});

function create_custom_house_agent() {
    if (form_validate("create")) {
        var customHouseAgent = saveFormData('config/customHouseAgentManagement/addCustomHouseAgent', 'create');
        if (customHouseAgent.status == true) {
            transform_form('create', 'update', customHouseAgent);
            $('#update').BindJsonByForm(customHouseAgent.object, 'update');
            $('#update').BindJsonByForm(customHouseAgent.object.addressBook, 'update');

        }

    }
}

function update_custom_house_agent() {
    if (form_validate("update")) {
        var customHouseAgent = saveFormData('config/customHouseAgentManagement/updateCustomHouseAgent', 'update');
        if (customHouseAgent.status == true) {
            $('#update').BindJsonByForm(customHouseAgent.object, 'update');
            $('#update').BindJsonByForm(customHouseAgent.object.addressBook, 'update');

        }
    }
}

function update_custom_house_agent_modal() {
    if (form_validate("modal")) {
        var customHouseAgent = saveFormData('config/customHouseAgentManagement/updateCustomHouseAgent', 'modal');
        $('#modal').BindJsonByForm(customHouseAgent.object, 'modal');
        $('#modal').BindJsonByForm(customHouseAgent.object.addressBook, 'update');
    }
}
function delete_custom_house_agent() {

}
function load_custom_house_agent_table_data() {
    var customHouseAgentName = $('input#search_customHouseAgentName').val();
    var taxTypeSeq = $('select#search_taxTypeSeq').val();
    var taxRegistrationNo = $('input#search_taxRegistrationNo').val();
    var description = $('input#search_description').val();
    var data = {
        'customHouseAgentName': customHouseAgentName,
        'taxTypeSeq': taxTypeSeq,
        'taxRegistrationNo': taxRegistrationNo,
        'description': description
    };
    var pageData = loadPageData('config/customHouseAgentManagement/searchCustomHouseAgentData', data, "POST");
    $("#loadCustomHouseAgentListData").html(pageData);
    $('.datatable').DataTable();
}

function load__custom_house_agent_popup(customHouseAgentSeq) {
    var customHouseAgent = loadObjectData('config/customHouseAgentManagement/findByCustomHouseAgentSeq/', customHouseAgentSeq, 'GET');
    var formContent = $(".customHouseAgentForm").html();
    display_update_modal(formContent, 'myModal', 'modal', customHouseAgentSeq);
    $('#modal').BindJsonByForm(customHouseAgentSeq.addressBook, 'modal');
}


