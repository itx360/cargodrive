$(function () {
    $(".select").selectpicker();
});

function create_branch() {
    if (form_validate("create")) {
        var branch = saveFormData('config/branchManagement/addBranch', 'create', 'reset');
        add_option_to_dropdown('update_branchSeq', branch.object, 'branchSeq', 'branchName');
        $('#update').BindJson(branch.object, 'update');
        $('#update').BindJson(branch.object.addressBook, 'update');

    }
}

function update_branch() {
    if (form_validate("update")) {
        var branch = saveFormData('config/branchManagement/updateBranch', 'update');
        $('#update_branchSeq option[value='+branch.object.branchSeq+']').text(branch.object.branchName);
        $('#update_branchSeq').selectpicker('refresh');
    }
}

function update_branch_popup() {
    if (form_validate("modal")) {
        var branch = saveFormData('config/branchManagement/updateBranch', 'modal');
        $('#update_branchSeq option[value='+branch.object.branchSeq+']').text(branch.object.branchName);
        $('#update_branchSeq').selectpicker('refresh');

    }
}

function load_branch_by_branch_seq_to_update() {
    var branchSeq = $("select#update_branchSeq").val();
    if (branchSeq != '-1') {
        var branch = loadObjectData('config/branchManagement/findBranchByBranchSeq/', branchSeq, 'GET');
        $('#update').BindJson(branch, 'update');
        $('#update').BindJson(branch.addressBook, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function load_branch_table_data() {
    var branchName = $('input#search_branchName').val();
    var branchCode = $('input#search_branchCode').val();
    var bankName = $('input#search_bankName').val();
    var data = {'branchCode': branchCode, 'branchName': branchName, 'bankName': bankName};
    var pageData = loadPageData('config/branchManagement/searchBranchData', data, "POST");
    $("#loadBranchListData").html(pageData);
    $('.datatable').DataTable();
}


function load__branch_popup(branchSeq) {
    var data = {'branchSeq': branchSeq};
    var branch = loadObjectData('config/branchManagement/getBranchDetails/', branchSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(branch, 'modal');
    $('#modal').BindJson(branch.addressBook, 'modal');
    $("#myModal").modal("show");
}
