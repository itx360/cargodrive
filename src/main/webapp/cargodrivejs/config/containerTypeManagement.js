/**
 * Created by Udaya-Ehl on 10/5/2016.
 */

$(function () {
    $(".select").selectpicker();
});


function create_container_type() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/containerTypeManagement/createContainerType', 'create');
        if (responseObject.status == true) {
            transform_form('create', 'update', responseObject);
        }
    }
}

function update_container_type() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/containerTypeManagement/updateContainerType', 'update');
        if (responseObject.status == true) {
            $('#update').BindJsonByForm(responseObject.object, 'update');
        }
    }
}

function search_container_type() {
    var containerSizeSeq = $('select#search_containerSizeSeq').val();
    var containerCategorySeq = $('select#search_containerCategorySeq').val();
    var description = $('input#search_description').val();
    var data = {
        'containerSizeSeq': containerSizeSeq,
        'containerCategorySeq': containerCategorySeq,
        'description': description
    };
    var responseObject = loadPageData('config/containerTypeManagement/searchContainerType', data, "POST");
    $("#loadContainerTypeData").html(responseObject);
    $('.datatable').DataTable();
}

function update_container_type_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/containerTypeManagement/updateContainerType', 'modal');
        $('#modal').BindJsonByForm(responseObject.object, 'modal');
    }
}

function load_container_type_data_to_modal(containerTypeSeq) {
    var containerType = loadObjectData('config/containerTypeManagement/findByContainerTypeSeq/', containerTypeSeq, 'GET');
    var formContent = $(".containerTypeForm").html();
    display_update_modal(formContent, 'myModal', 'modal', containerType);
}

function delete_container_type() {

}

function validateFloatKeyPress(el) {
    var v = parseFloat(el.value);
    el.value = (isNaN(v)) ? '' : v.toFixed(2);
}