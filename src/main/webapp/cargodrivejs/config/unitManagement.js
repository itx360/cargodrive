/**
 * Created by shanakajay on 10/4/2016.
 */
$(function () {
    $(".select").selectpicker();
});

function create_unit() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/unitManagement/createUnit', 'create');
        if (responseObject.status == true) {
            transform_form('create','update',responseObject);
        }
    }
}

function update_unit() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/unitManagement/updateUnit', 'update');
        if (responseObject.status == true) {
            $('#update').BindJsonByForm(responseObject.object, 'update');
        }
    }
}

function delete_unit() {

}

function search_unit() {
    var unitName = $('input#search_unitName').val();
    var unitCode = $('input#search_unitCode').val();
    var usedFor = $('select#search_usedFor').val();
    var data = {'unitName': unitName,'unitCode':unitCode,'usedFor':usedFor};
    var responseObject = loadPageData('config/unitManagement/searchUnit', data, "POST");
    $("#loadUnitList").html(responseObject);
    $('.datatable').DataTable();
}

function update_unit_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/unitManagement/updateUnit', 'modal');
        $('#modal').BindJsonByForm(responseObject.object, 'modal');
    }
}

function load_unit_data_to_modal(unitSeq) {
    var unit = loadObjectData('config/unitManagement/findByUnitSeq/', unitSeq, 'GET');
    var formContent = $(".unitForm").html();
    display_update_modal(formContent, 'myModal', 'modal', unit);
}
