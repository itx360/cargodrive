/**
 * Created by shanakajay on 9/27/2016.
 */
$(function () {
    $('.select').selectpicker();
});

function create_charge() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/chargeManagement/createCharge', 'create');
        add_option_to_dropdown('update_chargeSeq', responseObject.object, 'chargeSeq', 'chargeName');
        $('#update').BindJson(responseObject.object, 'update');
        $('input:checkbox').removeAttr('checked');
        var chargeModes = responseObject.object['chargeModes'];
        for (var i = 0; i < chargeModes.length; i++) {
            if (chargeModes[i].status == '1') {
                document.getElementById("update_" + chargeModes[i].companyModuleSeq).checked = 'checked';
            }
        }
        $('#create').trigger("reset");
    }
}

function update_charge() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/chargeManagement/updateCharge', 'update');
    }
}

function search_charge() {
    var chargeName = $('input#search_chargeName').val();
    var description = $('input#search_description').val();
    var data = {'chargeName': chargeName, 'description': description};
    var responseObject = loadPageData('config/chargeManagement/searchCharge', data, "POST");
    $("#loadChargeList").html(responseObject);
    $('.datatable').DataTable();
}

function load_charge_by_charge_seq_to_update() {
    var chargeSeq = $("select#update_chargeSeq").val();
    if (chargeSeq != '-1') {
        var charge = loadObjectData('config/chargeManagement/findChargeByChargeSeq/', chargeSeq, 'GET');
        $('#update').BindJson(charge, 'update');
        $('input:checkbox').removeAttr('checked');
        var chargeModes = charge['chargeModes'];
        for (var i = 0; i < chargeModes.length; i++) {
            if (chargeModes[i].status == '1') {
                document.getElementById("update_" + chargeModes[i].companyModuleSeq).checked = 'checked';
            }
        }
    }
    else {
        $('#update').trigger("reset");
    }
}

function update_charge_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/chargeManagement/updateCharge', 'modal');
    }
}

function load_charge_data_to_modal(chargeSeq) {
    var charge = loadObjectData('config/chargeManagement/getChargeDetails/', chargeSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(charge, 'modal');
    $('input:checkbox').removeAttr('checked');
    var chargeModes = charge['chargeModes'];
    for (var i = 0; i < chargeModes.length; i++) {
        if (chargeModes[i].status == '1') {
            document.getElementById("modal_" + chargeModes[i].companyModuleSeq).checked = 'checked';
        }
    }
    $("#myModal").modal("show");
}





