/**
 * Created by Nuwanr on 9/28/2016.
 */

$(function () {
    $(".select").selectpicker();
});

function create_consortium() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/consortiumManagement/createConsortium', 'create', 'reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_consortiumSeq', responseObject.object, 'consortiumSeq', 'consortiumName');
            $('#update').BindJson(responseObject.object, 'update');
        }
    }
}

function update_consortium() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/consortiumManagement/updateConsortium', 'update');
    }
}

function search_consortium() {
    var consortiumName = $('input#search_consortiumName').val();
        var data = {
            'consortiumName': consortiumName
        };
        if (form_validate("search")) {
            var responseObject = loadPageData('config/consortiumManagement/searchConsortium', data, "POST");
        }
        $("#loadConsortiumSearchResult").html(responseObject); //???
        $('.datatable').DataTable();
    }


function update_consortium_modal_data() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/consortiumManagement/updateConsortium', 'modal');
    }
}

function load_consortium_by_consortium_seq_to_update() {
    var consortiumSeq = $("select#update_consortiumSeq").val();
    if (consortiumSeq != '-1') {
        var consortium = loadObjectData('config/consortiumManagement/getConsortiumDetails/', consortiumSeq, 'GET');
        $('#update').BindJson(consortium, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function load_consortium_data_to_modal(consortiumSeq) {
    var responseObject = loadObjectData('config/consortiumManagement/getConsortiumDetails/', consortiumSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(responseObject, 'modal');
    $("#myModal").modal("show");
}