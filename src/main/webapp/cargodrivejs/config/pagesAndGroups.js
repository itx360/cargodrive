
$(function () {
    $(".select").selectpicker();
});

function update_module() {
    if (form_validate("updateModule")) {
        var responseObject = saveFormData('config/pagesAndGroups/updateModule', 'updateModule','reset');
    }
}
function create_module() {
    if (form_validate("moduleAdd")) {
        var responseObject = saveFormData('config/pagesAndGroups/addModule', 'moduleAdd','reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('updateModule_moduleSeq', responseObject.object, 'moduleSeq', 'moduleName');
        }
    }
}
function create_submodule() {
    if (form_validate("subModuleAdd")) {
        var responseObject = saveFormData('config/pagesAndGroups/addSubModule', 'subModuleAdd','reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('updateSubModule_subModuleSeq', responseObject.object, 'subModuleSeq', 'subModuleName');
        }
    }
}
function update_sub_module() {
    if (form_validate("updateSubModule")) {
        var responseObject = saveFormData('config/pagesAndGroups/updateSubModule', 'updateSubModule','reset');

    }
}
function create_actiongroup() {
    if (form_validate("actionGroupAdd")) {
        var responseObject = saveFormData('config/pagesAndGroups/addActionGroup', 'actionGroupAdd','reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('updateActionGroup_actionGroupSeq', responseObject.object, 'actionGroupSeq', 'actionGroupName');
        }
    }
}
function update_action_group() {
    if (form_validate("updateActionGroup")) {
        var responseObject = saveFormData('config/pagesAndGroups/updateActionGroup', 'updateActionGroup','reset');

    }
}
function create_group() {
    if (form_validate("groupAdd")) {
        var responseObject = saveFormData('config/pagesAndGroups/addGroup', 'groupAdd','reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('updateGroup_groupSeq', responseObject.object, 'groupSeq', 'groupName');
        }
    }
}
function update_group() {
    if (form_validate("updateGroup")) {
        var responseObject = saveFormData('config/pagesAndGroups/updateGroup', 'updateGroup','reset');

    }
}
function create_document_Link() {
    if (form_validate("documentLinkAdd")) {
        var responseObject = saveFormData('config/pagesAndGroups/addDocumentLink', 'documentLinkAdd','reset');

    }
}
function create_custom_role() {
    if (form_validate("customRoleAdd")) {
        var responseObject = saveFormData('config/pagesAndGroups/addCustomRole', 'customRoleAdd','reset');

    }
}
function load_module_by_module_seq_to_update() {
    var moduleSeq = $("#updateModule_moduleSeq").val();
    if (moduleSeq != '-1') {
        var module = loadObjectData('config/pagesAndGroups/findModuleByModuleSeq/', moduleSeq, 'GET');
        $('#updateModule').BindJson(module, 'updateModule');
    } else {
        $('#updateModule').trigger("reset");
    }

}
function load_submodule_by_submodule_seq_to_update() {
    var subModuleSeq = $("#updateSubModule_subModuleSeq").val();
    if (subModuleSeq != '-1') {
        var submodule = loadObjectData('config/pagesAndGroups/findSubModuleBySubModuleSeq/', subModuleSeq, 'GET');
        $('#updateSubModule').BindJson(submodule, 'updateSubModule');
    } else {
        $('#updateSubModule').trigger("reset");
    }

}
function load_action_group_by_action_group_seq_to_update() {
    var actionGroupSeq = $("#updateActionGroup_actionGroupSeq").val();
    if (actionGroupSeq != '-1') {
        var actiongroup = loadObjectData('config/pagesAndGroups/findActionGroupByActionGroupSeq/', actionGroupSeq, 'GET');
        $('#updateActionGroup').BindJson(actiongroup, 'updateActionGroup');
    } else {
        $('#updateActionGroup').trigger("reset");
    }

}
function load_group_by_group_seq_to_update() {
    var groupSeq = $("#updateGroup_groupSeq").val();
    if (groupSeq != '-1') {
        var group = loadObjectData('config/pagesAndGroups/findGroupByGroupSeq/', groupSeq, 'GET');
        $('#updateGroup').BindJson(group, 'updateGroup');
    } else {
        $('#updateGroup').trigger("reset");
    }

}




