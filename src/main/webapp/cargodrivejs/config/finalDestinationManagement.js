$(function () {
    $(".select").selectpicker();
});

function create_final_destination() {
    if (form_validate("create")) {
        var destination = saveFormData('config/finalDestinationManagement/addDestination', 'create');
        if (destination.status == true) {
            transform_form('create','update',destination);
            $('#update').BindJsonByForm(destination.object, 'update');
            $('#update').BindJsonByForm(destination.object.addressBook, 'update');

        }

    }
}

function update_final_destination (){
    if (form_validate("update")) {
        var  destination = saveFormData('config/finalDestinationManagement/updateDestination', 'update');
        if ( destination.status == true) {
            $('#update').BindJsonByForm( destination.object, 'update');
            $('#update').BindJsonByForm(destination.object.addressBook, 'update');

        }
    }
}

function update_final_destination_modal() {
    if (form_validate("modal")) {
        var  destination = saveFormData('config/finalDestinationManagement/updateDestination', 'modal');
        $('#modal').BindJsonByForm( destination.object, 'modal');
        $('#modal').BindJsonByForm(destination.object.addressBook, 'update');
    }
}
function delete_final_destination() {

}

function load_final_destination_table_data() {
    var finalDestinationCode = $('input#search_finalDestinationCode').val();
    var countrySeq = $('select#search_countrySeq').val();
    var city = $('input#search_city').val();
    var state = $('input#search_state').val();
    var zip = $('input#search_zip').val();
    var data = {'finalDestinationCode': finalDestinationCode,'countrySeq': countrySeq,'city': city,'state': state,'zip': zip};
    var pageData = loadPageData('config/finalDestinationManagement/searchDestinationData', data, "POST");
    $("#loadDestinationListData").html(pageData);
    $('.datatable').DataTable();
}

function load_final_destination_data_to_modal(finalDestinationSeq) {
    var destination = loadObjectData('config/finalDestinationManagement/findByFinalDestinationSeq/', finalDestinationSeq, 'GET');
    var formContent = $(".finalDestinationForm").html();
    display_update_modal(formContent, 'myModal', 'modal', destination);
    $('#modal').BindJsonByForm(destination.addressBook, 'modal');
}
