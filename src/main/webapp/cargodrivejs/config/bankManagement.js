/**
 * Created by shanakajay on 9/26/2016.
 */
$(function () {
    $('.select').selectpicker();
});

function create_bank() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/bankManagement/createBank', 'create');
        add_option_to_dropdown('update_bankSeq',responseObject.object,'bankSeq','bankName');
        $('#update').BindJson(responseObject.object, 'update');
        $('#create').trigger("reset");
    }
}

function update_bank() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/bankManagement/updateBank', 'update');
    }
}

function search_bank() {
    var bankName = $('input#search_bankName').val();
    var bankCode = $('input#search_bankCode').val();
    var data = {'bankName': bankName,'bankCode':bankCode};
    var responseObject = loadPageData('config/bankManagement/searchBank', data, "POST");
    $("#loadBankList").html(responseObject);
    $('.datatable').DataTable();
}

function load_bank_by_bank_seq_to_update() {
    var bankSeq = $("select#update_bankSeq").val();
    if (bankSeq != '-1') {
        var bank = loadObjectData('config/bankManagement/findBankByBankSeq/', bankSeq, 'GET');
        $('#update').BindJson(bank, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function update_bank_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/bankManagement/updateBank', 'modal');
    }
}

function load_bank_data_to_modal(bankSeq) {
    var bank = loadObjectData('config/bankManagement/getBankDetails/', bankSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(bank, 'modal');
    $("#myModal").modal("show");
}

