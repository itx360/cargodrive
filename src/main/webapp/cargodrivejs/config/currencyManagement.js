/**
 * Created by Udaya-Ehl on 9/19/2016.
 */

$(function () {
    $(".select").selectpicker();
});

/**
 * function used to load currency data to modal
 * @param currencySeq
 */
function load_currency_data_to_modal(currencySeq) {
    var responseObject = loadObjectData('config/currencyManagement/getCurrencyDetails/', currencySeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(responseObject, 'modal');
    $("#myModal").modal("show");
}

/**
 * function used to create new currency
 */
function create_currency() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/currencyManagement/createCurrency', 'create', 'reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_currencySeq', responseObject.object, 'currencySeq', 'currencyCode');
            $('#update').BindJson(responseObject.object, 'update');
        }
    }
}

/**
 * function used to update currency
 */
function update_currency() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/currencyManagement/updateCurrency', 'update');
    }
}

/**
 * function used to update currency modal data
 */
function update_currency_modal_data() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/currencyManagement/updateCurrency', 'modal');
    }
}

/**
 * function used to search currency using currency name and currency display
 */
function search_currency() {
    var currencyName = $('input#search_currencyName').val();
    var currencyDisplay = $('input#search_currencyDisplay').val();
    var data = {
        'currencyName': currencyName,
        'currencyDisplay': currencyDisplay
    };
    if (form_validate("search")) {
        var responseObject = loadPageData('config/currencyManagement/searchCurrency', data, "POST");
    }
    $("#loadCurrencySearchResult").html(responseObject);
    $('.datatable').DataTable();
}

/**
 * function used to load currency data
 */
function load_currency_by_currency_seq_to_update() {
    var currencySeq = $("select#update_currencySeq").val();
    if (currencySeq != '-1') {
        var currency = loadObjectData('config/currencyManagement/getCurrencyDetails/', currencySeq, 'GET');
        $('#update').BindJson(currency, 'update');
    } else {
        $('#update').trigger("reset");
    }
}