/**
 * Created by Thilangaj on 9/20/2016.
 */
$(function () {
    $(".select").selectpicker();
});

function create_HsCode() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/hsCodeManagement/createHsCode', 'create', 'reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_hsCodeSeq', responseObject.object, 'hsCodeSeq', 'hsCodeName');
            $('#update').BindJson(responseObject.object, 'update');
        }
    }
}

function load_hsCode_by_hsCode_seq_to_update() {
    var hsCodeSeq = $("select#update_hsCodeSeq").val();
    if (hsCodeSeq != '-1') {
        var hsCode = loadObjectData('config/hsCodeManagement/findHsCodeByHsCodeSeq/', hsCodeSeq, 'GET');
        $('#update').BindJson(hsCode, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function update_HsCode() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/hsCodeManagement/updateHsCode', 'update');
        $('#update_hsCodeSeq option[value=' + responseObject.object.hsCodeSeq + ']').text(responseObject.object.hsCodeName);
        $('#update .select').selectpicker('refresh');
    }
}

function update_hsCode_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/hsCodeManagement/updateHsCode', 'modal');
        $('#modal_hsCodeSeq option[value=' + responseObject.object.hsCodeSeq + ']').text(responseObject.object.hsCodeName);
        $('#modal').BindJson(responseObject.object, 'modal');
        $('#modal .select').selectpicker('refresh');
    }
}

function load_hsCode_data_to_modal(hsCodeSeq) {
    var hsCode = loadObjectData('config/hsCodeManagement/findHsCodeByHsCodeSeq/', hsCodeSeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(hsCode, 'modal');
    $("#myModal").modal("show");
}

function search_hsCode() {
    var hsCodeName = $('input#search_hsCodeName').val();
    var hsCategorySeq = $('select#search_hsCategorySeq').val();
    var description = $('input#search_description').val();
    var data = {'hsCodeName': hsCodeName, 'hsCategorySeq': hsCategorySeq, 'description': description};
    var responseObject = loadPageData('config/hsCodeManagement/searchHsCode', data, "POST");
    $("#loadHSCodeData").html(responseObject);
    $('.datatable').DataTable();
}