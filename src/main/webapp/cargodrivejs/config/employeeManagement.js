$(function () {
    $(".select").selectpicker();
});

function create_employee() {
    if (form_validate("create")) {
        var employee = saveFormData('config/employeeManagement/addEmployee', 'create');
        if (employee.status == true) {
            transform_form('create','update',employee);
            $('#update').BindJsonByForm(employee.object, 'update');
            $('#update').BindJsonByForm(employee.object.addressBook, 'update');

        }

    }
}

function update_employee (){
    if (form_validate("update")) {
        var  employee = saveFormData('config/employeeManagement/updateEmployee', 'update');
        if ( employee.status == true) {
            $('#update').BindJsonByForm( employee.object, 'update');
            $('#update').BindJsonByForm(employee.object.addressBook, 'update');

        }
    }
}

function update_employee_modal() {
    if (form_validate("modal")) {
        var  employee = saveFormData('config/employeeManagement/updateEmployee', 'modal');
        $('#modal').BindJsonByForm(employee.object, 'modal');
        $('#modal').BindJsonByForm(employee.object.addressBook, 'update');
    }
}
function delete_employee() {

}

function load_employee_table_data() {
    var employeeName = $('input#search_employeeName').val();
    var status = $('select#search_status').val();
    var employeeDesignationSeq = $('select#search_employeeDesignationSeq').val();
    var data = {'employeeName': employeeName,'status': status,'employeeDesignationSeq': employeeDesignationSeq};
    var pageData = loadPageData('config/employeeManagement/searchEmployeeData', data, "POST");
    $("#loadEmployeeListData").html(pageData);
    $('.datatable').DataTable();
}

function load_employee_data_to_modal(employeeSeq) {
    var employee = loadObjectData('config/employeeManagement/findByEmployeeSeq/', employeeSeq, 'GET');
    var formContent = $(".employeeForm").html();
    display_update_modal(formContent, 'myModal', 'modal', employee);
    $('#modal').BindJsonByForm(employee.addressBook, 'modal');
}
