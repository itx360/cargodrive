/**
 * Created by Thilangaj on 9/26/2016.
 */
$(function () {
    $(".select").selectpicker();
    $('.text-uppercase').val (function () {
        return this.value.toUpperCase();
    });
});

function create_commodity() {
    if (form_validate("create")) {
        var responseObject = saveFormData('config/commodityManagement/createCommodity', 'create','reset');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_commoditySeq', responseObject.object, 'commoditySeq', 'description');
            $('#update').BindJson(responseObject.object, 'update');
        }
    }
}

function load_commodity_by_commodity_seq_to_update() {
    var commoditySeq = $("select#update_commoditySeq").val();
    if (commoditySeq != '-1') {
        var commodity = loadObjectData('config/commodityManagement/findCommodityByCommoditySeq/', commoditySeq, 'GET');
        $('#update').BindJson(commodity, 'update');
    } else {
        $('#update').trigger("reset");
    }
}

function update_commodity() {
    if (form_validate("update")) {
        var responseObject = saveFormData('config/commodityManagement/updateCommodity', 'update');
        $('#update_commoditySeq option[value=' + responseObject.object.commoditySeq + ']').text(responseObject.object.description);
        $('#update .select').selectpicker('refresh');
    }
}

function update_commodity_modal() {
    if (form_validate("modal")) {
        var responseObject = saveFormData('config/commodityManagement/updateCommodity', 'modal');
        $('#modal_commoditySeq option[value=' + responseObject.object.commoditySeq + ']').text(responseObject.object.description);
        $('#modal').BindJson(responseObject.object, 'modal');
        $('#modal .select').selectpicker('refresh');
    }
}

function load_commodity_data_to_modal(commoditySeq) {
    var commodity = loadObjectData('config/commodityManagement/findCommodityByCommoditySeq/', commoditySeq, 'GET');
    $('#modal').trigger("reset");
    $('#modal').BindJson(commodity, 'modal');
    $("#myModal").modal("show");
}

function search_commodity() {
    var description = $('input#search_description').val();
    var hsCodeSeq = $('select#search_hsCodeSeq').val();
    var commodityTypeSeq = $('select#search_commodityTypeSeq').val();
    var data = {'hsCodeSeq': hsCodeSeq, 'commodityTypeSeq': commodityTypeSeq, 'description': description};
    var responseObject = loadPageData('config/commodityManagement/searchCommodity', data, "POST");
    $("#loadCommodityData").html(responseObject);
    $('.datatable').DataTable();
}