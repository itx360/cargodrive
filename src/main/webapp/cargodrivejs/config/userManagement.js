$(function () {
    $("#create_userPhoto").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-danger",
        fileType: "any"
    });

    $("#update_userPhoto").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-danger",
        fileType: "any"
    });

    $("#create_dateOfBirth,#update_dateOfBirth").mask('9999-99-99');
    $("#create_dateOfBirth,#update_dateOfBirth").datepicker();
    $(".select").selectpicker();
});

function create_user() {
    if (form_validate("create")) {
        var responseObject = saveFormDataWithAttachments('config/userManagement/createUser', 'create');
        if (responseObject.status == true) {
            add_option_to_dropdown('update_userSeq', responseObject.object, 'userSeq', 'username');
            add_option_to_dropdown('assignModule_userSeq', responseObject.object, 'userSeq', 'username');
            add_option_to_dropdown('removeModule_userSeq', responseObject.object, 'userSeq', 'username');
        }
    }
}

function update_user() {
    if (form_validate("update")) {
        var responseObject = saveFormDataWithAttachments('config/userManagement/updateUser', 'update');
    }
}

function load_user_by_user_seq_to_update() {
    var userSeq = $("select#update_userSeq").val();
    if (userSeq != '-1') {
        var user = loadObjectData('config/userManagement/findUserByUserSeq/', userSeq, 'GET');
        $('form#update').BindJson(user, 'update');
    } else {
        $('form#update').trigger("reset");
    }
}

function load_module_list_by_company() {
    var companySeq = $('select#assignModule_companyProfileSeq').val();
    var moduleList = loadObjectData('config/userManagement/getModuleListByCompanySeq/', companySeq, "GET");
    populate_dropdown('assignModule_moduleSeq', moduleList, 'moduleSeq', 'moduleName');
}

function assign_modules_to_user() {
    if (form_validate('assignModule')) {
        saveFormData('config/userManagement/assignModuleToUser', 'assignModule')
    }
}

function load_assigned_module_list_by_company() {
    var userSeq = $('select#removeModule_userSeq').val();
    var companySeq = $('select#removeModule_companyProfileSeq').val();
    var params = userSeq + '/' + companySeq;
    if (userSeq != null && companySeq != null) {
        var moduleList = loadObjectData('config/userManagement/getModuleListByUserSeqAndCompanySeq/', params, "GET");
        populate_dropdown('removeModule_moduleSeq', moduleList, 'moduleSeq', 'moduleName');
    }
}

function remove_modules_from_user() {
    if (form_validate('removeModule')) {
        saveFormData('config/userManagement/removeModuleFromUser', 'removeModule')
    }
}

function load_assigned_company_list_to_user() {
    var userSeq = $('select#removeModuleUserSeq').val();
    var companyList = loadObjectData('config/userManagement/getCompanyListByUserSeq/', userSeq, "GET");
    populate_dropdown('removeModule_companyProfileSeq', companyList, 'companyProfileSeq', 'companyName');
    if (companyList.length > 0) {
        load_assigned_module_list_by_company();
    }
}