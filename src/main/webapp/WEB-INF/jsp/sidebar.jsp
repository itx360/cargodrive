<%@ include file="/WEB-INF/jsp/include.jsp" %>
<h5 class="text-center">${module.moduleName} - ${subModule.subModuleName}</h5>
<c:forEach items="${subModule.actionGroupList}" var="actionGroup">
    <c:if test="${(actionGroup.actionGroupName eq 'General') || (fn:length(actionGroup.documentLinkList) eq 1)}">
        <c:forEach items="${actionGroup.documentLinkList}" var="documentLink">
            <sec:authorize access="hasRole('ROLE_${module.moduleName}@${documentLink.linkName}_VIEW')">
                <li id="${module.urlPattern}-${documentLink.linkName}">
                    <a href="#" onclick="loadPageContent('${module.urlPattern}/${documentLink.linkName}','${documentLink.pageName}')"><span
                            class="fa fa-${documentLink.icon}"></span> <span
                            class="xn-text">${documentLink.pageName}</span></a>
                </li>
            </sec:authorize>
        </c:forEach>
    </c:if>
    <c:if test="${actionGroup.actionGroupName ne 'General' && (fn:length(actionGroup.documentLinkList) gt 1)}">
        <li class="xn-openable">
            <a href="#"><span class="fa fa-${actionGroup.icon}"></span> <span class="xn-text">${actionGroup.actionGroupName}</span></a>
            <ul>
                <c:forEach items="${actionGroup.documentLinkList}" var="documentLink">
                    <sec:authorize access="hasRole('ROLE_${module.moduleName}@${documentLink.linkName}_VIEW')">
                        <li id="${module.urlPattern}-${documentLink.linkName}">
                            <a href="#" onclick="loadPageContent('${module.urlPattern}/${documentLink.linkName}','${documentLink.pageName}')"><span
                                    class="fa fa-${documentLink.icon}"></span> <span
                                    class="xn-text">${documentLink.pageName}</span></a>
                        </li>
                    </sec:authorize>
                </c:forEach>
            </ul>
        </li>
    </c:if>
</c:forEach>
<script type="text/javascript" src="../../cargodrivejs/sidebar.js"></script>