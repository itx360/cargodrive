<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>Cargodrive</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" id="theme" href="../../theme/css/theme-white.css"/>
    <style type="text/css">
        #loading {
            display:none;
            position:fixed;
            left:50%;
            top:50%;
            width:100%;
            z-index:100001;
            height:100%;
        }
        #loadShadowing {
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #CCA;
            z-index:100000;
            opacity:0.5;
            filter: alpha(opacity=50);
        }
    </style>
</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container">

    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar">

        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="index.html">Cargodrive</a>
                <a href="#" class="x-navigation-control"></a>
                <input type="hidden" id="defaultCompanyProfileSeq" value="<c:out value="${companyProfileSeq}" />">
            </li>
            <li class="xn-profile">
                <a href="#" class="profile-mini">
                    <img src="../../theme/assets/images/users/avatar.jpg" alt="${user.firstName}&nbsp;${user.secondName}"/>
                </a>

                <div class="profile">
                    <div class="profile-image">
                        <img src="/getLogo/${user.uploadDocumentSeq}" alt="${user.firstName}&nbsp;${user.secondName}"/>
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name">${user.firstName}&nbsp;${user.secondName}</div>
                        <div class="profile-data-title">Web Developer/Designer</div>
                    </div>
                    <div class="profile-controls">
                        <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                        <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                    </div>
                </div>
            </li>
            <span id="sideBarData">
            <%@include file="sidebar.jsp" %>
            </span>

        </ul>

    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">

        <%@include file="header.jsp" %>

        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap" id="pageContent" >


        </div>
    </div>


    <!-- START DASHBOARD CHART -->
    <div id="dashboard-area-1"></div>

</div>
<!-- END PAGE CONTENT WRAPPER -->
<div>
    <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out?</p>

                <p>Press No if you want to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="/logout" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->
<!-- Preloader -->
<div id="loading" style="display:none">
    <p><img alt="loading" src="../../theme/img/loaders/default.gif"/> Please Wait</p>
</div>
<div id="loadShadowing"></div>
<!--/Preloader -->

<!-- START PRELOADS -->
<audio id="audio-alert" src="../../theme/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="../../theme/audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS -->

<!-- START SCRIPTS -->
<!-- START PLUGINS -->
<script type="text/javascript" src="../../theme/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/jquery/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/bootstrap/bootstrap.min.js"></script>
<!-- END PLUGINS -->

<!-- START THIS PAGE PLUGINS-->
<script type='text/javascript' src='../../theme/js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="../../theme/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/scrolltotop/scrolltopcontrol.js"></script>

<script type="text/javascript" src="../../theme/js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/morris/morris.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='../../theme/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='../../theme/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>
<script type='text/javascript' src='../../theme/js/plugins/bootstrap/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="../../theme/js/plugins/owl/owl.carousel.min.js"></script>

<script type="text/javascript" src="../../theme/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/bootstrap/bootstrap-select.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/fileinput/fileinput.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/filetree/jqueryFileTree.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/tableexport/tableExport.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/tableexport/jquery.base64.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/tableexport/html2canvas.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/tableexport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/tableexport/jspdf/libs/base64.js"></script>
<!-- END THIS PAGE PLUGINS-->

<!-- START TEMPLATE -->
<script type="text/javascript" src="../../theme/js/settings.js"></script>

<script type="text/javascript" src="../../theme/js/plugins.js"></script>
<script type="text/javascript" src="../../theme/js/actions.js"></script>
<script type="text/javascript" src="../../cargodrivejs/index.js"></script>
<script type="text/javascript" src="../../cargodrivejs/scripts/form.transaction.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/jquery-validation/validator.min.js"></script>
<script type="text/javascript" src="../../theme/js/plugins/maskedinput/jquery.maskedinput.min.js"></script>
<script type='text/javascript' src='../../theme/js/plugins/noty/jquery.noty.js'></script>
<!-- END TEMPLATE -->
<!-- END SCRIPTS -->
</body>
</html>
