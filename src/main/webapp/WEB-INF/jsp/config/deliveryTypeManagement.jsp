<%--
  Created by IntelliJ IDEA.
  User: shanakajay
  Date: 10/5/2016
  Time: 3:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/deliveryTypeManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Unt Management</h3>
</div>
<!-- END PAGE TITLE -->

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Delivery Type Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Delivery Type Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-8">
                        <form class="form-horizontal deliveryTypeForm" method="post" id="create">
                            <div class="form-group removeFromModal">
                                <div class="col-md-7">
                                    <h4 class="subTitle">Delivery Type Details</h4>
                                </div>
                                <div class="col-md-5 operations" style="display: none">
                                    <button type="button" class="btn btn-success" onclick="new_form('update','create')">
                                        New
                                    </button>
                                    <button type="button" class="btn btn-danger" onclick="delete_delivery_Type()">
                                        Delete
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Delivery Type Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="deliveryTypeCode"
                                           name="deliveryTypeCode" maxlength="20"
                                           required data-error="Delivery Type Code is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="description"
                                           name="description" maxlength="50"
                                           required data-error="Description is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Delivery Type</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="deliveryTypeMode" id="deliveryTypeMode"
                                            class="form-control select"
                                            data-live-search="true"
                                            data-validate="true" required
                                            data-error="Please Select Delivery Mode"
                                            placeholder="Delivery Type">
                                        <option value="None">Select</option>
                                        <c:forEach items="${deliveryTypeModeList}" var="deliveryTypeMode">
                                            <option value="${deliveryTypeMode.deliveryTypeMode}">${deliveryTypeMode.deliveryTypeMode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${createStatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group createOperation removeFromModal">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="create Delivery Type"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@deliveryTypeManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_delivery_type()">Create
                                    </button>
                                </div>
                            </div>
                            <div class="form-group updateOperation" style="display: none">
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created By</label>
                                    <label class="col-md-8 createdBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created Date</label>
                                    <label class="col-md-8 createdDate"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified By</label>
                                    <label class="col-md-8 lastModifiedBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified Date</label>
                                    <label class="col-md-8 lastModifiedDate"></label>
                                </div>
                                <div class="col-sm-offset-2 col-sm-7 removeFromModal">
                                    <button type="button" class="btn btn-primary pull-right" value="Update Delivery Type"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@deliveryTypeManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_delivery_type()">Update
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="deliveryTypeSeq" id="deliveryTypeSeq" value=""/>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Search Delivery Type</h4>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Delivery Type Code</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase" id="search_deliveryTypeCode"
                                               name="deliveryTypeCode"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Description</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase" id="search_description"
                                               name="description"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Delivery Type</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select name="deliveryTypeMode" id="search_deliveryTypeMode"
                                                class="form-control select"
                                                data-live-search="true"
                                                placeholder="Delivery Type">
                                            <option value="None">None</option>
                                            <c:forEach items="${deliveryTypeModeList}" var="deliveryTypeMode">
                                                <option value="${deliveryTypeMode.deliveryTypeMode}">${deliveryTypeMode.deliveryTypeMode}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right" value="search Delivery Type"
                                                onclick="search_delivery_type()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadDeliveryTypeData"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delivery Type Details</h4>
            </div>
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize access="!hasRole('ROLE_Config@deliveryTypeManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_delivery_type_modal()">Save changes
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


