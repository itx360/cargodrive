<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/commodityTypeManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Commodity Type Management</h3>
</div>
<!-- END PAGE TITLE -->

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Commodity Type Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Commodity Type Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Commodity Type</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Type Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_commodityTypeCode"
                                           name="commodityTypeCode" maxlength="50"
                                           required data-error="Commodity Type Code is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3" name="description"
                                              id="create_description" required maxlength="150"
                                              data-error="Description is Mandatory"></textarea>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Remarks</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3" name="remarks"
                                              maxlength="150"
                                              id="create_remarks" required data-error="Remark is Mandatory"></textarea>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Category</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="commodityCategorySeq" id="create_commodityCategorySeq"
                                            class="form-control select" data-validate="true" required
                                            data-error="Please Select Commodity Category"
                                            data-live-search="true" placeholder="Country">
                                        <option value="">Select</option>
                                        <c:forEach items="${commodityCategoryList}" var="commodityCategory">
                                            <option value="${commodityCategory.commodityCategorySeq}">${commodityCategory.commodityName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${createStatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="createCommodityType"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@commodityTypeManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_commodityType()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Commodity Type</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Commodity Type</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="commodityTypeSeq" id="update_commodityTypeSeq"
                                            class="form-control select"
                                            data-live-search="true" required
                                            data-error="Please Select Commodity Type"
                                            data-validate="true"
                                            placeholder="Commodity Type"
                                            onchange="load_commodityType_by_commodityType_seq_to_update()">
                                        <option value="">Select</option>
                                        <c:forEach items="${commodityTypeList}" var="commodityType">
                                            <option value="${commodityType.commodityTypeSeq}">${commodityType.commodityTypeCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Type Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="update_commodityTypeCode"
                                           name="commodityTypeCode" maxlength="50"
                                           required data-error="Commodity Type Code is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3" name="description"
                                              id="update_description" required maxlength="150"
                                              data-error="Description is Mandatory"></textarea>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Remarks</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3" name="remarks"
                                              id="update_remarks" required maxlength="150"></textarea>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Category</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="commodityCategorySeq" id="update_commodityCategorySeq"
                                            class="form-control select" data-validate="true" required
                                            data-error="Please Select Commodity Category"
                                            data-live-search="true" placeholder="Commodity">
                                        <option value="">Select</option>
                                        <c:forEach items="${commodityCategoryList}" var="commodityCategory">
                                            <option value="${commodityCategory.commodityCategorySeq}">${commodityCategory.commodityName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="updateCommodityType"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@commodityTypeManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_commodityType()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Commodity Type Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Commodity Type Code</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase"
                                               id="search_commodityTypeCode"
                                               name="commodityTypeCode"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Description</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase" id="search_description"
                                               name="description"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right"
                                                value="searchCommodityType"
                                                onclick="search_commodityType()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadCommodityTypeData"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <form class="form-horizontal" method="post" name="modal" id="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Commodity Type Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Commodity Type Code</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control text-uppercase" id="modal_commodityTypeCode"
                                   name="commodityTypeCode" maxlength="50"
                                   required data-error="Commodity Type Code is Mandatory"/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Description</label>
                        <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3" name="description"
                                              id="modal_description" required maxlength="150"
                                              data-error="Description is Mandatory"></textarea>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Remarks</label>
                        <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3" name="remarks"
                                              id="modal_remarks" required maxlength="150"></textarea>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Commodity Category</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="commodityCategorySeq" id="modal_commodityCategorySeq"
                                    class="form-control select" data-validate="true" required
                                    data-error="Please Select Commodity Category"
                                    data-live-search="true" placeholder="Country">
                                <option value="">Select</option>
                                <c:forEach items="${commodityCategoryList}" var="commodityCategory">
                                    <option value="${commodityCategory.commodityCategorySeq}">${commodityCategory.commodityName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified By</label>
                        <label class="col-md-8" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified Date</label>
                        <label class="col-md-8" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize access="!hasRole('ROLE_Config@commodityTypeManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_commodityType_modal()">Save
                        changes
                    </button>
                    <input type="hidden" name="commodityTypeSeq" id="modal_commodityTypeSeq" value=""/>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->

</div>

