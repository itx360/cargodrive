<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/customerGroupManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-road"></span>Customer Group Management</h3>
</div>
<!-- END PAGE TITLE -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Customer Group Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Customer Group Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Customer Group</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Customer Group Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="customerGroupCode" maxlength="30"
                                           id="create_customerGroupCode" data-error="Please Provide Customer Group Code"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Customer Group Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="customerGroupName" maxlength="30"
                                           id="create_customerGroupName"
                                           data-error="Please Provide Correct Customer Group Name"
                                           pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="addCustomerGroup"

                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@customerGroupManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_customer_group()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Customer Group</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Customer Group</label>
                                <div class="col-md-6 col-xs-12">
                                    <select data-error="Please Select Customer Group" data-validate="true" required
                                            aria-required="true"
                                            name="customerGroupSeq" id="update_customerGroupSeq"
                                            class="form-control select"
                                            data-live-search="true" placeholder="Branch"
                                            onchange="load_customer_group_customer_group_seq_to_update()">
                                        <option value="">Choose</option>
                                        <c:forEach items="${customerGroupList}" var="customerGroup">
                                            <option value="${customerGroup.customerGroupSeq}">${customerGroup.customerGroupName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Customer Group Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="customerGroupCode" maxlength="30"
                                           id="update_customerGroupCode" data-error="Please Provide Customer Group Code"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Customer Group Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="customerGroupName"
                                           pattern="^[a-zA-Z0-9\s]+$" maxlength="30"
                                           id="update_customerGroupName"
                                           data-error="Please Provide Correct Customer Group Name"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateCustomerGroup"

                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@customerGroupManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_customer_group()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane" id="tab-second">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" name="search" id="search">
                            <div class="form-group">
                                <h4 class="subTitle">Search Customer Group</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Customer Group Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="customerGroupCode"
                                           id="search_customerGroupCode"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Customer Group Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="customerGroupName"
                                           id="search_customerGroupName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="loadCustomerGroupData"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@customerGroupManagement_VIEW')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="load_customer_group_table_data()">Search
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="form-group" id="loadCustomerGroupListData">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Customer Group Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Customer Group Code</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_customerGroupCode" maxlength="30"
                                   name="customerGroupCode"
                                   data-error="Please Provide Customer Group Code"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Customer Group Name</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_customerGroupName" maxlength="30"
                                   name="customerGroupName" pattern="^[a-zA-Z0-9\s]+$"
                                   data-error="Please Provide Correct Customer Group Name"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-error="Please Select Status"
                                    data-validate="true" required aria-required="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified By</label>
                        <label class="col-md-8" id="modal_lastModifiedBy"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified Date</label>
                        <label class="col-md-8" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <%--<div id="result"></div>--%>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary pull-right"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@customerGroupManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_customer_group_popup()">Save changes
                    </button>
                    <input type="hidden" name="customerGroupSeq" id="modal_customerGroupSeq" value=""/>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<!-- /.modal -->
