<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/commodityManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Commodity Management</h3>
</div>
<!-- END PAGE TITLE -->

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Commodity Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Search Commodity</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Commodity</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Type</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="commodityTypeSeq" id="create_commodityTypeSeq"
                                            class="form-control select"
                                            data-validate="true" required
                                            data-error="Please Select Commodity Type"
                                            data-live-search="true"
                                            placeholder="commodityType">
                                        <option value="">Select</option>
                                        <c:forEach items="${commodityTypeList}" var="commodityType">
                                            <option value="${commodityType.commodityTypeSeq}">${commodityType.description}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">HS Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="hsCodeSeq" id="create_hsCodeSeq"
                                            class="form-control select"
                                            data-validate="true" required
                                            data-error="Please Select HS Code"
                                            data-live-search="true"
                                            placeholder="hsCode">
                                        <option value="">Select</option>
                                        <c:forEach items="${hsCodeList}" var="hsCode">
                                            <option value="${hsCode.hsCodeSeq}">${hsCode.hsCodeName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3" name="description"
                                              id="create_description" required maxlength="150"
                                              data-error="Please Provide Description"></textarea>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${createStatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="createCommodity"
                                            <sec:authorize access="!hasRole('ROLE_Config@commodityManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_commodity()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Commodity</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Commodity</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="commoditySeq" id="update_commoditySeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            placeholder="commodity"
                                            onchange="load_commodity_by_commodity_seq_to_update()">
                                        <option value="0">None</option>
                                        <c:forEach items="${commodityList}" var="commodity">
                                            <option value="${commodity.commoditySeq}">${commodity.description}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Type</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="commodityTypeSeq" id="update_commodityTypeSeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            data-validate="true" required
                                            data-error="Please Select Commodity Type"
                                            placeholder="commodityType">
                                        <option value="">Select</option>
                                        <c:forEach items="${commodityTypeList}" var="commodityType">
                                            <option value="${commodityType.commodityTypeSeq}">${commodityType.description}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">HS Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="hsCodeSeq" id="update_hsCodeSeq"
                                            class="form-control select"
                                            data-validate="true" required
                                            data-error="Please Select HS Code"
                                            data-live-search="true"
                                            placeholder="hsCode">
                                        <option value="">Select</option>
                                        <c:forEach items="${hsCodeList}" var="hsCode">
                                            <option value="${hsCode.hsCodeSeq}">${hsCode.hsCodeName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3" name="description"
                                              id="update_description" required maxlength="150"
                                              data-error="Please Provide Description"></textarea>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateCommodity"
                                            <sec:authorize access="!hasRole('ROLE_Config@commodityManagement_UPDATE')">
                                                disabled ="disabled"
                                            </sec:authorize>
                                            onclick="update_commodity()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Commodity Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Commodity Type</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select name="commodityTypeSeq" id="search_commodityTypeSeq"
                                                class="form-control select"
                                                data-live-search="true"
                                                placeholder="commodityType">
                                            <option value="-1">All</option>
                                            <c:forEach items="${commodityTypeList}" var="commodityType">
                                                <option value="${commodityType.commodityTypeSeq}">${commodityType.description}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">HS Code</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select name="hsCodeSeq" id="search_hsCodeSeq"
                                                class="form-control select"
                                                data-live-search="true"
                                                placeholder="hsCode">
                                            <option value="-1">All</option>
                                            <c:forEach items="${hsCodeList}" var="hsCode">
                                                <option value="${hsCode.hsCodeSeq}">${hsCode.hsCodeName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Commodity Description</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase" id="search_description"
                                               name="description"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right" value="searchCommodity"
                                                onclick="search_commodity()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadCommodityData"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <form class="form-horizontal" method="post" name="modal" id="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Commodity Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Commodity Type</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="commodityTypeSeq" id="modal_commodityTypeSeq"
                                    class="form-control select"
                                    data-live-search="true"
                                    data-validate="true" required
                                    data-error="Please Select Commodity Type"
                                    placeholder="commodityType">
                                <option value="">Select</option>
                                <c:forEach items="${commodityTypeList}" var="commodityType">
                                    <option value="${commodityType.commodityTypeSeq}">${commodityType.description}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">HS Code</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="hsCodeSeq" id="modal_hsCodeSeq"
                                    class="form-control select"
                                    data-validate="true" required
                                    data-error="Please Select HS Code"
                                    data-live-search="true"
                                    placeholder="hsCode">
                                <option value="">Select</option>
                                <c:forEach items="${hsCodeList}" var="hsCode">
                                    <option value="${hsCode.hsCodeSeq}">${hsCode.hsCodeName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Description</label>
                        <div class="col-md-6 col-xs-12">
                        <textarea class="form-control text-uppercase" rows="3" name="description" id="modal_description"
                                  maxlength="150"
                                  required data-error="Please Provide Description"></textarea>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified By</label>
                        <label class="col-md-8" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified Date</label>
                        <label class="col-md-8" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize access="!hasRole('ROLE_Config@commodityManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_commodity_modal()">Save changes
                    </button>
                    <input type="hidden" name="commoditySeq" id="modal_commoditySeq" value=""/>
                </div>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->


