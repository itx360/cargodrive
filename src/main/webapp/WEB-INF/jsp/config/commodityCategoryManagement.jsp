<%--
  Created by IntelliJ IDEA.
  User: Udaya-Ehl
  Date: 9/26/2016
  Time: 10:52 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/commodityCategoryManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Commodity Category Management</h3>
</div>
<!-- END PAGE TITLE -->

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Commodity Category Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Commodity Category Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Commodity Category</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_commodityCode" data-error="Please provide correct commodity code"
                                           name="commodityCode" pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_commodityName" data-error="Please provide correct commodity name"
                                           name="commodityName" pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${createStatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="createCommodityCategory"
                                            onclick="create_commodity_category()"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@commodityCategoryManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>>Create
                                    </button>
                                    <input type="hidden" name="commodityCategorySeq" id="create_commodityCategorySeq">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Commodity Category</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Commodity</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="commodityCategorySeq" id="update_commodityCategorySeq"
                                            class="form-control select" data-validate="true"
                                            data-error="Please Select Commodity Category"
                                            aria-required="true" required
                                            data-live-search="true" placeholder="commodity Code"
                                            onchange="load_commodity_category_by_commodity_category_seq_to_update()">
                                        <option value="-1">None</option>
                                        <c:forEach items="${commodityCategoryList}" var="commodityCategory">
                                            <option value="${commodityCategory.commodityCategorySeq}">${commodityCategory.commodityCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="update_commodityCode" data-error="Please provide correct commodity code"
                                           name="commodityCode" pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Commodity Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="update_commodityName" data-error="Please provide correct commodity name"
                                           name="commodityName" pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="updateCommodityCategory"
                                            onclick="update_commodity_category()"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@commodityCategoryManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>>Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Search Commodity Category</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Commodity Code</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_commodityCode"
                                               name="commodityCode"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Commodity Name</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_commodityName"
                                               name="commodityName"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right"
                                                value="searchCommodityCategory"
                                                onclick="search_Commodity_category()"
                                                <sec:authorize
                                                        access="!hasRole('ROLE_Config@commodityCategoryManagement_VIEW')">
                                                    disabled="disabled"
                                                </sec:authorize>>Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadCommodityCategorySearchResult"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Commodity Category Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Commodity Code</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control text-uppercase" id="modal_commodityCode" data-error="Please provide correct commodity code"
                                   name="commodityCode"  pattern="^[a-zA-Z0-9\s]+$" required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Commodity Name</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control text-uppercase" id="modal_commodityName" data-error="Please provide correct commodity name"
                                   name="commodityName" pattern="^[a-zA-Z0-9\s]+$" required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true"
                                    data-live-search="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified By</label>
                        <label class="col-md-8" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified Date</label>
                        <label class="col-md-8" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="update_commodity_category_modal_data()" class="btn btn-primary"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@commodityCategoryManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>>Update
                    </button>
                    <input type="hidden" name="commodityCategorySeq" id="modal_commodityCategorySeq" value=""/>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End modal -->





