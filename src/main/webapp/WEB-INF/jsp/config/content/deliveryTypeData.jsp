<%--
  Created by IntelliJ IDEA.
  User: shanakajay
  Date: 10/5/2016
  Time: 3:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!-- START DEFAULT DATATABLE -->
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Delivery Type Details</h3>
            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export
                    Data
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" onClick="$('#deliveryTypeList').tableExport({type:'csv',escape:'false'});"><img
                            src='../../../../theme/img/icons/csv.png' width="24"/> CSV</a></li>
                    <li><a href="#" onClick="$('#deliveryTypeList').tableExport({type:'txt',escape:'false'});"><img
                            src='../../../../theme/img/icons/txt.png' width="24"/> TXT</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick="$('#deliveryTypeList').tableExport({type:'excel',escape:'false'});"><img
                            src='../../../../theme/img/icons/xls.png' width="24"/> XLS</a></li>
                    <li><a href="#" onClick="$('#deliveryTypeList').tableExport({type:'doc',escape:'false'});"><img
                            src='../../../../theme/img/icons/word.png' width="24"/> Word</a></li>
                    <li><a href="#" onClick="$('#deliveryTypeList').tableExport({type:'pdf',escape:'false'});"><img
                            src='../../../../theme/img/icons/pdf.png' width="24"/> PDF</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <table class="table datatable" id="deliveryTypeList">
                <thead>
                <tr>
                    <th>Delivery Type Code</th>
                    <th>Description</th>
                    <th>Mode</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${deliveryTypeList}" var="deliveryType">
                    <tr onclick="load_delivery_type_data_to_modal(${deliveryType.deliveryTypeSeq})" class="clickable">
                        <td>${deliveryType.deliveryTypeCode}</td>
                        <td>${deliveryType.description}</td>
                        <td>${deliveryType.deliveryTypeMode}</td>
                        <td>${deliveryType.statusDescription}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

