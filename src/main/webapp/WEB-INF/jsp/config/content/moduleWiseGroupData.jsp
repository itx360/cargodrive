<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: shanakajay
  Date: 9/8/2016
  Time: 3:08 PM
  To change this template use File | Settings | File Templates.
--%>

<div class="panel-body">
        <c:forEach items="${moduleWiseGroupList}" var="group">
            <div class="col-md-5">
                <label class="check"><input type="checkbox" id="${group.groupSeq}"
                                            name="groupSeq[]"
                                            value="${group.groupSeq}" class="icheckbox"/>
                        ${group.groupName}</label>
            </div>
        </c:forEach>
</div>
