<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!-- START DEFAULT DATATABLE -->
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Commodity Type Details</h3>
            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export
                    Data
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" onClick="$('#commodityTypeList').tableExport({type:'csv',escape:'false'});"><img
                            src='../../../../theme/img/icons/csv.png' width="24"/> CSV</a></li>
                    <li><a href="#" onClick="$('#commodityTypeList').tableExport({type:'txt',escape:'false'});"><img
                            src='../../../../theme/img/icons/txt.png' width="24"/> TXT</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick="$('#commodityTypeList').tableExport({type:'excel',escape:'false'});"><img
                            src='../../../../theme/img/icons/xls.png' width="24"/> XLS</a></li>
                    <li><a href="#" onClick="$('#commodityTypeList').tableExport({type:'doc',escape:'false'});"><img
                            src='../../../../theme/img/icons/word.png' width="24"/> Word</a></li>
                    <li><a href="#" onClick="$('#commodityTypeList').tableExport({type:'pdf',escape:'false'});"><img
                            src='../../../../theme/img/icons/pdf.png' width="24"/> PDF</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <table class="table datatable" id="commodityTypeList">
                <thead>
                <tr>
                    <th>Commodity Type Code</th>
                    <th>Commodity Category</th>
                    <th>Description</th>
                    <th>Remarks</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${commodityTypeList}" var="commodityType">
                    <tr onclick="load_commodityType_data_to_modal(${commodityType.commodityTypeSeq})" class="clickable">
                        <td>${commodityType.commodityTypeCode}</td>
                        <td>${commodityType.commodityCategory.commodityName}</td>
                        <td>${commodityType.description}</td>
                        <td>${commodityType.remarks}</td>
                        <td>${commodityType.statusDescription}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
