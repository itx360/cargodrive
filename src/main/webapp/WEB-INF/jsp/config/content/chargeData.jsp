<%--
  Created by IntelliJ IDEA.
  User: shanakajay
  Date: 9/27/2016
  Time: 3:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Charge List</h3>
        </div>
        <div class="panel-body">
            <table class="table datatable">
                <thead>
                <tr>
                    <th>Charge Seq</th>
                    <th>Charge Name</th>
                    <th>description</th>
                    <th>Charge Modes</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${chargeList}" var="charge">
                    <tr onclick="load_charge_data_to_modal(${charge.chargeSeq})" class="clickable">
                        <td>${charge.chargeSeq}</td>
                        <td>${charge.chargeName}</td>
                        <td>${charge.description}</td>
                        <td>
                            <c:forEach items="${charge.chargeModes}" var="chargeMode">
                                <c:if test="${chargeMode.status == 1}">
                                    ${chargeMode.module.moduleName},
                                </c:if>
                            </c:forEach>
                        </td>
                        <td>${charge.statusDescription}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
