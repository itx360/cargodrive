<%--
  Created by IntelliJ IDEA.
  User: shanakajay
  Date: 9/20/2016
  Time: 11:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Country List</h3>
        </div>
        <div class="panel-body">
            <table class="table datatable">
                <thead>
                <tr>
                    <th>Country Seq</th>
                    <th>Country Code</th>
                    <th>Name</th>
                    <th>Region</th>
                    <th>Currency</th>
                    <th>IATA Code</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${countryList}" var="country">
                    <tr onclick="load_country_data_to_modal(${country.countrySeq})" class="clickable">
                        <td>${country.countrySeq}</td>
                        <td>${country.countryCode}</td>
                        <td>${country.countryName}</td>
                        <td>${country.region.regionName}</td>
                        <td>${country.currency.currencyCode}</td>
                        <td>${country.iataCode}</td>
                        <td>${country.statusDescription}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

