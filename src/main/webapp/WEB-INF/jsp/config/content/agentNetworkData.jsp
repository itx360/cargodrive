<%@ include file="/WEB-INF/jsp/include.jsp" %>
<div class="col-md-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export
                    Data
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#"
                           onClick="$('#agentNetworkDataSearch').tableExport({type:'csv',escape:'false'});"><img
                            src='../../../../theme/img/icons/csv.png' width="24"/> CSV</a></li>
                    <li><a href="#"
                           onClick="$('#agentNetworkDataSearch').tableExport({type:'txt',escape:'false'});"><img
                            src='../../../../theme/img/icons/txt.png' width="24"/> TXT</a></li>
                    <li class="divider"></li>
                    <li><a href="#"
                           onClick="$('#agentNetworkDataSearch').tableExport({type:'excel',escape:'false'});"><img
                            src='../../../../theme/img/icons/xls.png' width="24"/> XLS</a></li>
                    <li><a href="#"
                           onClick="$('#agentNetworkDataSearch').tableExport({type:'doc',escape:'false'});"><img
                            src='../../../../theme/img/icons/word.png' width="24"/> Word</a></li>
                    <li><a href="#"
                           onClick="$('#agentNetworkDataSearch').tableExport({type:'pdf',escape:'false'});"><img
                            src='../../../../theme/img/icons/pdf.png' width="24"/> PDF</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <table id="agentNetworkDataSearch" class="table datatable">
                <thead>
                <tr>
                    <th>Agent Network Code</th>
                    <th>Agent Network Name</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${agentNetworkListDB}" var="agentNetwork">
                    <tr onclick="load_agent_network_data_to_modal(${agentNetwork.agentNetworkSeq})" class="clickable">
                        <td>${agentNetwork.agentNetworkCode}</td>
                        <td>${agentNetwork.agentNetworkName}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

