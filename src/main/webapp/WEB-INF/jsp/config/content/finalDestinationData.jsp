<%@ include file="/WEB-INF/jsp/include.jsp" %>
<div class="col-md-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export
                    Data
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" onClick="$('#destinationDataSearch').tableExport({type:'csv',escape:'false'});"><img
                            src='../../../../theme/img/icons/csv.png' width="24"/> CSV</a></li>
                    <li><a href="#" onClick="$('#destinationDataSearch').tableExport({type:'txt',escape:'false'});"><img
                            src='../../../../theme/img/icons/txt.png' width="24"/> TXT</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick="$('#destinationDataSearch').tableExport({type:'excel',escape:'false'});"><img
                            src='../../../../theme/img/icons/xls.png' width="24"/> XLS</a></li>
                    <li><a href="#" onClick="$('#destinationDataSearch').tableExport({type:'doc',escape:'false'});"><img
                            src='../../../../theme/img/icons/word.png' width="24"/> Word</a></li>
                    <li><a href="#" onClick="$('#destinationDataSearch').tableExport({type:'pdf',escape:'false'});"><img
                            src='../../../../theme/img/icons/pdf.png' width="24"/> PDF</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <table id="destinationDataSearch" class="table datatable">
                <thead>
                <tr>
                    <th>Destination Code</th>
                    <th>Country</th>
                    <th>Zip</th>
                    <th>State</th>
                    <th>City</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${destinationListDB}" var="finalDestination">
                    <tr onclick="load_final_destination_data_to_modal(${finalDestination.finalDestinationSeq})" class="clickable">
                        <td>${finalDestination.finalDestinationCode}</td>
                        <td>${finalDestination.country.countryName}</td>
                        <td>${finalDestination.addressBook.zip}</td>
                        <td>${finalDestination.addressBook.state}</td>
                        <td>${finalDestination.addressBook.city}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

