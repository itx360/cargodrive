<%--
  Created by IntelliJ IDEA.
  User: Nuwanr
  Date: 9/28/2016
  Time: 12:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/consortiumManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Consortium Creation and Search</h3>
</div>
<!-- END PAGE TITLE -->
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Consortium Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Consortium Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Consortium</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Consortium Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_consortiumName"
                                           name="consortiumName"  data-error="Please Provide correct Consortium Name" pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${createStatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="createConsortium"
                                            onclick="create_consortium()">Create
                                    </button>
                                    <input type="hidden" name="consortiumSeq" id="create_consortiumSeq">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Consortium</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Consortium</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="consortiumSeq" id="update_consortiumSeq"
                                            class="form-control select" data-validate="true"
                                            data-live-search="true" placeholder="consortium Name"
                                            onchange="load_consortium_by_consortium_seq_to_update()">
                                        <option value="-1">None</option>
                                       <c:forEach items="${consortiumList}" var="consortium">
                                            <option value="${consortium.consortiumSeq}">${consortium.consortiumName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Consortium Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_consortiumName"
                                           name="consortiumName"  data-error="Please Provide correct Consortium Name" pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status" class="form-control select"
                                            data-live-search="true"
                                            placeholder="Status" required>
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateConsortium"
                                            onclick="update_consortium()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle"> Consortium Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Consortium Name</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_consortiumName"
                                               name="consortiumName">
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right"
                                                value="searchConsortium"
                                                onclick="search_consortium()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadConsortiumSearchResult"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Start modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Consortium Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Consortium Name</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_consortiumName"
                                   name="consortiumName"
                                   data-error="Please Provide correct Consortium Name" pattern="^[a-zA-Z0-9\s]+$"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified By</label>
                        <label class="col-md-8" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified Date</label>
                        <label class="col-md-8" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="update_consortium_modal_data()" class="btn btn-primary">Update</button>
                    <input type="hidden" name="consortiumSeq" id="modal_consortiumSeq" value=""/>
                </div>
            </form>
        </div>
    </div>
</div>

