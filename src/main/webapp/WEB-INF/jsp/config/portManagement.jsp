<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/portManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Port Management</h3>
</div>
<!-- END PAGE TITLE -->

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Create/Manage Port</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Port Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Port</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Port Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_portCode" name="portCode"
                                           maxlength="3" required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Port Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_portName" name="portName"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" id="create_countrySeq" name="countrySeq"
                                            data-live-search="true" onchange="load_locations_by_country('create')">
                                        <option value=0>None</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}
                                                - ${country.countryCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Location</label>
                                <div class="col-md-6">
                                    <select class="form-control select" id="create_locationSeq" name="locationSeq">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Mode</label>
                                <div class="col-md-6">
                                    <select class="form-control select" id="create_transportMode" name="transportMode"
                                            onchange="">
                                        <option>None</option>
                                        <c:forEach items="${modes}" var="mode">
                                            <option value="${mode.modeCode}">${mode.mode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Trade Lane</label>
                                <div class="col-md-6">
                                    <select class="form-control select disabled" id="create_tradeLaneSeq"
                                            name="tradeLaneSeq">
                                        <option value="-1">None</option>
                                        <c:forEach items="${tradeLaneList}" var="tradeLane">
                                            <option value="${tradeLane.tradeLaneSeq}">${tradeLane.tradeLaneName}
                                                - ${tradeLane.tradeLaneCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="createPort"
                                            onclick="create_port()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Port</h4>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Port</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" id="update_port" name="port"
                                            data-live-search="true" onchange="load_port_to_update()">
                                        <option value="">None</option>
                                        <c:forEach items="${portList}" var="port">
                                            <option value="${port.portSeq}">${port.portName} - ${port.portCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Port Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_portCode" name="portCode"
                                           maxlength="3" required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Port Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_portName" name="portName"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" id="update_countrySeq" name="countrySeq"
                                            data-live-search="true" onchange="load_locations_by_country('update')">
                                        <option value=0>None</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Location</label>
                                <div class="col-md-6">
                                    <select class="form-control select" id="update_locationSeq"
                                            name="locationSeq"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Mode</label>
                                <div class="col-md-6">
                                    <select class="form-control select" id="update_transportMode" name="transportMode"
                                            onchange="">
                                        <option>None</option>
                                        <c:forEach items="${modes}" var="mode">
                                            <option value="${mode.modeCode}">${mode.mode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Trade Lane</label>
                                <div class="col-md-6">
                                    <select class="form-control select disabled" id="update_tradeLaneSeq"
                                            name="tradeLaneSeq">
                                        <c:forEach items="${tradeLaneList}" var="tradeLane">
                                            <option value="${tradeLane.tradeLaneSeq}">${tradeLane.tradeLaneName}
                                                - ${tradeLane.tradeLaneCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updatePort"
                                            onclick="update_port('update', 'update')">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="searchPorts" id="searchPorts">
                                <div class="form-group">
                                    <h4 class="subTitle">Port Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Mode</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select class="form-control select" id="searchPorts_transportMode"
                                                name="transportMode">
                                            <option value="-1">All</option>
                                            <c:forEach items="${modes}" var="mode">
                                                <option value="${mode.modeCode}">${mode.mode}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Port Name</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="searchPorts_portName"
                                               name="portName"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Location</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select class="form-control select" id="searchPorts_locationSeq"
                                                name="locationSeq"
                                                data-live-search="true">
                                            <option value="-1">None</option>
                                            <c:forEach items="${locationList}" var="location">
                                                <option value="${location.locationSeq}">${location.locationName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right" value="searchPorts"
                                                onclick="search_port()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadPortSearchResult"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--Modal Start--%>
<div class="modal fade" id="portDetailsModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" id="modal_portDetails" name="portDetails">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Port Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Port Name</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="hidden" id="portSeq">
                            <input class="form-control" type="text" id="modal_portName" name="portName" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Port Code</label>
                        <div class="col-md-6 col-xs-12">
                            <input class="form-control" type="text" id="modal_portCode" name="portCode" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Country</label>
                        <div class="col-md-6 col-xs-12">
                            <select class="form-control select" id="modal_countrySeq" name="countrySeq"
                                    data-live-search="true" onchange="load_locations_by_country('modal')" required>
                                <c:forEach items="${countryList}" var="country">
                                    <option value="${country.countrySeq}">${country.countryName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Location</label>
                        <div class="col-md-6 col-xs-12">
                            <select class="form-control select" id="modal_locationSeq" name="locationSeq"
                                    data-live-search="true" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12s">Trade Lane</label>
                        <div class="col-md-3 col-xs-12">
                            <select class="form-control select" id="modal_tradeLane" name="tradeLane" required
                                    data-live-search="true">
                                <c:forEach items="${tradeLaneList}" var="tradeLane">
                                    <option value="${tradeLane.tradeLaneSeq}">${tradeLane.tradeLaneName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Mode</label>
                        <div class="col-md-6 col-xs-12">
                            <select class="form-control select" id="modal_transportMode" name="transportMode">
                                <c:forEach items="${modes}" var="mode">
                                    <option value="${mode.modeCode}">${mode.mode}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select class="form-control select" id="modal_status" name="status">
                                <c:forEach items="${status}" var="st">
                                    <option value="${st.statusSeq}">${st.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Created By</label>
                        <label class="col-md-6 col-xs-12" id="modal_createdBy"> - </label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Created Date</label>
                        <label class="col-md-6 col-xs-12" id="modal_createdDate"> - </label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Last Modified By</label>
                        <label class="col-md-6 col-xs-12" id="modal_modifiedBy"> - </label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12">Last Modified Date</label>
                        <label class="col-md-6 col-xs-12" id="modal_modifiedDate"> - </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" value="updatePort" name="saveChanges"
                            onclick="update_port('modal_portDetails', 'modal_portDetails')">Save Changes
                    </button>
                    <%--There is a bug in the form transactions, method SaveFormData specifies using form name, but it
                        looks up for formdata using form id. create and update works by coincidence--%>
                </div>
            </form>
        </div>
    </div>
</div>

