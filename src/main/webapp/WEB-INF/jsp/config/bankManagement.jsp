<%--
  Created by IntelliJ IDEA.
  User: shanakajay
  Date: 9/26/2016
  Time: 10:53 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/bankManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Bank Creation, Modification and Search</h3>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Bank Creation and
                    Modification</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Search Result</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Bank</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Bank Code</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="10" class="form-control" id="create_bankCode" name="bankCode"
                                           data-error="Please Provide Correct Bank Code"
                                           pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Bank Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="100" class="form-control" id="create_bankName" name="bankName"
                                           data-error="Please Provide Correct Bank Name"
                                           pattern="^[a-zA-Z\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">SLIPS Code</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_slipsCode" name="slipsCode"
                                           data-error="Please Provide Correct Slips Code"
                                           pattern="^([0-9]*\s+)*[0-9]*$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status" class="form-control select"
                                            data-live-search="true"
                                            data-error="Please Select Status" data-validate="true" required
                                            aria-required="true"
                                            placeholder="Status">
                                        <c:forEach items="${createStatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="createBank"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@bankManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_bank()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Bank</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Bank Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="bankSeq" id="update_bankSeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            placeholder="Select Bank Name"
                                            data-error="Please Select Status" data-validate="true" required
                                            aria-required="true"
                                            onchange="load_bank_by_bank_seq_to_update()">
                                        <option value="">None</option>
                                        <c:forEach items="${bankList}" var="bank">
                                            <option value="${bank.bankSeq}">${bank.bankName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Bank Code</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="10" class="form-control" id="update_bankCode" name="bankCode"
                                           data-error="Please Provide Correct Bank Code"
                                           pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Bank Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="100" class="form-control" id="update_bankName" name="bankName"
                                           data-error="Please Provide Correct Bank Name"
                                           pattern="^[a-zA-Z\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">SLIPS Code</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_slipsCode" name="slipsCode"
                                           data-error="Please Provide Correct Slips Code"
                                           pattern="^([0-9]*\s+)*[0-9]*$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status" class="form-control select"
                                            data-live-search="true"
                                            data-error="Please Select Status" data-validate="true" required
                                            aria-required="true"
                                            placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateBank"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@bankManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_bank()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Bank Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Bank Name</label>

                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_bankName"
                                               name="bankName" required/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Bank Code</label>

                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_bankCode"
                                               name="bankCode"
                                               data-error="Please Provide Correct Bank Code"
                                               pattern="^[a-zA-Z0-9\s]+$"
                                               required/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right" value="searchBank"
                                                <sec:authorize
                                                        access="!hasRole('ROLE_Config@bankManagement_VIEW')">
                                                    disabled="disabled"
                                                </sec:authorize>
                                                onclick="search_bank()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadBankList">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <form class="form-horizontal" method="post" name="modal" id="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Bank Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Bank Code</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" maxlength="10" class="form-control" id="modal_bankCode" name="bankCode"
                                   data-error="Please Provide Correct Bank Code"
                                   pattern="^[a-zA-Z0-9\s]+$"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Bank Name</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" maxlength="100" class="form-control" id="modal_bankName" name="bankName"
                                   data-error="Please Provide Correct Bank Name"
                                   pattern="^[a-zA-Z\s]+$"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">SLIPS Code</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_slipsCode" name="slipsCode"
                                   data-error="Please Provide Correct Slips Code"
                                   pattern="^([0-9]*\s+)*[0-9]*$"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>

                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true" required
                                    aria-required="true"
                                    data-live-search="true"
                                    data-error="Please Select Status"
                                    placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_createdDate">Created
                            By</label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified By</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified Date</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@bankManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_bank_modal()">Save changes
                    </button>
                    <input type="hidden" name="bankSeq" id="modal_bankSeq" value=""/>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->

</div>

