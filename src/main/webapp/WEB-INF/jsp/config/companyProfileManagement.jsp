<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="../../../theme/js/plugins/fileinput/fileinput.min.js"></script>
<script type="text/javascript" src="../../../theme/js/plugins/filetree/jqueryFileTree.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/companyProfileManagement.js"></script>

<div class="page-title">
    <h2><span class="fa fa-arrow-circle-o-left"></span>Company Profile Configuration</h2>
</div>
<!-- END PAGE TITLE -->

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist" data-toggle="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Company Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Manage Company Modules</a></li>
                <li><a href="#tab-third" role="tab" data-toggle="tab">Departments</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first" data-toggle="tab">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="createCompanyProfile" id="create"
                              enctype="multipart/form-data" data-toggle="form">
                            <div class="form-group">
                                <h4>Create Company</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Company Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_companyName" name="companyName"
                                           required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Short Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_shortCode" name="shortCode" required
                                    maxlength="3" minlength="3"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_description" name="description" maxlength="200">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Email</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="email" class="form-control" id="create_email" name="email" required
                                           data-error="Invalid email address"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Contact No</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" id="create_telephoneNumber" name="telephoneNumber"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}" data-error="Invalid telephone number format" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-md-3 col-xs-12 control-label">Company Logo</label>
                                    <input type="file"  id="create_companyLogo" name="companyLogo"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-md-3 col-xs-12 control-label">Enabled</label>
                                    <input type='checkbox' value=1 name="status" id="create_status" checked>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-primary pull-right" value="create"
                                            onclick="create_company()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="updateCompanyProfile" id="update"
                              enctype="multipart/form-data" data-toggle="form">
                            <div class="form-group">
                                <h4>Update Company</h4>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Company</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="companyProfileSeq" id="updateCompanyProfileFromCompanySeq" class="form-control select"
                                            data-live-search="true"
                                            onchange="load_company_profiles_by_company_seq_to_update()">
                                        <option value="-1">None</option>
                                        <c:forEach items="${companyProfiles}" var="companyProfile">
                                            <option value="${companyProfile.companyProfileSeq}">${companyProfile.companyName} - ${companyProfile.shortCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Company Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_companyName" name="companyName"
                                           required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_description" name="description" maxlength="200" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Email</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="email" class="form-control" id="update_email" name="email" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Contact No</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" id="update_telephoneNumber" name="telephoneNumber" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-md-3 col-xs-12 control-label">Company Logo</label>
                                    <input type="file" id="update_companyLogo" name="companyLogo" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-md-3 col-xs-12 control-label">Enabled</label>
                                    <input type='checkbox' value=1 name="status" id="update_status" >
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-primary pull-right" value="update" onclick="update_company()">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="assignModulesToCompanyProfile"
                              id="assignModulesToCompanyProfile"
                              enctype="multipart/form-data">
                            <div class="form-group">
                                <h4>Assign Modules</h4>
                            </div>
                            <div class="form-group">
                                <div>
                                    <label class="control-label col-md-3 col-xs-12">Company</label>
                                    <div class="col-md-9 col-xs-12">
                                        <select name="companyToAssign" id="companyToAssign" class="form-control select"
                                                data-live-search="true">
                                            <%--onchange="load_modules_for_company()">--%>
                                            <option value="0">None</option>
                                            <c:forEach items="${companyProfiles}" var="companyProfile">
                                                <option value="${companyProfile.companyProfileSeq}">${companyProfile.companyName}
                                                    - ${companyProfile.shortCode}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <label class="control-label col-md-3">Module</label>
                                    <div class="col-md-9">
                                        <select class="col-md-6 form-control select" name="companyModules"
                                                id="companyModules"
                                                data-live-search="true" multiple>
                                            <option value="0">None</option>
                                            <c:forEach items="${modules}" var="module">
                                                <option value="${module.moduleSeq}">${module.moduleName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary pull-right" value="assignModules"
                                            onclick="assign_modules_to_company()">Assign
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="removeModulesFromCompany"
                              id="removeModulesFromCompany"
                              enctype="multipart/form-data">
                            <div class="form-group">
                                <h4>Remove Modules</h4>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Company</label>
                                <div class="col-md-9 col-xs-12">
                                    <select name="companyToRemove" id="companyToRemove" class="form-control select"
                                            data-live-search="true" onchange="load_assigned_module_list_by_company()">
                                        <option value="0">None</option>
                                        <c:forEach items="${companyProfiles}" var="companyProfile">
                                            <option value="${companyProfile.companyProfileSeq}">${companyProfile.companyName}
                                                - ${companyProfile.shortCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <label class="control-label col-md-3">Module</label>
                                    <div class="col-md-9">
                                        <select class="col-md-6 form-control select" name="companyModulesToRemove"
                                                id="companyModulesToRemove"
                                                data-live-search="true" multiple>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger pull-right" value="assignModules"
                                            onclick="remove_modules_from_company()">Remove
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



