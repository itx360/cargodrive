<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/finalDestinationManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-road"></span>Final Destination</h3>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Final Destination Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Final Destination Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-8">
                        <form class="form-horizontal finalDestinationForm" method="post" name="create" id="create">
                            <div class="form-group removeFromModal">
                                <div class="col-md-7">
                                    <h4 class="subTitle">Final Destination Details</h4>
                                </div>
                                <div class="col-md-5 operations" style="display: none">
                                    <button type="button" class="btn btn-success" onclick="new_form('update','create')">
                                        New
                                    </button>
                                    <button type="button" class="btn btn-danger" onclick="delete_final_destination()">
                                        Delete
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Destination Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="finalDestinationCode"
                                           id="create_finalDestinationCode"
                                           data-error="Please Provide Destination Code"
                                           required/>
                                    <span class="help-block">Required, Destination Code</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">City</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="city"
                                           id="create_city"
                                           data-error="Please Provide City"
                                           required/>
                                    <span class="help-block">Required, City</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">State</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="state"
                                           id="create_state"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Zip</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="zip"
                                           id="create_zip"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="countrySeq" id="create_countrySeq" data-error="Please Select Country"
                                            data-validate="true" required aria-required="true" placeholder="Country">
                                        <option value="">Choose</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="help-block">Required</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group createOperation removeFromModal">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="addDestination"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@finalDestinationManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_final_destination()">Create
                                    </button>
                                </div>
                            </div>

                            <div class="form-group updateOperation" style="display: none">
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created By</label>
                                    <label class="col-md-8 createdBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created Date</label>
                                    <label class="col-md-8 createdDate"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified By</label>
                                    <label class="col-md-8 lastModifiedBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified Date</label>
                                    <label class="col-md-8 lastModifiedDate"></label>
                                </div>
                                <div class="col-sm-offset-2 col-sm-7 removeFromModal">
                                    <button type="button" class="btn btn-primary pull-right" value="Update Final Destination"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@finalDestinationManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_final_destination()">Update
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="finalDestinationSeq" id="finalDestinationSeq" value=""/>
                        </form>
                    </div>
                </div>

                <div class="tab-pane" id="tab-second">
                    <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" name="search" id="search">
                            <div class="form-group">
                                <h4 class="subTitle">Search Final Destination</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Destination Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="finalDestinationCode"
                                           id="search_finalDestinationCode"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="countrySeq" id="search_countrySeq"
                                            placeholder="Country">
                                        <option value="-1">All</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">City</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="city"
                                           id="search_city"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">State</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="state"
                                           id="search_state"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Zip</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="zip"
                                           id="search_zip"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="loadDestinationData"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@finalDestinationManagement_VIEW')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="load_final_destination_table_data()">Search
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="form-group" id="loadDestinationListData"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Final Destination Details</h4>
            </div>
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@finalDestinationManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_final_destination_modal()">Save changes
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
