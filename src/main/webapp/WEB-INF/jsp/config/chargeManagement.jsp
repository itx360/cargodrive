<%--
  Created by IntelliJ IDEA.
  User: shanakajay
  Date: 9/27/2016
  Time: 1:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/chargeManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Charge Creation, Modification and Search</h3>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Charge Creation and
                    Modification</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Search Result</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Charge</h4>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Charge Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_chargeName" name="chargeName"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_description" name="description"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Charge Modes</label>
                                <div class="col-md-6 col-xs-12">
                                    <c:forEach items="${modeList}" var="mode">
                                        <label class="check"><input type="checkbox" id="create_${mode.companyModuleSeq}"
                                                                    name="modeSeq[]"
                                                                    value="${mode.companyModuleSeq}" class="icheckbox"/>
                                                ${mode.module.moduleName}</label>
                                    </c:forEach>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status" class="form-control select"
                                            data-live-search="true"
                                            placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="createCharge"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@chargeManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_charge()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Charge</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Charge Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="chargeSeq" id="update_chargeSeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            placeholder="Select Charge Name"
                                            onchange="load_charge_by_charge_seq_to_update()">
                                        <option value="0">None</option>
                                        <c:forEach items="${chargeList}" var="charge">
                                            <option value="${charge.chargeSeq}">${charge.chargeName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Charge Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_chargeName" name="chargeName"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_description" name="description"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Charge Modes</label>
                                <div class="col-md-6 col-xs-12">
                                    <c:forEach items="${modeList}" var="mode">
                                        <label class="check"><input type="checkbox" id="update_${mode.companyModuleSeq}"
                                                                    name="modeSeq[]"
                                                                    value="${mode.companyModuleSeq}" class="icheckbox"/>
                                                ${mode.module.moduleName}</label>
                                    </c:forEach>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status" class="form-control select"
                                            data-live-search="true"
                                            placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateCharge"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@chargeManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_charge()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Charge Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Charge Name</label>

                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_chargeName"
                                               name="chargeName" required/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Description</label>

                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_description"
                                               name="description" required/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right" value="searchCharge"
                                                <sec:authorize
                                                        access="!hasRole('ROLE_Config@chargeManagement_VIEW')">
                                                    disabled="disabled"
                                                </sec:authorize>
                                                onclick="search_charge()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadChargeList">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <form class="form-horizontal" method="post" name="modal" id="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Charge Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Charge Name</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_chargeName" name="chargeName"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Description</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_description" name="description"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Charge Modes</label>
                        <div class="col-md-6 col-xs-12">
                            <c:forEach items="${modeList}" var="mode">
                                <label class="check"><input type="checkbox" id="modal_${mode.companyModuleSeq}"
                                                            name="modeSeq[]"
                                                            value="${mode.companyModuleSeq}" class="icheckbox"/>
                                        ${mode.module.moduleName}</label>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>

                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_createdDate">Created
                            By</label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified By</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified Date</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@chargeManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_charge_modal()">Save changes
                    </button>
                    <input type="hidden" name="chargeSeq" id="modal_chargeSeq" value=""/>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->

</div>


