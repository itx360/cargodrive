<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/currencyManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Currency Creation and Search</h3>
</div>
<!-- END PAGE TITLE -->

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Currency Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Currency Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Currency</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Currency Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_currencyCode" name="currencyCode" data-error="Please provide correct currency code"
                                           pattern="^[a-zA-Z0-9\s]+$" required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Currency Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_currencyName" name="currencyName" data-error="Please provide correct currency name"
                                           pattern="^[a-zA-Z0-9\s]+$" required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Currency Display</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_currencyDisplay" data-error="Please provide currency display"
                                           name="currencyDisplay" required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${createStatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="createCurrency"
                                            onclick="create_currency()"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@currencyManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>>Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Currency</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Currency</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="currencySeq" id="update_currencySeq"
                                            class="form-control select text-uppercase" data-validate="true"
                                            data-live-search="true" placeholder="currency Code"
                                            onchange="load_currency_by_currency_seq_to_update()">
                                        <option value="-1">None</option>
                                        <c:forEach items="${currencyList}" var="currency">
                                            <option value="${currency.currencySeq}">${currency.currencyCode}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Currency Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="update_currencyCode" name="currencyCode" data-error="Please provide correct currency code"
                                           pattern="^[a-zA-Z0-9\s]+$" required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Currency Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="update_currencyName" name="currencyName" data-error="Please provide correct currency name"
                                           pattern="^[a-zA-Z0-9\s]+$" required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Currency Display</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_currencyDisplay" data-error="Please provide currency display"
                                           name="currencyDisplay" required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateCurrency"
                                            onclick="update_currency()"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@currencyManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>>Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Currency Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Currency Name</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase" id="search_currencyName"
                                               name="currencyName"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Currency Display</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_currencyDisplay"
                                               name="currencyDisplay"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right" value="searchCurrency"
                                                onclick="search_currency()"
                                                <sec:authorize
                                                        access="!hasRole('ROLE_Config@currencyManagement_VIEW')">
                                                    disabled="disabled"
                                                </sec:authorize>>Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadCurrencySearchResult"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Currency Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Currency Code</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control text-uppercase" id="modal_currencyCode" data-error="Please provide correct currency code"
                                   pattern="^[a-zA-Z0-9\s]+$" name="currencyCode" required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Currency Name</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control text-uppercase" id="modal_currencyName" data-error="Please provide correct currency name"
                                   pattern="^[a-zA-Z0-9\s]+$" name="currencyName" required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Currency Display</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_currencyDisplay" required data-error="Please provide correct currency display"
                                   name="currencyDisplay"/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true"
                                    data-live-search="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified By</label>
                        <label class="col-md-8" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified Date</label>
                        <label class="col-md-8" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="update_currency_modal_data()" class="btn btn-primary"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@currencyManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>>Update
                    </button>
                    <input type="hidden" name="currencySeq" id="modal_currencySeq" value=""/>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End modal -->




