<%--
  Created by IntelliJ IDEA.
  User: Udaya-Ehl
  Date: 10/7/2016
  Time: 10:00 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/vesselManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Vessel Management</h3>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Vessel Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Vessel Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-8">
                        <form class="form-horizontal vesselForm" method="post" id="create">
                            <div class="form-group removeFromModal">
                                <div class="col-md-7">
                                    <h4 class="subTitle">Vessel Details</h4>
                                </div>
                                <div class="col-md-5 operations" style="display: none">
                                    <button type="button" class="btn btn-success" onclick="new_form('update','create')">
                                        New
                                    </button>
                                    <button type="button" class="btn btn-danger"
                                            onclick="delete_vessel()">
                                        Delete
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Vessel Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_vesselName"
                                           name="vesselName" required data-error="Vessel Name is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Vessel Flag</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_vesselFlag"
                                           name="vesselFlag" required data-error="Vessel Flag is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Shipping Line</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="freightLineSeq" id="create_freightLineSeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            data-validate="true" required
                                            data-error="Please Select Shipping Line"
                                            aria-required="true">
                                        <option value="">Select</option>
                                        <c:forEach items="${freightLineList}" var="freightLine">
                                            <option value="${freightLine.freightLineSeq}">${freightLine.freightLineName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Vessel Operator</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="vesselOperatorSeq" id="create_vesselOperatorSeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            data-validate="true" required
                                            data-error="Please Select Vessel Operator"
                                            aria-required="true">
                                        <option value="">Select</option>
                                        <c:forEach items="${freightLineList}" var="freightLine">
                                            <option value="${freightLine.freightLineSeq}">${freightLine.freightLineName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Vessel Reg Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="countrySeq" id="create_countrySeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            data-validate="true" required
                                            data-error="Please Select Country"
                                            aria-required="true">
                                        <option value="">Select</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group createOperation removeFromModal">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="create Container Type"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@vesselManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_vessel()">Create
                                    </button>
                                </div>
                            </div>
                            <div class="form-group updateOperation" style="display: none">
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created By</label>
                                    <label class="col-md-8 createdBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created Date</label>
                                    <label class="col-md-8 createdDate"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified By</label>
                                    <label class="col-md-8 lastModifiedBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified Date</label>
                                    <label class="col-md-8 lastModifiedDate"></label>
                                </div>
                                <div class="col-sm-offset-2 col-sm-7 removeFromModal">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="Update Vessel"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@vesselManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_vessel()">Update
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="vesselSeq" id="vesselSeq" value=""/>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Search Vessel</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Vessel Name</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase" id="search_vesselName"
                                               name="vesselName" required data-error="Vessel Name is Mandatory"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Vessel Flag</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase" id="search_vesselFlag"
                                               name="vesselFlag" required data-error="Vessel Flag is Mandatory"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Shipping Line</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select name="freightLineSeq" id="search_freightLineSeq"
                                                class="form-control select"
                                                data-live-search="true"
                                                data-validate="true" required
                                                data-error="Please Select Shipping Line"
                                                aria-required="true">
                                            <option value="">Select</option>
                                            <c:forEach items="${freightLineList}" var="freightLine">
                                                <option value="${freightLine.freightLineSeq}">${freightLine.freightLineName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right"
                                                value="search Vessel"
                                                onclick="search_vessel()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadVesselData"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Vessel Details</h4>
            </div>
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize access="!hasRole('ROLE_Config@vesselManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_vessel_modal()">Save changes
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>





