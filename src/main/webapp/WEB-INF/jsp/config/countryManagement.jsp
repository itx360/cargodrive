<%--
  Created by IntelliJ IDEA.
  User: shanakajay
  Date: 9/19/2016
  Time: 5:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/countryManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Country Creation, Modification and Search</h3>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Country Creation and
                    Modification</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Search Result</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Country</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country Code</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_countryCode" name="countryCode"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_countryName" name="countryName"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Region</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="regionSeq" id="create_regionSeq" class="form-control select"
                                            data-live-search="true"
                                            placeholder="Region">
                                        <option value="0">None</option>
                                        <c:forEach items="${regionList}" var="region">
                                            <option value="${region.regionSeq}">${region.regionName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Currency</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="currencySeq" id="create_currencySeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            placeholder="Currency">
                                        <option value="0">None</option>
                                        <c:forEach items="${currencyList}" var="currency">
                                            <option value="${currency.currencySeq}">${currency.currencyName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">IATA Code</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="create_iataCode" name="iataCode"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status" class="form-control select"
                                            data-live-search="true"
                                            placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="createUser"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@countryManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_country()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Country</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Country Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="countrySeq" id="update_countrySeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            placeholder="Country Name"
                                            onchange="load_country_by_country_seq_to_update()">
                                        <option value="0">None</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country Code</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_countryCode" name="countryCode"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country Name</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_countryName" name="countryName"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Region</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="regionSeq" id="update_regionSeq" class="form-control select"
                                            data-live-search="true"
                                            placeholder="Region">
                                        <option value="0">None</option>
                                        <c:forEach items="${regionList}" var="region">
                                            <option value="${region.regionSeq}">${region.regionName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Currency</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="currencySeq" id="update_currencySeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            placeholder="Currency">
                                        <option value="0">None</option>
                                        <c:forEach items="${currencyList}" var="currency">
                                            <option value="${currency.currencySeq}">${currency.currencyName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">IATA Code</label>

                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" id="update_iataCode" name="iataCode"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>

                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status" class="form-control select"
                                            data-live-search="true"
                                            placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateCountry"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@countryManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_country()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Country Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Country Name</label>

                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_countryName"
                                               name="countryName" required/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Country Code</label>

                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_countryCode"
                                               name="countryCode" required/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right" value="searchCountry"
                                                <sec:authorize
                                                        access="!hasRole('ROLE_Config@countryManagement_VIEW')">
                                                    disabled="disabled"
                                                </sec:authorize>
                                                onclick="search_country()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadCountryList">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <form class="form-horizontal" method="post" name="modal" id="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Country Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Country Code</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_countryCode" name="countryCode"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Country Name</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_countryName" name="countryName"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Region</label>

                        <div class="col-md-6 col-xs-12">
                            <select name="regionSeq" id="modal_regionSeq"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Region">
                                <c:forEach items="${regionList}" var="region">
                                    <option value="${region.regionSeq}">${region.regionName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Currency</label>

                        <div class="col-md-6 col-xs-12">
                            <select name="currencySeq" id="modal_currencySeq"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Currency">
                                <c:forEach items="${currencyList}" var="currency">
                                    <option value="${currency.currencySeq}">${currency.currencyCode}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">IATA Code</label>

                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_iataCode" name="iataCode"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>

                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_createdDate">Created
                            By</label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified By</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified Date</label>
                        <label class="col-md-6 col-xs-12 control-label text-left" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@countryManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_country_modal()">Save changes
                    </button>
                    <input type="hidden" name="countrySeq" id="modal_countrySeq" value=""/>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->

</div>
