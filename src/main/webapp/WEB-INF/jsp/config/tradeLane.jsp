<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/tradeLane.js"></script>
<div class="page-title">
    <h3><span class="fa fa-road"></span>Trade Lane Creation and Maintain</h3>
</div>
<!-- END PAGE TITLE -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Trade Lane Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Trade Lane Maintain</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Trade Lane</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Trade Lane Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="tradeLaneCode"
                                           id="create_tradeLaneCode" data-error="Please Provide Trade Lane Code" maxlength="10"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Trade Lane Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="tradeLaneName"
                                           id="create_tradeLaneName" data-error="Please Provide Correct Trade Lane Name" pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="description" minlength="1" maxlength="100"
                                           id="create_tradeLaneDescription"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="status" id="create_status" data-error="Please Select Status"
                                            data-validate="true" required aria-required="true" placeholder="Status">
                                        <option value="">Choose</option>
                                        <c:forEach items="${statusList}" var="statusEnum">
                                            <option value="${statusEnum.statusSeq}">${statusEnum.status}</option>
                                        </c:forEach>
                                    </select>

                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="addTradeLane"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@tradeLane_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_trade_lane()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Trade Lane</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Trade Lane</label>
                                <div class="col-md-6 col-xs-12">
                                    <select data-error="Please Select Trade Lane" data-validate="true" required
                                            aria-required="true"
                                            name="tradeLaneSeq" id="update_tradeLaneSeq"
                                            class="form-control select"
                                            data-live-search="true" placeholder="Trade Lane"
                                            onchange="load_trade_lane_trade_lane_seq_to_update()">
                                        <option value="">Choose</option>
                                        <c:forEach items="${tradeLaneList}" var="tradeLane">
                                            <option value="${tradeLane.tradeLaneSeq}">${tradeLane.tradeLaneName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Trade Lane Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" maxlength="30" name="tradeLaneName"
                                           id="update_tradeLaneName" data-error="Please Provide Correct Trade Lane Name" pattern="^[a-zA-Z0-9\s]+$"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Trade Lane Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="tradeLaneCode"
                                           id="update_tradeLaneCode" data-error="Please Provide Trade Lane Code" maxlength="10"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="description" minlength="1" maxlength="100"
                                           id="update_description"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="status" id="update_status" data-error="Please Select Status"
                                            data-validate="true" required aria-required="true" placeholder="Status">
                                        <option value="">Choose</option>
                                        <c:forEach items="${statusList}" var="statusEnum">
                                            <option value="${statusEnum.statusSeq}">${statusEnum.status}</option>
                                        </c:forEach>
                                    </select>

                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateTradeLane"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@tradeLane_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_trade_lane()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane" id="tab-second">

                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" name="search" id="search">
                            <div class="form-group">
                                <h4 class="subTitle">Search Trade Lane</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Trade Lane Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="tradeLaneCode"
                                           id="search_tradeLaneCode"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Trade Lane Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="tradeLaneName"
                                           id="search_tradeLaneName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="loadTradeLaneDataNameWise"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@tradeLane_VIEW')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="load_trade_lane_table_data()">Search
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="form-group" id="loadTradeLaneListData"></div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Trade Lane Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Trade Lane Code</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_tradeLaneCode"
                                   name="tradeLaneCode"
                                   data-error="Please Provide Trade Lane Code" maxlength="10"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Trade Lane Name</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_tradeLaneName"
                                   name="tradeLaneName" maxlength="30"
                                   data-error="Please Provide Correct Trade Lane Name" pattern="^[a-zA-Z0-9\s]+$"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Description</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_description" minlength="1" maxlength="100"
                                   name="description"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-error="Please Select Status"
                                    data-validate="true" required aria-required="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified By</label>
                        <label class="col-md-8" id="modal_modifiedBy"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified Date</label>
                        <label class="col-md-8" id="modal_modifiedDate"></label>
                    </div>
                </div>
                <%--<div id="result"></div>--%>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary pull-right"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@tradeLane_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>

                            onclick="update_trade_lane_popup()">Save changes
                    </button>
                    <input type="hidden" name="tradeLaneSeq" id="modal_tradeLaneSeq" value=""/>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<!-- /.modal -->