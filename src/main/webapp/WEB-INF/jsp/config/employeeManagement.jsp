<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/employeeManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-road"></span>Employee Management</h3>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Employee Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Employee Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-8">
                        <form class="form-horizontal employeeForm" method="post" name="create" id="create">
                            <div class="form-group removeFromModal">
                                <div class="col-md-7">
                                    <h4 class="subTitle">Employee Details</h4>
                                </div>
                                <div class="col-md-5 operations" style="display: none">
                                    <button type="button" class="btn btn-success" onclick="new_form('update','create')">
                                        New
                                    </button>
                                    <button type="button" class="btn btn-danger" onclick="delete_employee()">
                                        Delete
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Employee Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="employeeName"
                                           id="create_employeeName" pattern="^[a-zA-Z0-9\s]+$"
                                           data-error="Please Provide Correct Employee Name"
                                           required/>
                                    <span class="help-block">Required, Employee Name</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Designation</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="employeeDesignationSeq" id="create_employeeDesignationSeq"
                                            data-error="Please Select Designation"
                                            data-validate="true" required aria-required="true"
                                            placeholder="Designation">
                                        <option value="">Choose</option>
                                        <c:forEach items="${designationList}" var="employeeDesignation">
                                            <option value="${employeeDesignation.employeeDesignationSeq}">${employeeDesignation.designation}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="help-block">Required</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Nic</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="nicNo" maxlength="15"
                                           id="create_nicNo"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">CHA License No</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="chaLicenseNo" maxlength="15"
                                           id="create_chaLicenseNo"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="help-block">Required</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <h6 class="subTitle">Address Details</h6>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address1</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="address1"
                                           id="create_address1"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address2</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="address2"
                                           id="create_address2"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">City</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="city"
                                           id="create_city"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">State</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="state"
                                           id="create_state"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Zip</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="zip" maxlength="10"
                                           id="create_zip"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="countrySeq" id="create_countrySeq" data-error="Please Select Country"
                                            data-validate="true" required aria-required="true" placeholder="Country">
                                        <option value="">Choose</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="help-block">Required</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Telephone</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="telephone"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Phone Number." maxlength="15"
                                           id="create_telephone"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Telephone Extension</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="telephoneExtension"
                                           data-error="Please Provide Numeric Ext Code" maxlength="5"
                                           pattern="^([0-9]*\s+)*[0-9]*$"
                                           id="create_telephoneExtension"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Mobile</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="mobile"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Mobile Number." maxlength="15"
                                           id="create_mobile"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Fax</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="fax"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Fax Number." maxlength="15"
                                           id="create_fax"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Email</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="email" name="email" class="form-control"
                                           data-error="Please Provide Correct E-mail Address."
                                           pattern="[a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+"
                                           id="create_email"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Web Site</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="url" name="website" class="form-control" placeholder="http://"
                                           data-error="Please Provide Correct Website Url."
                                           id="create_website"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group createOperation removeFromModal">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="addDestination"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@employeeManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_employee()">Create
                                    </button>
                                </div>
                            </div>
                            <div class="form-group updateOperation" style="display: none">
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created By</label>
                                    <label class="col-md-8 createdBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created Date</label>
                                    <label class="col-md-8 createdDate"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified By</label>
                                    <label class="col-md-8 lastModifiedBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified Date</label>
                                    <label class="col-md-8 lastModifiedDate"></label>
                                </div>
                                <div class="col-sm-offset-2 col-sm-7 removeFromModal">
                                    <button type="button" class="btn btn-primary pull-right" value="Update Employee"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@employeeManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_employee()">Update
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="employeeSeq" id="employeeSeq" value=""/>
                        </form>

                    </div>
                </div>

                <div class="tab-pane" id="tab-second">
                    <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" name="search" id="search">
                            <div class="form-group">
                                <h4 class="subTitle">Search Employee</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Employee Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="30" class="form-control" name="employeeName"
                                           id="search_employeeName"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="search_status"
                                            class="form-control select"
                                            data-live-search="true" placeholder="Status">
                                        <option value="-1">All</option>
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Designation</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="employeeDesignationSeq" id="search_employeeDesignationSeq"
                                            placeholder="Designation">
                                        <option value="-1">All</option>
                                        <c:forEach items="${designationList}" var="employeeDesignation">
                                            <option value="${employeeDesignation.employeeDesignationSeq}">${employeeDesignation.designation}</option>
                                        </c:forEach>
                                    </select>

                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="loadEmployeeData"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@employeeManagement_VIEW')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="load_employee_table_data()">Search
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="form-group" id="loadEmployeeListData"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Employee Details</h4>
            </div>
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize
                                    access="!hasRole('ROLE_Config@employeeManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_employee_modal()">Save changes
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
