<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/agentNetworkManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-road"></span>Agent Network</h3>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Agent Network Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Agent Network Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-8">
                        <form class="form-horizontal agentNetworkForm" method="post" id="create">
                            <div class="form-group removeFromModal">
                                <div class="col-md-7">
                                    <h4 class="subTitle">Agent Network Details</h4>
                                </div>
                                <div class="col-md-5 operations" style="display: none">
                                    <button type="button" class="btn btn-success" onclick="new_form('update','create')">
                                        New
                                    </button>
                                    <button type="button" class="btn btn-danger" onclick="delete_agent_network()">
                                        Delete
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Agent Network Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="40" class="form-control" name="agentNetworkCode"
                                           id="create_agentNetworkCode"
                                           data-error="Please Provide Agent Network Code"
                                           required/>
                                    <span class="help-block">Required, Agent Network Code</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Agent Network Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" maxlength="40" class="form-control" name="agentNetworkName"
                                           id="create_agentNetworkName"
                                           data-error="Please Provide Agent Network Name"
                                           required/>
                                    <span class="help-block">Required, Agent Network Name</span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${StatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group createOperation removeFromModal">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="create Agent Network"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@agentNetworkManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_agent_network()">Create
                                    </button>
                                </div>
                            </div>
                            <div class="form-group updateOperation" style="display: none">
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created By</label>
                                    <label class="col-md-8 createdBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created Date</label>
                                    <label class="col-md-8 createdDate"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified By</label>
                                    <label class="col-md-8 lastModifiedBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified Date</label>
                                    <label class="col-md-8 lastModifiedDate"></label>
                                </div>
                                <div class="col-sm-offset-2 col-sm-7 removeFromModal">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="Update Agent Network"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@agentNetworkManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_agent_network()">Update
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="agentNetworkSeq" id="agentNetworkSeq" value=""/>

                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-8">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Search Agent Network</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Agent Network Code</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_agentNetworkCode"
                                               name="agentNetworkCode"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Agent Network Name</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_agentNetworkName"
                                               name="agentNetworkName"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right"
                                                value="search Agent Network"
                                                <sec:authorize
                                                        access="!hasRole('ROLE_Config@agentNetworkManagement_VIEW')">
                                                    disabled="disabled"
                                                </sec:authorize>
                                                onclick="search_agent_network()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadAgentNetworkData"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agent Network Details</h4>
            </div>
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize access="!hasRole('ROLE_Config@agentNetworkManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_agent_network_modal()">Save changes
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>