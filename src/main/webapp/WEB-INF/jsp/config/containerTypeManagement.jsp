<%--
  Created by IntelliJ IDEA.
  User: Udaya-Ehl
  Date: 10/5/2016
  Time: 2:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/containerTypeManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Container Type Management</h3>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Container Type Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Container Type Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-8">
                        <form class="form-horizontal containerTypeForm" method="post" id="create">
                            <div class="form-group removeFromModal">
                                <div class="col-md-7">
                                    <h4 class="subTitle">Container Type Details</h4>
                                </div>
                                <div class="col-md-5 operations" style="display: none">
                                    <button type="button" class="btn btn-success" onclick="new_form('update','create')">
                                        New
                                    </button>
                                    <button type="button" class="btn btn-danger"
                                            onclick="delete_container_type()">
                                        Delete
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Container Size</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="containerSizeSeq" id="create_containerSizeSeq"
                                            class="form-control select"
                                            data-validate="true" required
                                            data-error="Please Select Container Size"
                                            placeholder="Container Size"
                                            aria-required="true">
                                        <option value="">Select</option>
                                        <c:forEach items="${containerSizeList}" var="containerSize">
                                            <option value="${containerSize.containerSizeSeq}">${containerSize.containerSizeValue}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Container Category</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="containerCategorySeq" id="create_containerCategorySeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            data-validate="true" required
                                            data-error="Please Select Container Category"
                                            placeholder="Container Category"
                                            aria-required="true">
                                        <option value="">Select</option>
                                        <c:forEach items="${containerCategoryList}" var="containerCategory">
                                            <option value="${containerCategory.containerCategorySeq}">${containerCategory.containerCategoryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Asycuda Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_asycudaCode"
                                           name="asycudaCode" maxlength="4"
                                           required data-error="AsycudaCode is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control text-uppercase" rows="3"
                                              id="create_description"
                                              name="description" maxlength="150"
                                              required data-error="Description is Mandatory"></textarea>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Dimension</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="length"
                                           id="create_length" placeholder="Length" required
                                           pattern="^[0-9]*(.[0-9]*)?([eE][-+][0-9]*)?$"
                                           onchange="validateFloatKeyPress(this)"
                                           data-error="Container Length,Height and Width is Mandatory"/>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="width"
                                           id="create_width" placeholder="Width" required
                                           pattern="^[0-9]*(.[0-9]*)?([eE][-+][0-9]*)?$"
                                           onchange="validateFloatKeyPress(this)"
                                           data-error="Container Length,Height and Width is Mandatory"/>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="height"
                                           id="create_height" placeholder="Height" required
                                           pattern="^[0-9]*(.[0-9]*)?([eE][-+][0-9]*)?$"
                                           onchange="validateFloatKeyPress(this)"
                                           data-error="Container Length,Height and Width is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">TEU</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="teu" pattern="[0-9]*"
                                           onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"
                                           id="create_teu" required maxlength="2"
                                           data-error="TEU is Mandatory"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${createStatusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group createOperation removeFromModal">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="create Container Type"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@containerTypeManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_container_type()">Create
                                    </button>
                                </div>
                            </div>
                            <div class="form-group updateOperation" style="display: none">
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created By</label>
                                    <label class="col-md-8 createdBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Created Date</label>
                                    <label class="col-md-8 createdDate"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified By</label>
                                    <label class="col-md-8 lastModifiedBy"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 text-right">Last Modified Date</label>
                                    <label class="col-md-8 lastModifiedDate"></label>
                                </div>
                                <div class="col-sm-offset-2 col-sm-7 removeFromModal">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="Update Container Type"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@containerTypeManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="update_container_type()">Update
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="containerTypeSeq" id="containerTypeSeq" value=""/>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Search Container Type</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Container Size</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select name="containerSizeSeq" id="search_containerSizeSeq"
                                                class="form-control select"
                                                data-live-search="true"
                                                data-validate="true" required
                                                data-error="Please Select Container Size"
                                                placeholder="Container Size">
                                            <option value="-1">All</option>
                                            <c:forEach items="${containerSizeList}" var="containerSize">
                                                <option value="${containerSize.containerSizeSeq}">${containerSize.containerSizeValue}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Container Category</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select name="containerCategorySeq" id="search_containerCategorySeq"
                                                class="form-control select"
                                                data-live-search="true"
                                                data-validate="true" required
                                                data-error="Please Select Container Category"
                                                placeholder="Container Category">
                                            <option value="-1">All</option>
                                            <c:forEach items="${containerCategoryList}" var="containerCategory">
                                                <option value="${containerCategory.containerCategorySeq}">${containerCategory.containerCategoryName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Description</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control" id="search_description"
                                               name="description"
                                               required data-error="Description is Mandatory"/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right"
                                                value="search Container Type"
                                                onclick="search_container_type()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadContainerTypeData"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Container Type Details</h4>
            </div>
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            <sec:authorize access="!hasRole('ROLE_Config@containerTypeManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_container_type_modal()">Save changes
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>




