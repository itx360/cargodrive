<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/locationManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-user"></span>Location Management</h3>
</div>
<!-- END PAGE TITLE -->

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Location Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Location Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Location</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Location Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="create_locationName" name="locationName"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="countrySeq" id="create_countrySeq"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Country">
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="create_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="createLocation"
                                            onclick="create_location()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Location</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Location</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="locationSeq" id="update_locationSeq"
                                            class="form-control select"
                                            data-live-search="true"
                                            placeholder="location" onchange="load_location_by_location_seq_to_update()">
                                        <option value="0">None</option>
                                        <c:forEach items="${locationList}" var="location">
                                            <option value="${location.locationSeq}">${location.locationName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Location Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control text-uppercase" id="update_locationName"
                                           name="locationName"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="countrySeq" id="update_countrySeq"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Country">
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateLocation"
                                            onclick="update_location()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="tab-second">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal" method="post" name="search" id="search">
                                <div class="form-group">
                                    <h4 class="subTitle">Location Search</h4>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Location Name</label>
                                    <div class="col-md-6 col-xs-12">
                                        <input type="text" class="form-control text-uppercase" id="search_locationName"
                                               name="currencyName" required/>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Country</label>
                                    <div class="col-md-6 col-xs-12">
                                        <select name="countrySeq" id="search_countrySeq"
                                                class="form-control select" data-validate="true" required
                                                data-live-search="true" placeholder="Country">
                                            <option value="-1" selected>All</option>
                                            <c:forEach items="${countryList}" var="country">
                                                <option value="${country.countrySeq}">${country.countryName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-7">
                                        <button type="button" class="btn btn-primary pull-right" value="searchLocation"
                                                onclick="search_location()">Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-group" id="loadLocationData"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <form class="form-horizontal" method="post" name="modal" id="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Location Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Location Name</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" id="modal_locationName" name="locationName"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Country</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="countrySeq" id="modal_countrySeq"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Country">
                                <c:forEach items="${countryList}" var="country">
                                    <option value="${country.countrySeq}">${country.countryName}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified By</label>
                        <label class="col-md-8" id="modal_lastModifiedBy"></label>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Last Modified Date</label>
                        <label class="col-md-8" id="modal_lastModifiedDate"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="update_location_modal()">Save changes
                    </button>
                    <input type="hidden" name="locationSeq" id="modal_locationSeq" value=""/>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->

</div>

