<%@ include file="/WEB-INF/jsp/include.jsp" %>
<script type="text/javascript" src="../../../theme/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../cargodrivejs/config/warehouseManagement.js"></script>
<div class="page-title">
    <h3><span class="fa fa-road"></span>Warehouse Management</h3>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Warehouse Creation</a></li>
                <li><a href="#tab-second" role="tab" data-toggle="tab">Warehouse Search</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab-first">
                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="create" id="create">
                            <div class="form-group">
                                <h4 class="subTitle">Create Warehouse</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Warehouse Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="warehouseCode" maxlength="30"
                                           id="create_warehouseCode" data-error="Please Provide Warehouse Code"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Warehouse Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="warehouseName" maxlength="30"
                                           pattern="^[a-zA-Z0-9\s]+$"
                                           id="create_warehouseName" data-error="Please Provide Correct Warehouse Name"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control" rows="3" id="create_description" maxlength="150"
                                              name="description"></textarea>
                                </div>

                            </div>
                            <div class="form-group">
                                <h6 class="subTitle">Address Details</h6>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address1</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="address1"
                                           id="create_address1"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address2</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="address2"
                                           id="create_address2"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">City</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="city"
                                           id="create_city"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">State</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="state"
                                           id="create_state"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Zip</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="zip"
                                           id="create_zip"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="countrySeq" id="create_countrySeq" data-error="Please Select Country"
                                            data-validate="true" required aria-required="true" placeholder="Country">
                                        <option value="">Choose</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>

                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Telephone</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="telephone"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Phone Number." maxlength="15"
                                           id="create_telephone"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Telephone Extension</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="telephoneExtension"
                                           data-error="Please Provide Numeric Ext Code" pattern="^([0-9]*\s+)*[0-9]*$"
                                           maxlength="6"
                                           id="create_telephoneExtension"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Mobile</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="mobile"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Mobile Number." maxlength="15"
                                           id="create_mobile"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Fax</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="fax"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Fax Number." maxlength="15"
                                           id="create_fax"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Email</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="email" name="email" class="form-control"
                                           data-error="Please Provide Correct E-mail Address." pattern="[a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+"
                                           id="create_email"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Web Site</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="url" name="website" class="form-control" placeholder="http://"
                                           data-error="Please Provide Correct Website Url."
                                           id="create_website"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="addBranch"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@warehouseManagement_CREATE')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="create_warehouse()">Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-6">
                        <form class="form-horizontal" method="post" name="update" id="update">
                            <div class="form-group">
                                <h4 class="subTitle">Update Warehouse</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Warehouse</label>
                                <div class="col-md-6 col-xs-12">
                                    <select data-error="Please Select Warehouse" data-validate="true" required
                                            aria-required="true"
                                            name="warehouseSeq" id="update_warehouseSeq"
                                            class="form-control select"
                                            data-live-search="true" placeholder="Warehouse"
                                            onchange="load_warehouse_by_warehouse_seq_to_update()">
                                        <option value="">Choose</option>
                                        <c:forEach items="${warehouseList}" var="warehouse">
                                            <option value="${warehouse.warehouseSeq}">${warehouse.warehouseName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Warehouse Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="warehouseCode" maxlength="30"
                                           id="update_warehouseCode" data-error="Please Provide Warehouse Code"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Warehouse Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="warehouseName"
                                           pattern="^[a-zA-Z0-9\s]+$"
                                           id="update_warehouseName" data-error="Please Provide Correct Warehouse Name"
                                           required/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control" rows="3" id="update_description" maxlength="150"
                                              name="description"></textarea>
                                </div>

                            </div>
                            <div class="form-group">
                                <h6 class="subTitle">Address Details</h6>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address 1</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="address1" id="update_address1"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address 2</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="address2"
                                           id="update_address2"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">City</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="city"
                                           id="update_city"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">State</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="state"
                                           id="update_state"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Zip</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="zip"
                                           id="update_zip"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="countrySeq" id="update_countrySeq" data-error="Please Select Country"
                                            data-validate="true" required aria-required="true" placeholder="Country">
                                        <option value="">Choose</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>

                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Telephone</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="telephone"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Phone Number." maxlength="15"
                                           id="update_telephone"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Telephone Extension</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="telephoneExtension"
                                           data-error="Please Provide Numeric Ext Code" pattern="^([0-9]*\s+)*[0-9]*$"
                                           maxlength="6"
                                           id="update_telephoneExtension"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Mobile</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="mobile"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Mobile Number." maxlength="15"
                                           id="update_mobile"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Fax</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="tel" class="form-control" name="fax"
                                           pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                           data-error="Please Provide Correct Fax Number." maxlength="15"
                                           id="update_fax"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Email</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="email" name="email" class="form-control"
                                           data-error="Please Provide Correct E-mail Address."  pattern="[a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+"
                                           id="update_email"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Web Site</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="url" name="website" class="form-control" placeholder="http://"
                                           data-error="Please Provide Correct Website Url."
                                           id="update_website"/>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select name="status" id="update_status"
                                            class="form-control select" data-validate="true" required
                                            data-live-search="true" placeholder="Status">
                                        <c:forEach items="${statusList}" var="status">
                                            <option value="${status.statusSeq}">${status.status}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right" value="updateBranch"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@warehouseManagement_UPDATE')">
                                                disabled="disabled"
                                            </sec:authorize>

                                            onclick="update_warehouse()">Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane" id="tab-second">
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" name="search" id="search">
                            <div class="form-group">
                                <h4 class="subTitle">Search Warehouse</h4>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Country</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" data-live-search="true"
                                            name="countrySeq" id="search_countrySeq"
                                            placeholder="Country">
                                        <option value="-1">All</option>
                                        <c:forEach items="${countryList}" var="country">
                                            <option value="${country.countrySeq}">${country.countryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Warehouse Code</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="warehouseCode"
                                           id="search_warehouseCode"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Warehouse Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" name="warehouseName"
                                           id="search_warehouseName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-7">
                                    <button type="button" class="btn btn-primary pull-right"
                                            value="loadWarehouseData"
                                            <sec:authorize
                                                    access="!hasRole('ROLE_Config@warehouseManagement_VIEW')">
                                                disabled="disabled"
                                            </sec:authorize>
                                            onclick="load_warehouse_table_data()">Search
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="form-group" id="loadWarehouseData">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post" name="modal" id="modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Warehouse Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Warehouse Code</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="warehouseCode" maxlength="30"
                                   id="modal_warehouseCode" data-error="Please Provide Warehouse Code"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Warehouse Name</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="warehouseName" pattern="^[a-zA-Z0-9\s]+$"
                                   maxlength="30"
                                   id="modal_warehouseName" data-error="Please Provide Correct Warehouse Name"
                                   required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Description</label>
                        <div class="col-md-6 col-xs-12">
                            <textarea class="form-control" rows="3" id="modal_description" maxlength="150"
                                      name="description"></textarea>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Address 1</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="address1"
                                   id="modal_address1"
                            />
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Address 2</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="address2"
                                   id="modal_address2"
                            />
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">City</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="city"
                                   id="modal_city"
                            />
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">State</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="state"
                                   id="modal_state"
                            />
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Zip</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="zip"
                                   id="modal_zip"
                            />
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Country</label>
                        <div class="col-md-6 col-xs-12">
                            <select class="form-control select" data-live-search="true"
                                    name="countrySeq" id="modal_countrySeq" data-error="Please Select Country"
                                    data-validate="true" required aria-required="true" placeholder="Country">
                                <option value="">Choose</option>
                                <c:forEach items="${countryList}" var="country">
                                    <option value="${country.countrySeq}">${country.countryName}</option>
                                </c:forEach>
                            </select>

                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Telephone</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="tel" class="form-control" name="telephone"
                                   pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                   data-error="Please Provide Correct Phone Number." maxlength="15"
                                   id="modal_telephone"/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Telephone Extension</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="telephoneExtension"
                                   data-error="Please Provide Numeric Ext Code" pattern="^([0-9]*\s+)*[0-9]*$"
                                   maxlength="6"
                                   id="modal_telephoneExtension"
                            />
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Mobile</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="tel" class="form-control" name="mobile" pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                   data-error="Please Provide Correct Mobile Number." maxlength="15"
                                   id="modal_mobile"
                            />
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Fax</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="tel" class="form-control" name="fax" pattern="[0-9]{10}|[0-9]{12}|[0-9]{15}"
                                   data-error="Please Provide Correct Fax Number." maxlength="15"
                                   id="modal_fax"/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Email</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="email" name="email" class="form-control"
                                   data-error="Please Provide Correct E-mail Address." pattern="[a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+"
                                   id="modal_email"/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Web Site</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="url" class="form-control" name="website"
                                   data-error="Please Provide Correct Website Url."
                                   id="modal_website"/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Status</label>
                        <div class="col-md-6 col-xs-12">
                            <select name="status" id="modal_status"
                                    class="form-control select" data-validate="true" required
                                    data-live-search="true" placeholder="Status">
                                <c:forEach items="${statusList}" var="status">
                                    <option value="${status.statusSeq}">${status.status}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created By</label>
                        <label class="col-md-8" id="modal_createdBy"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Created Date</label>
                        <label class="col-md-8" id="modal_createdDate"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified By</label>
                        <label class="col-md-8" id="modal_lastModifiedBy"></label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Modified Date</label>
                        <label class="col-md-8" id="modal_lastModifiedDate"></label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary pull-right"

                            <sec:authorize
                                    access="!hasRole('ROLE_Config@warehouseManagement_UPDATE')">
                                disabled="disabled"
                            </sec:authorize>
                            onclick="update_warehouse_popup()">Save changes
                    </button>
                    <input type="hidden" name="warehouseSeq" id="modal_warehouseSeq" value=""/>
                </div>

            </form>
        </div>
    </div><!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<!-- /.modal -->