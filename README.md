# Cargodrive | ITX360
--------
## Introduction
>Cargodrive is an ERP system devloped by ITX360 for the internal customers and the logistic industry in general for the purpose of seamless managment of cargo operations.

## Technology and Infrastructure
>Application is developed solely on Java based technologies and follows a MVC based archtecture and implemented with [Spring] and other open source technologies. Deployed on WildFly 10 Application server.

#### Technology Stack
##### Backend
- Java 8
- Spring Boot
- Spring MVC
- Spring Data JPA
- Oracle Database
- Hibernate ORM
- EhCache
- WildFly 10.0.0 Application Server

##### Frontend
- jsp/jstl
- custom theme

##### Management and deployemnt
- Maven
- Git/GitLab
- Jenkins
- JIRA

Access [JIRA] here.

Access a [QA/Demo] version of the application here.


[Spring]: http://spring.io/projects
[JIRA]: https://itx360.atlassian.net/login
[QA/Demo]: http://10.2.20.146:8080
